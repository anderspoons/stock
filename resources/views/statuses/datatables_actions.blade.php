{!! Form::open(['route' => ['statuses.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
      <a href="{{ route('statuses.edit', $id) }}" class='btn btn-default btn-xs'>
        <i data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class='action-tooltip'>Edit Status</span>"  class="glyphicon glyphicon-edit"></i>
    </a>
</div>
{!! Form::close() !!}
