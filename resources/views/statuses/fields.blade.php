<div class="form-group col-sm-6">
    @if(isset($status) && $status->job == 1)
        {{ Form::checkbox('job', '1', true, ['class' => 'icheck']) }}
    @else
        {{ Form::checkbox('job', '1', false, ['class' => 'icheck']) }}
    @endif
    {!! Form::label('job', '  Job Status?') !!}
</div>

<div class="form-group col-sm-6">
    @if(isset($status) && $status->item == 1)
        {{ Form::checkbox('item', '1', true, ['class' => 'icheck']) }}
    @else
        {{ Form::checkbox('item', '1', false, ['class' => 'icheck']) }}
    @endif
    {!! Form::label('item', '  Item Status?') !!}
</div>

<div class="form-group col-sm-6">
    @if(isset($status) && $status->order == 1)
        {{ Form::checkbox('order', '1', true, ['class' => 'icheck']) }}
    @else
        {{ Form::checkbox('order', '1', false, ['class' => 'icheck']) }}
    @endif
    {!! Form::label('order', '  Order Status?') !!}
</div>

<div class="form-group col-sm-6">
    @if(isset($status) && $status->auto_status == 1)
        {{ Form::checkbox('auto_status', '1', true, ['class' => 'icheck']) }}
    @else
        {{ Form::checkbox('auto_status', '1', false, ['class' => 'icheck']) }}
    @endif
    {!! Form::label('auto_status', '  Automatic Status?') !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('statuses.index') !!}" class="btn btn-default">Cancel</a>
</div>
