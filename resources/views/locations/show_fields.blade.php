<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $location->id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $location->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $location->updated_at !!}</p>
</div>

<!-- Store Id Field -->
<div class="form-group">
    {!! Form::label('store_id', 'Store Id:') !!}
    <p>{!! $location->store_id !!}</p>
</div>

<!-- Item Id Field -->
<div class="form-group">
    {!! Form::label('item_id', 'Item Id:') !!}
    <p>{!! $location->item_id !!}</p>
</div>

<!-- Room Field -->
<div class="form-group">
    {!! Form::label('room', 'Room:') !!}
    <p>{!! $location->room !!}</p>
</div>

<!-- Rack Field -->
<div class="form-group">
    {!! Form::label('rack', 'Rack:') !!}
    <p>{!! $location->rack !!}</p>
</div>

<!-- Shelf Field -->
<div class="form-group">
    {!! Form::label('shelf', 'Shelf:') !!}
    <p>{!! $location->shelf !!}</p>
</div>

<!-- Bay Field -->
<div class="form-group">
    {!! Form::label('bay', 'Bay:') !!}
    <p>{!! $location->bay !!}</p>
</div>

<!-- Qty Field -->
<div class="form-group">
    {!! Form::label('qty', 'Qty:') !!}
    <p>{!! $location->qty !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $location->deleted_at !!}</p>
</div>

