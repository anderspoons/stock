<!-- Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id', 'Id:') !!}
    {!! Form::number('id', null, ['class' => 'form-control']) !!}
</div>

<!-- Created At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_at', 'Created At:') !!}
    {!! Form::date('created_at', null, ['class' => 'form-control']) !!}
</div>

<!-- Updated At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_at', 'Updated At:') !!}
    {!! Form::date('updated_at', null, ['class' => 'form-control']) !!}
</div>

<!-- Store Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('store_id', 'Store Id:') !!}
    {!! Form::number('store_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Item Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('item_id', 'Item Id:') !!}
    {!! Form::number('item_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Room Field -->
<div class="form-group col-sm-6">
    {!! Form::label('room', 'Room:') !!}
    {!! Form::text('room', null, ['class' => 'form-control']) !!}
</div>

<!-- Rack Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rack', 'Rack:') !!}
    {!! Form::text('rack', null, ['class' => 'form-control']) !!}
</div>

<!-- Shelf Field -->
<div class="form-group col-sm-6">
    {!! Form::label('shelf', 'Shelf:') !!}
    {!! Form::text('shelf', null, ['class' => 'form-control']) !!}
</div>

<!-- Bay Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bay', 'Bay:') !!}
    {!! Form::text('bay', null, ['class' => 'form-control']) !!}
</div>

<!-- Qty Field -->
<div class="form-group col-sm-6">
    {!! Form::label('qty', 'Qty:') !!}
    {!! Form::number('qty', null, ['class' => 'form-control']) !!}
</div>

<!-- Deleted At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    {!! Form::date('deleted_at', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('locations.index') !!}" class="btn btn-default">Cancel</a>
</div>
