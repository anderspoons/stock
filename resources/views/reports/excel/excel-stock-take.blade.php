<table class="stock-table">
    <thead>
    <tr>
        <th>Room</th>
        <th>Rack</th>
        <th>Shelf</th>
        <th>Bay</th>
        <th>Item</th>
        <th>Part Number</th>
        <th>Material</th>
        <th>Type</th>
        <th>Size / Sex</th>
        <th>System Qty</th>
        <th>Actual Qty</th>
        <th>Barcode</th>
        <th>Unit Cost</th>
        <th>Total</th>
    </tr>
    </thead>

    <tbody>
    @php
        $count = 2;
    @endphp


    @foreach($locations as $location)
        @if(is_object($location) && !empty($location))
        <tr>
            <td>{{ $location->room }}</td>
            <td>{{ $location->rack }}</td>
            <td>{{ $location->shelf }}</td>
            <td>{{ $location->bay }}</td>

            @if(isset($location->item->description))
                <td>{{ $location->item->description }}</td>
            @else
                <td></td>
            @endif

            @if(isset($location->item->partnumber))
                <td>{{ $location->item->partnumber }}</td>
            @else
                <td></td>
            @endif

            @if(!empty($location->item->materials))
                <td style="wrap-text:true;">
                @foreach($location->item->materials as $material)
                    {{ $material->name }}
                    <br>
                @endforeach
                </td>
            @else
                <td style="wrap-text:true;"></td>
            @endif

            @if(!empty($location->item->types))
                <td style="wrap-text:true;">
                @foreach($location->item->types as $type)
                    {{ $type->name }}  <br>
                @endforeach
                </td>
            @else
                <td style="wrap-text:true;"></td>
            @endif

            @if(!empty($location->item->sizes))
            <td style="wrap-text:true;">

                @foreach($location->item->sizes as $size)
                    {!! $size->name !!}
                    @if(isset($size->id))
                        <?php $set = ''; ?>
                        @if(count($location->item->sexes) > 0)
                            @foreach($location->item->sexes as $sex)
                                @if($sex->id == $size->pivot->sex_id)
                                    &nbsp;|&nbsp;{{ $sex->name }}<br>
                                    @php
                                        break;
                                    @endphp
                                @endif
                            @endforeach
                        @endif
                    @endif
                @endforeach
            </td>

            @else
                <td style="wrap-text:true;"></td>
            @endif

            <td style="wrap-text:true;">{{ $location->qty }}</td>
            <td style="wrap-text:true;"></td>
            @if(isset($location->item->barcode))
                <td style="wrap-text:true;">{{ $location->item->barcode }}</td>
            @else
                <td></td>
            @endif

            @php
            try {
                $cost = $location->item->unit_cost->first()->cost;
            } catch (Exception $ex) {
                $cost = '';
            }
            @endphp

            @if(!empty($cost))
                <td style="wrap-text:true;">{{ $cost }}</td>
            @else
                <td></td>
            @endif

            <td>=K{{ $count }}*M{{ $count }}</td>

            @php
                $count++;
            @endphp
        </tr>
        @endif
    @endforeach
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Total</td>
            <td>=SUM(N1:N{{ $count - 1 }})</td>
        </tr>
    </tbody>
</table>