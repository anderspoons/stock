<tr>
    @if(isset($location->item->description))
        <td>{{ $location->item->description }}</td>
    @else
        <td></td>
    @endif
    <td>@if(isset($location->qty)){{ $location->qty }}@endif</td>
    @if(isset($location->item->types))
        <td>
            @foreach($location->item->types as $type)
                {{ $type->name }}&nbsp;
            @endforeach
        </td>
    @else
        <td></td>
    @endif
    @if(isset($location->item)&&is_object($location->item->last_price->first()))
        <td>@if(isset($location->item->last_price->first()->cost)){{ $location->item->last_price->first()->cost }}@endif</td>
    @else
        <td>0</td>
    @endif
    <td>=B{{ $row }}*D{{ $row }}</td>
</tr>
