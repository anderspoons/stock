<html>
@include('reports.pdf.pdf-style')

<body>
<h1 style="font-size: 12px; width: 100%; text-align: center">{{ $title }}</h1>
<table class="stock-table" style="width:100%">
    <thead>
    <tr>
        <th>A/C</th>
        <th>Date</th>
        <th>Ref</th>
        <th>Ex.Ref</th>
        <th>N/C*</th>
        <th>Department</th>
        <th>Project Ref</th>
        <th>Details</th>
        <th>Net</th>
        <th>T/C</th>
    </tr>
    </thead>

    <tbody>
    @foreach($counters as $counter)
        <tr>
            @if(isset($counter->boat->sage_ref))
                @if(strlen($counter->boat->sage_ref) > 0)
                    <td>{{ $counter->boat->sage_ref }}</td>
                @endif
            @elseif(strlen($counter->customer->sage_ref) > 0)
                <td>{{ $counter->customer->sage_ref }}</td>
            @else
                <td></td>
            @endif
            <td>{{ Date('d/m/y', strtotime($counter->invoiced_at)) }}</td>
            <td>{{ $counter->ref }}</td>
            <td></td>
            <td>4004</td>
            <td></td>
            <td></td>
            <td>{{ $counter->note }}</td>
            <td>{{ $counter->net_total }}</td>
            <td></td>
        </tr>
    @endforeach
    </tbody>
</table>
</body>
</html>