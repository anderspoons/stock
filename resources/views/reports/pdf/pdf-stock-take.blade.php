<html>
    @include('reports.pdf.pdf-style')


    <body>
        <h1 style="font-size: 12px; width: 100%; text-align: center">{{ $title }}</h1>
        <table class="stock-table" style="width:100%">
            <thead>
            <tr>
                <th>Room</th>
                <th>Rack</th>
                <th>Shelf</th>
                <th>Bay</th>
                <th>Item</th>
                <th>Part Number</th>
                <th>Material</th>
                <th>Type</th>
                <th>Size / Sex</th>
                <th>System Qty</th>
                <th>Actual Qty</th>
                <th>Barcode</th>
            </tr>
            </thead>

            <tbody>
            @foreach($locations as $location)
                <tr>
                    <td>{{ $location->room }}</td>
                    <td>{{ $location->rack }}</td>
                    <td>{{ $location->shelf }}</td>
                    <td>{{ $location->bay }}</td>

                    @if(isset($location->item->description))
                        <td>{{ $location->item->description }}</td>
                    @else
                        <td></td>
                    @endif

                    @if(isset($location->item->partnumber))
                        <td>{{ $location->item->partnumber }}</td>
                    @else
                        <td></td>
                    @endif

                    @if(count($location->item->materials) > 0)
                        <td>
                        @foreach($location->item->materials as $material)
                            {{ $material->name }} <br/>
                        @endforeach
                        </td>
                    @else
                        <td></td>
                    @endif

                    @if(count($location->item->types) > 0)
                    <td>
                        @foreach($location->item->types as $type)
                            {{ $type->name }} <br/>
                        @endforeach
                    </td>
                    @else
                        <td></td>
                    @endif

                    @if(count($location->item->sizes) > 0)
                    <td>
                        @foreach($location->item->sizes as $size)
                            {{ $size->name }}
                            @if(count($location->item->sexes) > 0)
                                @foreach($location->item->sexes as $sex)
                                    @if($size->id == $sex->pivot->size_id)
                                        &nbsp;|&nbsp;{{ $sex->name }}
                                        @php break; @endphp
                                    @endif
                                @endforeach
                            @endif
                            <br/>
                        @endforeach
                    </td>
                    @else
                        <td></td>
                    @endif

                    <td>{{ $location->qty }}</td>
                    <td></td>
                    @if(isset($location->item->barcode))
                        <td>{{ $location->item->barcode }}</td>
                    @else
                        <td></td>
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
    </body>
</html>