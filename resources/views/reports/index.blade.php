@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Reports</h1>
        <br/>
    </section>

    <div class="content">
        <div class="clearfix"></div>
        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                {{ Form::open(array('url' => url('report/generate'), 'method' => 'POST')) }}
                {{ Form::label('location', 'Report Selection:') }}
                <select name="report_name" class="form-control" id="report-select">
                    <option data-options="1" value="stock-value">Stock Valuation</option>
                    <option data-options="2" value="supplier-spend">Supplier Spending</option>
                    <option data-options="3" value="stock-check">Stock Check Report</option>
                    {{-- <option data-options="4" value="year-end">Financial Year End Work in Progress</option> --}}
                    {{-- <option data-options="5" value="all-wip">All Work in Progress</option> --}}
                    <option data-options="6" value="batch-counter">Counter Sales to Post to Sage</option>
                </select>

                <div class="visible-options">
                </div>

                <br/>
                {{ Form::submit('Generate', array('class' => 'btn btn-default', 'id' => 'generate-btn')) }}
                {{ Form::close() }}


                <div class="hidden hidden-options">
                    <div class="form-group option-set" data-option-set="1">
                        @if(isset($stores))
                            {{ Form::label('location', 'Location to Report:') }}
                            <select name="store_id" class="form-control" id="store-select">
                                <option value="all">All Locations</option>
                                @foreach($stores as $store)
                                    <option value="{{ $store->id }}">{{ $store->name }}</option>
                                @endforeach
                            </select>
                        @endif
                    </div>

                    <div class="form-group option-set" data-option-set="2">
                        @if(isset($suppliers))
                            {{ Form::label('location', 'Supplier to Report:') }}
                            <select name="store_id" class="form-control" id="store-select">
                                <option value="all">All Suppliers</option>
                                @foreach($suppliers as $supplier)
                                    <option value="{{ $supplier->id }}">{{ $supplier->name }}</option>
                                @endforeach
                            </select>
                        @endif
                    </div>

                    <div class="row option-set" data-option-set="3">
                        @if(isset($stores))
                            <div class="col-sm-12 col-md-4 stock-store">
                                {{ Form::label('location', 'Store:') }}
                                <select name="store_id" class="form-control">
                                    <option value="all">All Locations</option>
                                    @foreach($stores as $store)
                                        <option value="{{ $store->id }}">{{ $store->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        @endif

                        @if(isset($locations['rooms']))
                            <div class="col-sm-12 col-md-2 stock-room">
                                {{ Form::label('location', 'Room:') }}
                                <select name="room" class="form-control">
                                    <option value="all">All</option>
                                    @foreach($locations['rooms'] as $room)
                                        <option data-store="{{ $room['store_id'] }}" value="{{ $room['room'] }}">{{ $room['room'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        @endif

                        @if(isset($locations['racks']))
                            <div class="col-sm-12 col-md-2 stock-rack">
                                {{ Form::label('location', 'Rack:') }}
                                <select name="rack" class="form-control">
                                    <option value="all">All</option>
                                    @foreach($locations['racks'] as $rack)
                                        <option data-store="{{ $rack['store_id'] }}" value="{{ $rack['rack'] }}">{{ $rack['rack'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        @endif

                        @if(isset($locations['shelves']))
                            <div class="col-sm-12 col-md-2 stock-shelf">
                                {{ Form::label('location', 'Shelf:') }}
                                <select name="shelf" class="form-control">
                                    <option value="all">All</option>
                                    @foreach($locations['shelves'] as $shelf)
                                        <option data-store="{{ $shelf['store_id'] }}" value="{{ $shelf['shelf'] }}">{{ $shelf['shelf'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        @endif

                        @if(isset($locations['bays']))
                            <div class="col-sm-12 col-md-2 stock-bay">
                                {{ Form::label('location', 'Bay:') }}
                                <select name="bay" class="form-control">
                                    <option value="all">All</option>
                                    @foreach($locations['bays'] as $bay)
                                        <option data-store="{{ $bay['store_id'] }}" value="{{ $bay['bay'] }}">{{ $bay['bay'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        @endif
                    </div>

                    <div class="row option-set" data-option-set="4">
                        <div class="col-sm-12 col-md-2 year-select">
                            {{ Form::label('year', 'Select Year End to Run on') }}
                            <select name="year" class="form-control">
                                @foreach($dates as $date)
                                    <option vlaue="{{ $date }}">{{ $date }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="row option-set" data-option-set="5">
                        <div class="col-sm-12 col-md-2 year-select">
                            {{ Form::label('year', 'Jobs / Counter Sales?') }}
                            <select name="year" class="form-control">
                                <option value="all">Both</option>
                                <option value="jobs">Jobs</option>
                                <option value="counters">Counter Sales</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('footer-script')
    <script type="text/javascript">
        (function(window, $) {
            moveOptions($('#report-select'));


            //Go Away and generate the report
            $('body').on('click', '#generate-btn', function(e){
                switch($('#report-select').val()){
                    case 'stock-value':

                        break;

                    case 'supplier-spend':

                        break;

                    case 'stock-check':

                        break;

                    case 'year-end':

                        break;

                    case 'wip':

                        break;
                }
            });
            //The options that match up to the selected report tpye.
            $('body').on('change', '#report-select', function(e) {
                moveOptions($('#report-select'));
            });

            //The location boxes for doing a location report
            $('body').on('change', 'div[data-option-set="3"] select', function(e) {
                var store_id = $('div[data-option-set="3"] select[name="store_id"]').val();
                console.log('Store: ' + store_id);
                $('div[data-option-set="3"] select').each(function(){
                    if($(this).attr('name') != 'store_id'){
                        $(this).find('option').show();
                        $(this).find('option').each(function(e){
                            if($(this).attr('data-store') != store_id && $(this).val() != 'all'){
                                $(this).hide();
                            }
                        });
                    }
                });
            });

            function moveOptions(elem){
                var option = $('option:selected', elem).attr('data-options');
                console.log(option);
                $('.visible-options .option-set').each(function() {
                    if($(this).attr('data-option-set') != option){
                        $(this).appendTo(".hidden-options");
                    }
                })

                $('.hidden-options .option-set').each(function() {
                    if($(this).attr('data-option-set') == option){
                        $(this).appendTo(".visible-options");
                    }
                })
            }
        })(window, jQuery);
    </script>
@endpush