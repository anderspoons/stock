@extends('layouts.app')

@section('content')
<div class="content">
    <br>
    <div class="row">
        <div class="col-md-6">
            <!-- Info Boxes Style 2 -->
            <div class="info-box bg-blue">
                <span class="info-box-icon"><i class="ion ion-podium"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Inventory</span>
                    <span class="info-box-number">
                        {{ $inventory['total_items'] }} items, totalling {{ $inventory['total_count'] }} individually <br>
                        Last Updated: {{ $inventory['updated'] }}
                    </span>
                  <span class="progress-description">
                  </span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.box -->
        </div>

        <div class="col-md-6">
            <!-- Info Boxes Style 2 -->
            <div class="info-box bg-green">
                <span class="info-box-icon"><i class="ion ion-ios-cart-outline"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Orders</span>
                    <span class="info-box-number">
                        Awaiting Arrival:  {{ $orders_awaiting['orders_awaiting'] }}, Open (Not Placed): {{ $orders_open['orders_open'] }}<br>
                        Last Updated: {{ $orders_awaiting['updated'] }}
                    </span>
                    <span class="progress-description">
                  </span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.box -->
        </div>


        <div class="col-md-6">
            <!-- Info Boxes Style 2 -->
            <div class="info-box bg-yellow">
                <span class="info-box-icon"><i class="ion ion-android-boat"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Jobs Awaiting Items</span>
                    <span class="info-box-number"></span>
                    <span class="progress-description"></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.box -->
        </div>


        <div class="col-md-6">
            <!-- Info Boxes Style 2 -->
            <div class="info-box bg-blue">
                <span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Stock Cost Value</span>
                    <span class="info-box-number">
                        £{{ $value['value'] }} <br>
                        Last Updated: {{ $value['updated'] }}
                    </span>
                  <span class="progress-description">
                  </span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.box -->
        </div>

        <div class="col-md-6">
            <!-- Info Boxes Style 2 -->
            <div class="info-box bg-red">
                <span class="info-box-icon"><i class="ion ion-ios-cart-outline"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Under Stocked Items</span>
                    <span class="info-box-number">
                        {{ $under_stocked['under_stocked'] }} Items are Under Stocked<br>
                        Last Updated: {{ $under_stocked['updated'] }}
                    </span>
                    <span class="progress-description">
                  </span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.box -->
        </div>

        <div class="col-md-6">
            <!-- Info Boxes Style 2 -->
            <div class="info-box bg-yellow">
                <span class="info-box-icon"><i class="ion ion-ios-cart-outline"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Over Stocked Items</span>
                    <span class="info-box-number">
                        {{ $over_stocked['over_stocked'] }} Items are Over Stocked<br>
                        Last Updated: {{ $over_stocked['updated'] }}
                    </span>
                    <span class="progress-description">
                  </span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</div>
@endsection
