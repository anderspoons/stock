@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Issue Stock</h1>
    </section>
    <div class="content">
        {{ Form::open(array('url' => 'order/process', 'method' => 'POST', 'id' => 'issue-stock-form')) }}
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    @if(isset($order->id))
                        {!! Form::hidden('id', $order->id) !!}
                    @endif

                    @if(isset($order->description))
                        <div class="col-sm-12 col-md-6">
                            {!! Form::label('description', 'Description:') !!}
                            {!! Form::text('description', $order->description, array('disabled' => 'disabled', 'class' => 'form-control')) !!}
                        </div>
                    @endif

                    <div class="col-sm-12 col-md-6">
                        {!! Form::label('po', 'Order Number:') !!}
                        @if(isset($order->po))
                            {!! Form::text('po', $order->po, array('disabled' => 'disabled', 'class' => 'form-control')) !!}
                        @else
                            {!! Form::text('po', null, array('disabled' => 'disabled', 'class' => 'form-control')) !!}
                        @endif
                    </div>

                    <div class="col-sm-12 col-md-6">
                        {!! Form::label('custom_po', 'Custom Order Number:') !!}
                        @if(isset($order->custom_po))
                            {!! Form::text('custom_po', $order->custom_po, array('disabled' => 'disabled', 'class' => 'form-control')) !!}
                        @else
                            {!! Form::text('custom_po', null, array('disabled' => 'disabled', 'class' => 'form-control')) !!}
                        @endif
                    </div>

                    @if(isset($order->supplier))
                        <div class="col-sm-12 col-md-6">
                            {!! Form::label('supplier', 'Supplier:') !!}
                            {!! Form::hidden('supplier_id', $order->supplier->id, array('id' => 'order_supplier_id')) !!}
                            {!! Form::text('supplier', $order->supplier->name, array('disabled' => 'disabled', 'class' => 'form-control')) !!}
                        </div>
                    @endif

                    @if(isset($order->store))
                        <div class="col-sm-12 col-md-6">
                            {!! Form::label('store', 'Store:') !!}
                            {!! Form::text('store', $order->store->name, array('disabled' => 'disabled', 'class' => 'form-control')) !!}
                        </div>
                    @endif

                    @if(isset($order->id))
                        <div class="col-sm-12 col-md-6">
                            <br>
                            <a href="{{ route('orders.edit', $order->id) }}" target="_blank" class='btn btn-default'>
                                Edit <i class="glyphicon glyphicon-edit"></i>
                            </a>
                        </div>
                    @endif

                    <div class="clearfix"></div>
                </div>

                <div class="row">
                    <br/>
                    <div class="clearfix"></div>
                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                            {!! Form::label('items[]', 'Item Search: (Enter Barcode)') !!}
                            {!! Form::text('barcode', '', array('maxlength' => '9', 'id' => 'barcode-search', 'class' => 'form-control', 'autocomplete' => 'off', 'readonly' => 'readonly')) !!}
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                            {!! Form::label('', '&nbsp;') !!}
                            <div class="clearfix"></div>
                            {!! Form::button('<i class="glyphicon glyphicon-plus"></i>  Add Miscellaneous Item', array('data-toggle' => 'modal', 'data-target' => '.bs-misc-modal-sm', 'id' => 'misc-item', 'class' => 'btn btn-default', 'the-id' => $order->id)) !!}
                            {!! Form::button('<i class="glyphicon glyphicon-plane"></i>  Add Carriage', array('data-toggle' => 'modal', 'data-target' => '.bs-carriage-modal-sm', 'id' => 'carriage-item', 'class' => 'btn btn-default', 'the-id' => $order->id)) !!}
                            {!! Form::button('<i class="glyphicon glyphicon-search"></i> Search Items', array('id' => 'search-item', 'class' => 'btn btn-default')) !!}
                        </div>
                    </div>
                    <hr class="col-sm-12">
                    <div class="clearfix"></div>
                </div>

                <div class="" id="item-row">

                </div>

                <div class="row">
                    <!-- Submit Field -->
                    <div class="form-group col-sm-12">
                        <a href="{{ url('/orders/') }}" class="btn btn-info">Back</a>
                        {!! Form::button('Add to Order', array('data-toggle' => 'modal', 'data-target' => '.bs-confirm-issue-modal-sm', 'class' => 'btn btn-default')) !!}

                    </div>
                </div>
            </div>
        </div>
        @include('orders.includes.confirm')
        {{ Form::close() }}
    </div>

    @include('orders.script')
    @include('orders.includes.issue-normal-row')
    @include('orders.includes.issue-misc-row')
    @include('orders.includes.issue-carriage-row')

    @include('orders.includes.order-details')
    @include('orders.includes.order-misc')
    @include('orders.includes.order-carriage')
@endsection

