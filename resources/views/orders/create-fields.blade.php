<!-- Supplier Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('supplier_id', 'Supplier:') !!}
    @include('suppliers.select')
</div>

<!-- Store Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('store_id', 'Store (Delivery to):') !!}
    @include('stores.select')
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Po Field -->
<div class="form-group col-sm-6">
    {!! Form::label('po', 'Purchase Order:') !!}
    {!! Form::hidden('po', $number) !!}
    {!! Form::text('', $number, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
</div>

<!-- Po Field -->
<div class="form-group col-sm-6">
    {!! Form::label('custom_po', 'Purchase Order:') !!}
    {!! Form::text('custom_po', null, ['class' => 'form-control']) !!}
</div>

@include('shared.status')