{!! $done = false !!}

@foreach($item->supplier_units as $unit)
    @if($unit->pivot->supplier_id == $supplier->id)
        <?php $done = true; ?>
        {!! Form::text('supplier[][measurement]', $unit->pivot->measurement, ['class' => 'form-control']) !!}
        @break
    @endif
@endforeach

@if(!$done)
    {!! Form::text('supplier[][measurement]', '', ['class' => 'form-control']) !!}
@endif