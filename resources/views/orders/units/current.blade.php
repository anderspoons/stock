{{-- 0 Is never going to be used --}}
<?php $unit_id = 0; ?>
<select name="supplier[][unit_id]" class="form-control">
    @foreach($item->supplier_units as $unit)
        @if($unit->pivot->supplier_id == $supplier->id)
            <?php $unit_id = $unit->pivot->unit_id; ?>
            @break
        @endif
    @endforeach

    @if($unit_id == 0)
        <option value=""></option>
    @endif

    @foreach($units as $unit)
        @if($unit->id == $unit_id)
            <option selected="selected" value="{!! $unit->id !!}">{!! $unit->name !!} ({!! $unit->short !!})</option>
        @else
            <option value="{!! $unit->id !!}">{!! $unit->name !!} ({!! $unit->short !!})</option>
        @endif

    @endforeach
</select>
