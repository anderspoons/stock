{{-- 0 Is never going to be used --}}
<select name="supplier_category_id" class="form-control new-supplier-unit-cat">
    <option value=""></option>
    @foreach($category as $cat)
        <option size-num="{!! $cat->num_sizes !!}" value="{!! $cat->id !!}">{!! $cat->name !!}</option>
    @endforeach
</select>
