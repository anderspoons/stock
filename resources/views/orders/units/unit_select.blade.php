<select name="supplier-unit_id" class="form-control new-supplier-unit">
        <option data-cat="999" value=""></option>
    @foreach($units as $unit)
        <option data-alias="{!! $unit->short !!}" data-cat="{!! $unit->category_id !!}" value="{!! $unit->id !!}">{!! $unit->name !!} ({!! $unit->short !!})</option>
    @endforeach
</select>
