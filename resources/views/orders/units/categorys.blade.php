<select name="supplier[][category_id]" class="form-control">
    <option value=""></option>
    @foreach($category as $cat)
        <option value="{!! $cat->id !!}">{!! $cat->name !!}</option>
    @endforeach
</select>