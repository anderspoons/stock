
@push('footer-script')
<script type="">
    (function(window, $) {


        $('#search-item').click(function(e){
            OpenInNewTab("{!! url('/items?order_issue') !!}");
        })

        $('body').on('click', '.remove-row', function(e){
            $(this).parent().parent().parent().remove();
            reindex_rows();
        });

        function OpenInNewTab(url) {
            var win = window.open(url, '_blank');
            win.focus();
        }

        crosstab.on('order-a-item', function(data) {
            $('#barcode-search').val(data.data.barcode);
            lookupItem(true);
        });



        $('#barcode-search').bind('input',function(){
            if($(this).val().length == 9){
                lookupItem(true);
            }
            if ($(this).val().toUpperCase().indexOf("MS") == -1 && $(this).val().length == 7) {
                lookupItem(false);
            }
        });

        function openModal(data){
            $('#sizing').html('');
            var user_store = "{!! $user->store_id !!}";
            $('.bs-details-modal-sm').find('.loc-select').html('');
            $.each(data[0].stores, function(i, store){
                if(store.id == user_store){
                    $('.bs-details-modal-sm').find('.loc-select').append('<option value="'+store.pivot.id+'" data-qty="'+store.pivot.qty+'">'+ store.name + ' : ' +store.pivot.room + store.pivot.rack + store.pivot.shelf + store.pivot.bay +' : Qty: '+store.pivot.qty+'</option>')
                }
            });

            $('.bs-details-modal-sm').find('#cost').val(0);

            if(data[0].unit_category != null){
                $('.bs-details-modal-sm #unit-report').val(data[0].unit_id);
                if(data[0].unit_category.id != '6'){
                    HideQty(true);
                    //6 Meaning that its individual in which case qty box is used.
                    var sizes = $('.modal-content .size-template').clone();
                    var count = sizes.find('.size-box').length;
                    var size_row = sizes.find('.size-box').first().clone();
                    for (var i = count; i < data[0].unit_category.num_sizes; i++) {
                        sizes.find('.size-box').after(size_row);
                    }
                    $('#sizing').append(sizes);
                    //3 is squared which means these will be legnths which is 2 ... shit I know but hey ho...
                    if(data[0].unit_category.id == 3) {
                        doUnits(2, data[0].unit_id);
                    } else {
                        doUnits(data[0].unit_category.id, data[0].unit_id);
                    }
                } else {
                    //Its a size meaning we need to hide quantity
                    HideQty(false);
                }
            } else {
                var sizes = $('.modal-content .size-template').clone();
                $('#sizing').append(sizes);
                $('#sizing').append('<p class="label-warning">The units this item is sold in has not been set.</p>');
                doUnits(0, data[0].unit_id);
            }
            $('.element-select').each(function(){
                $(this).select2("val", "");
            })
            reindex_rows();
        }

        function reindex_rows(){
            console.log('Re-indexing rows');
            var x = 0;
            $.each($('#item-row > div'), function(){
                $.each($(this).find("[name*='item[]']"), function(){
                    $(this).attr('name', $(this).attr('name').replace('item[]', 'item['+x+']'));
                });
                x++;
                var s = 0;
                $.each($(this).find('.size-template > input'), function(){
                    $(this).attr('name', $(this).attr('name').replace('[size][]', '[size]['+s+']'));
                    s++;
                })
            })

        }


        $('#item-details-modal').find('#qty').on('keyup', function() {
            var maxval =  $('#item-details-modal').find('.loc-select').find(":selected").attr('data-qty');
            $(this).attr({
                "max" : maxval,
                "max" : maxval,
                "min" : 1
            });
            if(parseInt($(this).val()) > parseInt(maxval)){
                $(this).val('');
                $(this).val(maxval);
                showError('The qty entered is more than is currently in stock.');
            }
        });

        $('body').on('click', '.confirm-order-button', function(e){
            var locationid = $('#item-details-modal').find('.loc-select').val();
            var qty = 0;
            //Checks for a 0 quantity value
            if($('#item-details-modal').find('#qty').val().length > 0) {
                qty = $('#item-details-modal').find('#qty').val();
            } else {
                showError('Please specify a quantity.');
                return true;
            }

            var cost = 0;
            var real_cost = 0;
            if($('#item-details-modal').find('#cost').val().length > 0) {
                cost = parseFloat($('#item-details-modal').find('#cost').val()).toFixed(2);
                real_cost = parseFloat($('#item-details-modal').find('#real-cost').val()).toFixed(2);
            } else {
                showError('Please specify a cost.');
                return true;
            }

            var template = $('#item-template').clone();
            template.find('.item-id').val(lastItem[0].id);
            template.find('.item-quantity').val(qty);
            template.find('.item-unit-cost').val(parseFloat(cost).toFixed(2));
            template.find('.item-cost').val(parseFloat(qty * cost).toFixed(2));
            template.find('.item-real-cost').val(parseFloat(qty * real_cost).toFixed(2));
            template.find('.item-description').val(lastItem[0].description);
            $.each(lastItem[0].stores, function(i, loc){
                if(loc.pivot.id == locationid) {
                    template.find('.item-location').val(loc.name + ' : ' + loc.pivot.room + loc.pivot.rack + loc.pivot.shelf + loc.pivot.bay);
                    template.find('.item-location-id').val(loc.pivot.id);
                }
            });
            template.find('.item-barcode').val(lastItem[0].barcode);

            var tempID = Date.now().toString();
            var elements = '';
            if($('#item-details-modal .element-select').val() != null){
                var elements = $('#item-details-modal .element-select').val().toString();
            } else {
                showError('Please specify a job element this is being used for.');
                return true;
            }

            var element_names = $('#item-details-modal .element-select').select2('data');
            template.find('.element-select-template').attr('identity', tempID)
            var element_text = '';
            $.each(element_names, function(e){
                element_text = element_text + this.text + ', ';
            });
            template.find('.element-names').val(element_text.replace(/,\s*$/, ""));
            template.find('.element-ids').val(elements);

            if($('#item-details-modal .size-unit').is(":visible")){
                var visible_count = 0;
                $.each($('#item-details-modal .size-unit'), function(){
                    if($(this).is(':visible')){
                        visible_count++;
                    }
                });
                for (var i = template.find('.size-template').length; i < visible_count; i++) {
                    var size_box = template.find('.size-template').eq(0).clone();
                    template.find('.size-stuff div').eq(0).append(size_box);
                }
                var i = 0;
                $.each($('#item-details-modal .size-unit'), function(){
                    if($(this).is(':visible')){
                        template.find('.size-box').eq(i).val($(this).val());
                    }
                });
                template.find('.size-stuff').removeClass('hidden');
                template.find('.item-unit').append($('#item-details-modal .unit-select option:selected')[0].outerHTML);
            } else {
                template.find('.size-stuff').remove();
            }

            //Remove the hidden class
            template.removeClass('hidden');
            //set ID to thew time so we don't accidently reuse this ID
            template.attr('id', tempID);
            template.find('.row-id').val(tempID);
            $('#item-row').append(template);
            $('#item-details-modal').find('.close').click();
            reindex_rows();
        })

        function lookupItem(normal){
            var barcode = '';
            if(normal){
                barcode = $('#barcode-search').val().toUpperCase();
            } else {
                barcode = 'MS'+$('#barcode-search').val();
            }
            $.ajax({
                url: "{!! url('/lookup/barcode') !!}/"+barcode,
                type: 'GET',
                dataType: 'Json',
                success: function (data) {
                    if(data.success == true){
                        lastItem = data.data;
                        openModal(data.data);
                        $('#item-details-modal').modal('show');
                    } else {
                        lastItem = {};
                        showError("No item with that barcode found.");
                        $('#barcode-search').val('');
                    }
                }
            });
        }

        function HideQty(boool){
            if(boool == false){
                $('.bs-details-modal-sm').find('.qty-box').show();
                $('.bs-details-modal-sm').find('.qty-info').hide();
            } else {
                $('.bs-details-modal-sm').find('.qty-box').hide();
                $('.bs-details-modal-sm').find('.qty-info').show();
            }
        }
    })(window, jQuery);
</script>
@endpush