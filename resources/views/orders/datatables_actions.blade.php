<div class='btn-group'>
    <a href="{{ route('orders.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="glyphicon glyphicon-eye-open"></i>
    </a>
    <a href="{{ route('orders.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="glyphicon glyphicon-edit"></i>
    </a>
    <a href="{{ url('orders/add/'.$id) }}" class='btn btn-default btn-xs'>
        <i class="glyphicon glyphicon-shopping-cart"></i>
    </a>
</div>
