<div class="btn-group">
    @if(isset($item_id))
        <a href="{{ url('items/'.$item_id.'/edit') }} " class="btn btn-default btn-xs" target="_blank">
            <i data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class='action-tooltip'>Edit the item, add locations etc.</span>" class="glyphicon glyphicon-edit"></i>
        </a>
    @endif
    <a href="#" class="btn btn-default btn-xs" data-toggle="modal" data-target=".bs-quick-qty-modal-sm">
        <i data-toggle="tooltip" data-placement="bottom" data-html="true" line-id="{!! $id !!}" title="<span class='action-tooltip'>Adjust quantity on order</span>" class="quick-qty glyphicon glyphicon-sort"></i>
    </a>
    <a href="#" class="btn btn-default btn-xs" data-toggle="modal" data-target=".bs-set-cost-modal-sm">
        <i data-toggle="tooltip"  line-id='{!! $id !!}' line-cost='{!! $line_unit_cost !!}'data-placement="bottom" data-html="true" title="<span class='action-tooltip'>Adjust unit cost for this line</span>"
           class="quick-cost glyphicon glyphicon-gbp">
        </i>
    </a>
    <a href="#" line-id='{!! $id !!}' class="btn btn-default btn-xs" data-toggle="modal" data-target=".bs-set-cost-modal-sm">
        <i data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class='action-tooltip'>Print a label for this item</span>" class="print-label glyphicon glyphicon-barcode"></i>
    </a>
    <a href="#" line-id='{!! $id !!}' class="quick-delete-action btn btn-danger btn-xs" data-toggle="modal" data-target=".bs-delete-modal-sm">
        <i data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class='action-tooltip'>Delete line from order</span>" class="glyphicon glyphicon-remove"></i>
    </a>
</div>