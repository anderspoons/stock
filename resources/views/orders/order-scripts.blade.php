@section('scripts')
    <script type="text/javascript">
        var lastItem;
        (function(window, $) {
            $('#barcode-search').bind('input',function(){
                if($(this).val().length == 9){
                    lookupItem(true);
                }
                if ($(this).val().toUpperCase().indexOf("MS") == -1 && $(this).val().length == 7) {
                    lookupItem(false);
                }
            });

            $('#item-details-modal').find('#qty').on('keyup', function() {
                var maxval =  $('#item-details-modal').find('.loc-select').find(":selected").attr('data-qty');
                $(this).attr({
                    "max" : maxval,
                    "max" : maxval,
                    "min" : 1
                });
                if(parseInt($(this).val()) > parseInt(maxval)){
                    $(this).val('');
                    $(this).val(maxval);
                    showError('The qty entered is more than is currently in stock.');
                }
            });

            $('body').on('keyup', '#item-details-modal #size', function() {
                if(checkVal($(this), $('#item-details-modal').find('.loc-select option:selected').attr('data-qty')) == false){
                    $(this).val('0');
                    showError('The qty entered is more than is currently in stock.');
                }
            });

            $('body').on('change', '.unit-select', function() {
                if(checkVal($('#item-details-modal #size').eq(0), $('#item-details-modal').find('.loc-select option:selected').attr('data-qty')) == false){
                    $(this).val('0');
                    showError('The unit chosen has less in stock when converted than the one changed from.');
                 }
            })

            function checkVal(element, qty){
                console.log('Check Value called');
                var entered_amount = parseFloat($(element).val());
                var reporting_unit = $(element).parent().parent().find(".unit-select option[value='" + $('.bs-details-modal-sm #unit-report').val() + "']").attr('data-alias');
                var selected_unit = $(element).parent().parent().find('.unit-select option:selected').attr('data-alias');
                var entered_qty = new Qty(entered_amount, selected_unit);
                var location_qty = new Qty(parseFloat(qty), reporting_unit);
                if(entered_qty.gt(location_qty)){
                    return false;
                } else {
                    console.log('Reporting: '+reporting_unit);
                    console.log('conversion: '+entered_qty.to(reporting_unit));
                    $('#item-details-modal #qty').val(parseFloat(entered_qty.to(reporting_unit)).toFixed(2).toString());
                    return true;
                }
            }

            $('body').on('click', '.confirm-misc-button', function(e) {
                e.preventDefault();
                var template = $('#misc-template').clone();
                var qty = 0;
                var cost = 0;
                if($('#misc-details-modal').find('#qty').val().length > 0) {
                    qty = $('#misc-details-modal').find('#qty').val();
                } else {
                    showError('Please specify a quantity.');
                    return true;
                }
                if($('#misc-details-modal').find('#cost').val().length > 0) {
                    cost = $('#misc-details-modal').find('#cost').val();
                } else {
                    cost = '0';
                }
                template.find('.item-id').val('misc');
                template.find('.item-quantity').val(qty);
                template.find('.item-unit-cost').val(parseFloat(cost).toFixed(2));
                template.find('.item-cost').val(parseFloat(cost * qty).toFixed(2));
                template.find('.item-details').val($('#misc-details-modal .info-box').val());
                template.find('.item-description').val($('#misc-details-modal #misc_name').val());

                var tempID = Date.now().toString();
                //Remove the hidden class
                template.removeClass('hidden');
                //set ID to the time so we don't accidently reuse this ID
                template.attr('id', tempID);
                template.find('.row-id').val(tempID);
                $('#item-row').append(template);
                $('#misc-details-modal').find('.close').click();
                reindex_rows();
            });

            $('body').on('click', '.confirm-labour-button', function(e) {
                e.preventDefault();
                var template = $('#labour-template').clone();
                var cost = 0;
                if($('#labour-details-modal').find('#cost').val().length > 0) {
                    cost = $('#labour-details-modal').find('#cost').val();
                } else {
                    cost = '0';
                }
                template.find('.item-id').val('labour');
                template.find('.item-cost').val(cost);
                template.find('.item-name').val($('#labour-details-modal .labour-name').val());

                var tempID = Date.now().toString();
                //Remove the hidden class
                template.removeClass('hidden');
                //set ID to thew time so we don't accidently reuse this ID
                template.attr('id', tempID);
                template.find('.row-id').val(tempID);
                $('#item-row').append(template);
                $('#labour-details-modal').find('.close').click();
                reindex_rows();
            });


            $('body').on('click', '.confirm-carriage-button', function(e) {
                e.preventDefault();
                var template = $('#carriage-template').clone();
                var cost = 0;
                if($('#carriage-details-modal').find('#cost').val().length > 0) {
                    cost = $('#carriage-details-modal').find('#cost').val();
                } else {
                    cost = '0';
                }
                template.find('.item-id').val('carriage');
                template.find('.item-cost').val(cost);
                template.find('.item-name').val($('#carriage-details-modal .carriage-name').val());

                var tempID = Date.now().toString();
                //Remove the hidden class
                template.removeClass('hidden');
                //set ID to thew time so we don't accidently reuse this ID
                template.attr('id', tempID);
                template.find('.row-id').val(tempID);
                $('#item-row').append(template);
                $('#carriage-details-modal').find('.close').click();
                reindex_rows();
            });

            $('body').on('click', '.confirm-issue-button', function(e){
                e.preventDefault();
                var locationid = $('#item-details-modal').find('.loc-select').val();
                var qty = 0;
                //Checks for a 0 quantity value
                if($('#item-details-modal').find('#qty').val().length > 0) {
                    qty = $('#item-details-modal').find('#qty').val();
                } else {
                    showError('Please specify a quantity.');
                    return true;
                }
                var cost = 0;
                var real_cost = 0;
                if($('#item-details-modal').find('#cost').val().length > 0) {
                    cost = parseFloat($('#item-details-modal').find('#cost').val()).toFixed(2);
                    real_cost = parseFloat($('#item-details-modal').find('#real-cost').val()).toFixed(2);
                } else {
                    showError('Please specify a cost.');
                    return true;
                }

                var template = $('#item-template').clone();
                template.find('.item-id').val(lastItem[0].id);
                template.find('.item-quantity').val(qty);
                template.find('.item-cost').val(parseFloat(qty * cost).toFixed(2));
                template.find('.item-unit-cost').val(parseFloat(cost).toFixed(2));
                template.find('.item-real-cost').val(parseFloat(real_cost).toFixed(2));
                template.find('.item-description').val(lastItem[0].description);
                $.each(lastItem[0].stores, function(i, loc){
                    if(loc.pivot.id == locationid) {
                        template.find('.item-location').val(loc.name + ' : ' + loc.pivot.room + loc.pivot.rack + loc.pivot.shelf + loc.pivot.bay);
                        template.find('.item-location-id').val(loc.pivot.id);
                    }
                });
                template.find('.item-barcode').val(lastItem[0].barcode);

                var tempID = Date.now().toString();
                if($('#item-details-modal .size-unit').is(":visible")){
                    var visible_count = 0;
                    $.each($('#item-details-modal .size-unit'), function(){
                        if($(this).is(':visible')){
                            visible_count++;
                        }
                    });
                    for (var i = template.find('.size-template').length; i < visible_count; i++) {
                        var size_box = template.find('.size-template').eq(0).clone();
                        template.find('.size-stuff div').eq(0).append(size_box);
                    }
                    var i = 0;
                    $.each($('#item-details-modal .size-unit'), function(){
                        if($(this).is(':visible')){
                            template.find('.size-box').eq(i).val($(this).val());
                        }
                    });
                    template.find('.size-stuff').removeClass('hidden');
                    template.find('.item-unit').append($('#item-details-modal .unit-select option:selected')[0].outerHTML);
                } else {
                    template.find('.size-stuff').remove();
                }

                //Remove the hidden class
                template.removeClass('hidden');
                //set ID to thew time so we don't accidently reuse this ID
                template.attr('id', tempID);
                template.find('.row-id').val(tempID);
                $('#item-row').append(template);
                $('#item-details-modal').find('.close').click();
                reindex_rows();
            })

            $('#search-item').click(function(e){
                OpenInNewTab("{!! url('/items?order_issue') !!}");
            })

            $('body').on('click', '.remove-row', function(e){
                $(this).parent().parent().parent().remove();
                reindex_rows();
            });

            function OpenInNewTab(url) {
                var win = window.open(url, '_blank');
                win.focus();
            }

            function doUnits(id, unit_id){
                var unit_box = $('#sizing').find('.unit-select');
                var all_units = $('#sizing').find('#unit-options');
                console.log(unit_box);
                console.log(all_units);
                if(id == 0){
                    $.each(all_units.find('option'), function(e){
                        unit_box.append($(this));
                    });
                } else {
                    $.each(all_units.find('option'), function(e){
                        if($(this).attr('data-cat') == id){
                            if($(this).val() == unit_id){
                                unit_box.append($(this));
                                $(this).attr('selected', 'selected');
                            } else {
                                unit_box.append($(this));
                            }
                        }
                    });
                }
                //unit_box.find('option').eq(0).attr('selected', 'selected');
            }

            crosstab.on('issue-a-item', function(data) {
                $('#barcode-search').val(data.data.barcode);
                lookupItem();
            });

            function openModal(data){
                $('#sizing').html('');
                $('.bs-details-modal-sm').find('.loc-select').html('');
                $.each(data[0].suppliers, function(i, supplier){
                    $('.bs-details-modal-sm').find('.supplier-select').append('<option value="'+supplier.id+'">'+ supplier.name + '</option>')
                });

                var cost = 0;
                var real_cost = 0;
                try {
                    real_cost = data[0].last_price[0].cost;
                } catch (exp) {
                    console.log('No cost price known');
                }
                if(data[0].markup_fixed === 0 && data[0].markup_percent != 0){
                    if(data[0].last_price[0].cost != 0){
                        cost = ((real_cost / 100) * data[0].markup_percent) + real_cost;
                    } else {
                        //the cost we have is 0 so we can't do a percentage markup.
                        //Enter cost as zero until someone does something with the price?
                    }
                } else {
                    cost = data[0].markup_fixed;
                }
                $('.bs-details-modal-sm').find('#cost').val(cost);
                $('.bs-details-modal-sm').find('#real-cost').val(real_cost);

                if(data[0].unit_category != null){
                    $('.bs-details-modal-sm #unit-report').val(data[0].unit_id);
                    if(data[0].unit_category.id != '6'){
                        HideQty(true);
                        //6 Meaning that its individual in which case qty box is used.
                        var sizes = $('.modal-content .size-template').clone();
                        var count = sizes.find('.size-box').length;
                        var size_row = sizes.find('.size-box').first().clone();
                        for (var i = count; i < data[0].unit_category.num_sizes; i++) {
                            sizes.find('.size-box').after(size_row);
                        }
                        $('#sizing').append(sizes);
                        //3 is squared which means these will be legnths which is 2 ... shit I know but hey ho...
                        if(data[0].unit_category.id == 3) {
                            doUnits(2, data[0].unit_id);
                        } else {
                            doUnits(data[0].unit_category.id, data[0].unit_id);
                        }
                    } else {
                        //Its a size meaning we need to hide quantity
                        HideQty(false);
                    }
                } else {
                    var sizes = $('.modal-content .size-template').clone();
                    $('#sizing').append(sizes);
                    $('#sizing').append('<p class="label-warning">The units this item is sold in has not been set.</p>');
                    doUnits(0, data[0].unit_id);
                }
                $('.element-select').each(function(){
                    $(this).select2("val", "");
                })
                reindex_rows();
            }
            
            function lookupItem(normal){
                var barcode = '';
                if(normal){
                    barcode = $('#barcode-search').val().toUpperCase();
                } else {
                    barcode = 'MS'+$('#barcode-search').val();
                }
                $.ajax({
                    url: "{!! url('/lookup/barcode') !!}/"+barcode,
                    type: 'GET',
                    dataType: 'Json',
                    success: function (data) {
                        if(data.success == true){
                            lastItem = data.data;
                            openModal(data.data);
                            $('#item-details-modal').modal('show');
                        } else {
                            lastItem = {};
                            showError("No item with that barcode found.");
                            $('#barcode-search').val('');
                        }
                    }
                });
            }

            $('body').on('click', '.confirm-pin-button', function(e){
                e.preventDefault();
                disableRows(false);
                submitForm($('#issue-stock-form'));
            });

            function submitForm(element){
                $.ajax({
                    url: "{!! url('issue/counter/process') !!}",
                    type: 'POST',
                    dataType: 'Json',
                    data: $(element).serialize(),
                    success: function(data) {
                        if(data.success == true){
                            new PNotify({
                                title: "Success",
                                text: data.text,
                                type: 'success',
                                addclass: "alert alert-success",
                            });
                            setTimeout(function(e){
                                location.reload(false);
                            },3000);
                        } else {
                            disableRows(true);
                            new PNotify({
                                title: "Problems",
                                text: data.text,
                                type: 'error',
                                addclass: "alert alert-warning",
                            });
                            highlightRows(data);
                        }
                    }
                });
            }

            function reindex_rows() {
                console.log('Re-indexing rows');
                var x = 0;
                $.each($('#item-row > div'), function () {
                    $.each($(this).find("[name*='item[]']"), function () {
                        $(this).attr('name', $(this).attr('name').replace('item[]', 'item[' + x + ']'));
                    });
                    x++;
                    var s = 0;
                    $.each($(this).find('.size-template > input'), function () {
                        $(this).attr('name', $(this).attr('name').replace('[size][]', '[size][' + s + ']'));
                        s++;
                    })
                })
            }
            function showError(msg){
                var myStack = {
                    "dir1":"down",
                    "dir2":"right",
                    "push":"top"
                };
                new PNotify({
                    title: "Problem",
                    text: msg,
                    type: "error",
                    addclass: "alert alert-warning",
                    stack: myStack
                });
            }

            function disableRows(toggle){
                if(toggle == false){
                    $('#item-row [disabled=disabled]').removeAttr('disabled');
                } else {
                    $('#item-row input,#item-row select').attr('disabled', true);
                }
            }


            function HideQty(boool){
                if(boool == false){
                    $('.bs-details-modal-sm').find('.qty-box').show();
                    $('.bs-details-modal-sm').find('.qty-info').hide();
                } else {
                    $('.bs-details-modal-sm').find('.qty-box').hide();
                    $('.bs-details-modal-sm').find('.qty-info').show();
                }
            }

        })(window, jQuery);
    </script>
@endsection