@extends('layouts.app')

@push('meta-tags')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endpush

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Order Items</h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                {!! $dataTable->table(['width' => '100%']) !!}
                <a href="{{ url('/orders/') }}" class="btn btn-default">Back</a>
                <button disabled class="process-items btn btn-info">Process Items</button>
            </div>
        </div>
    </div>
    @include('orders.includes.cost_modal')
    @include('orders.includes.quickqty')
    @include('orders.includes.delete')
@endsection

@push('footer-script')
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
    <script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
    <script src="../../vendor/datatables/buttons.server-side.js"></script>
    {!! $dataTable->scripts() !!}

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        (function (window, $) {
            //TODO, tick box to mark row will populate the qty automatically

            //TODO, location box will shot all locations for item but by default be set to the same one on order

            //TODO,


        });

        //Fucntion called as a callback for the datatables draw function.
        function doDraw(){
            //Select 2 stuff
            $(".location-select").select2({
                ajax: {
                    url: "{{ url('orders/location/json') }}",
                    dataType: "json",
                    method: 'POST',
                    data: function() {
                        var line_id = $(this).attr('line-id');
                        return {line: line_id }
                    },
                    results: function (data) {
                        return {
                            results: data.results
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data.results, function (item) {
                                return {
                                    text: item.text,
                                    id: item.id,
                                }
                            })
                        };
                    }
                },
                createSearchChoice: function (term, data) {
                    if ($(data.results).filter(function () {
                            return this.text.localeCompare(term) === 0;
                        }).length === 0) {
                        return {
                            id: term,
                            text: term
                        };
                    }
                },
                width: '100%',
            });

            $(".icheck").iCheck({
                checkboxClass: "icheckbox_square-blue",
                radioClass: "iradio_square-blue",
                increaseArea: "20%" // optional
            });

            $(".icheck").on('ifChecked', function(e){
                var row = $(this).parent().parent().parent();
                var complete = false;
                if($(this).hasClass('complete')){
                    complete = true;
                }
                populate_row(row, complete);
                check_btn();
            });

            $(".icheck").on('ifUnchecked', function(e){
                var row = $(this).parent().parent().parent();
                var complete = false;
                if($(this).hasClass('complete')){
                    complete = true;
                }
                clear_row(row, complete);
                check_btn();
            });
        }

        function check_btn(){
            var has_active = false;
            $(".icheck").each(function() {
                if($(this).iCheck('update')[0].checked){
                    has_active = true;
                }
            });
            if(has_active){
                $('.process-items').prop("disabled", false);
            } else {
                $('.process-items').prop("disabled", true);
            }
        }
        //Populate the row.
        function populate_row(row, complete){
            row.find('td').eq(2).find('input').prop("disabled", false);
            if(complete){
                row.find('td').eq(2).find('input').val(row.find('td').eq(4).text());
                row.find('td').find('.icheck').eq(0).iCheck('check');
            }
            row.find('td').eq(5).find('select').prop("disabled", false);
        }

        //Clear the row.
        function clear_row(row, complete){
            row.find('td').eq(2).find('input').prop("disabled", true);
            if(complete){
                row.find('td').eq(2).find('input').val('0');
            }
            if( row.find('td').find('.icheck').eq(1).iCheck('update')[0].checked){
                row.find('td').find('.icheck').eq(1).iCheck('uncheck');
            }
            row.find('td').eq(5).find('select').prop("disabled", true);
        }

        $('body').on('keyup', '.receive-qty', function() {
            if(parseInt($(this).val()) > $(this).attr('max') || parseInt($(this).val()) < 0){
                $(this).val('0');
                showError('The qty entered is more or less than is currently on order.');
            }
        });


        function showError(msg) {
            var myStack = {
                "dir1": "down",
                "dir2": "right",
                "push": "top"
            };
            new PNotify({
                title: "Problem",
                text: msg,
                type: "error",
                addclass: "alert alert-warning",
                stack: myStack
            });
        }

        function showSuccess(msg) {
            var myStack = {
                "dir1": "down",
                "dir2": "right",
                "push": "top"
            };
            new PNotify({
                title: "Success",
                text: msg,
                type: "success",
                addclass: "alert alert-success",
                stack: myStack
            });
        }

        $('body').on('click', '.process-items', function(){
            console.log('Process items')
            var row_data = [];
            $(".icheck").each(function() {
                if($(this).iCheck('update')[0].checked && $(this).hasClass('complete') === false) {
                    var row = $(this).parent().parent().parent();
                    if(row.find('td').eq(2).find('input').val() > 0){
                        row_data.push({
                            line_id: $(this).attr('line_id'),
                            for: $(this).attr('for'),
                            qty: row.find('td').find('.receive-qty').val(),
                            location: row.find('td').find('select').val()
                        });
                    }
                }
            });

            $.ajax({
                url: "{{ url('order/receive/process') }}",
                data: {
                    items: JSON.stringify(row_data)
                },
                method: "POST"
            }).done(function(data) {
                $('.buttons-reload').click();
            });
        });
    </script>
@endpush