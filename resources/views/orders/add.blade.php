@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Order
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                {!! Form::model($order, ['url' => ['orders/issue', $order->id], 'method' => 'patch']) !!}
                <div class="row">

                    <div class="col-sm-12 col-md-6">
                        <h4>Supplier Details</h4>
                        {{ Form::label('Name') }}
                        {{ Form::text('supplier', $order->supplier->name, array('class' => 'form-control', 'disabled' => 'disabled')) }}

                        {{ Form::label('address') }}
                        {{ Form::textarea('address', $order->supplier->formatted_address, array('class' => 'form-control', 'disabled' => 'disabled')) }}

                        {{ Form::label('Minimum Order') }}
                        {{ Form::text('minimum', '£'.$order->supplier->min_order, array('class' => 'form-control', 'disabled' => 'disabled')) }}
                    </div>

                    <div class="col-sm-12 col-md-6">
                        <h4>Store Details</h4>
                        {{ Form::label('store') }}
                        {{ Form::text('store', $order->store->name, array('class' => 'form-control', 'disabled' => 'disabled')) }}

                        {{ Form::label('address') }}
                        {{ Form::textarea('address', $order->store->formatted_address, array('class' => 'form-control', 'disabled' => 'disabled')) }}
                    </div>

                    <div class="col-sm-12 col-md-6">

                    </div>

                </div>
                <hr/>

                <div class="row">
                    <br/>
                    <div class="clearfix"></div>
                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                            {!! Form::label('items[]', 'Item Search: (Enter Barcode)') !!}
                            {!! Form::text('barcode', '', array('maxlength' => '9', 'id' => 'barcode-search', 'class' => 'form-control' )) !!}
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                            {!! Form::label('', '&nbsp;') !!}
                            <div class="clearfix"></div>
                            {!! Form::button('<i class="glyphicon glyphicon-plus"></i>  Add Miscellaneous Item', array('data-toggle' => 'modal', 'data-target' => '.bs-misc-details-modal-sm', 'id' => 'issue-item', 'class' => 'btn btn-default', 'the-id' => $order->id)) !!}
                            {!! Form::button('<i class="glyphicon glyphicon-search"></i>  Search Items', array('id' => 'search-item', 'class' => 'btn btn-default')) !!}
                        </div>
                    </div>
                    <hr class="col-sm-12">
                    <div class="clearfix"></div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@include('orders.scripts');
@include('orders.includes.item-details-modal')
@include('orders.includes.misc-details-modal')