<div class="btn-group">
    <a href="#" id='{!! $id !!}' class="btn btn-default btn-xs" data-toggle="modal" data-target=".bs-qty-modal-sm">
        <i data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class='action-tooltip'>Adjust quantity on order</span>" class="quick-qty glyphicon glyphicon-sort"></i>
    </a>
    <a href="#" class="btn btn-default btn-xs" data-toggle="modal" data-target=".bs-set-cost-modal-sm">
        <i data-toggle="tooltip"  line-id='{!! $id !!}' line-cost='{!! $line_unit_cost !!}'data-placement="bottom" data-html="true" title="<span class='action-tooltip'>Adjust unit cost for this line</span>"
           class="quick-cost glyphicon glyphicon-gbp">
        </i>
    </a>
    <a href="#" line-id='{!! $id !!}' class="quick-delete-action btn btn-danger btn-xs" data-toggle="modal" data-target=".bs-delete-modal-sm">
        <i data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class='action-tooltip'>Delete line from order</span>" class="glyphicon glyphicon-remove"></i>
    </a>
</div>