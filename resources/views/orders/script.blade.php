@push('footer-script')
    <script type="text/javascript">
        var lastItem;

        $( document ).ready(function() {
            $('#barcode-search').bind('input', function () {
                if($(this).val().length == 9){
                    lookupItem(true);
                }
                if ($(this).val().toUpperCase().indexOf("MS") == -1 && $(this).val().length == 7) {
                    lookupItem(false);
                }
            });


            $('#order-item-details-modal').find('#qty').on('keyup', function () {
                var maxval = $('#order-item-details-modal').find('.loc-select').find(":selected").attr('data-qty');
                $(this).attr({
                    "max": maxval,
                    "min": 1
                });
                if (parseInt($(this).val()) > parseInt(maxval)) {
                    $(this).val('');
                    $(this).val(maxval);
                    showError('The qty entered is more than is currently in stock.');
                }
            });

            //Existing
            $('body').on('keyup', '#order-item-details-modal #new-qty', function (){
                $('#order-item-details-modal #qty').val($('#order-item-details-modal #new-qty').val());
            });
            //Existing
            $('body').on('keyup', '#order-item-details-modal .existing-supplier-row .size-unit', function (){
                convert(true);
            });
            //New Supplier
            $('body').on('keyup', '#order-item-details-modal .new-supplier-row .size-unit', function(){
                convert(false);
            });
            $('body').on('change', '#order-item-details-modal .new-supplier-row .new-supplier-unit', function (){
                convert(false);
            });
            $('body').on('change', '#order-item-details-modal .existing-supplier-row .new-supplier-unit', function (){
                convert(true);
            });

            function convert(existing){
                if(existing){
                    console.log('Do existing supplier conversion');
                    //is this a single size or 2?
                    if($('#order-item-details-modal .existing-supplier-row .size-unit').length > 1){
                        //2 Boxes
                        check_area_val('.existing-supplier-row');
                    }
                    if($('#order-item-details-modal .existing-supplier-row .size-unit').length == 1){
                        //1 Box
                        checkVal('.existing-supplier-row');
                    }
                } else {
                    console.log('Do new supplier conversion');
                    if($('#order-item-details-modal .new-supplier-row .size-unit').length > 1){
                        //
                        check_area_val('.new-supplier-row');
                    }
                    if($('#order-item-details-modal .new-supplier-row .size-unit').length == 1){
                        //1 Box
                        checkVal('.new-supplier-row');
                    }
                }
            }

            function checkVal(element) {
                console.log('Check Value called');
                var entered_amount = parseFloat($(element + " .size-unit").eq(0).val());
                var selected_unit = $(element + " .new-supplier-unit option:selected").attr('data-alias');
                var reporting_unit = $(element + " .new-supplier-unit option[value='" + $('#order-item-details-modal #unit-report').val() + "']").attr('data-alias');

                if(selected_unit === reporting_unit){
                    $('#order-item-details-modal #qty').val(parseFloat(entered_amount).toFixed(2).toString());
                    return true;
                }

                var entered_qty = new Qty(parseFloat(entered_amount), selected_unit);
                if (parseFloat(entered_qty.to(reporting_unit)).toFixed(2).toString() > 0) {
                    console.log('Should set qty:' + parseFloat(entered_qty.to(reporting_unit)).toFixed(2).toString());
                    $('#order-item-details-modal #qty').val(parseFloat(entered_qty.to(reporting_unit)).toFixed(2).toString());
                    return true;
                }

                console.log('Qty unset: ');
                return false;
            };

            function check_area_val(selector) {
                var num_1 = $(selector + ' .size-unit').eq(0).val();
                var num_2 = $(selector + ' .size-unit').eq(1).val();
                var selected_unit = $(selector + ' .new-supplier-unit option:selected').attr('data-alias');
                console.log('Selected Unit: ' + selected_unit);
                var reporting_unit = $(selector + " .new-supplier-unit option[value='" + $('#order-item-details-modal #unit-report').val() + "']").attr('data-alias');
                console.log('Reporting Unit: ' + reporting_unit);
                var num_1_converted = new Qty(parseFloat(num_1), selected_unit);
                var num_2_converted = new Qty(parseFloat(num_2), selected_unit);
                var converted_area = parseFloat(num_1_converted.to(reporting_unit)).toFixed(2) * parseFloat(num_2_converted.to(reporting_unit)).toFixed(2);
                if(parseFloat(converted_area).toFixed(2).toString() > 0){
                    console.log('Should set qty (area)');
                    $('#order-item-details-modal #qty').val(parseFloat(converted_area).toFixed(2).toString());
                    return true;
                }
                return false;
            };

            $('body').on('click', '.confirm-carriage-button', function (e) {
                e.preventDefault();
                var template = $('#carriage-template').clone();
                var cost = 0;
                if ($('#carriage-details-modal').find('#cost').val().length > 0) {
                    cost = $('#carriage-details-modal').find('#cost').val();
                } else {
                    cost = '0';
                }
                template.find('.item-id').val('carriage');
                template.find('.item-cost').val(cost);
                template.find('.item-name').val($('#carriage-details-modal .carriage-name').val());

                var tempID = Date.now().toString();
                //Remove the hidden class
                template.removeClass('hidden');
                //set ID to thew time so we don't accidently reuse this ID
                template.attr('id', tempID);
                template.find('.row-id').val(tempID);
                $('#item-row').append(template);
                $('#carriage-details-modal').find('.close').click();
                reindex_rows();
            });


            function please_select(name){
                switch(name){
                    case('counter'):
                        showError('Please select a counter sale this item is being ordered for.');
                        break;
                    case('job'):
                        showError('Please select a job this item is being ordered for.');
                        break;
                }
            }

            $('body').on('click', '.confirm-misc-button', function (e) {
                e.preventDefault();
                var template = $('#misc-template').clone();
                var qty = 0;
                var cost = 0;

                if ($('#misc-details-modal').find('#misc_name').val().length < 1) {
                    showError('Please specify a name.');
                    return true;
                }


                if ($('#misc-details-modal').find('#qty').val().length > 0) {
                    qty = $('#misc-details-modal').find('#qty').val();
                } else {
                    showError('Please specify a quantity.');
                    return true;
                }

                if ($('#misc-details-modal').find('#cost').val().length > 0) {
                    cost = $('#misc-details-modal').find('#cost').val();
                } else {
                    cost = '0';
                }
                template.find('.item-id').val('misc');
                template.find('.item-quantity').val(qty);

                template.find('.item-unit-cost').val(parseFloat(cost).toFixed(2));
                template.find('.item-real-cost').val(parseFloat(cost).toFixed(2));
                template.find('.item-cost-text').val(parseFloat(cost * qty).toFixed(2));

                template.find('.item-name').val($('#misc-details-modal #misc_name').val());
                template.find('.item-description').val($('#misc-details-modal #misc_details').val());
                if($('#misc-details-modal #misc_details').val().length > 0){
                    template.find('.note-box input[type="text"]').val($('#misc-details-modal #misc_details').val());
                }

                if($('#misc-details-modal #order-for-select').val() === 'job'){
                    template.find('.job-for-id').val($('#misc-details-modal .job-select').val());
                    template.find('.job-for-text').val($('#misc-details-modal .job-select').text());
                    template.find('.for-job').removeClass('hidden');
                }

                if($('#misc-details-modal #order-for-select').val() === 'counter'){
                    template.find('.counter-for-id').val($('#misc-details-modal .counter-select').val());
                    template.find('.counter-for-text').val($('#misc-details-modal .counter-select').text());
                    template.find('.for-counter').removeClass('hidden');
                }

                var tempID = Date.now().toString();
                //Remove the hidden class
                template.removeClass('hidden');
                //set ID to the time so we don't accidentally reuse this ID
                template.attr('id', tempID);
                template.find('.row-id').val(tempID);
                $('#item-row').append(template);
                $('#misc-details-modal').find('.close').click();
                reindex_rows();
            });


            $('body').on('click', '.confirm-order-button', function (e) {
                e.preventDefault();
                var template = $('#item-template').clone();
                var locationid = $('#order-item-details-modal').find('.loc-select').val();
                var qty = 0;
                //Checks for a 0 quantity value
                qty = $('#order-item-details-modal').find('#qty').val();

                if($('.qty-box-existing').find('#qty').is(':visible')){
                    qty = $('.qty-box-existing').find('#qty').val();
                }

                if($('.qty-box-new').find('#qty').is(':visible')){
                    qty = $('.qty-box-new').find('#qty').val();
                }

                if (qty == 0) {
                    showError('Please specify a quantity.');
                    return true;
                }

                if($('#order-item-details-modal #supplier_lots_of').is(':visible')){
                    if ($('#order-item-details-modal #supplier_lots_of').val().length <= 0){
                        showError('Please fill in the ordered in lots of box.');
                        return true;
                    }
                }

                if($('#order-item-details-modal .new-supplier-unit-cat').is(':visible')){
                    if ($('#order-item-details-modal .new-supplier-unit-cat:visible').val().length <= 0){
                        showError('Please fill in the ordered as box.');
                        return true;
                    }
                }

                if($('#order-item-details-modal .new-supplier-unit').is(':visible')){
                    if ($('#order-item-details-modal .new-supplier-unit:visible').val().length <= 0){
                        showError('Please fill in the sizing unit box.');
                        return true;
                    }
                }

                var cost = 0;
                var real_cost = 0;
                if ($('#order-item-details-modal').find('#cost').val().length > 0) {
                    cost = parseFloat($('#order-item-details-modal').find('#cost').val()).toFixed(2);
                    real_cost = parseFloat($('#order-item-details-modal').find('#real-cost').val()).toFixed(2);
                } else {
                    showError('Please specify a cost.');
                    return true;
                }

                template.find('.item-id').val(lastItem[0].id);
                template.find('.item-quantity').val(qty);
                template.find('.item-cost').val(parseFloat(qty * cost).toFixed(2));
                template.find('.item-unit-cost').val(parseFloat(cost).toFixed(2));
                template.find('.item-real-cost').val(parseFloat(real_cost).toFixed(2));
                template.find('.item-description').val(lastItem[0].description);
                if (locationid == 0) {
                    template.find('.item-location').val('Location TBD');
                    template.find('.item-location-id').val('0');
                } else {
                    $.each(lastItem[0].stores, function (i, loc) {
                        if (loc.pivot.id == locationid) {
                            template.find('.item-location').val(loc.name + ' : ' + loc.pivot.room + loc.pivot.rack + loc.pivot.shelf + loc.pivot.bay);
                            template.find('.item-location-id').val(loc.pivot.id);
                        }
                    });
                }
                template.find('.item-barcode').val(lastItem[0].barcode);

                if ($('#order-item-details-modal .custom-po').val().length > 0) {
                    template.find('.custom-po input').val($('#order-item-details-modal .custom-po').val());
                    template.find('.custom-po').removeClass('hidden');
                } else {
                    template.find('.custom-po').remove();
                }

                if ($('#order-item-details-modal #note').val().length > 0) {
                    template.find('.item-note').val($('#order-item-details-modal #note').val());
                    template.find('.item-note').parent().removeClass('hidden');
                } else {
                    template.find('.item-note').parent().remove();
                }

                var tempID = Date.now().toString();
                if ($('#order-item-details-modal .size-unit').is(":visible")) {
                    var visible_count = 0;
                    $.each($('#order-item-details-modal .size-unit'), function () {
                        if ($(this).is(':visible')) {
                            visible_count++;
                        }
                    });

                    for (var i = template.find('.size-template').length; i < visible_count; i++) {
                        var size_box = template.find('.size-template').eq(0).clone();
                        template.find('.size-stuff div').eq(0).append(size_box);
                    }
                    var i = 0;
                    $.each($('#order-item-details-modal .size-unit'), function () {
                        if ($(this).is(':visible')) {
                            console.log($(this).val());
                            template.find('.size-box').eq(i).val($(this).val());
                            i++
                        }
                        ;
                    });
                    template.find('.size-stuff').removeClass('hidden');
                } else {
                    template.find('.size-stuff').remove();
                }

                var selector = '.existing-supplier-row';
                if($('.new-supplier-row').is(':visible')){
                    selector = '.new-supplier-row';
                }
                template.find('.ordered-as-unit-id').val($('#order-item-details-modal ' + selector + ' .new-supplier-unit option:selected').val());
                template.find('.ordered-as-unit').val($('#order-item-details-modal ' + selector + ' .new-supplier-unit option:selected').text());

                //new-supplier-stuff
                if ($('#order-item-details-modal .new-supplier-unit-cat').is(":visible")) {
                    template.find('.new-supplier-stuff .ordered-as-cat-id').val($('#order-item-details-modal .new-supplier-unit-cat option:selected').val());
                    template.find('.new-supplier-stuff .ordered-as-cat').val($('#order-item-details-modal .new-supplier-unit-cat option:selected').text());
                    template.find('.new-supplier-stuff .lots-of').val($('#order-item-details-modal #supplier_lots_of').val());
                    template.find('.new-supplier-stuff').removeClass('hidden');
                }

                if($('#order-item-details-modal #order-for-select').val() === 'job'){
                    console.log('Kicked in.');
                    template.find('.job-for-id').val($('#order-item-details-modal .job-select').val());
                    template.find('.job-for-text').val($('#order-item-details-modal .job-select').text());
                    template.find('.for-job').removeClass('hidden');
                }

                if($('#order-item-details-modal #order-for-select').val() === 'counter'){
                    template.find('.counter-for-id').val($('#order-item-details-modal .counter-select').val());
                    template.find('.counter-for-text').val($('#order-item-details-modal .counter-select').text());
                    template.find('.for-counter').removeClass('hidden');
                }
                //Remove the hidden class
                template.removeClass('hidden');
                //set ID to thew time so we don't accidently reuse this ID
                template.attr('id', tempID);
                $('#item-row').append(template);
                template.find('.row-id').val(tempID);
                $('#order-item-details-modal').find('.close').click();
                reindex_rows();
            })

            $('#search-item').click(function (e) {
                OpenInNewTab("{!! url('/items?order_issue') !!}");
            })

            $('body').on('click', '.remove-row', function (e) {
                $(this).parent().parent().parent().remove();
                reindex_rows();
            });

            function OpenInNewTab(url) {
                var win = window.open(url, '_blank');
                win.focus();
            }

            TabTalk.setCallback(function (data) {
                if (data.message.event == 'order-a-item') {
                    $('#barcode-search').val(data.message.barcode);
                    lookupItem(true);
                }
            });

            function clearModals(){
                $('.new-supplier-unit-cat option').eq(0).prop('selected', true);
                $('.new-supplier-unit option').eq(0).prop('selected', true);
                $('.bs-details-modal-sm').find('input').val('');
                HideQty(false, false);
                HideQty(false, true);
                HideSizes(true);
                HideSizes(false);
            }

            function openModal(data) {
                console.log(data);
                clearModals();
                var order_store = "{!! $order->store_id !!}";
                $('.bs-details-modal-sm').find('.loc-select').html('');
                $('.bs-details-modal-sm').find('.loc-select').append('<option value="0">Location TBD</option>')

                //This adds locations that are applicable to the store the order is set to.
                $.each(data[0].stores, function (i, store) {
                    if (store.id == order_store) {
                        $('.bs-details-modal-sm').find('.loc-select').append('<option value="' + store.pivot.id + '" data-qty="' + store.pivot.qty + '">' + store.code + ' : ' + store.pivot.room + store.pivot.rack + store.pivot.shelf + store.pivot.bay + ' : Qty: ' + store.pivot.qty + '</option>')
                    }
                });

                var supplier_new = true;
                var supplier_order = $('#order_supplier_id').val();
                $('#sizing').html('');
                $.each(data[0].suppliers, function (i, supplier) {
                    if (parseInt(supplier.id) === parseInt(supplier_order)) {
                        supplier_new = false;
                        doUnits(0, true);
                        return false;
                    }
                });

                if (!supplier_new) {
                    if ($('.existing-supplier-row').hasClass('hidden')) {
                        $('.existing-supplier-row').removeClass('hidden');
                        $('.new-supplier-row').addClass('hidden');
                    }
                } else {
                    if ($('.new-supplier-row').hasClass('hidden')) {
                        $('.new-supplier-row').removeClass('hidden');
                        $('.existing-supplier-row').addClass('hidden');
                    }
                }

                //If the supplier isn't fount need to show all the boxes to allow this to be added, and then the sizes used here will be used later on when adding this as a supplier for that item.
                var real_cost = 0;
                try {
                    //should get the
                    $.each(data[0].last_price, function (i, price) {
                        if (price.supplier_id === supplier_order) {
                            real_cost = price.cost;
                            return false;
                        }
                    });
                } catch (exp) {
                    console.log('No cost price known');
                }

                var existing = true;
                if (!supplier_new) {
                    $('.bs-details-modal-sm').find('#cost').val(real_cost);
                } else {
                    $('.bs-details-modal-sm').find('#cost').val('0');
                    existing = false;
                }

                //TODO will need to check what the reporting unit is so that when a a new supplier is added you cant order things in weight or legnth when you sell them as a qty.

                if (data[0].unit_category != null) {
                    $('.bs-details-modal-sm #unit-report').val(data[0].unit_id);
                    $('.bs-details-modal-sm .new-supplier-unit').val(data[0].unit_id);
                    $('.bs-details-modal-sm .new-supplier-unit-cat').val(data[0].unit_category_id);
                    if (data[0].unit_category.id != '6') {
                        HideQty(true, true);
                        //6 Meaning that its individual in which case qty box is used.
                        var sizes = $('.modal-content .size-template').clone();
                        var count = sizes.find('.size-box').length;
                        var size_row = sizes.find('.size-box').first().clone();
                        for (var i = count; i < data[0].unit_category.num_sizes; i++) {
                            sizes.find('.size-box').after(size_row);
                        }
                        $('#sizing').append(sizes);
                        //3 is squared which means these will be legnths which is 2 ... shit I know but hey ho...
                        if (data[0].unit_category.id == 3) {
                            doUnits(2);
                            //We need to know the total number of sheets and there measurements to work out a quantity figure and allow us to check that X number of sheets arrived
                            HideQty(false, true);
                        } else {
                            doUnits(data[0].unit_category.id);
                        }
                    } else {
                        //Its a size meaning we need to hide quantity
                        HideQty(false);
                    }
                } else {
                    var sizes = $('.modal-content .size-template').clone();
                    $('#sizing').append(sizes);
                    $('#sizing').append('<p class="label-warning">The units this item is sold in has not been set.</p>');

                    if (!supplier_new) {
                        doUnits(0, true)
                    } else {
                        doUnits(0, false)
                    }
                }
                $('.element-select').each(function () {
                    $(this).select2("val", "");
                })
                reindex_rows();
            }

            function lookupItem(normal) {
                var barcode = '';
                if(normal){
                    barcode = $('#barcode-search').val().toUpperCase();
                } else {
                    barcode = 'MS'+$('#barcode-search').val();
                }
                $.ajax({
                    url: "{!! url('/lookup/barcode') !!}/"+barcode,
                    type: 'GET',
                    dataType: 'Json',
                    success: function (data) {
                        if (data.success == true) {
                            lastItem = data.data;
                            openModal(data.data);
                            $('#order-item-details-modal').modal('show');
                        } else {
                            lastItem = {};
                            showError("No item with that barcode found.");
                            $('#barcode-search').val('');
                        }
                    }
                });
            }

            $('body').on('click', '.confirm-pin-button', function (e) {
                e.preventDefault();
                disableRows(false);
                submitForm($('#issue-stock-form'));
            });

            $('body').on('change', '.new-supplier-unit-cat', function (e) {
                sizeBoxes($(this), false);
                unitOptions($(this));
            });

            function unitOptions(select){
                var cat = $(select).find('option:selected').val();
                if(cat == '3'){
                    cat = '2';
                }
                if(cat == '7'){
                    return false;
                }
                $('.new-supplier-unit option').removeClass('hidden');
                $.each($('.new-supplier-unit option'), function(e){
                    if(parseInt($(this).attr('data-cat')) != parseInt(cat)){
                        $(this).addClass('hidden');
                    }
                });
            }

            function sizeBoxes(select, existing) {
                if($(select).find('option:selected').attr('size-num') > 0){
                    var sizes = $('#order-item-details-modal .size-template').clone();
                    var selector = '';
                    if(existing) {
                        selector = '#sizing';
                        HideQty(true, false);
                    } else {
                        selector = '#sizing-new';
                        HideQty(true, false);
                    }
                    $('#order-item-details-modal '+selector).html(sizes.html());
                    for(var i = 1; i < parseInt($(select).find('option:selected').attr('size-num')); i++){
                        $('#order-item-details-modal '+ selector + ' .size-row').eq(0).clone().insertAfter($('#order-item-details-modal '+selector).find('.size-row'));
                    }
                    if(!existing) {
                        $('#order-item-details-modal '+ selector + ' .new-supplier-unit').remove();
                    }
                } else {
                    if(existing) {
                        HideQty(false, true);
                        HideSizes(true);
                    } else {
                        HideQty(false, false);
                        HideSizes(false);
                    }
                    console.log('Remove sizes show qty.');
                }
            }

            //ID is the category of units
            //Unit_id is the unit that its set to report as for that category.
            function doUnits(id, new_supplier) {
                var selector = '.existing-supplier-row .new-supplier-unit';
                if(new_supplier){
                    selector = '.new-supplier-row .new-supplier-unit';
                }

                var cat = id;
                if(cat == '3'){
                    cat = '2';
                }
                if(cat == '7'){
                    //show all.
                    $(selector + ' option').removeClass('hidden');
                    return false;
                }
                $(selector + ' option').removeClass('hidden');
                $.each($(selector + ' option'), function(e){
                    if(parseInt($(this).attr('data-cat')) != parseInt(cat) && parseInt($(this).attr('data-cat')) != 999){
                        $(this).addClass('hidden');
                    }
                });
                $(selector + ' option').eq(0).prop('selected', true);
            }

            function submitForm(element) {
                $.ajax({
                    url: "{!! url('order/process') !!}",
                    type: 'POST',
                    dataType: 'Json',
                    data: $(element).serialize(),
                    success: function (data) {
                        if (data.success == true) {
                            new PNotify({
                                title: "Success",
                                text: data.text,
                                type: 'success',
                                addclass: "alert alert-success",
                            });
                            setTimeout(function (e) {
                                location.reload(false);
                            }, 3000);
                        } else {
                            disableRows(true);
                            new PNotify({
                                title: "Problems",
                                text: data.text,
                                type: 'error',
                                addclass: "alert alert-warning",
                            });
                            highlightRows(data);
                            //TODO: the above function seems to be missing WTF?!
                        }
                    },
                    error: function(data, status, error){
                            disableRows(true);
                            new PNotify({
                                title: "Problems",
                                text: data.responseJSON.text,
                                type: 'error',
                                addclass: "alert alert-warning",
                            });
                            highlightRows(data);
                    }
                });
            }

            function reindex_rows() {
                console.log('Re-indexing rows');
                var x = 0;
                $.each($('#item-row > div'), function () {
                    $.each($(this).find("[name*='item[]']"), function () {
                        $(this).attr('name', $(this).attr('name').replace('item[]', 'item[' + x + ']'));
                    });
                    x++;
                    var s = 0;
                    $.each($(this).find('.size-template > input'), function () {
                        $(this).attr('name', $(this).attr('name').replace('[size][]', '[size][' + s + ']'));
                        s++;
                    })
                })
            }

            function showError(msg) {
                var myStack = {
                    "dir1": "down",
                    "dir2": "right",
                    "push": "top"
                };
                new PNotify({
                    title: "Problem",
                    text: msg,
                    type: "error",
                    addclass: "alert alert-warning",
                    stack: myStack
                });
            }

            function disableRows(toggle) {
                if (toggle == false) {
                    $('#item-row [disabled=disabled]').removeAttr('disabled');
                } else {
                    $('#item-row input,#item-row select').attr('disabled', true);
                }
            }

            function HideQty(boool, existing) {
                var selector = '';
                if(existing){
                    selector = '.qty-box-existing';
                } else {
                    selector = '.qty-box-new';
                }
                if (boool == false) {
                    $('.bs-details-modal-sm').find(selector).show();
                    $('.bs-details-modal-sm').find('.qty-info').hide();
                } else {
                    $('.bs-details-modal-sm').find(selector).hide();
                    $('.bs-details-modal-sm').find('.qty-info').show();
                }
            }

            function HideSizes(existing) {
                var selector = '';
                if(existing){
                    selector = '#order-item-details-modal #sizing';
                } else {
                    selector = '#order-item-details-modal #sizing-new';
                }
                $(selector).html('');
                $('.bs-details-modal-sm').find('.qty-info').hide();
            }

            function highlightRows(data){
                console.log('Highlighting Problem rows and removing the ones that correctly added.');
                console.log(data);
            }

        });

        (function (window, $) {

        })(window, jQuery);
    </script>
@endpush