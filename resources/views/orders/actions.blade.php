<div class="btn-group">
    <a href="#" class="btn btn-default btn-xs">
        <i class="glyphicon glyphicon-edit"></i>
    </a>

    <a href="#" class="btn btn-danger btn-xs">
        <i class="glyphicon glyphicon-remove"></i>
    </a>
</div>