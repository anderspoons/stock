@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Order
        </h1>
    </section>
    <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               {!! Form::model($order, ['route' => ['orders.update', $order->id], 'method' => 'patch']) !!}
               <div class="row">

                    @include('orders.fields')

               </div>
               <div class="row">
                   <!-- Submit Field -->
                   <div class="form-group col-sm-12">
                       {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                       <a href="{!! route('orders.index') !!}" class="btn btn-default">Cancel</a>
                       <a href="{!! url('orders/issue/') !!}/{{ $order->id }}" class="btn btn-primary">Issue Items</a>
                   </div>
               </div>
               {!! Form::close() !!}
           </div>
       </div>
    </div>
@endsection