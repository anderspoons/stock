<!-- Supplier Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('supplier_id', 'Supplier:') !!}
    @include('suppliers.select')
</div>

<!-- Store Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('store_id', 'Store (Delivery to):') !!}
    @include('stores.select')
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Po Field -->
<div class="form-group col-sm-6">
    {!! Form::label('po', 'Order Number:') !!}
    {!! Form::text('po', null, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
</div>

<!-- Po Field -->
<div class="form-group col-sm-6">
    {!! Form::label('custom_po', 'Custom Order Number:') !!}
    {!! Form::text('custom_po', null, ['class' => 'form-control']) !!}
</div>

<!-- Statuses Field -->
@include('shared.status')

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('job_id', 'Yard Job:') !!}
    @include('orders.includes.yard-select')
</div>


<!-- Expected Field -->
<div class="form-group col-sm-6">
    {!! Form::label('expected_at', 'Date Expected:') !!}
    {{ Form::date('expected_at', null, array('class' => 'form-control')) }}
</div>

