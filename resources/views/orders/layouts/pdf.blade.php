<html>
@include('orders.layouts.pdf-style')
<body>

<h1 style="font-size: 12px; width: 100%; text-align: center">
    MACDUFF SHIPYARDS LTD <br/>
    THE HARBOUR<br/>
    MACDUFF<br/>
    ABERDEENSHIRE<br/>
    AB44 1QT<br/>
    Tel: 01261 831765
</h1>

<table class="stock-table" style="width:100%">
    <thead>
    <tr>
        <th colspan="9">
            <h1>PURCHASE ORDER NUMBER: {{ $order->po }} </h1>
        </th>
    </tr>
    <tr>
        <th>PART CODE</th>
        <th>PART NUMBER</th>
        <th>DESCRIPTION</th>
        <th>TYPE</th>
        <th>GRADE</th>
        <th>MATERIRAL</th>
        <th>SIZE / SEX</th>
        <th>QUANTITY</th>
        <th>UNIT</th>
    </tr>
    </thead>

    <tbody>
    @foreach($order->items as $item)
        <tr>
            <td>
                @foreach($item->item->suppliers as $supplier)
                    @if($supplier->id == $order->supplier_id)
                        {{ $supplier->pivot->code }}
                    @endif
                @endforeach
            </td>

            <td>{{ $item->item->partnumber }}</td>
            <td>{{ $item->item->description }}</td>

            <td>
                @foreach($item->item->types as $type)
                    {{ $type->name }}
                    <br/>
                @endforeach
            </td>

            <td>
                @foreach($item->item->grades as $grade)
                    {{ $grade->name }}
                    <br/>
                @endforeach
            </td>

            <td>
                @foreach($item->item->materials as $material)
                    {{ $material->name }}
                    <br/>
                @endforeach
            </td>

            <td>
                @foreach($item->item->sizes as $size)
                        {!! $size->name !!}
                        @if(isset($size->id))
                            @if(count($item->item->sexes) > 0)
                                @foreach($item->item->sexes as $sex)
                                    @if($sex->id == $size->pivot->sex_id)
                                        {{ $sex->name }}
                                    @endif
                                @endforeach
                            @endif
                        @endif
                    <br/>
                @endforeach
            </td>

            <td>
                @if(isset($item->sizes))
                    @foreach($item->sizes as $size)
                        {{ $size->size }} {{ $size->unit->short }}
                    @endforeach
                    <br/>
                @endif
                Qty: {{ $item->qty }}
            </td>
            {{-- Units shit --}}
            <td>
                @if(!empty($item->new_supplier))
                    {{ $item->new_supplier->unit->short }}
                @else
                    @foreach($item->item->supplier_units as $unit)
                        @if($unit->pivot->supplier_id == $order->supplier_id)
                            {{ $unit->short }}
                        @endif
                    @endforeach
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
</body>
</html>