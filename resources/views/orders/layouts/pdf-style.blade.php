<style>
    table.stock-table {
        border-width: 1px;
        border-spacing: 2px;
        border-style: outset;
        border-color: gray;
        border-collapse: collapse;
        background-color: white;
    }
    table.stock-table th {
        border-width: 1px;
        padding: 2px;
        border-style: inset;
        border-color: gray;
    }
    table.stock-table td {
        border-width: 1px;
        padding: 2px;
        border-style: inset;
        border-color: gray;
    }

    * {
        font-size: 12px;
        font-family: tahoma, verdana, arial, sans-serif;
    }
</style>