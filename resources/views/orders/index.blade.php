@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Orders</h1>
        <h1 class="pull-right">
           <a class="btn btn-primary pull-right" style="margin-top: -10px; margin-bottom: 5px" href="{!! route('orders.create') !!}">Add New</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3 col-lg-2 no-left-gutter">
                    <div class="box box-primary">
                        <div class="box-body">
                            @include('orders.filters')
                        </div>
                    </div>
                </div>

                <div class="col-md-9 col-lg-10 no-right-gutter">
                    <div class="box box-primary">
                        <div class="box-body">
                            @include('orders.table')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @include('orders.includes.email')

@endsection

@push('footer-script')
<script type="text/javascript">
    $( document ).ready(function() {
        $('body').on('click', '.email-order', function(e){
            var id = $(this).attr('order-id');
            var email = $(this).attr('order-email');
            $('.email-invoice-form .id-box').val(id);
            $('.email-invoice-form .email-box').val(email);
        });

        function showError(msg) {
            var myStack = {
                "dir1": "down",
                "dir2": "right",
                "push": "top"
            };
            new PNotify({
                title: "Problem",
                text: msg,
                type: "error",
                addclass: "alert alert-warning",
                stack: myStack
            });
        }

        function showSuccess(msg) {
            var myStack = {
                "dir1": "down",
                "dir2": "right",
                "push": "top"
            };
            new PNotify({
                title: "Success",
                text: msg,
                type: "success",
                addclass: "alert alert-success",
                stack: myStack
            });
        }

        $('body').on('click', '.close-order', function(e){
            var id = $(this).attr('order-id');
            $.ajax({
                url: "{!! url('/orders/close') !!}/" + id,
                type: 'GET',
                dataType: 'Json',
                success: function (data) {
                    if (data.success == true) {
                        showSuccess('Closed Order.')
                        $('.buttons-reload').click();
                    } else {
                        showError('Error closing order.')
                        $('.buttons-reload').click();
                    }
                }
            });
        });
    });
</script>
@endpush