<div class="modal fade bs-misc-modal-sm" id="misc-details-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Details</h4>
            </div>

            <div class="modal-body">
                <div class="row ">
                    <div class="form-group col-sm-12">
                        {!! Form::label('name', 'Name:') !!}
                        {!! Form::text('name', null, array('class' => 'form-control', 'id' => 'misc_name')) !!}
                    </div>

                    <div class="form-group col-sm-12">
                        {!! Form::label('details', 'Details:') !!}
                        {!! Form::textarea('details', null, array('class' => 'form-control', 'id' => 'misc_details')) !!}
                    </div>

                    <div class="form-group col-md-6 col-sm-12">
                        {!! Form::label('misc-cost', 'Cost: (Excluding VAT each)') !!}
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-gbp"></i>
                            </span>
                            {!! Form::number('cost', '', array('class' => 'form-control', 'id' => 'cost')) !!}
                        </div>
                    </div>

                    <div class="form-group col-md-6 col-sm-12 qty-box">
                        {!! Form::label('qty', 'Quantity:') !!}
                        {!! Form::number('qty', '', array('class' => 'form-control')) !!}
                    </div>
                </div>

                <hr>

                <div class="row">
                    <div class="form-group col-md-6 col-sm-12">
                        {!! Form::label('bc', 'Ordered For:') !!}
                        <select id="order-for-select" class="form-control">
                            <option value="stock">Stock</option>
                            <option value="job">Job</option>
                            <option value="counter">Counter Sale</option>
                        </select>
                    </div>

                    <div class="form-group col-md-6 col-sm-12">
                        {!! Form::label('custom_po', 'Custom PO: (optional)') !!}
                        {!! Form::text('custom_po', '', array('class' => 'custom-po form-control')) !!}
                    </div>

                    <div class="hidden counter for-group">
                        <div class="form-group col-md-6 col-sm-12">
                            {!! Form::label('counter', 'Counter Sale:') !!}
                            @include('counters.select')
                        </div>
                    </div>

                    <div class="hidden job for-group">
                        <div class="form-group col-md-6 col-sm-12">
                            {!! Form::label('job', 'Job:') !!}
                            @include('jobs.select')
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger confirm-misc-button">Confirm</button>
            </div>
        </div>
    </div>
</div>
