<div class="modal fade bs-delete-modal-sm" id="delete-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirm Deletion</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['url' => url('order/item/remove'), 'method' => 'POST', 'class' => 'delete-form', 'id' => 'delete-form']) }}
                {!! Form::hidden('id', '', array('class' => 'order-item-id')) !!}
                <div class="row">
                    <div class="col-sm-12">
                       <p>Warning you are trying to delete this item from this order, to do this please select your name and enter your security pin below.</p>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-12 col-md-6 form-group">
                    {!! Form::label('pin_id', 'Name:') !!}
                        @if(isset($pincodes))
                            <select class="form-control" name="pin_id">
                                @foreach($pincodes as $user)
                                    <option value="{!! $user->id !!}">{!! $user->name !!}</option>
                                @endforeach
                            </select>
                        @endif
                    </div>
                    <div class="col-sm-12 col-md-6 form-group">
                        {!! Form::label('pin', 'Security Pin:') !!}
                        {!! Form::password('pin', array('class' => 'form-control')) !!}
                        <span class="code-error hidden">Wrong code entered.</span>
                    </div>
                </div>

                {{ Form::close() }}
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger delete-item">Confirm Deletion</button>
            </div>
        </div>
    </div>
</div>

@push('footer-script')
    <script type="text/javascript">

        $('body').on('click', '.quick-delete-action', function(){
            var id = $(this).attr('line-id');
            $('#delete-modal .order-item-id').val(id);
        });

        $('body').on('click', '.delete-item', function(e){
            e.preventDefault();
            var form = $('.delete-form');
            $(form).submit();
        });

        $('body').on('submit', '.delete-form', function(e){
            e.preventDefault();
            $('#pin').css({'border' : 'none'});
            if($('.code-error').is('visible')) {
                $('.code-error').addClass('hidden');
            }
            var form = $(this);
            $.ajax({
                type:"POST",
                url:form.attr("action"),
                data:form.serialize(),
                success: function(response){
                    form.parent().parent().find('button[data-dismiss="modal"]').click();
                    var myStack = {"dir1":"down", "dir2":"right", "push":"top"};
                    new PNotify({
                        title: "Success",
                        text: response.data,
                        addclass: "stack-custom",
                        stack: myStack
                    })
                    setTimeout(function(e){
                        @if(isset($order))
                            window.location = "{{ url('/orders/items/'.$order->id) }}"
                        @else
                            window.location = "{{ url('/orders/items/'.$id) }}"
                        @endif
                    }, 1000);
                }
            });
        });

        $( document ).ajaxError(function(event, jqxhr, settings, thrownError ) {
            var object = JSON.parse(jqxhr.responseText);
            console.log(object);
            $('#pin').css({'border' : 'thin solid #ff3333'});
            if(!$('.code-error').is('visible')){
                $('.code-error').removeClass('hidden');
            }
        });

    </script>
@endpush