<div class="modal fade bs-confirm-issue-modal-sm" id="confirm-issue-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirm Issue</h4>
            </div>
            <div class="modal-body">
                <div class="row ">
                    <div class="form-group">
                        <div class="col-sm-12 col-md-6 form-group">
                            {!! Form::label('pin_id', 'Name:') !!}
                            @if(isset($pins))
                                <select class="form-control" id="pin_id" name="pin_id">
                                    @foreach($pins as $user)
                                        <option value="{!! $user->id !!}">{!! $user->name !!}</option>
                                    @endforeach
                                </select>
                            @endif
                        </div>
                        <div class="col-sm-12 col-md-6 form-group">
                            {!! Form::label('code', 'Security Pin:') !!}
                            {!! Form::password('code', array('id' => 'pin_code', 'class' => 'form-control')) !!}
                            <span class="code-error hidden">Wrong code entered.</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Confirm', ['class' => 'btn btn-danger confirm-pin-button']) !!}
            </div>
        </div>
    </div>
</div>
