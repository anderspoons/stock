<div class="modal fade bs-carriage-modal-sm" id="carriage-details-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Carriage</h4>
            </div>
            <div class="modal-body">
                <div class="row ">
                    <div class="form-group">
                        <div class="col-sm-12 col-md-6 form-group">
                            {{ Form::label('name', 'Carriage Name:') }}
                            {{ Form::hidden('counter_id', '', array('id' => 'the-id')) }}
                            {{ Form::text('name', '', array('class' => 'form-control carriage-name')) }}
                        </div>
                        <div class="col-sm-12 col-md-6 form-group">
                            {{ Form::label('cost', 'Carriage Cost (Excluding VAT, If Known):') }}
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-gbp"></i>
                                </span>
                                {{ Form::number('cost', '', array('id' => 'cost', 'class' => 'form-control')) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger confirm-carriage-button">Confirm</button>
            </div>
        </div>
    </div>
</div>

@push('footer-script')
<script type="text/javascript">
    jQuery( document ).ready(function($){
        $('body').on('click', '#carriage-details-modal', function(){
            $('#carriage-modal #the-id').val($(this).attr('the-id'));
        });
    });
</script>
@endpush

