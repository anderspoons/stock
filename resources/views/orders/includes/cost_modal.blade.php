<div class="modal fade bs-set-cost-modal-sm" id="set-cost-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Unit Cost</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['url' => 'order/cost', 'method' => 'post']) }}
                {{ Form::hidden('line-id', '', array('class' => 'line-id')) }}
                <div class="row ">
                    <div class="col-sm-12 form-group">
                        <div class="form-group">
                            <div class="unit-cost">
                                {{ Form::label('line_cost', 'Unit Cost (Excluding VAT):') }}
                                <div class="input-group unit-cost">
                                    <span class="input-group-addon">
                                        <i class="fa fa-gbp"></i>
                                    </span>
                                    {{ Form::number('line_unit_cost', '', array('class' => 'form-control line-cost', 'min' => '0')) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Upadte', ['class' => 'btn btn-info item-cost-button']) !!}
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

@push('footer-script')
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('body').on('click', '.quick-cost', function(e) {
            $('#set-cost-modal .line-id').val($(this).attr('line-id'));
            $('#set-cost-modal .line-cost').val($(this).attr('line-cost'));
        });

        //Submit the form by ajax
        $('body').on('click', '.item-cost-button', function (e) {
            e.preventDefault();
            var form = $('#set-cost-modal form');
            $.ajax({
                type: "POST",
                url: form.attr("action"),
                data: form.serialize(),
                success: function (response) {
                    $('#set-cost-modal .close').click();
                    showSuccess(response.text);
                    $('.buttons-reload').click();
                },
                error: function (response) {
                    showError(response.text);
                }
            });
        });
    });
</script>
@endpush
