<select name='yard_id' class="yard-select form-control">
</select>

@push('footer-script')
    <script type="text/javascript">
        (function(window, $) {
            jQuery( document ).ready(function( $ ) {
                $(".yard-select").select2({
                    ajax: {
                        url: "{!! url('jobs/yard_json') !!}",
                        dataType: "json",
                        data: function (term) {
                            return {query: term.term }
                        },
                        results: function (data) {
                            return {
                                results: data.results
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: $.map(data.results, function (item) {
                                    console.log(item);
                                    return {
                                        text: item.text,
                                        id: item.id,
                                        cust_id: item.customer_id,
                                        cust_name: item.customer_name,
                                    }
                                })
                            };
                        }
                    },
                    createSearchChoice: function (term, data) {
                        if ($(data.results).filter(function () {
                                return this.text.localeCompare(term) === 0;
                            }).length === 0) {
                            return {
                                id: term,
                                text: term
                            };
                        }
                    },
                    width: '100%',
                });
            });
        })(window, jQuery);
    </script>
@endpush