<div class="modal fade bs-ordered-modal-sm" id="ordered-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirm Order is On Order</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['url' => url('order/confirm'), 'method' => 'POST', 'class' => 'confirm-form']) }}
                {!! Form::hidden('id', null, array('class' => 'order-id')) !!}
                <div class="row">
                    <div class="col-sm-12">
                       <p>Warning you are about to confirm this order has been placed, to do this please select your name and enter your security pin below.</p>
                    </div>

                    <div class="col-sm-12 form-group">
                        {{ Form::label('expected_date', 'Expected Delivery Date') }}
                        {{ Form::date('expected_date', \Carbon\Carbon::now()->addDays(2), ['class' => 'form-control datetime']) }}
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 col-md-6 form-group">
                    {!! Form::label('pin_id', 'Name:') !!}
                        @if(isset($pincodes))
                            <select class="form-control" name="pin_id">
                                @foreach($pincodes as $user)
                                    <option value="{!! $user->id !!}">{!! $user->name !!}</option>
                                @endforeach
                            </select>
                        @endif
                    </div>
                    <div class="col-sm-12 col-md-6 form-group">
                        {!! Form::label('pin', 'Security Pin:') !!}
                        {!! Form::password('pin', array('class' => 'form-control')) !!}
                        <span class="code-error hidden">Wrong code entered.</span>
                    </div>
                </div>

                {{ Form::close() }}
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success order-confirm">Confirm</button>
            </div>
        </div>
    </div>
</div>

@push('footer-script')
    <script type="text/javascript">
        (function (window, $) {

        $('body').on('click', '.confirm-order-action', function(e){
            e.preventDefault();
            $('#ordered-modal .order-id').val($(this).attr('order-id'));
        });

        $('body').on('click', '.order-confirm', function(e){
            e.preventDefault();
            $('#pin').css({'border' : 'none'});
            if($('.code-error').is('visible')) {
                $('.code-error').addClass('hidden');
            }
            var form = $(this).parent().parent().find('.modal-body form');
            var element = $(this);
            $.ajax({
                type:"POST",
                url:form.attr("action"),
                data:form.serialize(),
                success: function(response){
                    element.parent().parent().find('button[data-dismiss="modal"]').click();
                    var myStack = {"dir1":"down", "dir2":"right", "push":"top"};
                    new PNotify({
                        title: "Success",
                        text: response.data,
                        addclass: "stack-custom",
                        stack: myStack
                    })
                    setTimeout(function(e){
                        window.location = "{{ url('/orders') }}";
                    }, 1000);
                }
            });
        });

        $( document ).ajaxError(function(event, jqxhr, settings, thrownError ) {
            var object = JSON.parse(jqxhr.responseText);
            console.log(object);
            $('#ordered-modal #pin').css({'border' : 'thin solid #ff3333'});
            if(!$('#ordered-modal .code-error').is('visible')){
                $('#ordered-modal .code-error').removeClass('hidden');
            }
        });

        })(window, jQuery);
    </script>
@endpush