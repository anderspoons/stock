<div class="modal fade bs-details-modal-sm" id="order-item-details-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Details</h4>
            </div>

            <div class="hidden">
                <div class="size-template">
                    {!! Form::label('size', 'Sizes / Weight:') !!}
                    <div class="size-row">
                        {!! Form::text('size', '', array('class' => 'size-unit form-control')) !!}
                    </div>
                    @include('orders.units.unit_select')
                    {{ Form::hidden('unit-report', '', array('id' => 'unit-report')) }}
                </div>
            </div>

            <div class="modal-body">
                {!! Form::hidden('qty', '', array('id' => 'qty')) !!}

                <div class="row ">
                    <div class="form-group col-md-6 col-sm-12">
                        {!! Form::label('location_id', 'Location:') !!}
                        @include('orders.includes.loc-select')
                    </div>

                    <div class="form-group col-md-6 col-sm-12">
                        {!! Form::label('misc-cost', 'Cost: (Excluding VAT each)') !!}
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-gbp"></i>
                            </span>
                            {!! Form::number('cost', '', array('class' => 'form-control', 'id' => 'cost', 'min' => '0')) !!}
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="form-group col-md-12">
                        {!! Form::label('note', 'Optional Note:') !!}
                        {!! Form::text('note', '', array('class' => 'form-control')) !!}
                    </div>
                </div>

                <hr>

                <div class="row existing-supplier-row">
                    <div class="form-group col-md-6 col-sm-12 qty-box-existing">
                        {!! Form::label('qty', 'Quantity:') !!}
                        {!! Form::number('qty', '', array('class' => 'form-control', 'min' => '0')) !!}
                    </div>

                    <div id="sizing" class="form-group col-md-6 col-sm-12">

                    </div>
                </div>

                <div class="row new-supplier-row hidden">
                    <div class="" id="ordered_as">
                        <div class="form-group col-sm-12 col-md-6">
                            {!! Form::label('supplier_unit_category', 'Ordered As:')  !!}
                            @include('orders.units.category_select')
                        </div>

                        <div class="form-group col-sm-12 col-md-6">
                            {!! Form::label('supplier_specific_unit]', 'Sizing Unit:')  !!}
                            @include('orders.units.unit_select')
                        </div>

                        <div class="form-group col-sm-12 col-md-6">
                            {!! Form::label('supplier_lots_of', 'Ordered in Lots of:')  !!}
                            {!! Form::number('supplier_lots_of', '', array('class' => 'form-control', 'id' => 'supplier_lots_of', 'min' => '0')) !!}
                        </div>
                    </div>

                    <div class="form-group col-md-6 col-sm-12 qty-box-new">
                        {!! Form::label('qty', 'Quantity:') !!}
                        {!! Form::number('qty', '', array('class' => 'form-control', 'id' => 'new-qty', 'min' => '0')) !!}
                    </div>

                    <div class="form-group col-md-6 col-sm-12" id="sizing-new">

                    </div>
                </div>

                <hr>

                <div class="row ">
                    <div class="form-group col-md-6 col-sm-12">
                        {!! Form::label('bc', 'Ordered For:') !!}
                        <select id="order-for-select" class="form-control">
                            <option value="stock">Stock</option>
                            <option value="job">Job</option>
                            <option value="counter">Counter Sale</option>
                        </select>
                    </div>

                    <div class="form-group col-md-6 col-sm-12">
                        {!! Form::label('custom_po', 'Custom PO: (optional)') !!}
                        {!! Form::text('custom_po', '', array('class' => 'custom-po form-control')) !!}
                    </div>

                    <div class="hidden counter for-group">
                        <div class="form-group col-md-6 col-sm-12">
                            {!! Form::label('counter', 'Counter Sale:') !!}
                            @include('counters.select')
                        </div>
                    </div>

                    <div class="hidden job for-group">
                        <div class="form-group col-md-6 col-sm-12">
                            {!! Form::label('job', 'Job:') !!}
                            @include('jobs.select')
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger confirm-order-button">Confirm</button>
            </div>

        </div>
    </div>
</div>
