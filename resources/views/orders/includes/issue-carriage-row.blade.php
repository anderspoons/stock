<div id="carriage-template" class="hidden">
    <div>
        <div class="row">
            {{-- Use the row id here so that if theres a problem with a particular row we can highlight this on the return for the ajax.--}}
            {!! Form::hidden('item[][row-id]', '', array('class' => 'row-id form-control')) !!}
            {!! Form::hidden('item[][id]', '1', array('class' => 'item-id form-control')) !!}
            <div class="col-sm-12 col-md-3 form-group">
                {!! Form::label('item[][name]', 'Carriage Name: ') !!}
                {!! Form::text('item[][name]', '', array('class' => 'form-control item-name', 'disabled' => 'disabled')) !!}
            </div>

            <div class="col-sm-12 col-md-3 form-group">
                {!! Form::label('item[][cost]', 'Carriage Cost (Excluding VAT): ') !!}
                {!! Form::text('item[][cost]', '', array('class' => 'form-control item-cost', 'disabled' => 'disabled')) !!}
            </div>

            <div class="clearfix"></div>
        </div>

        <div class="row">
            <div class="col-sm-12 col-md-3 form-group">
                <br>
                <div class="clearfix"></div>
                {!! Form::button('<i class="glyphicon glyphicon-remove"></i> Remove Item', ['class' => 'remove-row btn btn-default']) !!}
            </div>
            <hr class="col-sm-12"/>
            <div class="clearfix"></div>
        </div>
    </div>
</div>