<select name="order_id" id="order-select2" class="col-sm-12 form-control">
    @if(isset($orders))
        @foreach($orders as $order)
            <option selected="selected" supp-id="{{ $order->supplier->id }}" value="{!! $order->id !!}">{!! $order->po.' : '.$order->supplier->name !!}</option>
        @endforeach
    @endif
</select>

@push('footer-script')
<script type="text/javascript">
    jQuery( document ).ready(function( $ ) {
        $("#order-select2").select2({
            ajax: {
                url: "{!! url('order/json') !!}",
                dataType: "json",
                data: function(term) {
                    return {
                        q: term
                    };
                },
                results: function(data) {
                    return {
                        results: data.results
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.results, function (item) {
                            return {
                                text: item.text,
                                id: item.id,
                                supp_id: item.supp_id,
                            }
                        })
                    };
                }
            },
            createSearchChoice:function(term, data) {
                if ($(data.results).filter(function() {
                            return this.text.localeCompare(term) === 0;
                        }).length===0) {
                    return {
                        id:term,
                        text:term
                    };
                }
            },
            width: '100%'
        });
    });
</script>
@endpush