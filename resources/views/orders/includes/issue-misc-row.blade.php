<div id="misc-template" class="hidden">
    <div>
        <div class="row">
            {{-- Use the row id here so that if theres a problem with a particular row we can highlight this on the return for the ajax.--}}
            {!! Form::hidden('item[][row-id]', '', array('class' => 'row-id form-control')) !!}
            {!! Form::hidden('item[][id]', '', array('class' => 'item-id form-control')) !!}

            <div class="col-sm-12 col-md-3 form-group">
                {!! Form::label('item[][name]', 'Name: ') !!}
                {!! Form::text('item[][name]', '', array('class' => 'form-control item-name', 'disabled' => 'disabled')) !!}
            </div>

            <div class="col-sm-12 col-md-3 form-group">
                {!! Form::label('item[][item_description]', 'Details: ') !!}
                {!! Form::text('item[][item_description]', '', array('class' => 'form-control item-description', 'disabled' => 'disabled')) !!}
            </div>

            <div class="form-group col-md-12 note-box hidden">
                {!! Form::label('note', 'Optional Note:') !!}
                {!! Form::text('note', '', array('class' => 'form-control')) !!}
            </div>

            <div class="col-sm-12 col-md-3 form-group hidden">
                {!! Form::label('item[][custom_po]', 'Custom PO: ') !!}
                {!! Form::text('item[][custom_po]', '', array('class' => 'form-control item-custom-po', 'disabled' => 'disabled')) !!}
            </div>

            <div class="col-sm-12 col-md-3 form-group">
                {!! Form::label('item[][qty]', 'Quantity: ') !!}
                {!! Form::text('item[][qty]', '', array('class' => 'form-control item-quantity', 'disabled' => 'disabled')) !!}
            </div>

            <div class="col-sm-12 col-md-3 form-group">
                {!! Form::label('item[][cost]', 'Line Cost (Excluding VAT): ') !!}
                {!! Form::hidden('item[][unit_cost]', '', array('class' => 'item-unit-cost')) !!}
                {!! Form::hidden('item[][real_cost]', '', array('class' => 'item-real-cost')) !!}
                {!! Form::text('', '', array('class' => 'form-control item-cost-text', 'disabled' => 'disabled')) !!}
            </div>

            <div class="col-sm-12 col-md-3 form-group for-job hidden item-target">
                {!! Form::label('item[][job_id]', 'Ordered For Job: ') !!}
                {!! Form::hidden('item[][job_id]', '', array('class' => 'job-for-id')) !!}
                {!! Form::text('item[][job_text]', '', array('class' => 'form-control job-for-text', 'disabled' => 'disabled')) !!}
            </div>

            <div class="col-sm-12 col-md-3 form-group for-counter hidden item-target">
                {!! Form::label('item[][counter_id]', 'Ordered For Counter: ') !!}
                {!! Form::hidden('item[][counter_id]', '', array('class' => 'counter-for-id')) !!}
                {!! Form::text('item[][counter_text]', '', array('class' => 'form-control counter-for-text', 'disabled' => 'disabled')) !!}
            </div>

            <div class="clearfix"></div>
        </div>

        <div class="row">
            <div class="col-sm-12 col-md-3 form-group">
                <br>
                <div class="clearfix"></div>
                {!! Form::button('<i class="glyphicon glyphicon-remove"></i> Remove Item', ['class' => 'remove-row btn btn-default']) !!}
            </div>
            <hr class="col-sm-12"/>
            <div class="clearfix"></div>
        </div>
    </div>
</div>