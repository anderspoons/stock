<div class="modal bs-quick-order-modal-sm" id="order-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add to Order</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['url' => url('orders/add-item'), 'method' => 'post', 'class' => 'add-item-form']) }}
                {!! Form::hidden('item_id', '', array('class' => 'item-id')) !!}
                <div class="form-group">

                    <div class="col-sm-12 form-group">
                        {!! Form::label('order_id', 'Order:') !!}
                        @include('orders.includes.orders-select')
                    </div>

                    <div class="col-sm-12 col-md-6 form-group">
                        {!! Form::label('qty', 'Quantity:') !!}
                        {!! Form::text('qty', '',array('class' => 'form-control')) !!}
                    </div>

                </div>
                {{ Form::close() }}
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger delete-item">Confirm Deletion</button>
            </div>
        </div>
    </div>
</div>


@section('scripts')
    <script type="text/javascript">
        (function(window, $) {
            $('body').on('click', '.quick-order', function(e){
                console.log('Clicked add to order');
            });

            $('body').on('click', '.delete-item', function(e){
                e.preventDefault();
                $('#pin').css({'border' : 'none'});
                if($('.code-error').is('visible')) {
                    $('.code-error').addClass('hidden');
                }
                var form = $(this).parent().parent().find('.modal-body form');
                var element = $(this);
                $.ajax({
                    type:"POST",
                    url:form.attr("action"),
                    data:form.serialize(),
                    success: function(response){
                        element.parent().parent().find('button[data-dismiss="modal"]').click();
                        var myStack = {"dir1":"down", "dir2":"right", "push":"top"};
                        new PNotify({
                            title: "Success",
                            text: response.data,
                            addclass: "stack-custom",
                            stack: myStack
                        });
                        setTimeout(function(e){

                        }, 1000);
                    }
                });
            });
        })(window, jQuery);
    </script>
@endsection