<div class="modal fade bs-quick-qty-modal-sm" id="qty-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Adjust order amount.</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['url' => 'order/qty', 'method' => 'post', 'class' => 'quick-qty-form']) }}
                {!! Form::hidden('line_id', '', array('class' => 'line-id')) !!}
                <br>
                <div class="row">
                    <div class="col-sm-12">
                        {!! Form::label('qty', 'Quantity:') !!}
                        {!! Form::input('number', 'qty', null, array('class' => 'form-control qty-box', 'min' => '0')) !!}
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        {!! Form::label('pin_id', 'Name:') !!}
                        @if(isset($pins))
                            <select class="form-control" name="pin_id">
                                @foreach($pins as $user)
                                    <option value="{!! $user->id !!}">{!! $user->name !!}</option>
                                @endforeach
                            </select>
                        @endif
                    </div>
                    <div class="col-sm-12 col-md-6">
                        {!! Form::label('pin', 'Security Pin:') !!}
                        {!! Form::password('pin', array('class' => 'form-control')) !!}
                        <span class="code-error hidden">Wrong pin code entered.</span>
                    </div>
                    <div class="col-sm-12">
                        {{ Form::label('reason', 'Reason for Change:') }}
                        {{ Form::text('reason', '', array('class' => 'form-control')) }}
                    </div>
                </div>

                {{ Form::close() }}

                <div class="row qty-template hidden">
                    <div class="col-sm-2">
                        {!! Form::hidden('location[][id]', '', array('class' => 'loc-id')) !!}
                        {!! Form::text('name[]', '', array('disabled' => 'disabled', 'class' => 'loc-name')) !!}
                    </div>
                    <div class="col-sm-2">
                        {!! Form::text('room[]', '', array('disabled' => 'disabled', 'class' => 'loc-room')) !!}
                    </div>
                    <div class="col-sm-2">
                        {!! Form::text('rack[]', '', array('disabled' => 'disabled', 'class' => 'loc-rack')) !!}
                    </div>
                    <div class="col-sm-2">
                        {!! Form::text('shelf[]', '', array('disabled' => 'disabled', 'class' => 'loc-shelf')) !!}
                    </div>
                    <div class="col-sm-2">
                        {!! Form::text('bay[]', '', array('disabled' => 'disabled', 'class' => 'loc-bay')) !!}
                    </div>
                    <div class="col-sm-2">
                        {!! Form::text('location[][qty]', '', array('class' => 'loc-qty')) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary change-qty">Save changes</button>
            </div>
        </div>
    </div>
</div>

@push('footer-script')
    <script>
        $('body').on('click', '.quick-qty', function(){
            setID($(this).attr('line-id'));
        });

        $('body').on('click', '.change-qty', function(e){
            e.preventDefault();
            sendForm();
        });

        setID = function(line_id){
            $('#qty-modal .line-id').val(line_id);
        };

        sendForm = function(){
            $.ajax({
                url: "{{ url('order/qty') }}",
                data: $('#qty-modal form').serialize(),
                method: "POST",
                dataType: JSON
            }).done(function(data) {
                var response = JSON.parse(data.responseText);
                $('#qty-modal .close').click();
                showSuccess(response.text);
                $('.buttons-reload').click();
            }).error(function(data){
                var response = JSON.parse(data.responseText);
                showError(response.error);
            });
        };

    </script>
@endpush
