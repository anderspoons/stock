<div id="item-template" class="hidden">
    <div>
        <div class="row">
            {{-- Use the row id here so that if theres a problem with a particular row we can highlight this on the return for the ajax.--}}
            {!! Form::hidden('item[][row-id]', '', array('class' => 'row-id form-control')) !!}
            {!! Form::hidden('item[][id]', '', array('class' => 'item-id form-control')) !!}
            <div class="col-sm-12 col-md-3 form-group">
                {!! Form::label('item[][name]', 'Description: ') !!}
                {!! Form::text('item[][name]', '', array('class' => 'form-control item-description', 'disabled' => 'disabled')) !!}
            </div>

            <div class="col-sm-12 col-md-3 form-group">
                {!! Form::hidden('item[][location]', '', array('class' => 'item-location-id form-control')) !!}
                {!! Form::label('item[][location-desc]', 'Location: ') !!}
                {!! Form::text('item[][location-desc]', '', array('class' => 'form-control item-location', 'disabled' => 'disabled')) !!}
            </div>

            <div class="col-sm-12 col-md-3 form-group hidden">
                {!! Form::label('item[][item-note]', 'Note: ') !!}
                {!! Form::text('item[][item-note]', '', array('class' => 'form-control item-note', 'disabled' => 'disabled')) !!}
            </div>

            <div class="col-sm-12 col-md-3 form-group">
                {!! Form::label('item[][qty]', 'Quantity: ') !!}
                {!! Form::text('item[][qty]', '', array('class' => 'form-control item-quantity', 'disabled' => 'disabled')) !!}
            </div>

            <div class="col-sm-12 col-md-3 form-group">
                {!! Form::label('item[][cost]', 'Line Cost (Excluding VAT): ') !!}
                {!! Form::hidden('item[][unit_cost]', '', array('class' => 'item-unit-cost')) !!}
                {!! Form::text('item[][cost]', '', array('class' => 'form-control item-cost', 'disabled' => 'disabled')) !!}
            </div>

            <div class="col-sm-12 col-md-3 form-group">
                {!! Form::label('item[][barcode]', 'Barcode') !!}
                {!! Form::text('item[][barcode]', '', array('class' => 'form-control item-barcode', 'disabled' => 'disabled')) !!}
            </div>
            <div class="clearfix"></div>


        </div>

        <div class="row">
            <div class="size-stuff hidden">
                <div class="col-sm-12 col-md-3 form-group">
                    {!! Form::label('item[][size][]', 'Size: ') !!}
                    <div class="size-template">
                        {!! Form::text('item[][size][]', '', array('class' => 'form-control size-box', 'disabled' => 'disabled')) !!}
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-3 form-group">
                {!! Form::label('item[][unit]', 'Size Unit: ') !!}
                {!! Form::hidden('item[][ordered_as_unit]', '', array('class' => 'ordered-as-unit-id')) !!}
                {!! Form::text('item[][ordered_as_unit_text]', '', array('class' => 'form-control ordered-as-unit', 'disabled' => 'disabled')) !!}
            </div>

            <div class="new-supplier-stuff hidden">
                <div class="col-sm-12 col-md-3 form-group">
                    {!! Form::label('item[][ordered_as_cat]', 'Ordered As: ') !!}
                    {!! Form::hidden('item[][ordered_as_cat]', '', array('class' => 'ordered-as-cat-id')) !!}
                    {!! Form::text('item[][ordered_as_cat_text]', '', array('class' => 'form-control ordered-as-cat', 'disabled' => 'disabled')) !!}
                </div>

                <div class="col-sm-12 col-md-3 form-group">
                    {!! Form::label('item[][lots_of]', 'Ordered in Lots of: ') !!}
                    {!! Form::number('item[][lots_of]', '', array('class' => 'form-control lots-of', 'disabled' => 'disabled')) !!}
                </div>
            </div>

            <div class="col-sm-12 col-md-3 form-group custom-po hidden">
                {!! Form::label('item[][custom_po]', 'Custom PO: ') !!}
                {!! Form::text('item[][custom_po]', '', array('class' => 'form-control', 'disabled' => 'disabled')) !!}
            </div>

            <div class="col-sm-12 col-md-3 form-group for-job hidden item-target">
                {!! Form::label('item[][job_id]', 'Ordered For Job: ') !!}
                {!! Form::hidden('item[][job_id]', '', array('class' => 'job-for-id')) !!}
                {!! Form::text('item[][job_text]', '', array('class' => 'form-control job-for-text', 'disabled' => 'disabled')) !!}
            </div>

            <div class="col-sm-12 col-md-3 form-group for-counter hidden item-target">
                {!! Form::label('item[][counter_id]', 'Ordered For Counter: ') !!}
                {!! Form::hidden('item[][counter_id]', '', array('class' => 'counter-for-id')) !!}
                {!! Form::text('item[][counter_text]', '', array('class' => 'form-control counter-for-text', 'disabled' => 'disabled')) !!}
            </div>

            <div class="col-sm-12 col-md-3 form-group">
                <br>
                <div class="clearfix"></div>
                {!! Form::button('<i class="glyphicon glyphicon-remove"></i> Remove Item', ['class' => 'remove-row btn btn-default']) !!}
            </div>

            <hr class="col-sm-12"/>
            <div class="clearfix"></div>
        </div>
    </div>
</div>