@if(isset($suppliers))
    <div class="filter-set">
        <div class="header-bar">
            <div class="filter-search-box" style="padding:0 !important;">
                {!! Form::text('supplier', '', array('class' => 'search-filters', 'placeholder' => 'Supplier')) !!}
            </div>
            <button class="btn btn-xs pull-right filter-clear hidden"><i class="glyphicon glyphicon-remove"></i></button>
            <button class="btn btn-xs pull-right filter-expand"><i class="glyphicon glyphicon-plus"></i></button>
        </div>
        <div class="clearfix"></div>
        <ul>
            @foreach($suppliers as $i => $supplier)
                <li>
                    {!! Form::checkbox('supplier_filter', $supplier->id, false, ['id' => 'supplier_' . $i]) !!}
                    {!! Form::label('supplier_' . $i, $supplier->name) !!}
                </li>
            @endforeach
        </ul>
    </div>
@endif

@if(isset($statuses))
    <div class="filter-set">
        <div class="header-bar">
            <div class="filter-search-box" style="padding:0 !important;">
                {!! Form::text('status', '', array('class' => 'search-filters', 'placeholder' => 'Status')) !!}
            </div>
            <button class="btn btn-xs pull-right filter-clear hidden"><i class="glyphicon glyphicon-remove"></i></button>
            <button class="btn btn-xs pull-right filter-expand"><i class="glyphicon glyphicon-plus"></i></button>
        </div>
        <div class="clearfix"></div>
        <ul>
            @foreach($statuses as $i => $status)
                <li>
                    {!! Form::checkbox('status_filter', $status->id, false, ['id' => 'status_' . $i]) !!}
                    {!! Form::label('status_' . $i, $status->name) !!}
                </li>
            @endforeach
        </ul>
    </div>
@endif

@if(isset($stores))
    <div class="filter-set">
        <div class="header-bar">
            <div class="filter-search-box" style="padding:0 !important;">
                {!! Form::text('store', '', array('class' => 'search-filters', 'placeholder' => 'Store')) !!}
            </div>
            <button class="btn btn-xs pull-right filter-clear hidden"><i class="glyphicon glyphicon-remove"></i></button>
            <button class="btn btn-xs pull-right filter-expand"><i class="glyphicon glyphicon-plus"></i></button>
        </div>
        <div class="clearfix"></div>
        <ul>
            @foreach($stores as $store)
                <li>
                    {!! Form::checkbox('store_filter', $store->id, false, ['id' => 'store_' . $i]) !!}
                    {!! Form::label('store_' . $i, $store->name) !!}
                </li>
            @endforeach
        </ul>
    </div>
@endif

@if(isset($boats))
    @if(count($boats) > 0)
        <div class="filter-set">
            <div class="header-bar">
                <div class="filter-search-box" style="padding:0 !important;">
                    {!! Form::text('yard_id', '', array('class' => 'search-filters', 'placeholder' => 'Yard Orders')) !!}
                </div>
                <button class="btn btn-xs pull-right filter-clear hidden"><i class="glyphicon glyphicon-remove"></i></button>
                <button class="btn btn-xs pull-right filter-expand"><i class="glyphicon glyphicon-plus"></i></button>
            </div>
            <div class="clearfix"></div>
            <ul>
                @foreach($boats as $boat)
                    <li>
                        {!! Form::checkbox('yard_filter', $boat->id, false, ['id' => 'yard_' . $i]) !!}
                        {!! Form::label('yard_' . $i, $boat->name) !!}
                    </li>
                @endforeach
            </ul>
        </div>
    @endif
@endif

@if(isset($suppliers) || isset($boats) || isset($stores) || isset($statuses))
    <br/>
    <div class="clearfix"></div>
    {{ Form::checkbox('all_closed', null, false, array('class' => 'closed-filter', 'id' => 'all_closed')) }}
    {{ Form::label('all_closed', 'Only Closed Orders', array( 'style' => 'margin-bottom:10px;')) }}
    <br/>
    {{ Form::button('Filter', array('class' => 'col-md-12 do-filter')) }}
    {{ Form::button('Reset Filters', array('class' => 'col-md-12 buttons-reset', 'style' => 'margin-top:10px;')) }}
@endif
{{--
THE SCRIPT FOR THIS SHIT IS IN THE SCRIPTS VIEW
--}}

@push('footer-script')
<script type="text/javascript">
    $('body').on('click', '.filter-expand', function(e){
        var btn = $(this);
        expandFilters(btn, 'toggle');
    });

    function expandFilters(btn, open){
        if(open == 'toggle'){
            if(btn.children('i').hasClass('glyphicon-plus')){
                btn.parent().parent().find('ul').slideDown('fast', function(e){
                    btn.children('i').removeClass('glyphicon-plus');
                    btn.children('i').addClass('glyphicon-minus');
                });
            } else {
                btn.parent().parent().find('ul').slideUp('fast', function(e){
                    btn.children('i').removeClass('glyphicon-minus');
                    btn.children('i').addClass('glyphicon-plus');
                });
            }
        } else {
            if(btn.children('i').hasClass('glyphicon-plus')){
                btn.parent().parent().find('ul').slideDown('fast', function(e){
                    btn.children('i').removeClass('glyphicon-plus');
                    btn.children('i').addClass('glyphicon-minus');
                });
            }
        }
    }

    $('body').on('click', '.filter-clear', function(e){
        if(debug){
            console.log('Clear filters clicked ken min');
        }
        var btn = $(this);
        $(this).parent().parent().find('ul').each(function (i) {
            $(this).find('input').each(function(i){
                $(this).attr('checked', false);
            });
        })
        $('.search-filters').each(function(e){
            $(this).val('');
            searchText($(this));
        })
        btn.addClass('hidden');
        doFilters();
    });

    function doFilters() {
        anyFilters = {};
        $('.filter-set ul').each(function (i) {
            var tmp = [];
            var name = '';
            $(this).find('input:checked').each(function(i){
                if($(this).parent().parent().parent().find('.filter-clear').hasClass('hidden')){
                    $(this).parent().parent().parent().find('.filter-clear').removeClass('hidden');
                }
                name = $(this).attr('name');
                tmp.push($(this).val());
            });
            if($(this).find('input:checked').length < 1){
                $(this).parent().find('.filter-clear').addClass('hidden')
            }
            if(tmp.length > 0){
                anyFilters[name] = tmp;
            }
        })
        if(debug){
            console.log(anyFilters);
        }
    }

    $('body').on('change', '.filter-set input[type="checkbox"]', function(e){
        if(debug){
            console.log('OMG min! a filters been checked ... ken fit lyk.');
        }
        doFilters();
    });

    $('body').on('click', '.buttons-reset', function(e){
        anyFilters = {};
        $('.filter-set ul').each(function (i) {
            $(this).find('input:checked').each(function(i){
                $(this).attr('checked', false);
            });
        })
        $('.buttons-reload').click();
    });

    $('body').on('click', '.specific-filter', function(e){
        anyFilters['search_type'] = 'specific';
        setTimeout(function(){
            $('a[data-dt-idx="1"]').click().delay(800 );
        }, 1000);
        $('.buttons-reload').click();
    });

    $('body').on('click', '.broad-filter', function(e){
        anyFilters['search_type'] = 'broad';
        setTimeout(function(){
            $('a[data-dt-idx="1"]').click().delay(800 );
        }, 1000);
        $('.buttons-reload').click();
    });

    $('body').on('click', '.buttons-reset', function(e){
        anyFilters = {};
        $('.filter-set ul').each(function (i) {
            $(this).find('input:checked').each(function(i){
                $(this).attr('checked', false);
            });
        })
        $('.buttons-reload').click();
    });

    $('body').on('keyup', '.search-filters', function(e){
        searchText($(this));
    });

    function searchText(input){
        var term = input.val();
        var btn = input.parent().parent().find('.filter-expand');
        if(term.length > 0){
            expandFilters(btn, 'open');
        }
        if(debug){
            console.log('Filter search text: '+ term + ' length: ' + term.length);
        }
        input.parent().parent().parent().find('ul li').each(function(i){
            if($(this).hasClass('hidden')){
                $(this).removeClass('hidden');
            }
            var text = $(this).text();
            if(text.toLowerCase().search(term.toLowerCase()) < 0 && $(this).find('input').prop('checked') == false){
                $(this).addClass('hidden');
            }
        })
    }
    $('[data-toggle="modal"][title]').tooltip();
    $('[data-toggle="tooltip"]').tooltip();


</script>
@endpush