@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Order Items</h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                {!! $dataTable->table(['width' => '100%']) !!}
            </div>
            <a href="{{ url('orders') }}" style="" class="btn btn-default">Back</a>
        </div>

    </div>

    @include('orders.includes.cost_modal')
    @include('orders.includes.delete')
@endsection

@push('footer-script')
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
    <script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
    <script src="../../vendor/datatables/buttons.server-side.js"></script>
    {!! $dataTable->scripts() !!}

    <script type="text/javascript">
        (function (window, $) {
            $('body').on('click', '.child-expand', function () {
                var tr = $(this).closest('tr');
                var row = window.LaravelDataTables.dataTableBuilder.row(tr);
                var data = row.data();

                if (row.child.isShown()) {
                    row.child.hide();
                    $(this).find('i').removeClass('glyphicon-minus');
                    $(this).find('i').addClass('glyphicon-plus');
                    tr.removeClass('shown');
                }
                else {
                    var sizetable = '';
                    var typetable = '';
                    var materialtable = '';
                    var gradetable = '';

                    var oddEven = '';
                    if (tr.hasClass('odd')) {
                        oddEven = 'odd';
                    } else {
                        oddEven = 'even';
                    }

                    if (data.item.sizes.length > 0 || data.item.sexes.length > 0) {
                        if (data.item.sizes.length < 0) {
                            if (data.item.sexes.length > 0) {
                                $.each(data.item.sexes, function (i, sex) {
                                    sizetable = sizetable + '<tr><td>&nbsp;</td><td>' + sex.name + '</td></tr>';
                                })
                            }
                        } else {
                            $.each(data.item.sizes, function (i, size) {
                                sizetable = sizetable + '<tr><td>' + size.name + '</td>';
                                if (data.item.sexes.length > 0) {
                                    $.each(data.item.sexes, function (i, sex) {
                                        if (size.pivot.sex_id == sex.id) {
                                            sizetable = sizetable + '<td>' + sex.name + '</td>';
                                            return false;
                                        }
                                    })
                                } else {
                                    //empty row to keep formatting
                                    sizetable = sizetable + '<td>&nbsp;</td>';
                                }
                                sizetable = sizetable + '</tr>';
                            })
                        }
                    } else {
                        //empty row to keep formatting
                        sizetable = sizetable + '<tr><td>&nbsp;</td><td>&nbsp;</td></tr>';
                    }

                    if (data.item.types.length > 0) {
                        $.each(data.item.types, function (i, type) {
                            typetable = typetable + '<tr><td>' + type.name + '</td></tr>';
                        })
                    } else {
                        //empty row to keep formatting
                        typetable = typetable + '<tr><td>&nbsp;</td></tr>';
                    }

                    if (data.item.materials.length > 0) {
                        $.each(data.item.materials, function (i, material) {
                            materialtable = materialtable + '<tr><td>' + material.name + '</td></tr>';
                        })
                    } else {
                        //empty row to keep formatting
                        materialtable = materialtable + '<tr><td>&nbsp;</td></tr>';
                    }

                    if (data.item.grades.length > 0) {
                        $.each(data.item.grades, function (i, grade) {
                            gradetable = gradetable + '<tr><td>' + grade.name + '</td></tr>';
                        })
                    } else {
                        //empty row to keep formatting
                        gradetable = gradetable + '<tr><td>&nbsp;</td></tr>';
                    }

                    var infotable = '<div class="col-md-3 col-sm-6 col-xs-12"><table class="tableSmall no-footer"><tbody>' +
                        '<tr><td><b>Location</b></td><td>' + data.the_location + '</td></tr>' +
                        '<tr><td><b>Size</b></td><td>' + data.the_sizing + '</td></tr>' +
                        '<tr><td><b>Unit Sized As</b></td><td>' + data.the_unit + '</td></tr>' +
                        '<tr><td><b>Issuer</b></td><td>' + data.the_issuer + '</td></tr>' +
                        '<tr><td><b>Barcode</b></td><td>' + data.item.barcode + '</td></tr>' +
                        '</tbody></table></div>';
                    sizetable = '<div class="col-md-3 col-sm-6 col-xs-12"><table class="tableSmall no-footer"><thead><th>Size</th><th>Sex</th></thead><tbody>' + sizetable + '</tbody></table></div>';
                    typetable = '<div class="col-md-2 col-sm-6 col-xs-12"><table class="tableSmall no-footer"><thead><th>Type</th></thead><tbody>' + typetable + '</tbody></table></div>';
                    materialtable = '<div class="col-md-2 col-sm-6 col-xs-12"><table class="tableSmall no-footer"><thead><th>Material</th></thead><tbody>' + materialtable + '</tbody></table></div>';
                    gradetable = '<div class="col-md-2 col-sm-6 col-xs-12"><table class="tableSmall no-footer"><thead><th>Grade</th></thead><tbody>' + gradetable + '</tbody></table></div>';

                    row.child($('<tr class="' + oddEven + '">' +
                        '<td colspan="13" class="child-row"><div class="row">' +
                        '<div>' +
                        infotable +
                        sizetable +
                        typetable +
                        materialtable +
                        gradetable +
                        '</div>' +
                        '</div></td></tr>')).show();

                    tr.addClass('shown');
                    $(this).find('i').addClass('glyphicon-minus');
                    $(this).find('i').removeClass('glyphicon-plus');
                }
            });
        })(window, jQuery);
    </script>
@endpush