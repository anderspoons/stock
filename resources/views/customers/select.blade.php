<select name="customer_id"

        @if(isset($billed))
        @if($billed)
        disabled
        @endif
        @endif
    class="form-control" id="customer-select">
    @if(isset($counter))
        @if(!empty($counter->customer))
            <option selected value="{{ $counter->customer->id }}">
                {{ $counter->customer->name }}
            </option>
        @endif
    @endif

    @if(isset($job))
        @if(!empty($job->customer))
            <option selected value="{{ $job->customer->id }}">
                {{ $job->customer->name }}
            </option>
        @endif
    @endif

    @if(isset($boat))
        @if(!empty($boat->customer))
            <option selected value="{{ $boat->customer->id }}">
                {{ $boat->customer->name }}
            </option>
        @endif
    @endif
</select>

@push('footer-script')
    <script type="text/javascript">
        (function(window, $) {
            jQuery( document ).ready(function( $ ) {
                var last_results = [];
                $("#customer-select").select2({
                    ajax: {
                        url: "{!! url('customers/json') !!}",
                        dataType: "json",
                        data: function(term) {
                            return {
                                q: term,
                                @if(Request::is('job*'))
                                type: 'job'
                                @elseif(Request::is('order*'))
                                type: 'order'
                                @elseif(Request::is('item*'))
                                type: 'item'
                                @elseif(Request::is('counter*'))
                                type: 'counter'
                                @endif
                            };
                        },
                        results: function(data) {
                            console.log(data);
                            return {
                                results: data.results
                            };
                        }
                    },
                    createSearchChoice:function(term, data) {
                        if ($(data.results).filter(function() {
                                    return this.text.localeCompare(term) === 0;
                                }).length===0) {
                            return {
                                id:term,
                                text:term
                            };
                        }
                    }
                });
            });
        })(window, jQuery);
    </script>
@endpush