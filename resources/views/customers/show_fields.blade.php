<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $customer->name !!}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', 'Phone:') !!}
    <p>{!! $customer->phone !!}</p>
</div>

<!-- Fax Field -->
<div class="form-group">
    {!! Form::label('fax', 'Fax:') !!}
    <p>{!! $customer->fax !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $customer->email !!}</p>
</div>

<div class="clearfix">
    <!-- Address Id Field -->
    <div class="form-group">
        {!! Form::label('address', 'Address:') !!}
        <div class='btn-group'>
            <a href="{{  url('/addresses/'.$customer->address_id.'/edit') }}" class='btn btn-default btn-xs'>
                <i class="glyphicon glyphicon-edit"></i>
            </a>
        </div>
        <div class="row">
            <div class="col-md-6">
                @if(strlen($customer->address->line_1) > 0)
                    <b>Line 1:</b> {{ $customer->address->line_1 }}
                    <br>
                @endif
                @if(strlen($customer->address->line_2) > 0)
                    <b>Line 2:</b> {{ $customer->address->line_2 }}
                    <br>
                @endif
                @if(strlen($customer->address->line_3) > 0)
                    <b>Line 3:</b> {{ $customer->address->line_3 }}
                    <br>
                @endif
                @if(strlen($customer->address->town) > 0)
                    <b>Town:</b> {{ $customer->address->town }}
                    <br>
                @endif
                @if(strlen($customer->address->county) > 0)
                    <b>County:</b> {{ $customer->address->county }}
                    <br>
                @endif
                @if(strlen($customer->address->postcode) > 0)
                    <b>Postcode:</b> {{ $customer->address->postcode }}
                @endif
            </div>
        </div>
    </div>
</div>

@if($customer->boats())
<div class="clearfix">
    <div class="form-group">
        {!! Form::label('boats', 'Boats:') !!}
        @foreach($customer->boats()->get() as $boat)
            <div class="row">
                <div class="col-md-6">
                    <div class="col-md-4">
                        <div class='btn-group'>
                            <a href="{{  url('/boats/'.$boat->id) }}" class='btn btn-default btn-xs'>
                                <i class="glyphicon glyphicon-eye-open"></i>
                            </a>
                            <a href="{{  url('/boats/'.$boat->id.'/edit') }}" class='btn btn-default btn-xs'>
                                <i class="glyphicon glyphicon-edit"></i>
                            </a>
                        </div>
                        @if(strlen($boat->name) > 0)
                            <b>Name:</b> {!! $boat->name !!}
                        @endif
                    </div>
                    @if(strlen($boat->number) > 0)
                        <div class="col-md-4">
                            <b>Number:</b> {!! $boat->number !!}
                        </div>
                    @endif
                    @if(strlen($boat->sage_ref) > 0)
                        <div class="col-md-4">
                            <b>Sage Ref:</b> {!! $boat->sage_ref !!}
                        </div>
                    @endif
                </div>
            </div>
        @endforeach
    </div>
</div>
@endif
