@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Customer
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               {!! Form::model($customer, ['route' => ['customers.update', $customer->id], 'method' => 'patch']) !!}
               <div class="row">
                   @include('customers.fields')
               </div>

               <div class="row">
                   @include('addresses.shared.fields')
               </div>

               <!-- Submit Field -->
               <div class="form-group col-sm-12">
                   {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                   <a href="{!! route('customers.index') !!}" class="btn btn-default">Cancel</a>
               </div>

               {!! Form::close() !!}
           </div>
       </div>
   </div>
@endsection
@include('shared.modal.delete')