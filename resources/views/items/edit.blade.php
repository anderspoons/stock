@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Item
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="row">
            <div class="col-xs-12">

                {{ Form::model($item, ['route' => ['items.update', $item->id], 'method' => 'patch']) }}

                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#general" data-toggle="tab" aria-expanded="true">General
                                Information</a></li>
                        <li class=""><a href="#types" data-toggle="tab" aria-expanded="false">Types</a></li>
                        <li class=""><a href="#material" data-toggle="tab" aria-expanded="false">Material</a></li>
                        <li class=""><a href="#sizesex" data-toggle="tab" aria-expanded="false">Size/ Sex</a></li>
                        <li class=""><a href="#grade" data-toggle="tab" aria-expanded="false">Grade</a></li>
                        <li class=""><a href="#locations" data-toggle="tab" aria-expanded="false">Locations</a></li>
                        <li class=""><a href="#supplier" data-toggle="tab" aria-expanded="false">Supplier Details</a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="general">
                            @include('items.fields')
                        </div>

                        <div class="tab-pane" id="types">
                            @include('items.type.fields')
                        </div>
                        <div class="tab-pane" id="material">
                            @include('items.material.fields')
                        </div>
                        <div class="tab-pane" id="sizesex">
                            @include('items.sex.fields')
                            @include('items.size.fields')
                        </div>
                        <div class="tab-pane" id="grade">
                            @include('items.grade.fields')
                        </div>
                        <div class="tab-pane" id="locations">
                            @include('items.location.fields')
                        </div>
                        <div class="tab-pane" id="supplier">
                            @include('items.supplier.fields')
                        </div>
                    </div>
                </div>


                <div class="row">
                    <!-- Submit Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary btn-save']) !!}
                        <a href="{!! route('items.index') !!}" class="btn btn-default">Cancel</a>
                    </div>

                    <div class="form-group col-sm-6 right">
                        <a href="#" class="btn btn-danger" data-toggle="modal" data-target=".bs-delete-modal-sm"><i class="delete-item glyphicon glyphicon-remove"></i>Delete Item</a>
                    </div>
                </div>

                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection
@include('shared.modal.delete')