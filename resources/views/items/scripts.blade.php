<script src="/js/extjs.js"></script>
<script type="text/javascript">
    (function(window, $) {
        var debug = true;
        var anyFilters = {};

        window.LaravelDataTables = window.LaravelDataTables || {};
        window.LaravelDataTables["dataTableBuilder"] = $("#dataTableBuilder").DataTable({
            "serverSide": true,
            "processing": true,
            "sPaginationType": "extStyle",
            "ajax": {
                url: '/items',
                dataType: 'json',
                cache:false,
                type: 'GET',
                data: function (d) {
                    if(debug){
                        console.log(anyFilters);
                    }
                    $.each( anyFilters, function( key, value ) {
                        d[key] = value;
                    });
                }
            },
            stateSave: true,
            "columns": [{
                "name": "expand",
                "data": "expand",
                "orderable": false,
                "searchable": false,
                "title": "Expand"
            }, {
                "name": "description",
                "data": "description",
                "title": "Description",
                "orderable": true,
                "searchable": true
            }, {
                "name": "partnumber",
                "data": "partnumber",
                "title": "Partnumber",
                "title": "Partnumber",
                "orderable": true,
                "searchable": true
            },{
                "name": "markup_percent",
                "data": "markup_percent",
                "title": "Markup Percent",
                "orderable": true,
                "searchable": true
            }, {
                "name": "markup_fixed",
                "data": "markup_fixed",
                "title": "Markup Fixed",
                "orderable": true,
                "searchable": true
            }, {
                "name": "barcode",
                "data": "barcode",
                "title": "Barcode",
                "orderable": true,
                "searchable": true
            }, {
                "name": "on_order",
                "data": "on_order",
                "title": "On Order",
                "orderable": true,
                "searchable": true
            }, {
                "name": "on_reserve",
                "data": "on_reserve",
                "title": "Reserved",
                "orderable": true,
                "searchable": true
            }, {
                "name": "price",
                "data": "price",
                "orderable": true,
                "searchable": true,
                "title": "Price"
            },{
                "name": "actions",
                "data": "actions",
                "orderable": false,
                "searchable": false,
                "title": "Actions",
                "width": "140px"
            }],
            "dom": "Bfrtip",
            "scrollX": true,
            "buttons": ["csv", "excel", "pdf", "print", "reset", "reload"],
            "drawCallback": function() {
                window.expandRows();
                window.checkIssue();
                $("td > .btn-group").each(function(){
                    $btngrpWidth = 0;
                    $(this).find("a, button").each(function(i){
                        $btngrpWidth += +$(this).outerWidth();
                    });
                    $(this).width($btngrpWidth);
                });
            },
            "columnDefs": [
                {
                    "targets": [3],
                    "render": function ( data ) {
                        return data+'%';
                    }
                },
                {
                    "targets": [4],
                    "render": function ( data ) {
                        return '£'+data.toFixed(2);
                    }
                }
            ]
        });

        var unit_cats = {!! $category !!}
        // Add event listener for opening and closing details
        $('body').on('click', '.child-expand', function () {
            var tr = $(this).closest('tr');
            var row = window.LaravelDataTables.dataTableBuilder.row(tr);
            var data = row.data();

            if (debug) {
                console.log('Expand Row Clicked: ' + data.id);
            }

            if ( row.child.isShown() ) {
                row.child.hide();
                $(this).find('i').removeClass('glyphicon-minus');
                $(this).find('i').addClass('glyphicon-plus');
                tr.removeClass('shown');
            }
            else
            {
                var locationtable = '';
                var suppliertable = '';
                var sizetable ='';
                var typetable ='';
                var sextable ='';
                var suppliertable ='';
                var materialtable ='';
                var gradetable ='';
                var oddEven = '';
                if(tr.hasClass('odd')){
                    oddEven = 'odd';
                } else {
                    oddEven = 'even';
                }

                if(data.stores.length > 0) {
                    $.each(data.stores, function(i, store) {
                        locationtable = locationtable +  '<tr>' +
                            '<td>'+ store.code  +'</td>' +
                            '<td>' + store.pivot.room + '</td>' +
                            '<td>' + store.pivot.rack +'</td>' +
                            '<td>' + store.pivot.shelf +'</td>' +
                            '<td>' + store.pivot.bay +'</td>' +
                            '<td>' + store.pivot.min + '</td>' +
                            '<td>' + store.pivot.max + '</td>' +
                            '<td>' + store.pivot.qty + '</td>' +
                            '<td>' + store.pivot.on_order + '</td>' +
                            '</tr>';
                    })
                } else {
                    //empty row to keep formatting
                    locationtable = locationtable + '<tr><td colspan="6">&nbsp;</td></tr>';
                }

                if(data.sizes.length > 0 || data.sexes.length > 0) {
                    if(data.sizes.length < 0){
                        if(data.sexes.length > 0) {
                            $.each(data.sexes, function (i, sex) {
                                sizetable = sizetable + '<tr><td>&nbsp;</td><td>' + sex.name + '</td></tr>';
                            })
                        }
                    } else {
                        $.each(data.sizes, function(i, size) {
                            sizetable = sizetable +  '<tr><td>'+ size.name  +'</td>';
                            if(data.sexes.length > 0) {
                                $.each(data.sexes, function (i, sex) {
                                    if(size.pivot.sex_id == sex.id){
                                        sizetable = sizetable + '<td>' + sex.name + '</td>';
                                        return false;
                                    }
                                })
                            } else {
                                //empty row to keep formatting
                                sizetable = sizetable + '<td>&nbsp;</td>';
                            }
                            sizetable = sizetable +  '</tr>';
                        })
                    }
                } else {
                    //empty row to keep formatting
                    sizetable = sizetable + '<tr><td>&nbsp;</td><td>&nbsp;</td></tr>';
                }

                if(data.types.length > 0) {
                    $.each(data.types, function(i, type) {
                        typetable = typetable +  '<tr><td>' + type.name + '</td></tr>';
                    })
                } else {
                    //empty row to keep formatting
                    typetable = typetable + '<tr><td>&nbsp;</td></tr>';
                }

                if(data.suppliers.length > 0) {
                    $.each(data.suppliers, function (i, supplier) {
                        var supID = supplier.id;
                        var pricing = '';
                        var sizes = '';
                        var unitshort = '';
                        var catname = '';
                        var lots = '';

                        if(data.prices.length > 0) {
                            $.each(data.prices, function (i, price) {
                                console.log('SupID: '+supID);
                                console.log('Price Supplier: '+price.supplier_id);
                                console.log('Price: '+price.cost);
                                if(price.supplier_id == supID){
                                    pricing = '£'+ price.cost.toFixed(2);
                                    return false;
                                }
                            })
                        }

                        if(data.supplier_units.length > 0) {
                            $.each(data.supplier_units, function (i, supunit) {
                                if(supunit.pivot.supplier_id == supID){
                                    supunitSet = true;
                                    var catID = supunit.pivot.unit_cat_id
                                    //Unit ordered in category
                                    $.each(unit_cats, function(i, cat) {
                                        if(cat.id == catID){
                                           catname = cat.name;
                                        }
                                    })
                                    unitshort = supunit.name;
                                    lots = supunit.pivot.lots;
                                    //Sizes of
                                    if(data.supplier_sizing.length > 0){
                                        $.each(data.supplier_sizing, function (i, size) {
                                            if(size.pivot.supplier_id == supID){
                                                if(sizes.length > 0){
                                                    sizes = sizes + ' x ' + size.data;
                                                } else {
                                                    sizes = sizes + size.data;
                                                }
                                            }
                                        })
                                    }
                                }
                            })
                        }
                        //Main info
                        suppliertable = suppliertable + '<tr><td>' + supplier.pivot.code + '</td><td>' + supplier.code + '</td>';
                        //price
                        suppliertable = suppliertable + '<td>' + pricing + '</td>'
                        //category name
                        suppliertable = suppliertable + '<td>' + catname + '</td>';
                        //unit is measured in
                        suppliertable = suppliertable + '<td>' + unitshort + '</td>';
                        //lots of
                        suppliertable = suppliertable + '<td>' + lots + '</td>';
                        //sizes
                        suppliertable = suppliertable + '<td>' + sizes + '</td>';
                        suppliertable = suppliertable + '</tr>';
                    })
                } else {
                    //empty row to keep formatting
                    suppliertable = suppliertable + '<tr><td colspan="6">&nbsp;</td></tr>';
                }

                if(data.materials.length > 0) {
                    $.each(data.materials, function (i, material) {
                        materialtable = materialtable + '<tr><td>' + material.name + '</td></tr>';
                    })
                } else {
                    //empty row to keep formatting
                    materialtable = materialtable + '<tr><td>&nbsp;</td></tr>';
                }

                if(data.grades.length > 0) {
                    $.each(data.grades, function (i, grade) {
                        gradetable = gradetable + '<tr><td>' + grade.name + '</td></tr>';
                    })
                } else {
                    //empty row to keep formatting
                    gradetable = gradetable + '<tr><td>&nbsp;</td></tr>';
                }

                locationtable = '<div class="col-md-12"><table class="tableSmall no-footer">' +
                        '<thead><th>Store</th><th>Room</th><th>Rack</th><th>Shelf</th><th>Bay</th><th>Min Qty</th><th>Max Qty</th><th>Current Qty</th><th>On Order</th></thead><tbody>'
                        + locationtable +
                        '</tbody></table></div>';

                suppliertable = '<div class="col-md-12"><table class="tableSmall no-footer">' +
                        '<thead><th>Partcode</th><th>Supplier</th><th>Last Price</th><th>Ordered As</th><th>Size Unit</th><th>Lots Of</th><th>Sizes</th></thead><tbody>'
                        + suppliertable +
                        '</tbody></table></div>';

                sizetable = '<div class="col-md-3 col-sm-6 col-xs-12"><table class="tableSmall no-footer"><thead><th>Size</th><th>Sex</th></thead><tbody>' + sizetable + '</tbody></table></div>';
                typetable = '<div class="col-md-3 col-sm-6 col-xs-12"><table class="tableSmall no-footer"><thead><th>Type</th></thead><tbody>' + typetable + '</tbody></table></div>';
                materialtable = '<div class="col-md-3 col-sm-6 col-xs-12"><table class="tableSmall no-footer"><thead><th>Material</th></thead><tbody>' + materialtable + '</tbody></table></div>';
                gradetable = '<div class="col-md-3 col-sm-6 col-xs-12"><table class="tableSmall no-footer"><thead><th>Grade</th></thead><tbody>' + gradetable + '</tbody></table></div>';

                row.child($('<tr class="'+ oddEven +'">'+
                        '<td colspan="10" class="child-row"><div class="row">' +
                        '<div>'+
                        locationtable +
                        suppliertable +
                        '</div>'+
                        '<div>'+
                        sizetable +
                        typetable +
                        materialtable +
                        gradetable +
                        '</div>'+
                        '</div></td></tr>')).show();

                tr.addClass('shown');
                $(this).find('i').addClass('glyphicon-minus');
                $(this).find('i').removeClass('glyphicon-plus');
            }
        });

        $('body').on('click', '.quick-qty', function (e) {
            e.preventDefault();
            var tr = $(this).closest('tr');
            var row = window.LaravelDataTables.dataTableBuilder.row(tr);
            var data = row.data();
            $('#qty-modal .location-rows').html('');
            $('#qty-modal .item-id').val(data.id);
            var x = 1;
            $.each(data.stores, function (i, store) {2
                console.log(store);
                console.log('Location Row');
                var template = $('#qty-modal .qty-template').clone();
                template.removeClass('qty-template');
                template.removeClass('hidden');
                var name1 = template.find('.loc-id').attr('name');
                name1 = name1.replace('[][', '[' + x + '][');
                template.find('.loc-id').attr('name', name1);
                template.find('.loc-id').val(store.pivot.id);
                template.find('.loc-name').val(store.code);
                template.find('.loc-room').val(store.pivot.room);
                template.find('.loc-rack').val(store.pivot.rack);
                template.find('.loc-shelf').val(store.pivot.shelf);
                template.find('.loc-bay').val(store.pivot.bay);
                var name2 = template.find('.loc-qty').attr('name');
                name2 = name2.replace('[][', '[' + x + '][');
                template.find('.loc-qty').attr('name', name2);
                template.find('.loc-qty').val(store.pivot.qty);
                $('#qty-modal .location-rows').append(template);
                x = x + 1;
            })
        });

        $('body').on('click', '.quick-price', function (e) {

            var tr = $(this).closest('tr');
            var row = window.LaravelDataTables.dataTableBuilder.row(tr);
            var data = row.data();

            if (debug) {
                console.log('Quick Price Clicked for Item: ' + data.id);
                console.log(data);
            }
            $('#price-modal .item-id').val(data.id);
            $('#price-modal .supplier-select').html('<option value=""></option>');
            $.each(data.suppliers, function (i, supplier) {
                if(debug){
                    console.log(supplier.name);
                }
                var html = $('#price-modal .supplier-select').html();
                $('#price-modal .supplier-select').html(html + '<option value="' + supplier.id + '">' + supplier.name + '</option>');
            })
        });

        $('body').on('click', '.quick-order', function (e) {
            $('#quick-order-modal .warning-text').hide();
            var tr = $(this).closest('tr');
            var row = window.LaravelDataTables.dataTableBuilder.row(tr);
            var data = row.data();
            $('#quick-order-modal .item-id').val(data.id);
            newSupplier($('#quick-order-modal .item-id').val(), false, true);
        });

        newSupplier = function(id, current, first){
            $.ajax({
                url: "{!! url('/lookup/id') !!}/"+id,
                type: 'GET',
                dataType: 'Json',
                success: function (data) {
                    if(data.success == true){
                        var supplier_order = false;
                        var selected = false;
                        var order_id = 0;

                        if(current == false){
                            selected = false;
                        }

                        $.each(data.data.suppliers, function(i, supplier) {
                            $('.supplier-id').each(function(){
                                if (parseInt($(this).val()) == parseInt(supplier.id)) {
                                    supplier_order = true;
                                    order_id = $(this).attr('data-order');
                                }
                            });
                            if(current != false){
                                if(parseInt(current) === parseInt(supplier.id)){
                                    selected = true;
                                }
                            }
                        });
2

                        if(supplier_order == false || selected == false){
                            $('#quick-order-modal .new-supplier-row').removeClass('hidden');
                            $('#quick-order-modal .existing-supplier-row').addClass('hidden');
                            if(selected == false && supplier_order == false){
                                $('#quick-order-modal .warning-text').show();
                                $('#quick-order-modal .warning-text').html('No open orders have suppliers that match the suppliers set against this item.');
                            }
                        } else {
                            $('#quick-order-modal .new-supplier-row').addClass('hidden');
                            $('#quick-order-modal .existing-supplier-row').removeClass('hidden');
                        }

                        if(first && order_id > 0){
                            $('#order-select2').val(order_id).trigger('change');
                        }
                    }
                }
            });

        };

        $('#order-select2').on('select2:select', function(evt){
            var supp_id = evt.params.data.supp_id;
            console.log('Select: '+ supp_id);
            newSupplier($('#quick-order-modal .item-id').val(), supp_id, false);
        });

        $('body').on('click', '.add-order-item', function(e){
            e.preventDefault();
            if($('#quick-order-modal #qty').val() > 0){
                $.ajax({
                    url: "{!! url('/orders/add') !!}",
                    type: 'POST',
                    data: $('#quick-order-modal form').serialize(),
                    dataType: 'Json',
                    success: function (data) {
                        if(data.success == true){
                            $('#quick-order-modal .close').click()
                            var myStack = {"dir1":"down", "dir2":"right", "push":"top"};
                            new PNotify({
                                title: "Success",
                                text: 'Item added to order.',
                                addclass: "stack-custom",
                                stack: myStack
                            })
                            $('#quick-order-modal').click();
                        } else {
                            $('#quick-order-modal .warning-text').show();
                            $('#quick-order-modal .warning-text').html('No open orders have suppliers that match the suppliers set against this item.');
                        }
                    }
                });
            } else {
                $('#quick-order-modal .warning-text-qty').html('You can not add 0 quantity to an order.');
            }
        });

        $('body').on('click', '.edit-qty, .add-price', function(e){
            var form = $(this).parent().parent().find('.modal-body form');
            // form.reset();
            var element = $(this);
            $.ajax({
                type:"POST",
                url:form.attr("action"),
                data:form.serialize(),
                success: function(response){
                    element.parent().parent().find('button[data-dismiss="modal"]').click();
                    var myStack = {"dir1":"down", "dir2":"right", "push":"top"};
                    new PNotify({
                        title: "Success",
                        text: response.data,
                        addclass: "stack-custom",
                        stack: myStack
                    });
                    $('.buttons-reload').click();
                }
            });
        });


        $('body').on('click', '.filter-expand', function(e){
            var btn = $(this);
            expandFilters(btn, 'toggle');
        });

        function expandFilters(btn, open){
            if(open == 'toggle'){
                if(btn.children('i').hasClass('glyphicon-plus')){
                    btn.parent().parent().find('ul').slideDown('fast', function(e){
                        btn.children('i').removeClass('glyphicon-plus');
                        btn.children('i').addClass('glyphicon-minus');
                    });
                } else {
                    btn.parent().parent().find('ul').slideUp('fast', function(e){
                        btn.children('i').removeClass('glyphicon-minus');
                        btn.children('i').addClass('glyphicon-plus');
                    });
                }
            } else {
                if(btn.children('i').hasClass('glyphicon-plus')){
                    btn.parent().parent().find('ul').slideDown('fast', function(e){
                        btn.children('i').removeClass('glyphicon-plus');
                        btn.children('i').addClass('glyphicon-minus');
                    });
                }
            }
        }

        $('body').on('click', '.filter-clear', function(e){
            if(debug){
                console.log('Clear filters clicked ken min');
            }
            var btn = $(this);
            $(this).parent().parent().find('ul').each(function (i) {
                $(this).find('input').each(function(i){
                    $(this).attr('checked', false);
                });
            })
            $('.search-filters').each(function(e){
                $(this).val('');
                searchText($(this));
            })
            btn.addClass('hidden');
            doFilters();
        });

        function doFilters() {
            anyFilters = {};
            $('.filter-set ul').each(function (i) {
                var tmp = [];
                var name = '';
                $(this).find('input:checked').each(function(i){
                    if($(this).parent().parent().parent().find('.filter-clear').hasClass('hidden')){
                        $(this).parent().parent().parent().find('.filter-clear').removeClass('hidden');
                    }
                    name = $(this).attr('name');
                    tmp.push($(this).val());
                });
                if($(this).find('input:checked').length < 1){
                    $(this).parent().find('.filter-clear').addClass('hidden')
                }
                if(tmp.length > 0){
                    anyFilters[name] = tmp;
                }
            })
            if(debug){
                console.log(anyFilters);
            }
        }

        $('body').on('change', '.filter-set input[type="checkbox"]', function(e){
            if(debug){
                console.log('OMG min! a filters been checked ... ken fit lyk.');
            }
            doFilters();
        });

        $('body').on('click', '.buttons-reset', function(e){
            anyFilters = {};
            $('.filter-set ul').each(function (i) {
                $(this).find('input:checked').each(function(i){
                    $(this).attr('checked', false);
                });
            });
            $('.paginate_button.first').click();
            $('.buttons-reload').click();
        });

        $('body').on('click', '.specific-filter', function(e){
            anyFilters['search_type'] = 'specific';
            setTimeout(function(){
                $('a[data-dt-idx="1"]').click().delay(800 );
            }, 1000);
            $('.paginate_button.first').click(); 
            $('.buttons-reload').click();
        });

        $('body').on('click', '.broad-filter', function(e){
            anyFilters['search_type'] = 'broad';
            setTimeout(function(){
                $('a[data-dt-idx="1"]').click().delay(800 );
            }, 1000);
            $('.paginate_button.first').click(); 
            $('.buttons-reload').click();
        });

        $('body').on('click', '.buttons-reset', function(e){
            anyFilters = {};
            $('.filter-set ul').each(function (i) {
                $(this).find('input:checked').each(function(i){
                    $(this).attr('checked', false);
                });
            })
            $('.buttons-reload').click();
        });

        $('body').on('keyup', '.search-filters', function(e){
            searchText($(this));
        });

        function searchText(input){
            var term = input.val();
            var btn = input.parent().parent().find('.filter-expand');
            if(term.length > 0){
                expandFilters(btn, 'open');
            }
            if(debug){
                console.log('Filter search text: '+ term + ' length: ' + term.length);
            }
            input.parent().parent().parent().find('ul li').each(function(i){
                if($(this).hasClass('hidden')){
                    $(this).removeClass('hidden');
                }
                var text = $(this).text();
                if(text.toLowerCase().search(term.toLowerCase()) < 0 && $(this).find('input').prop('checked') == false){
                    $(this).addClass('hidden');
                }
            })
        }
        $('[data-toggle="modal"][title]').tooltip();
        $('[data-toggle="tooltip"]').tooltip();


        $('body').on('click', '.issue-this-item', function(e){
            e.preventDefault();
            var tr = $(this).closest('tr');
            var row = window.LaravelDataTables.dataTableBuilder.row(tr);
            var data = row.data();
            Talk({ 'event' : 'issue-item', 'barcode' : data.barcode });
            setTimeout(function(){
                window.close();
            },500);
        });

        $('body').on('click', '.order-this-item', function(e){
            e.preventDefault();
            var tr = $(this).closest('tr');
            var row = window.LaravelDataTables.dataTableBuilder.row(tr);
            var data = row.data();
            Talk({ 'event' : 'order-a-item', 'barcode' : data.barcode });
            setTimeout(function(){
                window.close();
            },500);
        });



    })(window, jQuery);

    window.checkIssue = function(){
        var cur_url = window.location.href;
        if(cur_url.indexOf("job_issue") != -1){
            $('.action-btns').each(function(i){
                $(this).append('<a data-toggle="tooltip" data-placement="bottom" title="Issue this item" href="" class="issue-this-item btn btn-default btn-xs"><i class="glyphicon glyphicon-plus"></i></a>')
            })
        }
        if(cur_url.indexOf("order_issue") != -1 ){
            $('.action-btns').each(function(i){
                $(this).append('<a data-toggle="tooltip" data-placement="bottom" title="Add Item to Order" href="" class="order-this-item btn btn-default btn-xs"><i class="glyphicon glyphicon-plus"></i></a>')
            })
        }
    }

    window.expandRows = function(){
        $('.child-expand').each(function (i) {
            $(this).click();
        })
    }

</script>
