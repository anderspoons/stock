<div class="row">
    <!-- Id Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('id', 'Database ID: (Could be used for support assistance)') !!}
        <div>{!! $item->id !!}</div>
    </div>

    <!-- Barcode Field -->
    <div class="form-group col-sm-3">
        {!! Form::label('barcode', 'Barcode:') !!}
        <div>{!! $item->barcode !!}</div>
    </div>
    <div class="form-group col-sm-3">
        <label>&nbsp;</label>
        <div class="clearfix"></div>
        <div class="col-sm-8">
            <img src="{!! url(DNS1D::getBarcodePNGPath($item->barcode, "C128", 2, 33)) !!}" alt="barcode"/>
        </div>
        <div class="col-sm-4">
            <a href="{!! url('/barcode/'.$item->barcode.'/small') !!}" target="_blank" class="print-barcode form-control" >Print</a>
        </div>
    </div>
</div>

<div class="row">
    <!-- Description Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('description', 'Description:') !!}
        <div>{!! $item->description !!}</div>
    </div>
    <!-- PartnumberField -->
    <div class="form-group col-sm-6">
        {!! Form::label('partnumber', 'Part Number:') !!}
        <div>{!! $item->partnumber !!}</div>
    </div>
</div>


<div class="row">
    <!-- Markup Fixed Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('markup_fixed', 'Markup Fixed:') !!}
        <div>{!! $item->markup_fixed !!}</div>
    </div>
    <!-- Markup Percent Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('markup_percent', 'Markup Percent:') !!}
        <div>{!! $item->markup_percent !!}</div>
    </div>
</div>

<div class="row">
    <!-- Created At Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('created_at', 'Created At:') !!}
        <div>{!! date('d/m/y H:i A', strtotime($item->created_at)) !!}</div>
    </div>
    <!-- Updated At Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('updated_at', 'Updated At:') !!}
        <div>{!! date('d/m/y H:i A', strtotime($item->updated_at)) !!}</div>
    </div>
</div>

<div class="row">
    @include('items.type.show_fields')
    @include('items.material.show_fields')
</div>

<div class="row">
    @include('items.sex.show_fields')
    @include('items.grade.show_fields')
</div>

<div class="row">
    @include('items.size.show_fields')
    @include('items.location.show_fields')
</div>

<div class="row">
    @include('items.supplier.show_fields')
    @include('items.prices.history')
</div>