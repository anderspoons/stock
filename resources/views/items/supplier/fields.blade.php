<div class="form-group col-sm-12">
    <div class="row">
        <div class="col-md-2">
            <h3>Supplier Details</h3>
        </div>
    </div>

    <div class="item-row">
        @if(isset($item))
            @foreach($item->suppliers as $supplier)
                <div class="sup-row">
                    <div class="row">

                        <div class="col-sm-11">
                            <div class="row">
                                <div class="col-sm-4">
                                    {!! Form::label('supplier[]', 'Supplier:') !!}
                                </div>
                                <div class="col-sm-4">
                                    {!! Form::label('supplier[][code]', 'Part Code:')  !!}
                                </div>
                                <div class="col-sm-4">
                                    {!! Form::label('supplier[][price]', 'Price:')  !!}
                                </div>

                                <div class="form-group col-sm-4">
                                    @include('items.supplier.suppliers')
                                </div>
                                <div class="form-group col-sm-4">
                                    {!! Form::text('supplier[][code]', $supplier->pivot->code, ['class' => 'form-control']) !!}
                                </div>
                                <div class="form-group col-sm-4">
                                    @include('items.prices.current')
                                </div>
                                <div class="col-sm-4">
                                    {!! Form::label('supplier[][unit-category]', 'Ordered As:')  !!}
                                </div>
                                <div class="col-sm-4">
                                    {!! Form::label('supplier[][specific-unit]', 'Sizing Unit:', array('class' => 'specific-unit'))  !!}
                                </div>
                                <div class="col-sm-4">
                                    {!! Form::label('supplier[][lots-of]', 'Ordered in Lots of:')  !!}
                                </div>
                                <div class="form-group col-sm-4">
                                    @include('items.units.category_select')
                                </div>
                                <div class="form-group col-sm-4">
                                    @include('items.units.unit_select')
                                </div>
                                <div class="form-group col-sm-4">
                                    {!! Form::text('supplier[][lots]', '1', ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-1 form-group remove-button-container">
                            {!! Form::button('<i class="glyphicon glyphicon-remove"></i>', ['class' => 'remove-item supplier-remove btn btn-default']) !!}
                        </div>

                        @if(isset($item->unit_category_id) && $item->unit_category_id == 3)
                            <div class="form-group col-sm-1 calculate-cost">
                                {!! Form::button('<i class="fa fa-calculator"></i>', ['class' => 'cost-calc-item btn btn-default', 'data-target' => '.bs-calc-cost-modal-sm', 'data-toggle' => 'modal']) !!}
                            </div>
                        @endif
                    </div>
                    <div class="clearfix"></div>

                    <div class="row">
                        <div class="col-sm-11">
                            <div class="row">
                                <div class="col-sm-4 size-label hidden">
                                    {!! Form::label('supplier[][unit-size][]', 'Sizing:')  !!}
                                </div>
                            </div>
                            <div class="row size-row">
                                @foreach($item->supplier_sizing as $size)
                                    @if($size->pivot->supplier_id == $supplier->id)
                                        <div class="col-sm-2">
                                            {!! Form::text('supplier[][unit-size][]', $size->data, ['class' => 'form-control size-box']) !!}
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        <div class="col-sm-1">
                            &nbsp;
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <br/>
                </div>
            @endforeach
        @endif
    </div>

    <div class="row">
        <div class="form-group col-sm-12">
            {!! Form::button('<i class="glyphicon glyphicon-plus"></i>  Add', ['class' => 'add-item supplier btn btn-default']) !!}
        </div>
    </div>


    <div class="item-template hidden sup-row">
        <div class="row">

            <div class="col-sm-11">
                <div class="row">

                    <div class="col-sm-4">
                        {!! Form::label('supplier[]', 'Supplier:') !!}
                    </div>
                    <div class="col-sm-4">
                        {!! Form::label('supplier[][code]', 'Part Code:')  !!}
                    </div>
                    <div class="col-sm-4">
                        {!! Form::label('supplier[][price]', 'Price:')  !!}
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="row">
                    <div class="form-group col-sm-4">
                        @include('items.supplier.suppliers')
                    </div>
                    <div class="form-group col-sm-4">
                        {!! Form::text('supplier[][code]', '', ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group col-md-4">
                        <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-gbp"></i>
                        </span>
                            {!! Form::number('supplier[][price]', null, ['class' => 'form-control', 'onkeypress' => 'validate(event)', 'pattern' => '[0-9]+([\.,][0-9]+)?', 'step' => '0.01']) !!}
                        </div>
                    </div>
                    <div class="col-sm-4">
                        {!! Form::label('supplier[][unit-category]', 'Ordered As:')  !!}
                    </div>
                    <div class="col-sm-4">
                        {!! Form::label('supplier[][specific-unit]', 'Sizing Unit:', array('class' => 'specific-unit'))  !!}
                    </div>
                    <div class="col-sm-4">
                        {!! Form::label('supplier[][lots-of]', 'Ordered in Lots of:')  !!}
                    </div>
                    <div class="form-group col-sm-4">
                        @include('items.units.category_select')
                    </div>
                    <div class="form-group col-sm-4">
                        @include('items.units.unit_select')
                    </div>
                    <div class="form-group col-sm-4">
                        {!! Form::text('supplier[][lots]', '1', ['class' => 'form-control']) !!}
                    </div>
                    <div class="col-sm-4 size-label hidden">
                        {!! Form::label('supplier[][unit-size][]', 'Sizing:')  !!}
                    </div>
                </div>
                <div class="row size-row">
                </div>
            </div>
            <div class="form-group col-sm-1 remove-button-container">
                {!! Form::button('<i class="glyphicon glyphicon-remove"></i>', ['class' => 'remove-item supplier-remove btn btn-default']) !!}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-11">
            <div class="hidden" id="size-box">
                <div class="col-sm-4">
                    {!! Form::text('supplier[][unit-size][]', '', ['class' => 'form-control size-input', 'disabled' => 'disabled']) !!}
                </div>
            </div>
        </div>
        <div class="col-sm-1">
            &nbsp;
        </div>
    </div>

</div>


@push('footer-script')
    @if(isset($item->unit_category_id) && $item->unit_category_id == 3)
        @include('shared.area-cost')
    @endif
@endpush

