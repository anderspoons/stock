@if(isset($suppliers))
    <select name='supplier[][supplier]' class="form-control supplier-select">
        @if(isset($supplier))
            <option value='{{ $supplier->id }}' selected>{{ $supplier->code }}</option>
        @endif
        @foreach($suppliers as $sup)
            @if(!isset($supplier) || $sup->id != $supplier->id)
                <option value='{{ $sup->id }}'>{{ $sup->code }}</option>
            @endif
        @endforeach
    </select>
@endif