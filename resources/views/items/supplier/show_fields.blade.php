<div class="form-group col-sm-5">
    <div class="row">
    <div class="col-sm-6">
        {!! Form::label('', 'Supplier:') !!}
    </div>
    <div class="col-sm-6">
        {!! Form::label('', 'Part Code:') !!}
    </div>
    </div>
    <div class="row">
        @if(isset($item))
            @foreach($item->suppliers as $supplier)
                <div class="form-group col-sm-6">
                    {!! $supplier->name !!}
                </div>
                <div class="form-group col-sm-6">
                    {!! $supplier->pivot->code !!}
                </div>
            @endforeach
        @else
            <div class="form-group col-sm-6">
                &nbsp;
            </div>
            <div class="form-group col-sm-6">
                &nbsp;
            </div>
        @endif
    </div>
</div>