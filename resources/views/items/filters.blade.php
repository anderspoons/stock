@if(isset($types))
    <div class="filter-set">
        <div class="header-bar">
            <div class="filter-search-box" style="padding:0 !important;">
                {!! Form::text('type', '', array('class' => 'search-filters', 'placeholder' => 'Type')) !!}
            </div>
            <button class="btn btn-xs pull-right filter-clear hidden"><i class="glyphicon glyphicon-remove"></i></button>
            <button class="btn btn-xs pull-right filter-expand"><i class="glyphicon glyphicon-plus"></i></button>
        </div>
        <div class="clearfix"></div>
        <ul>
            @foreach($types as $i => $type)
                <li>
                    {!! Form::checkbox('type_filter', $type->id, false, ['id' => 'type_' . $i]) !!}
                    {!! Form::label('type_' . $i, $type->name) !!}
                </li>
            @endforeach
        </ul>
    </div>
@endif

@if(isset($sizes))
    <div class="filter-set">
        <div class="header-bar">
            <div class="filter-search-box" style="padding:0 !important;">
                {!! Form::text('size', '', array('class' => 'search-filters', 'placeholder' => 'Size')) !!}
            </div>
            <button class="btn btn-xs pull-right filter-clear hidden"><i class="glyphicon glyphicon-remove"></i></button>
            <button class="btn btn-xs pull-right filter-expand"><i class="glyphicon glyphicon-plus"></i></button>
        </div>
        <div class="clearfix"></div>
        <ul>
            @foreach($sizes as $i => $size)
                <li>
                    {!! Form::checkbox('size_filter', $size->id, false, ['id' => 'size_' . $i]) !!}
                    {!! Form::label('size_' . $i, $size->name) !!}
                </li>
            @endforeach
        </ul>
    </div>
@endif

@if(isset($grades))
    <div class="filter-set">
        <div class="header-bar">
            <div class="filter-search-box" style="padding:0 !important;">
                {!! Form::text('grade', '', array('class' => 'search-filters', 'placeholder' => 'Grade')) !!}
            </div>
            <button class="btn btn-xs pull-right filter-clear hidden"><i class="glyphicon glyphicon-remove"></i></button>
            <button class="btn btn-xs pull-right filter-expand"><i class="glyphicon glyphicon-plus"></i></button>
        </div>
        <div class="clearfix"></div>
        <ul>
            @foreach($grades as $i => $grade)
                <li>
                    {!! Form::checkbox('grade_filter', $grade->id, false, ['id' => 'grade_' . $i]) !!}
                    {!! Form::label('grade_' . $i, $grade->name) !!}
                </li>
            @endforeach
        </ul>
    </div>
@endif

@if(isset($materials))
    <div class="filter-set">
        <div class="header-bar">
            <div class="filter-search-box" style="padding:0 !important;">
                {!! Form::text('material', '', array('class' => 'search-filters', 'placeholder' => 'Material')) !!}
            </div>
            <button class="btn btn-xs pull-right filter-clear hidden"><i class="glyphicon glyphicon-remove"></i></button>
            <button class="btn btn-xs pull-right filter-expand"><i class="glyphicon glyphicon-plus"></i></button>
        </div>
        <div class="clearfix"></div>
        <ul>
            @foreach($materials as $i => $material)
                <li>
                    {!! Form::checkbox('material_filter', $material->id, false, ['id' => 'material_' . $i]) !!}
                    {!! Form::label('material_' . $i, $material->name) !!}
                </li>
            @endforeach
        </ul>
    </div>
@endif

@if(isset($sexes))
    <div class="filter-set">
        <div class="header-bar">
            <div class="filter-search-box" style="padding:0 !important;">
                {!! Form::text('sex', '', array('class' => 'search-filters', 'placeholder' => 'Sex')) !!}
            </div>
            <button class="btn btn-xs pull-right filter-clear hidden"><i class="glyphicon glyphicon-remove"></i></button>
            <button class="btn btn-xs pull-right filter-expand"><i class="glyphicon glyphicon-plus"></i></button>
        </div>
        <div class="clearfix"></div>
        <ul>
            @foreach($sexes as $i => $sex)
                <li>
                    {!! Form::checkbox('sex_filter', $sex->id, false, ['id' => 'sex_' . $i]) !!}
                    {!! Form::label('sex_' . $i, $sex->name) !!}
                </li>
            @endforeach
        </ul>
    </div>
@endif

@if(isset($suppliers))
    <div class="filter-set">
        <div class="header-bar">
            <div class="filter-search-box" style="padding:0 !important;">
                {!! Form::text('supplier', '', array('class' => 'search-filters', 'placeholder' => 'Supplier')) !!}
            </div>
            <button class="btn btn-xs pull-right filter-clear hidden"><i class="glyphicon glyphicon-remove"></i></button>
            <button class="btn btn-xs pull-right filter-expand"><i class="glyphicon glyphicon-plus"></i></button>
        </div>
        <div class="clearfix"></div>
        <ul>
            @foreach($suppliers as $i => $supplier)
                <li>
                    {!! Form::checkbox('supplier_filter', $supplier->id, false, ['id' => 'supplier_' . $i]) !!}
                    {!! Form::label('supplier_' . $i, $supplier->code) !!}
                </li>
            @endforeach
        </ul>
    </div>
@endif

@if(isset($statuses))
    <div class="filter-set">
        <div class="header-bar">
            <div class="filter-search-box" style="padding:0 !important;">
                {!! Form::text('status', '', array('class' => 'search-filters', 'placeholder' => 'Status')) !!}
            </div>
            <button class="btn btn-xs pull-right filter-clear hidden"><i class="glyphicon glyphicon-remove"></i></button>
            <button class="btn btn-xs pull-right filter-expand"><i class="glyphicon glyphicon-plus"></i></button>
        </div>
        <div class="clearfix"></div>
        <ul>
            @foreach($statuses as $i => $status)
                <li>
                    {!! Form::checkbox('status_filter', $status->id, false, ['id' => 'status_' . $i]) !!}
                    {!! Form::label('status_' . $i, $status->name) !!}
                </li>
            @endforeach
        </ul>
    </div>
@endif

@if(isset($stores))
    <div class="filter-set">
        <div class="header-bar">
            <div class="filter-search-box" style="padding:0 !important;">
                {!! Form::text('store', '', array('class' => 'search-filters', 'placeholder' => 'Store')) !!}
            </div>
            <button class="btn btn-xs pull-right filter-clear hidden"><i class="glyphicon glyphicon-remove"></i></button>
            <button class="btn btn-xs pull-right filter-expand"><i class="glyphicon glyphicon-plus"></i></button>
        </div>
        <div class="clearfix"></div>
        <ul>
            @foreach($stores as $store)
                <li>
                    {!! Form::checkbox('store_filter', $store->id, false, ['id' => 'store_' . $i]) !!}
                    {!! Form::label('store_' . $i, $store->code) !!}
                </li>
            @endforeach
        </ul>
    </div>
@endif

@if(isset($types) || isset($sizes) || isset($sexes) || isset($grades) || isset($materials) || isset($suppliers) || isset($statuses) || isset($stores))
    <br/>
    <div class="clearfix"></div>
    {{ Form::button('Broad Filter', array('class' => 'col-md-12 broad-filter')) }}
    {{ Form::button('Specific Filter', array('class' => 'col-md-12 specific-filter', 'style' => 'margin-top:10px;')) }}
    {{ Form::button('Reset Filters', array('class' => 'col-md-12 buttons-reset', 'style' => 'margin-top:10px;')) }}
@endif
{{--
THE SCRIPT FOR THIS IS IN THE SCRIPTS VIEW
--}}

