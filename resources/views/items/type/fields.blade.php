<div class="form-group col-sm-12">
    <h3>Types</h3>
    <div class="item-row">
        @if(isset($item))
            @foreach($item->types as $type)
                <div class="row">
                    <div class="form-group col-sm-11">
                        {!! Form::text('types['.$type->id.']', $type->name, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group col-sm-1">
                        {!! Form::button('<i class="glyphicon glyphicon-remove"></i>', ['class' => 'remove-item btn btn-default']) !!}
                    </div>
                </div>
            @endforeach
        @endif
    </div>

    <div class="row">
        <div class="form-group col-sm-12">
            {!! Form::button('<i class="glyphicon glyphicon-plus"></i>  Add', ['class' => 'add-item btn btn-default']) !!}
        </div>
    </div>

    <div class="row item-template hidden">
        <div class="form-group col-sm-11">
            {!! Form::text('types[]', '', ['class' => 'form-control']) !!}
        </div>
        <div class="form-group col-sm-1">
            {!! Form::button('<i class="glyphicon glyphicon-remove"></i>', ['class' => 'remove-item btn btn-default']) !!}
        </div>
    </div>
</div>