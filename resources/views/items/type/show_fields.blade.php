<div class="form-group col-sm-6">
    {!! Form::label('', 'Types:') !!}
    <div class="row">
        @if(isset($item))
            @foreach($item->types as $type)
                <div class="form-group col-sm-11">
                    {!! $type->name !!}
                </div>
            @endforeach
        @else
            <div class="form-group col-sm-11">
                &nbsp;
            </div>
        @endif
    </div>
</div>