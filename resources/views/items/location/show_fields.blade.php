<div class="form-group col-sm-6">
    <div class="row">
        <div class="col-sm-3">
            {!! Form::label('location[][store]', 'Location') !!}
        </div>

        <div class="col-sm-1">
            {!! Form::label('location[][room]', 'Room') !!}
        </div>

        <div class="col-sm-2">
            {!! Form::label('location[][rack]', 'Rack') !!}
        </div>

        <div class="col-sm-1">
            {!! Form::label('location[][shelf]', 'Shelf') !!}
        </div>

        <div class="col-sm-2">
            {!! Form::label('location[][bay]', 'Bay') !!}
        </div>

        <div class="col-sm-1">
            {!! Form::label('location[][min]', 'Min') !!}
        </div>

        <div class="col-sm-1">
            {!! Form::label('location[][max]', 'Max') !!}
        </div>

        <div class="col-sm-1">
            {!! Form::label('location[][qty]', 'Qty') !!}
        </div>
    </div>
    @if(isset($item))
        @foreach($item->stores as $store)
            <div class="row">
                <div class="col-sm-3">
                    {!! $store->name !!}
                </div>
                <div class="col-sm-1">
                    {!! $store->pivot->room !!}
                </div>
                <div class="col-sm-2">
                    {!! $store->pivot->rack !!}
                </div>
                <div class="col-sm-1">
                    {!! $store->pivot->shelf !!}
                </div>
                <div class="col-sm-2">
                    {!! $store->pivot->bay !!}
                </div>
                <div class="col-sm-1">
                    {!! $store->pivot->min !!}
                </div>
                <div class="col-sm-1">
                    {!! $store->pivot->max !!}
                </div>
                <div class="col-sm-1">
                    {!! $store->pivot->qty !!}
                </div>
            </div>
        @endforeach
    @endif
</div>