@if(isset($stores))
    {{-- Stores: all stores --}}
    {{-- Store: this items store. --}}
    <select name='location[][store]' class="form-control location-select">
        @if(isset($store))
            <option value='{{ $store->id }}' selected>{{ $store->code }}</option>
        @endif
        @foreach($stores as $loc)
            @if(!isset($store) || $loc->id != $store->id)
                <option value='{{ $loc->id }}'>{{ $loc->code }}</option>
            @endif
        @endforeach
    </select>
@endif