<div class="col-sm-12 container-fluid">

    <div class="row">
        <div class="col-sm-12">
            <h3>Locations</h3>
        </div>
    </div>

    <div class="item-row row">
        @if(isset($item))
            @foreach($item->stores as $store)
                <div class="loc-row form-group">
                    <div class="row">
                        <div class="form-group col-sm-11">
                            {!! Form::hidden('location[][loc_id]', $store->pivot->id, array('id' => 'loc-id')) !!}

                            <div class="form-group col-sm-3">
                                {!! Form::label('location[][store]', 'Store:') !!}
                                @include('items.location.stores')
                            </div>
                            <div class="form-group col-sm-3">
                                {!! Form::label('location[][room]', 'Room:') !!}
                                {!! Form::text('location[][room]', $store->pivot->room, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group col-sm-3">
                                {!! Form::label('location[][rack]', 'Rack:') !!}
                                {!! Form::text('location[][rack]', $store->pivot->rack, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group col-sm-3">
                                {!! Form::label('location[][shelf]', 'Shelf:') !!}
                                {!! Form::text('location[][shelf]', $store->pivot->shelf, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group col-sm-3">
                                {!! Form::label('location[][bay]', 'Bay:') !!}
                                {!! Form::text('location[][bay]', $store->pivot->bay, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group col-sm-3">
                                {!! Form::label('location[][min]', 'Min Stock:') !!}
                                {!! Form::text('location[][min]', $store->pivot->min, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group col-sm-3">
                                {!! Form::label('location[][max]', 'Max Stock:') !!}
                                {!! Form::text('location[][max]', $store->pivot->max, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group col-sm-3">
                                {!! Form::label('location[][qty]', 'Qty:') !!}
                                {!! Form::text('location[][qty]', $store->pivot->qty, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group col-sm-1 remove-button-container">
                            {!! Form::button('<i class="glyphicon glyphicon-remove"></i>', ['class' => 'remove-item btn btn-default location']) !!}
                        </div>

                        @if(isset($item->unit_category_id) && $item->unit_category_id == 3)
                            <div class="form-group col-sm-1 calculate-location">
                                {!! Form::button('<i class="fa fa-calculator"></i>', ['class' => 'qty-calc-item btn btn-default', 'data-target' => '.bs-calc-modal-sm', 'data-toggle' => 'modal']) !!}
                            </div>
                        @endif

                    </div>
                </div>
            @endforeach
        @endif
    </div>

    <div class="row">
        <div class="form-group col-sm-12">
            {!! Form::button('<i class="glyphicon glyphicon-plus"></i>  Add', ['class' => 'add-item btn btn-default location']) !!}
        </div>
    </div>


    <div class="item-template hidden loc-row form-group">
        <div class="row">
            <div class="col-sm-11">
                <div class="form-group col-sm-3">
                    {!! Form::label('location[][store]', 'Store:') !!}
                    @include('items.location.stores')
                </div>
                <div class="form-group col-sm-3">
                    {!! Form::label('location[][room]', 'Room:') !!}
                    {!! Form::text('location[][room]', '', ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-sm-3">
                    {!! Form::label('location[][rack]', 'Rack:') !!}
                    {!! Form::text('location[][rack]', '', ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-sm-3">
                    {!! Form::label('location[][shelf]', 'Shelf:') !!}
                    {!! Form::text('location[][shelf]', '', ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-sm-3">
                    {!! Form::label('location[][bay]', 'Bay:') !!}
                    {!! Form::text('location[][bay]', '', ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-sm-3">
                    {!! Form::label('location[][min]', 'Min Stock:') !!}
                    {!! Form::text('location[][min]', '', ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-sm-3">
                    {!! Form::label('location[][max]', 'Max Stock:') !!}
                    {!! Form::text('location[][max]', '', ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-sm-3">
                    {!! Form::label('location[][qty]', 'Qty:') !!}
                    {!! Form::text('location[][qty]', '', ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-sm-1 remove-button-container">
                {!! Form::button('<i class="glyphicon glyphicon-remove"></i>', ['class' => 'remove-item btn btn-default']) !!}
            </div>

            @if(isset($item->unit_category_id) && $item->unit_category_id == 3)
                <div class="form-group col-sm-1 calculate-location">
                    {!! Form::button('<i class="fa fa-calculator"></i>', ['class' => 'qty-calc-item btn btn-default', 'data-target' => '.bs-calc-modal-sm', 'data-toggle' => 'modal']) !!}
                </div>
            @endif
        </div>
        <div class="clearfix"></div>
    </div>
</div>

@push('footer-script')
    @if(isset($item->unit_category_id) && $item->unit_category_id == 3)
        @include('shared.convert')
    @endif
@endpush