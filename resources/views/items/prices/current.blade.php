{!! $done = false !!}

@if($item->prices && $item->prices->find('supplier_id', $supplier->id))
    @foreach($item->prices as $price)
        @if($price->supplier_id == $supplier->id)
            <?php $done = true; ?>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-gbp"></i>
                </span>
                {!! Form::number('supplier[][price]', $price->cost, ['class' => 'form-control', 'onkeypress' => 'validate(event)', 'pattern' => '[0-9]+([\.,][0-9]+)?', 'step' => '0.01']) !!}
            </div>
            @break
        @endif
    @endforeach
@endif

{{-- If we got to this point it means that prices were most likely manually set so we need a empty box --}}
@if(!$done)
    <div class="input-group">
        <span class="input-group-addon">
            <i class="fa fa-gbp"></i>
        </span>
        {!! Form::number('supplier[][price]', null, ['class' => 'form-control', 'onkeypress' => 'validate(event)', 'pattern' => '[0-9]+([\.,][0-9]+)?', 'step' => '0.01']) !!}
    </div>
@endif