<div class="form-group col-sm-12">
    <h3>General Information</h3>
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Markup Percent Field -->
<div class="form-group col-sm-6">
    {!! Form::label('markup_percent', 'Markup Percent ( % ):') !!}
    <div class="input-group">
        @if(Request::is('items/create'))
            {!! Form::number('markup_percent', '20', ['class' => 'form-control', 'onkeypress' => 'validate(event)']) !!}
        @else
            {!! Form::number('markup_percent', null, ['class' => 'form-control', 'onkeypress' => 'validate(event)']) !!}
        @endif
        <span class="input-group-addon">
            <i class="fa fa-percent"></i>
        </span>
    </div>
</div>

<!-- Markup Fixed Field -->
<div class="form-group col-sm-6">
    {!! Form::label('markup_fixed', 'Markup Fixed ( £ ):') !!}
    <div class="input-group">
        <span class="input-group-addon">
            <i class="fa fa-gbp"></i>
        </span>
        {!! Form::number('markup_fixed', null, ['class' => 'form-control', 'onkeypress' => 'validate(event)', 'pattern' => '[0-9]+([\.,][0-9]+)?', 'step' => '0.01']) !!}

    </div>
</div>

<!-- PartnumberField -->
<div class="form-group col-sm-6">
    {!! Form::label('partnumber', 'Part Number:') !!}
    {!! Form::text('partnumber', null, ['class' => 'form-control']) !!}
</div>

<!-- Barcode Field -->
<div class="form-group col-sm-2">
    {!! Form::label('barcode', 'Barcode:') !!}
    @if(isset($item->barcode))
        {!! Form::text('barcode', $item->barcode, array('class' => 'form-control')) !!}
    @elseif(isset($barcode))
        {!! Form::text('barcode', $barcode, array('class' => 'form-control')) !!}
    @else
        {!! Form::text('barcode', null, array('class' => 'form-control', 'disabled' => 'disabled')) !!}
    @endif
</div>


<div class="form-group col-sm-4 barcode-field">
    @if(isset($item->barcode))
        <div class="col-sm-9 no-left-gutter">
            <img src="{!! url(DNS1D::getBarcodePNGPath($item->barcode, "C128", 2, 33)) !!}" alt="barcode"/>
        </div>
        <div class="col-sm-3">
            <a href="{!! url('/barcode/'.$item->barcode.'/small') !!}" target="_blank"
               class="print-barcode btn btn-default"><span class="glyphicon glyphicon-print"></span></a>
        </div>
    @elseif(isset($barcode))
    <div class="col-sm-9 no-left-gutter">
        <img src="{!! url(DNS1D::getBarcodePNGPath($barcode, "C128", 2, 33)) !!}" alt="barcode"/>
    </div>
    <div class="col-sm-3">
        <a href="{!! url('/barcode/'.$barcode.'/small') !!}" target="_blank"
           class="print-barcode btn btn-default"><span class="glyphicon glyphicon-print"></span></a>
    </div>
    @endif
</div>


<!-- Units and stuff  -->
<div class="form-group col-sm-6 no-left-gutter no-right-gutter">
    <div class="col-sm-6">
        {!! Form::label('unit_category_id', 'Sold As:') !!}
        <select name="unit_category_id" class="form-control" id="unit-cat-sold">
            <option value=""></option>
            @foreach($category as $cat)
                @if(isset($item->unit_category_id) && $item->unit_category_id == $cat->id)
                    <option selected="selected" value="{!! $cat->id !!}">{!! $cat->name !!}</option>
                @else
                    <option value="{!! $cat->id !!}">{!! $cat->name !!}</option>
                @endif
            @endforeach
        </select>
    </div>

    <div class="col-sm-6 no-left-gutter">
        {!! Form::label('unit_id', 'Reporting Unit:') !!}
        <select name="unit_id" class="form-control" id="unit-sold">
            <option value=""></option>
            @foreach($units as $unit)
                @if(isset($item->unit_id) && $item->unit_id == $unit->id)
                    <option data-cat="{!! $unit->category_id !!}" selected="selected" value="{!! $unit->id !!}">{!! $unit->name !!}</option>
                @else
                    <option data-cat="{!! $unit->category_id !!}" value="{!! $unit->id !!}">{!! $unit->name !!}</option>
                @endif
            @endforeach
        </select>
    </div>
</div>

@include('shared.status')