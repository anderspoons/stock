<div class="form-group col-sm-12">
    <div class="col-sm-5 no-left-gutter">
        <h3>Size</h3>
    </div>
    <div class="form-group col-sm-5">
        <h3>Sex</h3>
    </div>
    <div class="item-row">
        @if(isset($item))
            @foreach($item->sizes as $size)
                <div class="row sizing-row">
                    <div class="col-sm-5">
                        {!! Form::text('sizes[][size]', $size->name, ['class' => 'form-control']) !!}
                    </div>
                    @include('items.sex.select')

                    <div class="form-group col-sm-1">
                        {!! Form::button('<i class="glyphicon glyphicon-remove"></i>', ['class' => 'remove-item btn size btn-default']) !!}
                    </div>
                </div>
            @endforeach
        @endif
    </div>

    <div class="row">
        <div class="form-group col-sm-12">
            {!! Form::button('<i class="glyphicon glyphicon-plus"></i>  Add', ['class' => 'add-item size btn btn-default']) !!}
        </div>
    </div>

    <div class="row sizing-row item-template hidden">
        <div class="form-group col-sm-5">
            {!! Form::text('sizes[][size]', '', ['class' => 'form-control']) !!}
        </div>
        @include('items.sex.select')
        <div class="form-group col-sm-1">
            {!! Form::button('<i class="glyphicon glyphicon-remove"></i>', ['class' => 'remove-item btn btn-default']) !!}
        </div>
    </div>
</div>