<div class="form-group col-sm-6">
    <div class="form-group col-sm-6">
    {!! Form::label('', 'Size:') !!}
    </div>
        <div class="form-group col-sm-6">
    {!! Form::label('', 'Sex:') !!}
        </div>
    <div class="row">
        @if(isset($item))
            @foreach($item->sizes as $size)
                <div class="form-group col-md-6">
                    {!! $size->name !!}
                </div>
                <div class="form-group col-md-6">
                    @if(isset($size->id))
                        <?php $set = ''; ?>
                        @if(count($item->sexes) > 0)
                            @foreach($item->sexes as $sex)
                                @if($sex->id == $size->pivot->sex_id)
                                 {{ $sex->name }}
                                @endif
                            @endforeach
                        @endif
                    @endif
                </div>
            @endforeach
        @else
            <div class="form-group col-sm-12">
                &nbsp;
            </div>
        @endif
    </div>
</div>