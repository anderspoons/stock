<div class="form-group col-sm-6">
    {!! Form::label('', 'Grades:') !!}
    <div class="row">
        @if(isset($item))
            @foreach($item->grades as $grade)
                <div class="form-group col-sm-11">
                    {!! $grade->name !!}}
                </div>
            @endforeach
        @else
            <div class="form-group col-sm-11">
                &nbsp;
            </div>
        @endif
    </div>
</div>