<div class="modal fade bs-quick-qty-modal-sm" id="qty-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Quick Stock Adjust</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['url' => 'item/qty', 'method' => 'post', 'class' => 'quick-qty-form']) }}
                {!! Form::hidden('item_id', '', array('class' => 'item-id')) !!}
                <div class="row">
                    <div class="col-sm-2">
                        {!! Form::label('name[]', 'Location:') !!}
                    </div>
                    <div class="col-sm-2">
                        {!! Form::label('room[]', 'Room:') !!}
                    </div>
                    <div class="col-sm-2">
                        {!! Form::label('rack[]', 'Rack:') !!}
                    </div>
                    <div class="col-sm-2">
                        {!! Form::label('shelf[]', 'Shelf:') !!}
                    </div>
                    <div class="col-sm-2">
                        {!! Form::label('bay[]', 'Bay:') !!}
                    </div>
                    <div class="col-sm-2">
                        {!! Form::label('location[][qty]', 'Qty:') !!}
                    </div>
                </div>

                <div class="location-rows">

                </div>

                <br>
                {{-- <div class="row">
                    <div class="col-sm-12 col-md-6">
                        {!! Form::label('pin_id', 'Name:') !!}
                        @if(isset($pins))
                            <select class="form-control" name="pin_id">
                                @foreach($pins as $user)
                                    <option value="{!! $user->id !!}">{!! $user->name !!}</option>
                                @endforeach
                            </select>
                        @endif
                    </div>
                    <div class="col-sm-12 col-md-6">
                        {!! Form::label('pin', 'Security Pin:') !!}
                        {!! Form::password('pin', array('class' => 'form-control')) !!}
                        <span class="code-error hidden">Wrong pin code entered.</span>
                    </div>
                    <div class="col-sm-12">
                        {{ Form::label('reason', 'Reason for Change:') }}
                        {{ Form::text('reason', '', array('class' => 'form-control')) }}
                    </div>
                </div> --}}

                {{ Form::close() }}

                <div class="row qty-template hidden">
                    <div class="col-sm-2">
                        {!! Form::hidden('location[][id]', '', array('class' => 'loc-id')) !!}
                        {!! Form::text('name[]', '', array('disabled' => 'disabled', 'class' => 'loc-name')) !!}
                    </div>
                    <div class="col-sm-2">
                        {!! Form::text('room[]', '', array('disabled' => 'disabled', 'class' => 'loc-room')) !!}
                    </div>
                    <div class="col-sm-2">
                        {!! Form::text('rack[]', '', array('disabled' => 'disabled', 'class' => 'loc-rack')) !!}
                    </div>
                    <div class="col-sm-2">
                        {!! Form::text('shelf[]', '', array('disabled' => 'disabled', 'class' => 'loc-shelf')) !!}
                    </div>
                    <div class="col-sm-2">
                        {!! Form::text('bay[]', '', array('disabled' => 'disabled', 'class' => 'loc-bay')) !!}
                    </div>
                    <div class="col-sm-2">
                        {!! Form::text('location[][qty]', '', array('class' => 'loc-qty')) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary edit-qty">Save changes</button>
            </div>
        </div>
    </div>
</div>
