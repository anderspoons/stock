<div class="modal bs-quick-order-modal-sm" id="quick-order-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add to Order</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['url' => url('orders/add'), 'method' => 'post', 'class' => 'add-item-form']) }}
                {!! Form::hidden('item_id', '', array('class' => 'item-id')) !!}
                {{ Form::hidden('item_suppliers', '', array('class' => 'item-suppliers')) }}
                @if(isset($orders))
                    @foreach($orders as $order)
                        {!! Form::hidden('supplier_ids[]', $order->supplier_id, array('data-order' => $order->id, 'data-po' => $order->po, 'data-name' => $order->supplier_name, 'class' => 'supplier-id')) !!}
                    @endforeach
                @endif
                <div class="hidden">
                    <div class="size-template">
                        {!! Form::label('size', 'Sizes / Weight:') !!}
                        <div class="size-row">
                            {!! Form::text('size', '', array('class' => 'size-unit form-control')) !!}
                        </div>
                        @include('orders.units.unit_select')
                        {{ Form::hidden('unit-report', '', array('id' => 'unit-report')) }}
                    </div>
                </div>

                <div class="form-group">
                    <div class="row ">
                        <div class="col-sm-12">
                            <div class="warning-text"></div>
                        </div>
                    </div>

                    <div class="row ">
                        <div class="col-sm-10 form-group">
                            {!! Form::label('order_id', 'Order:') !!}
                            {{--TODO perhaps colour code the options to highlight the suppliers against this item--}}
                            @include('orders.includes.orders-select')
                        </div>

                        <div class="col-sm-2 form-group">
                            <a target="_blank" href="{{ url('/orders/create') }}" class="btn btn-success new-order">New Order</a>
                        </div>
                    </div>

                    <div class="row existing-supplier-row">
                        <div class="form-group col-md-6 col-sm-12 qty-box-existing">
                            {!! Form::label('qty', 'Quantity:') !!}
                            {!! Form::number('qty', '', array('class' => 'form-control', 'min' => '0')) !!}
                        </div>

                        <div id="sizing" class="form-group col-md-6 col-sm-12">

                        </div>
                    </div>

                    {{--TODO: Size stuff for size specific items --}}
                    <div class="row new-supplier-row hidden">
                        <div class="" id="ordered_as">
                            <div class="form-group col-sm-12 col-md-6">
                                {!! Form::label('supplier_unit_category', 'Ordered As:')  !!}
                                @include('orders.units.category_select')
                            </div>

                            <div class="form-group col-sm-12 col-md-6">
                                {!! Form::label('supplier_specific_unit]', 'Sizing Unit:')  !!}
                                @include('orders.units.unit_select')
                            </div>

                            <div class="form-group col-sm-12 col-md-6">
                                {!! Form::label('supplier_lots_of', 'Ordered in Lots of:')  !!}
                                {!! Form::number('supplier_lots_of', '', array('class' => 'form-control', 'id' => 'supplier_lots_of', 'min' => '0', 'autocomplete' => 'off')) !!}
                            </div>
                        </div>

                        <div class="form-group col-md-6 col-sm-12 qty-box-new">
                            {!! Form::label('qty', 'Quantity:') !!}
                            {!! Form::number('qty', '', array('class' => 'form-control', 'id' => 'new-qty', 'min' => '0')) !!}
                        </div>

                        <div class="form-group col-md-6 col-sm-12" id="sizing-new">

                        </div>
                    </div>

                    <hr>

                    <div class="row ">
                        <div class="form-group col-md-6 col-sm-12">
                            {!! Form::label('bc', 'Ordered For:') !!}
                            <select id="order-for-select" class="form-control">
                                <option value="stock">Stock</option>
                                <option value="customer">Customer</option>
                                <option value="boat">Boat</option>
                                <option value="job">Job</option>
                                <option value="counter">Counter Sale</option>
                            </select>
                        </div>

                        <div class="form-group col-md-6 col-sm-12">
                            {!! Form::label('custom_po', 'Custom PO: (optional)') !!}
                            {!! Form::text('custom_po', '', array('class' => 'form-control', 'readonly' => 'readonly' , 'autocomplete' => 'off')) !!}
                        </div>

                        <div class="hidden customer for-group">
                            <div class="form-group col-md-6 col-sm-12">
                                {!! Form::label('customer', 'Customer: (optional)') !!}
                                @include('customers.select')
                            </div>
                        </div>

                        <div class="hidden boat for-group">
                            <div class="form-group col-md-6 col-sm-12">
                                {!! Form::label('boat', 'Boat:') !!}
                                @include('boats.select')
                            </div>
                        </div>

                        <div class="hidden job for-group">
                            <div class="form-group col-md-6 col-sm-12">
                                {!! Form::label('job', 'Job:') !!}
                                @include('jobs.select')
                            </div>
                        </div>

                        <div class="hidden counter for-group">
                            <div class="form-group col-md-6 col-sm-12">
                                {!! Form::label('boat', 'Counter:') !!}
                                @include('counters.select')
                            </div>
                        </div>

                    </div>


                    <hr>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-sm-12 col-md-6 form-group">
                                {!! Form::label('pin_id', 'Name:') !!}
                                @if(isset($pins))
                                    <select class="form-control" name="pin_id">
                                        @foreach($pins as $user)
                                            <option value="{!! $user->id !!}">{!! $user->name !!}</option>
                                        @endforeach
                                    </select>
                                @endif
                            </div>
                            <div class="col-sm-12 col-md-6 form-group">
                                {!! Form::label('pin', 'Security Pin:') !!}
                                {!! Form::password('pin', array('class' => 'form-control', 'autocomplete' => 'off')) !!}
                                <span class="code-error hidden">Wrong code entered.</span>
                            </div>
                        </div>

                    </div>
                </div>

                {{ Form::close() }}
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success add-order-item">Add to Order</button>
            </div>
        </div>
    </div>
</div>

@push('footer-script')
    <script type="text/javascript">
        (function (window, $) {
            $('body').on('click', '.add-order-item', function (e) {
                e.preventDefault();
                if ($('.code-error').is('visible')) {
                    $('.code-error').addClass('hidden');
                }
                var form = $(this).parent().parent().find('.modal-body form');
                var element = $(this);
                $.ajax({
                    type: "POST",
                    url: form.attr("action"),
                    data: form.serialize(),
                    success: function (response) {
                        element.parent().parent().find('button[data-dismiss="modal"]').click();
                        var myStack = {"dir1": "down", "dir2": "right", "push": "top"};
                        new PNotify({
                            title: "Success",
                            text: response.data,
                            addclass: "stack-custom",
                            stack: myStack
                        });
                        setTimeout(function (e) {

                        }, 1000);
                    }
                });
            });

            $('body').on('change', '#quick-order-modal .new-supplier-unit-cat', function (e) {
                console.log('Changed');
                sizeBoxes($(this), false);
                unitOptions($(this));
            });

            function HideQty(boool, existing) {
                var selector = '';
                if(existing){
                    selector = '.qty-box-existing';
                } else {
                    selector = '.qty-box-new';
                }
                if (boool == false) {
                    $('#quick-order-modal').find(selector).show();
                    $('#quick-order-modal').find('.qty-info').hide();
                } else {
                    $('#quick-order-modal').find(selector).hide();
                    $('#quick-order-modal').find('.qty-info').show();
                }
            }

            function sizeBoxes(select, existing) {
                if($(select).find('option:selected').attr('size-num') > 0){
                    var sizes = $('#quick-order-modal .size-template').clone();
                    var selector = '';
                    if(existing) {
                        selector = '#sizing';
                        HideQty(true, false);
                    } else {
                        selector = '#sizing-new';
                        HideQty(true, false);
                    }
                    $('#order-item-details-modal '+selector).html(sizes.html());
                    for(var i = 1; i < parseInt($(select).find('option:selected').attr('size-num')); i++){
                        $('#order-item-details-modal '+ selector + ' .size-row').eq(0).clone().insertAfter($('#order-item-details-modal '+selector).find('.size-row'));
                    }
                    if(!existing) {
                        $('#order-item-details-modal '+ selector + ' .new-supplier-unit').remove();
                    }
                } else {
                    if(existing) {
                        HideQty(false, true);
                        HideSizes(true);
                    } else {
                        HideQty(false, false);
                        HideSizes(false);
                    }
                    console.log('Remove sizes show qty.');
                }
            }

            function unitOptions(select){
                var cat = $(select).find('option:selected').val();
                if(cat == '3'){
                    cat = '2';
                }
                if(cat == '7'){
                    return false;
                }
                $('#quick-order-modal .new-supplier-unit option').removeClass('hidden');
                $.each($('#quick-order-modal .new-supplier-unit option'), function(e){
                    if(parseInt($(this).attr('data-cat')) != parseInt(cat)){
                        $(this).addClass('hidden');
                    }
                });
            }

            function HideSizes(existing) {
                var selector = '';
                if(existing){
                    selector = '#quick-order-modal #sizing';
                } else {
                    selector = '#quick-order-modal #sizing-new';
                }
                $(selector).html('');
                $('.bs-details-modal-sm').find('.qty-info').hide();
            }

            //ID is the category of units
            //Unit_id is the unit that its set to report as for that category.
            function doUnits(id, new_supplier) {
                var selector = '#quick-order-modal .existing-supplier-row .new-supplier-unit';
                if(new_supplier){
                    selector = '#quick-order-modal .new-supplier-row .new-supplier-unit';
                }
                var cat = id;
                if(cat == '3'){
                    cat = '2';
                }
                if(cat == '7'){
                    //show all.
                    $(selector + ' option').removeClass('hidden');
                    return false;
                }
                $(selector + ' option').removeClass('hidden');
                $.each($(selector + ' option'), function(e){
                    if(parseInt($(this).attr('data-cat')) != parseInt(cat) && parseInt($(this).attr('data-cat')) != 999){
                        $(this).addClass('hidden');
                    }
                });
                $(selector + ' option').eq(0).prop('selected', true);
            }

        })(window, jQuery);
    </script>
@endpush