<div class="modal fade bs-quick-price-modal-sm" id="price-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Quick Price Adjust</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['url' => 'item/price', 'method' => 'post', 'class' => 'quick-price-form']) }}
                {!! Form::hidden('item_id', '', array('class' => 'item-id')) !!}
                <div class="row">
                    <div class="col-sm-6">
                        {!! Form::label('supplier[]', 'Supplier: (optional)') !!}
                    </div>
                    <div class="col-sm-6">
                        {!! Form::label('price[]', 'Cost: (£)') !!}
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6 supplier-price">
                        <select name="supplier" class="form-control supplier-select">
                            <option value=""></option>
                        </select>
                    </div>
                    <div class="col-sm-6">
                        {!! Form::text('price', '', array('class' => 'form-control price-input')) !!}
                    </div>
                </div>
                {{ Form::close() }}
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary add-price">Add Price</button>
            </div>
        </div>
    </div>
</div>
