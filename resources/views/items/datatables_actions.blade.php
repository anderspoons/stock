<div class='btn-group action-btns'>
    <a href="{!! route('items.show', [$id]) !!}" class="btn btn-default btn-xs">
        <i data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class='action-tooltip'>Show Item Details</span>" class="glyphicon glyphicon-eye-open"></i>
    </a>
    <a href="{!! route('items.edit', [$id]) !!}" class="btn btn-default btn-xs">
        <i data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class='action-tooltip'>Edit Item Details</span>" class="glyphicon glyphicon-edit"></i>
    </a>
    <a href="#" id='{!! $id !!}' class="btn quick-order btn-default btn-xs" data-toggle="modal" data-target=".bs-quick-order-modal-sm">
        <i data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class='action-tooltip'>Add To Order</span>" class="quick-order fa fa-truck"></i>
    </a>
    <a href="#" id='{!! $id !!}' class="btn btn-default btn-xs" data-toggle="modal" data-target=".bs-quick-qty-modal-sm">
        <i data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class='action-tooltip'>Quick Qty Adjustment</span>" class="quick-qty glyphicon glyphicon-sort"></i>
    </a>
    <a href="#" id="{!! $id !!}" class="btn btn-default btn-xs" data-toggle="modal" data-target=".bs-quick-price-modal-sm">
        <i data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class='action-tooltip'>Quick Price Adjustment</span>" class="quick-price glyphicon glyphicon-gbp"></i>
    </a>
    <a href="#" id="{!! $id !!}" class="btn btn-default btn-xs" data-toggle="modal" data-target=".bs-quick-internal-transfer-modal-sm">
        <i data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class='action-tooltip'>Internal Transfer</span>" class="quick-price glyphicon glyphicon-resize-small"></i>
    </a>
    <a target="_blank"  href="barcode/{!! $barcode !!}/small" class="btn btn-default btn-xs">
        <i data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class='action-tooltip'>Show Barcodes</span>" class="glyphicon glyphicon-barcode"></i>
    </a>
</div>
