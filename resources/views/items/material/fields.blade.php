<div class="form-group col-sm-12">
    <h3>Material</h3>
    <div class="item-row">
        @if(isset($item))
            @foreach($item->materials as $material)
                <div class="row">
                    <div class="form-group col-sm-11">
                        {!! Form::text('materials['.$material->id.']', $material->name, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group col-sm-1">
                        {!! Form::button('<i class="glyphicon glyphicon-remove"></i>', ['class' => 'remove-item btn btn-default']) !!}
                    </div>
                </div>
            @endforeach
        @endif
    </div>

    <div class="row">
        <div class="form-group col-sm-12">
            {!! Form::button('<i class="glyphicon glyphicon-plus"></i>  Add', ['class' => 'add-item btn btn-default']) !!}
        </div>
    </div>

    <div class="row item-template hidden">
        <div class="form-group col-sm-11">
            {!! Form::text('materials[]', '', ['class' => 'form-control']) !!}
        </div>
        <div class="form-group col-sm-1">
            {!! Form::button('<i class="glyphicon glyphicon-remove"></i>', ['class' => 'remove-item btn btn-default']) !!}
        </div>
    </div>
</div>