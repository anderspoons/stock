<div class="form-group col-sm-6">
    {!! Form::label('', 'Materials:') !!}
    <div class="row">
        @if(isset($item))
            @foreach($item->materials as $material)
                <div class="form-group col-sm-11">
                    {!! $material->name !!}
                </div>
            @endforeach
        @else
            <div class="form-group col-sm-11">
                &nbsp;
            </div>
        @endif
    </div>
</div>