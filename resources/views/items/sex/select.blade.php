<div class="form-group col-sm-6">
@if(isset($sexes))
    @if(isset($size->id))
        <select class="sex-select form-control" name='sizes[][sex]'>
            <?php $set = ''; ?>
            @if(count($item->sexes) > 0)
                @foreach($item->sexes as $sex)
                    @if($sex->id == $size->pivot->sex_id)
                        <option value='{{ $sex->id }}' selected>{{ $sex->name }}</option>
                        <?php $set = $sex->id; ?>
                    @endif
                @endforeach
            @endif

            <option value='' ></option>
            @foreach($sexes as $sex)
                @if($set != $sex->id)
                    <option value='{{ $sex->id }}'>{{ $sex->name }}</option>
                @endif
            @endforeach
        </select>
    @else
        <select class="sex-select form-control" name='sizes[][sex]'>
            <option value='' selected></option>
            @foreach($sexes as $sex)
                <option value='{{ $sex->id }}'>{{ $sex->name }}</option>
            @endforeach
        </select>
    @endif
@else
    "Problem Here"
@endif
</div>