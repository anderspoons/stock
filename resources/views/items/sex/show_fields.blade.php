<div class="form-group col-sm-6">
    {!! Form::label('', 'Sexes:') !!}
    <div class="row">
        @if(isset($item))
            @foreach($item->sexes as $sex)
                @if($sex->pivot->size_id == 0)
                <div class="form-group col-sm-5">
                    {!! $sex->name !!}
                </div>
                @endif
            @endforeach
        @else
            <div class="form-group col-sm-11">
                &nbsp;
            </div>
        @endif
    </div>
</div>