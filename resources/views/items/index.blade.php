@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Items</h1>
        <h1 class="pull-right">
            <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px"
               href="{!! route('items.create') !!}">Add New</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        {{--@include('laravelPnotify::notify')--}}
        @include('flash::message')

        <div class="clearfix"></div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3 col-lg-2 no-left-gutter">
                    <div class="box box-primary">
                        <div class="box-body">
                            @include('items.filters')
                        </div>
                    </div>
                </div>

                <div class="col-md-9 col-lg-10 no-right-gutter">
                    <div class="box box-primary">
                        <div class="box-body">

                            @include('items.table')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@include('items.modal.quickprice')
@include('items.modal.quickqty')
@include('items.modal.quickorder')