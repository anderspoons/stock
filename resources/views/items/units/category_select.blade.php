{{-- 0 Is never going to be used --}}
<?php $cat_id = 0; ?>
<select name="supplier[][category_id]" class="form-control unit-cat">
    @if(isset($item->supplier_units) && isset($supplier))
        @foreach($item->supplier_units as $unit)
            @if($unit->pivot->supplier_id == $supplier->id)
                <?php $cat_id = $unit->pivot->unit_cat_id; ?>
                @break
            @endif
        @endforeach
    @endif

    @if($cat_id == 0)
        <option value=""></option>
    @endif

    @foreach($category as $cat)
        @if($cat_id == 0 && strtolower($cat->name) == 'quantity')
            <option selected="selected" size-num="{!! $cat->num_sizes !!}" value="{!! $cat->id !!}">{!! $cat->name !!}</option>
        @elseif($cat->id == $cat_id)
            <option selected="selected" size-num="{!! $cat->num_sizes !!}" value="{!! $cat->id !!}">{!! $cat->name !!}</option>
        @else
            <option size-num="{!! $cat->num_sizes !!}" value="{!! $cat->id !!}">{!! $cat->name !!}</option>
        @endif
    @endforeach
</select>
