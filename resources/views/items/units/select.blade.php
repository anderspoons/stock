<select name="supplier[][unit_id]" class="form-control">
    <option value=""></option>
    @foreach($units as $unit)
        <option value="{!! $unit->id !!}">{!! $unit->name !!} ({!! $unit->short !!})</option>
    @endforeach
</select>