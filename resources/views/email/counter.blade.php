<html>
<head>
    <style>

    </style>
</head>
<body>
<p>
    Dear Sir/Madam,<br>
    <br>
    Thank you for your recent order.<br>
    <br>
    Please find attached the invoice for this order.<br>
    <br>
    If you have any queries regarding this invoice please contact us on the number below.<br>
    <br>
    {{ $data['message'] }}
    <br>
    <br>
    Regards,<br>
    <br>
    Macduff Shipyards Limited<br>
    The Harbour<br>
    Macduff<br>
    AB44 1QT<br>
    Telephone: 01261 832234<br>
</p>
</body>
</html>