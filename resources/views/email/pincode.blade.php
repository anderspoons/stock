<html>
<head>
    <style>

    </style>
</head>
<body>
    <p>
        Hi {{ $name }},
        <br><br>
        A pincode has been generated for your use within the Macduff Shipyards Stock System, keep this pincode safe and secure this code is personal to yourself.<br>
        This code is used to identify you if you perform actions such as deleting stock.
        <br><br>
        Pincode: {{ $code }}
        <br><br>
        Regards,<br>
        Macduff Stock System.
    </p>
    <p>
        If you feel you have received this email in error please contact <a href="mailto:support@boxportable.com">support@boxportable.com</a> and we will look into why this has happened.
    </p>
</body>
</html>