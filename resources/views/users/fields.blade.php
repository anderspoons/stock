<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('store_id', 'Default Store:') !!}
    @if(isset($user))
        {!! Form::select('store_id', $stores, $user->store_id, ['class' => 'form-control']) !!}
    @else
        {!! Form::select('store_id', $stores, null, ['class' => 'form-control']) !!}
    @endif
</div>

<!-- Active Field -->
<div class="form-group col-sm-6">
    <br>
    @if(isset($user) && $user->active == 1)
        {{ Form::checkbox('active', '1', true, ['class' => 'icheck']) }}
    @else
        {{ Form::checkbox('active', '1', false, ['class' => 'icheck']) }}
    @endif
    {!! Form::label('active', '  Active?') !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('users.index') !!}" class="btn btn-default">Cancel</a>
</div>
