@extends('layouts.app')

@section('content')
   <section class="content-header">
           <h1>
               Sex
           </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">

           <div class="box-body">
               <div class="row">
                   {!! Form::model($sex, ['route' => ['sexes.update', $sex->id], 'method' => 'patch']) !!}

                    @include('sexes.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection