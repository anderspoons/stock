<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $transfer->id !!}</p>
</div>

<!-- Item Id Field -->
<div class="form-group">
    {!! Form::label('item_id', 'Item Id:') !!}
    <p>{!! $transfer->item_id !!}</p>
</div>

<!-- Location From Field -->
<div class="form-group">
    {!! Form::label('location_from', 'Location From:') !!}
    <p>{!! $transfer->location_from !!}</p>
</div>

<!-- Location To Field -->
<div class="form-group">
    {!! Form::label('location_to', 'Location To:') !!}
    <p>{!! $transfer->location_to !!}</p>
</div>

<!-- Qty Field -->
<div class="form-group">
    {!! Form::label('qty', 'Qty:') !!}
    <p>{!! $transfer->qty !!}</p>
</div>

<!-- Note Field -->
<div class="form-group">
    {!! Form::label('note', 'Note:') !!}
    <p>{!! $transfer->note !!}</p>
</div>

<!-- Pin Id Request Field -->
<div class="form-group">
    {!! Form::label('pin_id_request', 'Pin Id Request:') !!}
    <p>{!! $transfer->pin_id_request !!}</p>
</div>

<!-- For Type Field -->
<div class="form-group">
    {!! Form::label('for_type', 'For Type:') !!}
    <p>{!! $transfer->for_type !!}</p>
</div>

<!-- For Id Field -->
<div class="form-group">
    {!! Form::label('for_id', 'For Id:') !!}
    <p>{!! $transfer->for_id !!}</p>
</div>

