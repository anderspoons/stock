<!-- Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id', 'Id:') !!}
    {!! Form::number('id', null, ['class' => 'form-control']) !!}
</div>

<!-- Item Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('item_id', 'Item Id:') !!}
    {!! Form::number('item_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Location From Field -->
<div class="form-group col-sm-6">
    {!! Form::label('location_from', 'Location From:') !!}
    {!! Form::number('location_from', null, ['class' => 'form-control']) !!}
</div>

<!-- Location To Field -->
<div class="form-group col-sm-6">
    {!! Form::label('location_to', 'Location To:') !!}
    {!! Form::number('location_to', null, ['class' => 'form-control']) !!}
</div>

<!-- Qty Field -->
<div class="form-group col-sm-6">
    {!! Form::label('qty', 'Qty:') !!}
    {!! Form::number('qty', null, ['class' => 'form-control']) !!}
</div>

<!-- Note Field -->
<div class="form-group col-sm-6">
    {!! Form::label('note', 'Note:') !!}
    {!! Form::text('note', null, ['class' => 'form-control']) !!}
</div>

<!-- Pin Id Request Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pin_id_request', 'Pin Id Request:') !!}
    {!! Form::number('pin_id_request', null, ['class' => 'form-control']) !!}
</div>

<!-- For Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('for_type', 'For Type:') !!}
    {!! Form::text('for_type', null, ['class' => 'form-control']) !!}
</div>

<!-- For Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('for_id', 'For Id:') !!}
    {!! Form::number('for_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('transfers.index') !!}" class="btn btn-default">Cancel</a>
</div>
