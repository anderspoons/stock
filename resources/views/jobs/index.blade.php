@extends('layouts.app')

@section('content')
{{ print_r(request()->session()->get('filters.jobs')) }}<br>
    <section class="content-header">
        <h1 class="pull-left">Jobs</h1>
        <h1 class="pull-right">
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('jobs.create') !!}">Add New</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3 col-lg-2 no-left-gutter">
                    <div class="box box-primary">
                        <div class="box-body">
                            @include('jobs.filters')
                        </div>
                    </div>
                </div>

                <div class="col-md-9 col-lg-10 no-right-gutter">
                    <div class="box box-primary">
                        <div class="box-body">
                            @include('jobs.table')
                            <br>
                            <a href="#" class="btn btn-warning pull-right" data-toggle="modal" data-target=".bs-close-modal-sm"><i
                                        class="close-selected-jobs glyphicon glyphicon-remove"></i> Close Selected Jobs</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('issue.transfer_as_counter_modal')
@endsection
@include('jobs.add-close')
@include('jobs.scripts')