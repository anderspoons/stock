@if(isset($job->customer))
    <tr>
        <td>{{ $job->customer->name }}</td>
        <td></td>
        <td stye="align:right; text-align:right;">
            @if(!isset($job->boat))
                {{ date('d/m/Y') }}
            @endif
        </td>
    </tr>

    @if(isset($job->customer->address->line_1) && strlen($job->customer->address->line_1) > 0)
        <tr>
            <td>{{ $job->customer->address->line_1 }}</td>
            <td></td>
            <td></td>
        </tr>
    @endif

    @if(isset($job->customer->address->line_2) && strlen($job->customer->address->line_2) > 0)
        <tr>
            <td>{{ $job->customer->address->line_2 }}</td>
            <td></td>
            <td></td>
        </tr>
    @endif

    @if(isset($job->customer->address->line_3) && strlen($job->customer->address->line_3) > 0)
        <tr>
            <td>{{ $job->customer->address->line_3 }}</td>
            <td></td>
            <td></td>
        </tr>
    @endif

    @if(isset($job->customer->address->town) && strlen($job->customer->address->town) > 0)
        <tr>
            <td>{{ $job->customer->address->town }}</td>
            <td></td>
            <td></td>
        </tr>
    @endif

    @if(isset($job->customer->address->county) && strlen($job->customer->address->county) > 0)
        <tr>
            <td>{{ $job->customer->address->county }}</td>
            <td></td>
            <td>INVOICE: {{ $job->sage_invoice }}</td>
        </tr>
    @else
        <tr>
            <td></td>
            <td></td>
            <td>INVOICE: {{ $job->sage_invoice }}</td>
        </tr>
    @endif

    <tr>
        @if(isset($job->customer->address->postcode) && strlen($job->customer->address->postcode) > 0)
            <td>{{ $job->customer->address->postcode }}</td>
            <td></td>
            <td></td>
        @else
            <td></td>
            <td></td>
            <td></td>
        @endif
    </tr>

@else
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
@endif