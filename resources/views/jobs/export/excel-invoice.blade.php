@php
    $items_start = false;
    $carriage_start = false;
    $address_done = false;
    $count = 1;
@endphp

<html>
<head>
    <style>
    </style>
</head>

<body>
<table>
    {{--<tr>--}}
    {{--@include('jobs.export.includes.excel-logo')--}}
    @include('jobs.export.excel-boat')
    @include('jobs.export.excel-customer')


    {{--</tr>--}}

    {{--@include('jobs.export.includes.excel-address')--}}

    @if(strlen($job->po) > 0)
        <tr>
            <td><b>Purchase Order: {{ $job->po }}</b></td>
        </tr>
    @endif

    @if(strlen($job->work_scope) > 0)
        <tr>
            <td style="wrap-text:true; height:{{ ceil((float)(strlen($job->work_scope) / 37)) * 20 }}px;" colspan="2">
                {!! nl2br($job->work_scope) !!}
            </td>
        </tr>
    @endif

    @if(isset($list))
        @foreach($list as $item)
            @if($items_start == false && $item['type'] == 'item')
                @php
                    $items_start = true;
                @endphp
                <tr>
                    <td></td><td></td><td></td>
                </tr>
                <tr>
                    <td>MATERIALS</td><td></td><td></td>
                </tr>
            @endif

            @if($carriage_start == false && $item['type'] == 'carriage')
                @php
                    $carriage_start = true;
                @endphp

                <tr>
                    <td></td><td></td><td></td>
                </tr>
            @endif
            <tr>
                <td>
                    @if($items_start && $item['type'] != 'carriage')
                        {{ $item['qty'] }} -
                    @endif
                    {{ $item['details'] }}
                </td>

                <td></td>

                <td>
                    @if(strlen($item['net_cost'] ) > 0)
                    {{ $item['net_cost'] }}
                    @endif
                </td>
            </tr>
        @endforeach

    @endif

    {{--<tr>--}}
    {{--@include('jobs.export.includes.excel-footer')--}}
    {{--</tr>--}}
</table>
</body>
</html>