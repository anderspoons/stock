<table>
    @php
        $page = 1;
        $new_page = true;
        $row = 2;
        $last_reset = 2;
        $last_description = '';
    @endphp

    @if(isset($store))
        @foreach($store as $location)

            @if($new_page == true)
                <thead>
                <tr>
                    <th>{{ $location->store->name }}</th>
                    <th>Quantity</th>
                    <th>Details</th>
                    <th></th>
                    <th></th>
                    <th>{{ $page }}</th>
                </tr>
                </thead>

                @php
                    $new_page = false;
                @endphp
            @endif

            <tbody>
            @include('reports.excel.includes.stock-value-row')
            </tbody>

            @if(($row - $last_reset) > 49 ){
            <tfoot>
            <tr>
                <th>TOTAL</th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            </tfoot>
            @php
                $last_reset = $row;
                $page++;
                $new_page = true;
            @endphp
            @endif

            @php
                $row++;
            @endphp
        @endforeach
    @endif
</table>
