

{{-- Company Info --}}
<tr>
    <td>Directors:</td><td></td><td></td>
</tr>
<tr>
    <td>J. Watt,</td><td></td><td></td>
</tr>
<tr>
    <td>C. L. Ritchie, R. Macleod</td><td></td><td></td>
</tr>
<tr>
    <td>Registered in Scotland No: 63241</td><td></td><td></td>
</tr>
<tr>
    <td>Registered Office</td><td></td><td></td>
</tr>
<tr>
    <td>The Harbour, Macduff AB44 1QT</td>
    <td colspan="2">
        Any queries regarding this invoice must be raised within 30 days
    </td>
</tr>
<tr>
    <td>V.A.T. Reg No. 265 9972 00</td>
    <td colspan="2">
        Seller retains right of ownership until payment for goods has been received
    </td>
</tr>
