<tr>
    @if(isset($location->item->description))
        <td>{{ $location->item->description }}</td>
    @else
        <td></td>
    @endif
    <td>{{ $location->qty }}</td>
    @if(isset($location->item->types))
        <td>
            @foreach($location->item->types as $type)
                {{ $type->name }}&nbsp;
            @endforeach
        </td>
    @else
        <td></td>
    @endif
    @if(is_object($location->item->last_price->first()))
        <td>{{ $location->item->last_price->first()->cost }}</td>
    @else
        <td>0</td>
    @endif
    <td>=B{{ $row }}*E{{ $row }}</td>
</tr>