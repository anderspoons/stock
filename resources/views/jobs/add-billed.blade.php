<div class="modal fade bs-add-billed-modal-sm" id="billed-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirm Job Billed</h4>
            </div>
            <div class="modal-body">
                {{-- Below we need to work out which thing were deleteling Here, item, supplier, customer --}}
                {{ Form::open(['url' => 'jobs/add_billed', 'method' => 'post', 'class' => 'delete-form']) }}
                {!! Form::hidden('job_id', $job->id, array('class' => 'job-id')) !!}
                <div class="row">
                    <div class="col-sm-12">
                        <p>Warning you are about to billed this job off, are you sure?</p>
                        {{-- <p>Warning you are about to billed this job off, to do this please select your name and enter your security pin below.</p> --}}
                    </div>
                </div>

                {{-- <div class="form-group">
                    <div class="col-sm-12 col-md-6 form-group">
                        {!! Form::label('pin_id', 'Name:') !!}
                        @if(isset($pins))
                            <select class="form-control" name="pin_id">
                                @foreach($pins as $user)
                                    <option value="{!! $user->id !!}">{!! $user->name !!}</option>
                                @endforeach
                            </select>
                        @endif
                    </div>
                    <div class="col-sm-12 col-md-6 form-group">
                        {!! Form::label('pin', 'Security Pin:') !!}
                        {!! Form::password('pin', array('class' => 'form-control', 'id' => 'pin-billed')) !!}
                        <span class="code-billed-error hidden">Wrong pin code entered.</span>
                    </div>
                </div> --}}

                {{ Form::close() }}
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger billed-job">Confirm Billed</button>
            </div>
        </div>
    </div>
</div>


@push('footer-script')
<script type="text/javascript">
    (function(window, $) {
    $('body').on('click', '.billed-job', function(e){
        e.preventDefault();
        $('#pin').css({'border' : 'none'});
        if($('.code-error').is('visible')) {
            $('.code-error').addClass('hidden');
        }
        var form = $(this).parent().parent().find('.modal-body form');
        var element = $(this);
        $.ajax({
            type:"POST",
            url:form.attr("action"),
            data:form.serialize(),
            success: function(response){
                element.parent().parent().find('button[data-dismiss="modal"]').click();
                var myStack = {"dir1":"down", "dir2":"right", "push":"top"};
                new PNotify({
                    title: "Success",
                    text: response.data,
                    addclass: "stack-custom",
                    stack: myStack
                })
                setTimeout(function(e){
                    window.location = '/jobs/';
                }, 1000);
            }
        }).error(function(jqxhr, settings, thrownError){
            var object = JSON.parse(jqxhr.responseText);
            console.log(object);
            $('#pin-billed').css({'border' : 'thin solid #ff3333'});
            if(!$('.code-billed-error').is('visible')) {
                $('.code-billed-error').removeClass('hidden');
            }
        });
    });
    })(window, jQuery);
</script>
@endpush