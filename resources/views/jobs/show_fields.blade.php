<div class="row">
    <!-- Name Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('name', 'Name:') !!}
        <p>{!! $job->name !!}</p>
    </div>

    <!-- Note Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('note', 'Note:') !!}
        <p>{!! $job->note !!}</p>
    </div>


    <!-- PO Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('po', 'PO:') !!}
        <p>{!! $job->po !!}</p>
    </div>

    <!-- PO Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('po', 'Sage Invoice Number:') !!}
        <p>{!! $job->sage_invoice !!}</p>
    </div>

    <!-- Created At Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('created_at', 'Created At:') !!}
        <p>{!! Date('d/m/y', strtotime($job->created_at)) !!}</p>
    </div>

    <!-- Updated At Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('closed_at', 'Job Closed At:') !!}
        <p>{!! Date('d/m/y', strtotime($job->closed_at)) !!}</p>
    </div>
</div>

<div class="row">

    <!-- Customer Details Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('customer_name', 'Customer:') !!} {{ $customer[0]->name }} <div class="clear"></div>
        {!! Form::label('customer_sage', 'Sage Ref:') !!} {{ $customer[0]->sage_ref }} <div class="clear"></div>
        {!! Form::label('customer_phone', 'Phone:') !!} {!! $customer[0]->phone !!} <div class="clear"></div>
        {!! Form::label('customer_fax', 'Fax:') !!} {!! $customer[0]->fax !!} <div class="clear"></div>
        {!! Form::label('customer_email', 'Email:') !!} {!! $customer[0]->email !!} <div class="clear"></div>

        {!! Form::label('customer_address', 'Customer Address:') !!} <div class="clear"></div>
        @if(!empty($cust_address[0]->line_1))
            {!! $cust_address[0]->line_1 !!}<div class="clear"></div>
        @endif
        @if(!empty($cust_address[0]->line_2))
            {!! $cust_address[0]->line_2 !!}<div class="clear"></div>
        @endif
        @if(!empty($cust_address[0]->line_3))
            {!! $cust_address[0]->line_3 !!}<div class="clear"></div>
        @endif
        @if(!empty($cust_address[0]->town))
            {!! $cust_address[0]->town !!}<div class="clear"></div>
        @endif
        @if(!empty($cust_address[0]->county))
            {!! $cust_address[0]->county !!}<div class="clear"></div>
        @endif
        @if(!empty($cust_address[0]->postcode))
            {!! $cust_address[0]->postcode !!}<div class="clear"></div>
        @endif
    </div>

    <!-- Boat Info Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('boat_name', 'Boat Name:') !!} {!! $boat->name !!}<div class="clear"></div>
        {!! Form::label('boat_number', 'Boat Number:') !!} {!! $boat->number !!}<div class="clear"></div>
        {!! Form::label('boat_sage_ref', 'Sage Ref:') !!} {!! $boat->sage_ref !!}<div class="clear"></div>
    </div>


</div>

