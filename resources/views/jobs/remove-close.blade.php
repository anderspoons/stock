<div class="modal fade bs-remove-close-modal-sm" id="close-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Confirm Billed Status Removal</h4>
            </div>
            <div class="modal-body">
                {{-- Below we need to work out which thing were deleteling Here, item, supplier, customer --}}
                @if(isset($job->id))
                    {{ Form::open(['url' => 'jobs/remove_close', 'method' => 'post', 'class' => 'delete-form']) }}
                    {!! Form::hidden('job_id', $job->id, array('class' => 'job-id')) !!}
                @endif
                <div class="row">
                    <div class="col-sm-12">
                        <p>Warning you are about to remove the close status from this job sale, are you sure?</p>
                    </div>
                </div>

{{--                 <div class="form-group">
                    <div class="col-sm-12 col-md-6 form-group">
                        {!! Form::label('pin_id', 'Name:') !!}
                        @if(isset($pins))
                            <select class="form-control" name="pin_id">
                                @foreach($pins as $user)
                                    <option value="{!! $user->id !!}">{!! $user->name !!}</option>
                                @endforeach
                            </select>
                        @endif
                    </div>
                    <div class="col-sm-12 col-md-6 form-group">
                        {!! Form::label('pin', 'Security Pin:') !!}
                        {!! Form::password('pin', array('class' => 'form-control', 'id' => 'pin-close')) !!}
                        <span class="code-close-error hidden">Wrong pin code entered.</span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12 form-group">
                        {!! Form::label('reason', 'Reason for Billed Status Removal:') !!}
                        {!! Form::text('reason', '', array('class' => 'form-control')) !!}
                    </div>
                </div> --}}

                {{ Form::close() }}
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger remove-close-job">Confirm</button>
            </div>
        </div>
    </div>
</div>


@push('footer-script')
<script type="text/javascript">
    (function(window, $) {
    $('body').on('click', '.remove-close-job', function(e){
        e.preventDefault();
        // $('#pin').css({'border' : 'none'});
        // if($('.code-error').is('visible')) {
        //     $('.code-error').addClass('hidden');
        // }
        var form = $('.bs-remove-close-modal-sm form');
        var element = $(this);
        $.ajax({
            type:"POST",
            url:form.attr("action"),
            data:form.serialize(),
            success: function(response){
                element.parent().parent().find('button[data-dismiss="modal"]').click();
                var myStack = {"dir1":"down", "dir2":"right", "push":"top"};
                new PNotify({
                    title: "Success",
                    text: response.data,
                    addclass: "stack-custom",
                    stack: myStack
                })
                setTimeout(function(e){
                    window.location.reload();// = '/jobs/';
                }, 1000);
            }
        }).error(function(jqxhr, settings, thrownError){
            var object = JSON.parse(jqxhr.responseText);
            console.log(object);
            $('#pin-close').css({'border' : 'thin solid #ff3333'});
            if(!$('.code-close-error').is('visible')) {
                $('.code-close-error').removeClass('hidden');
            }
        });
    });
    })(window, jQuery);
</script>
@endpush