<select name="job_id"
        @if(isset($billed))
        @if($billed)
        disabled
        @endif
        @endif
        class="form-control job-select" id="job-select">
</select>

@push('footer-script')
<script type="text/javascript">
    (function (window, $) {
        jQuery(document).ready(function ($) {
            $(".job-select").each(function(e) {
                $(this).select2({
                ajax: {
                    url: "{!! url('job/json') !!}",
                    dataType: "json",
                    data: function (term) {
                        return {query: term.term}
                    },
                    results: function (data) {
                        return {
                            results: data.results
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data.results, function (item) {
                                console.log(item);
                                return {
                                    text: item.text,
                                    id: item.id,
                                }
                            })
                        };
                    }
                },
                createSearchChoice: function (term, data) {
                    if ($(data.results).filter(function () {
                            return this.text.localeCompare(term) === 0;
                        }).length === 0) {
                        return {
                            id: term,
                            text: term
                        };
                    }
                },
                width: '100%',
                });
            });
        });
    })(window, jQuery);
</script>
@endpush