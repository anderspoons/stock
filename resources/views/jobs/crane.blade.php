@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">
            Job - Crane work added to this job. @include('jobs.title')
        </h1>
        <div class="pull-right">
            @if(!$billed)
                <a class="btn btn-primary" data-toggle="modal" line-id="{{ $job->id }}" data-target=".bs-add-crane-modal-sm">Add New</a>
            @endif
        </div>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                {!! $dataTable->table(['width' => '100%']) !!}
                <a href="{!! route('jobs.index') !!}" class="btn btn-default">Back</a>
            </div>
        </div>
    </div>
@endsection

@include('issue.modals.delete_line')
@include('issue.modals.cost_line')
@include('issue.modals.add_crane')


@push('footer-script')
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
    <script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
    <script src="{{ url('vendor/datatables/buttons.server-side.js') }}"></script>
    {!! $dataTable->scripts() !!}
@endpush