@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Job
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                {!! Form::model($job, ['route' => ['jobs.update', $job->id], 'method' => 'patch']) !!}

                <div class="row">
                    @include('jobs.fields')
                </div>
                <div class="row">
                    <!-- Submit Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                        <a href="{!! route('jobs.index') !!}" class="btn btn-default">Cancel</a>
                    </div>

                    <div class="form-group col-sm-6 right">
                        @if($delete)
                            <a href="#" class="btn btn-danger" data-toggle="modal" data-target=".bs-delete-modal-sm"><i
                                    class="delete-item glyphicon glyphicon-remove"></i>Delete Job</a>
                        @endif

                        @if(!$closed && !$billed)
                            &nbsp;
                            <a href="#" class="btn btn-warning" data-toggle="modal" data-target=".bs-close-modal-sm"><i
                                        class="close-job glyphicon glyphicon-remove"></i>Close Job</a>
                        @endif

                        @if(!$billed && $closed)
                            &nbsp;
                            <a href="#" class="btn btn-primary" data-toggle="modal" data-target=".bs-add-billed-modal-sm"><i
                                        class="billed-job glyphicon glyphicon-remove"></i>Mark as Billed</a>
                        @endif

                        @if($closed && !$billed)
                            &nbsp;
                            <a href="#" class="btn btn-warning" data-toggle="modal"
                               data-target=".bs-remove-close-modal-sm"><i
                                        class="remove-close-job glyphicon glyphicon-remove"></i>Remove Close Status</a>
                        @endif

                        @if($billed)
                            &nbsp;
                            <a href="#" class="btn btn-warning" data-toggle="modal"
                               data-target=".bs-remove-billed-modal-sm"><i
                                        class="remove-billed-job glyphicon glyphicon-remove"></i>Remove Billed Lock</a>
                        @endif
                    </div>
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@include('shared.modal.delete')
@if(!$closed && !$billed)
    @include('jobs.add-close')
@endif
@if($closed && !$billed)
    @include('jobs.remove-close')
@endif
@if(!$billed && $closed)
    @include('jobs.add-billed')
@endif
@if($billed && !$closed)
    @include('jobs.remove-billed')
@endif
