@if(isset($job))
    @if(isset($job->boat))
        &nbsp;Boat:{{ $job->boat->name }}
    @elseif(isset($job->customer))
        &nbsp;Customer:{{ $job->customer->name }}
    @endif

    @if(strlen($job->note) > 0)
        &nbsp;Note:{{ $job->note }}
    @endif
@endif
