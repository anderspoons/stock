


<!-- Boat Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('boat_id', 'Boat:') !!}
    @include('boats.select')
</div>

<!-- Customer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_id', 'Customer:') !!}
    @include('customers.select')
</div>

<!-- Note Field -->
<div class="form-group col-sm-6">
    {!! Form::label('note', 'Note:') !!}
    @if($billed)
        {!! Form::textarea('note', null, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
    @else
        {!! Form::textarea('note', null, ['class' => 'form-control']) !!}
    @endif
</div>

<!-- Note Field -->
<div class="form-group col-sm-6">
    {!! Form::label('note', 'Work Scope:') !!}
    @if($billed)
        {!! Form::textarea('work_scope', null, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
    @else
        {!! Form::textarea('work_scope', null, ['class' => 'form-control']) !!}
    @endif
</div>

<!-- PO Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('po', 'Customer PO:') !!}
    @if($billed)
        {!! Form::text('po', null, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
    @else
        {!! Form::text('po', null, ['class' => 'form-control']) !!}
    @endif
</div>

<!-- Sage Invoice Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sage_invoice', 'Sage Invoice:') !!}
    @if($billed)
        {!! Form::text('sage_invoice', null, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
    @else
        {!! Form::text('sage_invoice', null, ['class' => 'form-control']) !!}
    @endif
</div>


@include('jobs.elements')

@include('shared.status')
