<div class="form-group col-md-6 col-sm-12">
    {!! Form::label('element_ids', 'Job Elements:') !!}
    <select name="element_id[]"
            @if(isset($billed))
                @if($billed)
                    disabled
                @endif
            @endif
            id="elements" class="form-control" multiple="multiple">
        @if(isset($job))
            @foreach($job->elements()->get() as $element)
                <option selected="selected" value="{!! $element->id !!}">{!! $element->name !!}</option>
            @endforeach
        @endif
    </select>
</div>

@push('footer-script')
    <script type="text/javascript">
        jQuery( document ).ready(function( $ ) {
            var last_results = [];
            $("#elements").select2({
                multiple: true,
                tags: true,
                tokenSeparators: [','],
                ajax: {
                    url: "{!! url('elements/json') !!}",
                    dataType: "json",
                    data: function(term) {
                        return {
                            q: term
                        };
                    },
                    results: function(data) {
                        console.log(data);
                        return {
                            results: data.results
                        };
                    }
                },
                createSearchChoice:function(term, data) {
                    if ($(data.results).filter(function() {
                                return this.text.localeCompare(term) === 0;
                            }).length===0) {
                        return {
                            id:term,
                            text:term
                        };
                    }
                },
                createTag: function (params) {
                    return {
                        id: params.term,
                        text: params.term,
                        newOption: true
                    }
                },
                templateResult: function (data) {
                    var $result = $("<span></span>");
                    $result.text(data.text);
                    if (data.newOption) {
                        $result.append(" <em>(new)</em>");
                    }
                    return $result;
                }
            });
        });
    </script>
@endpush