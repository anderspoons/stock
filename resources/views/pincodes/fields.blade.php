<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Active Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>


<!-- Active Field -->
<div class="form-group col-sm-6">
    @if(isset($pincodes) && $pincodes->active == 1)
        {{ Form::checkbox('active', '1', true, ['class' => 'icheck']) }}
    @else
        {{ Form::checkbox('active', '1', false, ['class' => 'icheck']) }}
    @endif
    {!! Form::label('active', '  Active?') !!}
</div>
