@extends('layouts.app')

@section('content')
   <section class="content-header">
           <h1>
               Pincodes
           </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">

           <div class="box-body">
               {!! Form::model($pincodes, ['route' => ['pincodes.update', $pincodes->id], 'method' => 'patch']) !!}
               <div class="row">
                    @include('pincodes.fields')
               </div>

               <div class="row">
                   <!-- Submit Field -->
                   <div class="form-group col-sm-6">
                       {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                       <a href="{!! route('pincodes.index') !!}" class="btn btn-default">Cancel</a>
                   </div>

                   <div class="form-group col-sm-6 right">
                       <a href="#" class="btn btn-danger" data-toggle="modal" data-target=".bs-delete-modal-sm"><i class="delete-item glyphicon glyphicon-remove"></i>Delete PinCode</a>
                   </div>
               </div>

               {!! Form::close() !!}
           </div>
       </div>
   </div>
@endsection
@include('shared.modal.delete')