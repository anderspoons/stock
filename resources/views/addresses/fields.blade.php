<!-- Line 1 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('line_1', 'Line 1:') !!}
    {!! Form::text('line_1', null, ['class' => 'form-control']) !!}
</div>

<!-- Line 2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('line_2', 'Line 2:') !!}
    {!! Form::text('line_2', null, ['class' => 'form-control']) !!}
</div>

<!-- Line 3 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('line_3', 'Line 3:') !!}
    {!! Form::text('line_3', null, ['class' => 'form-control']) !!}
</div>

<!-- Town Field -->
<div class="form-group col-sm-6">
    {!! Form::label('town', 'Town:') !!}
    {!! Form::text('town', null, ['class' => 'form-control']) !!}
</div>

<!-- County Field -->
<div class="form-group col-sm-6">
    {!! Form::label('county', 'County:') !!}
    {!! Form::text('county', null, ['class' => 'form-control']) !!}
</div>

<!-- Postcode Field -->
<div class="form-group col-sm-6">
    {!! Form::label('postcode', 'Postcode:') !!}
    {!! Form::text('postcode', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('addresses.index') !!}" class="btn btn-default">Cancel</a>
</div>
