<div class="modal fade bs-delete-modal-sm" id="delete-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirm Deletion</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['url' => 'item', 'method' => 'delete', 'class' => 'delete-form']) }}
                {!! Form::hidden('item_id', '', array('class' => 'item-id')) !!}
                <div class="row">
                    <div class="col-sm-12">
                       <p>Warning you are trying to delete this item, to do this please select your name and enter your security pin below.</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-3">
                        {!! Form::label('Address', 'Address Selection:') !!}
                    </div>
                    <div class="col-sm-3">

                    </div>
                    <div class="col-sm-3">
                        {!! Form::label('pin', 'Security Pin:') !!}
                    </div>
                    <div class="col-sm-3">
                        {!! Form::password('pin', '', array('class' => 'form-control', 'style' => 'width:100%;')) !!}
                    </div>
                </div>

                {{ Form::close() }}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger delete-item">Confirm Deletion</button>
            </div>
        </div>
    </div>
</div>
