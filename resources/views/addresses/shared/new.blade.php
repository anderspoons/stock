<!-- Address Lookup Field -->
<div class="col-sm-6 form-group">
    {!! Form::label('addresspicker', 'Address Lookup:') !!}
    {!! Form::text('addresspicker', '', ['class' => 'form-control']) !!}
    <br/>
    <div id="map"></div>
</div> <!-- Postcode Field -->
<div class="form-group col-sm-6">
    {!! Form::label('line_1', 'Line 1:') !!}
    {!! Form::text('line_1', '', ['class' => 'form-control']) !!}
    {!! Form::label('line_2', 'Line 2:') !!}
    {!! Form::text('line_2', '', ['class' => 'form-control']) !!}
    {!! Form::label('line_3', 'Line 3:') !!}
    {!! Form::text('line_3', '', ['class' => 'form-control']) !!}
    {!! Form::label('town', 'Town:') !!}
    {!! Form::text('town', '', ['class' => 'form-control']) !!}
    {!! Form::label('county', 'County:') !!}
    {!! Form::text('county', '', ['class' => 'form-control']) !!}
    {!! Form::label('postcode', 'Postcode:') !!}
    {!! Form::text('postcode', '', ['class' => 'form-control']) !!}
    {!! Form::label('lng', 'Lng:') !!}
    {!! Form::text('lng', '', ['class' => 'form-control', 'disabled' => 'disabled']) !!}
    {!! Form::label('lat', 'Lat:') !!}
    {!! Form::text('lat', '', ['class' => 'form-control', 'disabled' => 'disabled']) !!}
</div>