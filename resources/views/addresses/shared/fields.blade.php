<div class="col-sm-12">
    {!! Form::label('address', 'Address:') !!}
    <br/>
    <input type="checkbox" checked id="toggle-new" data-size="normal">
    @include('addresses.scripts')
</div>
<div class="clearfix"></div>
<br/>


<div class="existing-address">

    <div class="form-group col-sm-6">
        <div class="row col-sm-10">
            {!! Form::label('address_id', 'Search:') !!}
            <select id="address-select" name="address_id" class="form-control select2 select2-hidden-accessible" single="" data-placeholder="Search Addresses" style="width: 100%;" tabindex="-1" aria-hidden="true">
            </select>
        </div>
        <div class="col-sm-2">
            <br/>
            <a href="{!! url('addresses/') !!}" target="_blank" disbaled class="btn btn-default" id="edit-link">Edit Address</a>
        </div>
    </div>

    <!-- Line 1 Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('line_1', 'Line 1:') !!}
        @if(isset($address->line_1))
            {!! Form::text('line_1', $address->line_1, ['class' => 'form-control', 'disabled' => 'disabled', 'id' => 'line1-address']) !!}
        @else
            {!! Form::text('line_1', '', ['class' => 'form-control', 'disabled' => 'disabled', 'id' => 'line1-address']) !!}
        @endif
    </div>

    <!-- Line 2 Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('line_2', 'Line 2:') !!}
        @if(isset($address->line_2))
            {!! Form::text('line_2', $address->line_2, ['class' => 'form-control', 'disabled' => 'disabled', 'id' => 'line2-address']) !!}
        @else
            {!! Form::text('line_2', '', ['class' => 'form-control', 'disabled' => 'disabled', 'id' => 'line2-address']) !!}
        @endif
    </div>


    <!-- Line 3 Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('line_3', 'Line 3:') !!}
        @if(isset($address->line_3))
            {!! Form::text('line_3', $address->line_3, ['class' => 'form-control', 'disabled' => 'disabled', 'id' => 'line3-address']) !!}
        @else
            {!! Form::text('line_3', '', ['class' => 'form-control', 'disabled' => 'disabled', 'id' => 'line3-address']) !!}
        @endif
    </div>

    <!-- Town Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('town', 'Town:') !!}
        @if(isset($address->town))
            {!! Form::text('town', $address->town, ['class' => 'form-control', 'disabled' => 'disabled', 'id' => 'town-address']) !!}
        @else
            {!! Form::text('town', '', ['class' => 'form-control', 'disabled' => 'disabled', 'id' => 'town-address']) !!}
        @endif
    </div>

    <!-- County Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('county', 'County:') !!}
        @if(isset($address->county))
            {!! Form::text('county', $address->county, ['class' => 'form-control', 'disabled' => 'disabled', 'id' => 'county-address']) !!}
        @else
            {!! Form::text('county', '', ['class' => 'form-control', 'disabled' => 'disabled', 'id' => 'county-address', 'id' => 'county-address']) !!}
        @endif
    </div>

    <!-- Postcode Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('postcode', 'Postcode:') !!}
        @if(isset($address->postcode))
            {!! Form::text('postcode', $address->postcode, ['class' => 'form-control', 'disabled' => 'disabled', 'id' => 'postcode-address']) !!}
        @else
            {!! Form::text('postcode', '', ['class' => 'form-control', 'disabled' => 'disabled', 'id' => 'postcode-address']) !!}
        @endif
    </div>

    <!-- lng Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('lng', 'Longitude:') !!}
        @if(isset($address->lng))
            {!! Form::text('lng', $address->lng, ['class' => 'form-control', 'disabled' => 'disabled', 'id' => 'lng-address']) !!}
        @else
            {!! Form::text('lng', '', ['class' => 'form-control', 'disabled' => 'disabled', 'id' => 'lng-address']) !!}
        @endif
    </div>

    <!-- lat Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('lat', 'Latitude:') !!}
        @if(isset($address->lat))
            {!! Form::text('lat', $address->lat, ['class' => 'form-control', 'disabled' => 'disabled', 'id' => 'lat-address']) !!}
        @else
            {!! Form::text('lat', '', ['class' => 'form-control', 'disabled' => 'disabled', 'id' => 'lat-address']) !!}
        @endif
    </div>

</div>
<div class="clearfix"></div>

<div class="new-address hidden">
    @include('addresses.shared.new')
</div>