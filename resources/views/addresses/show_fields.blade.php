<!-- Line 1 Field -->
<div class="form-group">
    {!! Form::label('line_1', 'Line 1:') !!}
    <p>{!! $address->line_1 !!}</p>
</div>

<!-- Line 2 Field -->
<div class="form-group">
    {!! Form::label('line_2', 'Line 2:') !!}
    <p>{!! $address->line_2 !!}</p>
</div>

<!-- Line 3 Field -->
<div class="form-group">
    {!! Form::label('line_3', 'Line 3:') !!}
    <p>{!! $address->line_3 !!}</p>
</div>

<!-- Town Field -->
<div class="form-group">
    {!! Form::label('town', 'Town:') !!}
    <p>{!! $address->town !!}</p>
</div>

<!-- County Field -->
<div class="form-group">
    {!! Form::label('county', 'County:') !!}
    <p>{!! $address->county !!}</p>
</div>

<!-- Postcode Field -->
<div class="form-group">
    {!! Form::label('postcode', 'Postcode:') !!}
    <p>{!! $address->postcode !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $address->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $address->updated_at !!}</p>
</div>

