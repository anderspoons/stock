
@section('scripts')
{{-- jQuery UI Shit --}}

<script src="https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyBLY78rALWIF9vR2dtLTdi_iwrzCE6yic4"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

{{-- https://github.com/sgruhier/jquery-addresspicker/ --}}
<script src="{{ asset('/js/jquery.ui.addresspicker.js') }}"></script>


<script type="text/javascript">
    (function(window, $) {
        var debug = true;

        console.log('Address Picker');
        var addresspicker = $( "#addresspicker" ).addresspicker({
            updateCallback: showCallback,
            mapOptions: {
                zoom: 4,
                center: new google.maps.LatLng(54, -3.5),
                scrollwheel: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            },
            elements: {
                map: "#map",
                lat: "#lat",
                lng: "#lng",
                street_number: '#line_1',
                route: '#line_2',
                locality: '#town',
                administrative_area_level_2: '#county',
                postal_code: '#postcode',
            }
        });

        function showCallback(geocodeResult, parsedGeocodeResult){
            console.log(JSON.stringify(parsedGeocodeResult, null, 4));
        }

        //Update zoom field
        var map = $("#addresspicker").addresspicker("map");
        google.maps.event.addListener(map, 'idle', function(){
            $('#zoom').val(map.getZoom());
        });

        $('#toggle-new').bootstrapToggle({
            on: 'Existing',
            off: 'New'
        });

        $('#toggle-new').change(function() {
            if($(this).parent().hasClass('off')){
                console.log('Toggled New');
                if($('.new-address').hasClass('hidden')){
                    $('.new-address').removeClass('hidden');
                    google.maps.event.trigger(window, 'resize', {});
                }
                $('.existing-address').addClass('hidden');
            } else {
                console.log('Toggled Existing');
                $('.new-address').addClass('hidden');
                if($('.existing-address').hasClass('hidden')){
                    $('.existing-address').removeClass('hidden');
                }
            }
        })

        var last_results = [];

        $("#address-select").select2({
            ajax: {
                url: "{!! url('addresses/json') !!}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        query: params.term
                    };
                },
                processResults: function (data) {
                    last_results = data.results;
                    return {
                        results: data.results
                    };
                },
                cache: false
            },
            escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
            minimumInputLength: 1
        });

        var base_url = '{!! url('/addresses') !!}';
        $('body').on('change', "#address-select", function(e){
            last_results.forEach(function (address) {
                if(address.id == $("#address-select").val()){
                    $('#line1-address').val(address.line_1);
                    $('#line2-address').val(address.line_2);
                    $('#line3-address').val(address.line_3);
                    $('#town-address').val(address.town);
                    $('#county-address').val(address.county);
                    $('#postcode-address').val(address.postcode);
                    $('#lng-address').val(address.lng);
                    $('#lat-address').val(address.lat);
                }
            });
            $('#edit-link').attr('href', base_url + '/' +  $("#address-select").val() + '/edit');
        });

        $('#edit-link').click(function(e){
            if($(this).attr('href') !== base_url + '/' +  $("#address-select").val() + '/edit'){
                e.preventDefault();
            }
        });


//      Selectaronio 2
        @if(isset($address->id))
            $('#address-select').empty().append('<option value="{!! $address->id !!}">{!! $address->line_1.', '.$address->line_2.', '.$address->line_3.', '.$address->town.', '.$address->county.', '.$address->postcode !!}</option>').val('{!! $address->id !!}').trigger('change');
        @endif

    })(window, jQuery);
</script>
@endsection