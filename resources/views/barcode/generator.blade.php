@if(isset($code))
    <p>The below are generated for: <strong>{!! $code !!}</strong></p>

    @if(isset($style))
        @if($style == 'small')
            @include('barcode.layouts.small')
        @endif

        @if($style == 'medium')
            @include('barcode.layouts.medium')
        @endif

        @if($style == 'zebra')
            @include('barcode.layouts.zebra')
        @endif

    @else
        <p>Seems to be a problem, no style was selected.</p>
    @endif



@else
    <p>No code entered, please use the box in the upper right to enter a code to generate barcodes for.</p>
@endif

