@extends('layouts.app')

{{-- TODO could possible look to lookup an item here by the barcode to make a styled lable with the type shit etc. --}}

@section('content')
    <section class="row content-header">
        <h1 class="pull-left col-md-6">Barcode</h1>
        <div class="pull-right col-md-6">
            {!! Form::open(array('url' => '/barcode', 'method' => 'post')) !!}
            <div class="col-md-5">
                @if(isset($style))
                    {!! Form::select('style', array('small' => '12mm With Code', 'medium' => '19mm With Code', 'zebra' => 'Zebra Info Label'), $style, array('class' => 'form-control')) !!}
                @else
                    {!! Form::select('style', array('small' => '12mm With Code', 'medium' => '19mm With Code', 'zebra' => 'Zebra Info Label'), 'small', array('class' => 'form-control')) !!}
                @endif
            </div>
            <div class="col-md-5">
                @if(isset($code))
                    {!! Form::text('code', $code, array('Placeholder' => 'Barcode to generate', 'class' => 'form-control')) !!}
                @else
                    {!! Form::text('code','', array('Placeholder' => 'Barcode to generate', 'class' => 'form-control')) !!}
                @endif
            </div>
            <div class="pull-right">
                {!! Form::submit('Generate', array('class' => 'btn btn-primary')) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </section>
    <div class="clearfix"></div>

    <div class="content">
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
                        @include('barcode.generator')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    {{-- Dymo Shit --}}
    <script type="text/javascript" src="/js/dymo/DYMO.Label.Framework.latest.js"></script>
    <script type="text/javascript" src="/js/dymo/PrintLabels.js"></script>
    <script type="text/javascript" src="/js/pdfmake/pdfmake.min.js"></script>
    <script type="text/javascript" src="/js/pdfmake/vfs_fonts.js"></script>
    <script type="text/javascript">
printers = dymo.label.framework.getPrinters();
for(var i = 0; i < printers.length; i++)
{
var printer = printers[i];
console.log(printer);
}

        function printPDF()
        {
            if (navigator.appName == 'Microsoft Internet Explorer') {

                //Wait until PDF is ready to print
                if (typeof document.getElementById("pdfDocument").print == 'undefined') {

                    setTimeout(function(){printPDF("pdfDocument");}, 1000);

                } else {

                    var x = document.getElementById("pdfDocument");
                    x.print();
                }

            } else{

                PDFIframeLoad();  // for chrome
            }
        }

        //for Chrome
        function PDFIframeLoad() {
            var iframe = document.getElementById('iframe_a');
            if(iframe.src) {
                var frm = iframe.contentWindow;

                frm.focus();// focus on contentWindow is needed on some ie versions
                frm.print();
                return false;
            }
        }



        //        function PrintElem(elem)
//        {
//            Popup($(elem).html());
//        }
//
//        function Popup(data)
//        {
//            var mywindow = window.open('', 'my_div', 'height=56mm,width=30mm');
//            mywindow.document.write('<style>body, html{ width: 56mm !important; height: 30mm !important; }<\/style>');
//            mywindow.document.write('<script type="text/javascript">function printPage() { print(); }<\/script>');
//            mywindow.document.write(data);
//            setTimeout(function() {
//                mywindow.print();
//                mywindow.close();
//            }, 1);
//            return true;
//        }
//
//        function printIframe(id)
//        {
//            var iframe = document.frames ? document.frames[id] : document.getElementById(id);
//            var ifWin = iframe.contentWindow || iframe;
//            iframe.focus();
//            ifWin.printPage();
//            return false;
//        }
//
//        function printMe() {
//            window.print()
//        }
//
//        function PrintPdf() {
//            idPdf.focus();
//            idPdf.print();
//            //setTimeout(function() { idContainer.innerHTML = '' }, 1000);
//        }
//
//        function idPdf_onreadystatechange() {
//            alert('on load');
//            if (idPdf.readyState === 4){
//                parent.console.log('hit ready');
//                setTimeout(function(){
//
//                }, 5000);
//            }
//        }
//
//        function LoadAndPrint(url)
//        {
//            idContainer.innerHTML =
//                '<object id="idPdf" onload="idPdf_onreadystatechange()"'+
//                'width="300" height="200" type="application/pdf"' +
//                'data="' + url + '?#view=Fit&scrollbar=0&toolbar=0&navpanes=0">' +
//                '<span>PDF plugin is not available.</span>'+
//                '</object>';
//        }
//
////        Obselete for now, the below is for the PDF version which doesn't have the issues of squiggly barcodes.
//
//        function printdiv(barcode) {
//            var mywindow = window.open("http://stock.macduffshipyards.co.uk/barcode/pdf/" + barcode, 'Zebra Label', 'height=400,width=600');
//            mywindow.onload = function () {
//                console.log('loaded');
//
//                mywindow.document.focus();
//                tryPrint(mywindow);
//                mywindow.document.close();
//            };
//        }
//        function tryPrint(mywindow){
//            try {
//                mywindow.document.execCommand('print', false, null);
//            } catch (e) {
//                mywindow.document.print();
//            }
//        }
    </script>

@endsection