<html>
<head>
    <meta http-equiv="Content-Type" content="charset=utf-8"/>
    <meta charset="UTF-8">
    <title>PDF Upn</title>

    <style type="text/css">
        * {
            margin: 0px !important;
            font-family:tahoma, verdana, arial, sans-serif;
            overflow: hidden;
        }
        html, body {
            overflow: hidden;
            padding: 1mm !important;
            padding-left:10px;
            margin: 0 !important;
        }
        #zebra {
            margin-top:auto;
            margin-bottom:auto;
        }
        .zebra-label .info {
            display:inline-block;
            padding: 0 !important;
            width: 30%;
            float: left;
            margin-right: 5px;
            font-size: 8pt;
        }
        .zebra-label .info span {
            display:block;
            width: 100%;
            text-align: center;
        }
        .zebra-label .details span {
            padding: 0 !important;
            margin: 0 !important;
            font-weight: normal !important;
        }
        .zebra-label .details {
            padding: 0 !important;
            margin: 0 !important;
        }
        .zebra-label h1 {
            width: 100%;
            padding: 0 !important;
            margin: 0 !important;
            font-weight:600;
            text-align: center;
            font-size: 8pt;
        }
        .zebra-label .title{
            margin-top:3px !important;
            margin-bottom:1px !important;
        }
        .zebra-label .barcode{
            display:inline-block;
            width: 100%;
            text-align: center;
        }
    </style>
</head>
<body>
@if(isset($item))
    <div class="zebra-label" id="zebra">
        <h1 class="title">@if(isset($item[0]->partnumber) && strlen($item[0]->partnumber) > 0){!! $item[0]->partnumber !!}@endif @if(isset($item[0]->description) && isset($item[0]->partnumber) && strlen($item[0]->partnumber) > 0)
                &nbsp;|&nbsp; @endif @if(isset($item[0]->description)){!! $item[0]->description !!}@endif</h1>
        <div class="details">
            @if($item[0]->sizes->count() > 0)
                <div class="info">
                    @foreach($item[0]->sizes as $size)
                        <span>{!! $size->name !!}</span></br>
                    @endforeach
                </div>
            @endif
            @if($item[0]->types->count() > 0)
                <div class="info">
                    @foreach($item[0]->types as $type)
                        <span>{!! $type->name !!}</span></br>
                    @endforeach
                </div>
            @endif
            @if($item[0]->materials->count() > 0)
                <div class="info">
                    @foreach($item[0]->materials as $material)
                        <span>{!! $material->name !!}</span></br>
                    @endforeach
                </div>
            @endif
            <h1>{!! $item[0]->barcode !!}</h1>
            <div class="barcode">
                <img src="data:image/png;base64,{!! DNS1D::getBarcodePNG(str_replace('MS', '', $item[0]->barcode), "C128", 2, 20) !!}"
                     alt="barcode"/>
            </div>
        </div>
    </div>
@else
    <p>Item not found for this barcode.</p>
@endif
</body>
</html>

