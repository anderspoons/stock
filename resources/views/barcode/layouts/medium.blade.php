<div class="barcode-wrap col-lg-6 col-md-6 col-sm-12">
    <h4>19mm Code Above Barcode</h4>


    <div id="small">
        <img src="{!! url(DNS1D::getBarcodePNGPath($code, "C128", 2, 50)) !!}" alt="barcode"/>
    </div>

    <input type="hidden" id="code" value="{!! $code !!}"/>

    <div class="clearfix"></div>
    <br>
    <input type="button" class="btn btn-primary" value="Print via Dymo" onclick="printLabel('19mm');"  />

</div>