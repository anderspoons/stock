<div class="barcode-wrap col-lg-6 col-md-6 col-sm-12">
    <h4>50mm x 30mm Zebra Info Label</h4>


    @if(isset($item))
        {{--<div id="zebra">--}}
            {{--<style type="text/css">--}}
                {{--.zebra-label {--}}
                    {{--font-family: Arial;--}}
                    {{--display: table-cell;--}}
                    {{--width: 55mm !important;--}}
                    {{--height: 30mm !important;--}}
                    {{--padding: 0 !important;--}}
                    {{--margin: 0 !important;--}}
                    {{--text-align: center;--}}
                    {{--vertical-align: middle;--}}
                {{--}--}}

                {{--.zebra-label .info {--}}
                    {{--padding: 0 !important;--}}
                    {{--width: 30%;--}}
                    {{--float: left;--}}
                    {{--margin-right: 5px;--}}
                    {{--font-size: 8pt;--}}
                {{--}--}}

                {{--.zebra-label h1 {--}}
                    {{--padding: 0 !important;--}}
                    {{--margin: 0 !important;--}}
                    {{--font-size: 8pt;--}}
                    {{--font-weight: bold;--}}
                    {{--text-align: center;--}}
                    {{--width: 100%;--}}
                {{--}--}}

                {{--.zebra-label .details {--}}

                {{--}--}}

                {{--.zebra-label .details span {--}}
                    {{--padding: 0 !important;--}}
                    {{--margin: 0 !important;--}}
                    {{--font-weight: normal !important;--}}
                {{--}--}}

                {{--.clearfix {--}}
                    {{--clear: both;--}}
                {{--}--}}
            {{--</style>--}}
            {{--<div class="zebra-label">--}}
                {{--<h1>@if(isset($item[0]->partnumber) && strlen($item[0]->partnumber) > 0){!! $item[0]->partnumber !!}@endif @if(isset($item[0]->description) && isset($item[0]->partnumber) && strlen($item[0]->partnumber) > 0)--}}
                        {{--&nbsp;|&nbsp; @endif @if(isset($item[0]->description)){!! $item[0]->description !!}@endif</h1>--}}
                {{--<div class="details">--}}
                    {{--@if($item[0]->sizes->count() > 0)--}}
                        {{--<div class="info">--}}
                            {{--@foreach($item[0]->sizes as $size)--}}
                                {{--<span>{!! $size->name !!}</span></br>--}}
                            {{--@endforeach--}}
                        {{--</div>--}}
                    {{--@endif--}}

                    {{--@if($item[0]->types->count() > 0)--}}
                        {{--<div class="info">--}}
                            {{--@foreach($item[0]->types as $type)--}}
                                {{--<span>{!! $type->name !!}</span></br>--}}
                            {{--@endforeach--}}
                        {{--</div>--}}
                    {{--@endif--}}

                    {{--@if($item[0]->materials->count() > 0)--}}
                        {{--<div class="info">--}}
                            {{--@foreach($item[0]->materials as $material)--}}
                                {{--<span>{!! $material->name !!}</span></br>--}}
                            {{--@endforeach--}}
                        {{--</div>--}}
                    {{--@endif--}}
                    {{--<div class="clearfix"></div>--}}
                    {{--<h1>{!! $item[0]->barcode !!}</h1>--}}
                    {{--<div class="barcode">--}}
                        {{--<img src="data:image/png;base64,{!! DNS1D::getBarcodePNG(str_replace('MS', '', $item[0]->barcode), "C128", 2, 30) !!}"--}}
                             {{--alt="barcode"/>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

        {{--</div>--}}

        <iframe id="iframe_a" name='iframe_a' src="{{ url('/barcode/pdf/'.$item[0]->barcode) }}" width="50%"
                height="50%"></iframe>        <!-- for Chrome -->
        <embed type="application/pdf" src="{{ url('/barcode/pdf/'.$item[0]->barcode) }}" id="pdfDocument" width="0%"
               height="0%"></embed> <!-- for IE - u can display  setted to  0% width and height-->
    @else
        <p>Item not found for this barcode.</p>
    @endif


    <input type="hidden" id="code" value="{!! $code !!}"/>

    <div class="clearfix"></div>
    <br>

    {{--<input type="button" class="btn btn-primary" value="Print via Zebra" onclick="PrintElem('#zebra');"/>--}}
    {{--<input type="button" class="btn btn-primary" value="Print via Zebra" onclick="printdiv('{{ $item[0]->barcode }}');"/>--}}

    <input type="submit"  value="Print"name="Submit" id="printbtn"onclick="printPDF()" />

    {{--<button id="idPrint"--}}
            {{--onclick="LoadAndPrint('http://stock.macduffshipyards.co.uk/barcode/pdf/' + '{{ $item[0]->barcode }}')">Print--}}
        {{--Zebra Label--}}
    {{--</button>--}}
    <br>
    <div id="idContainer"></div>
</div>