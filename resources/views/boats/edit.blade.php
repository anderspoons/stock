@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Boat
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($boat, ['route' => ['boats.update', $boat->id], 'method' => 'patch']) !!}

                        @include('boats.fields')

                        <!-- Submit Field -->
                       <div class="form-group col-sm-12">
                           {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                           <a href="{!! route('boats.index') !!}" class="btn btn-default">Cancel</a>
                       </div>
                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection