<div class="modal fade bs-new-boat-sm" id="new-boat-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add New Boat</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['route' => 'boats.store']) !!}

                <div class="row">
                    @include('boats.fields')
                </div>

                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary add-boat">Add Boat</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    (function(window, $) {

    })(window, jQuery);
</script>