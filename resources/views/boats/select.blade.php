<select name="boat_id"

        @if(isset($billed))
        @if($billed)
        disabled
        @endif
        @endif
    class="form-control" id="boat-select">

    @if(isset($counter))
        @if(!empty($counter->boat))
            <option selected value="{{ $counter->boat->id }}">
                {{ $counter->boat->name }}
            </option>
        @endif
    @endif

    @if(isset($job))
        @if(!empty($job->boat))
            <option selected value="{{ $job->boat->id }}">
                {{ $job->boat->name }}
            </option>
        @endif
    @endif

    @if(isset($boat))
        @if(!empty($boat->boat))
            <option selected value="{{ $boat->boat->id }}">
                {{ $boat->boat->name }}
            </option>
        @endif
    @endif
</select>

@push('footer-script')
<script type="text/javascript">
    (function(window, $) {
        jQuery( document ).ready(function( $ ) {

            $('#boat-select').on('select2:select', function(evt){
                cust_id = evt.params.data.cust_id;
                cust_name = evt.params.data.cust_name;

                var opt = "<option value='"+cust_id+"' selected ='selected'>" + cust_name + " </option>";
                $("#customer-select").html(opt);
                $("#customer-select").val(cust_id).trigger("change");
            });


            $("#boat-select").select2({
                ajax: {
                    url: "{!! url('boats/json') !!}",
                    dataType: "json",
                    data: function (term) {
                        return {query: term.term }
                    },
                    results: function (data) {
                        return {
                            results: data.results
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data.results, function (item) {
                                console.log(item);
                                return {
                                    text: item.text,
                                    id: item.id,
                                    cust_id: item.customer_id,
                                    cust_name: item.customer_name,
                                }
                            })
                        };
                    }
                },
                createSearchChoice: function (term, data) {
                    if ($(data.results).filter(function () {
                                return this.text.localeCompare(term) === 0;
                            }).length === 0) {
                        return {
                            id: term,
                            text: term
                        };
                    }
                },
                width: '100%',
            });
        });
    })(window, jQuery);
</script>
@endpush