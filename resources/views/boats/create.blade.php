@extends('layouts.app')

@section('content')
    <section class="content-header">
        <a href="{!! route('customers.create') !!}" target="_blank" class="btn btn-primary pull-right">Create Customer</a>
        <h1>
            Boat
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                {!! Form::open(['route' => 'boats.store']) !!}
                <div class="row">
                    @include('boats.fields')
                </div>
                <div class="row">
                    <!-- Submit Field -->
                    <div class="form-group col-sm-12">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                        <a href="{!! route('boats.index') !!}" class="btn btn-default">Cancel</a>
                    </div>
                    {{-- <div class="form-group col-sm-6">
                        <a href="{!! route('customers.create') !!}" target="_blank" class="btn btn-primary pull-right">Create Customer</a>
                    </div> --}}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
