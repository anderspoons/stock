<!-- Sage Ref Field -->
<div class="form-group">
    {!! Form::label('sage_ref', 'Sage Ref:') !!}
    <p>{!! $boat->sage_ref !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $boat->name !!}</p>
</div>

<!-- Note Field -->
<div class="form-group">
    {!! Form::label('name', 'Note:') !!}
    <p>{!! $boat->note !!}</p>
</div>

<!-- Number Field -->
<div class="form-group">
    {!! Form::label('number', 'Number:') !!}
    <p>{!! $boat->number !!}</p>
</div>

<!-- Customer Id Field -->
<div class="clearfix">
    <div class="form-group">
        {!! Form::label('customer_name', 'Customer:') !!}
        <div class="row">
            <div class="col-md-6">
                <div class='btn-group'>
                    <a href="{{  url('/customers/'.$boat->customer_id) }}" class='btn btn-default btn-xs'>
                        <i class="glyphicon glyphicon-eye-open"></i>
                    </a>
                </div>
                {!! $boat->customer->name !!}
            </div>
        </div>
    </div>
</div>

