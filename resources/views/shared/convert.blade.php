<div class="modal fade bs-calc-modal-sm" id="calculator-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Area to Quantity Calculator</h4>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 form-group">
                        {{ Form::label('item_list', 'Items to Calculate') }}
                        <div class="item-list col-sm-12">

                        </div>
                    </div>
                </div>

                <hr/>

                <div class="row col-sm-12">
                    <div class="form-group col-sm-3">
                        {{ Form::label('unit', 'Unit:') }}
                        <select class="unit-select col-sm-12 form-control">
                            <option data-report='3' value="in">Inch</option>
                            <option data-report='1' value="mm">Millimeter</option>
                            <option data-report='5' value="cm">Centimeter</option>
                            <option data-report='2' value="m">Meter</option>
                        </select>
                    </div>

                    <div class="form-group col-sm-3">
                        {{ Form::label('size1', 'Size 1:') }}
                        {{ Form::number('size1', '', array('class' => 'col-sm-12 size-box-1 form-control')) }}
                    </div>

                    <div class="form-group col-sm-3">
                        {{ Form::label('size2', 'Size 2:') }}
                        {{ Form::number('size2', '', array('class' => 'col-sm-12 size-box-2 form-control')) }}
                    </div>

                    <div class="form-group col-sm-2">
                        {{ Form::label('qty', 'Qty:') }}
                        {{ Form::number('qty', '', array('class' => 'col-sm-12 qty-box form-control')) }}
                    </div>

                    <div class="form-group col-sm-1">
                        <br/>
                        {!! Form::button('<i class="glyphicon glyphicon-plus"></i>', ['class' => 'btn btn-default pull-right add-sheet-row']) !!}
                    </div>
                </div>

                <hr/>

                <div class="row col-sm-12">
                    <div class="form-group col-sm-6">
                        {{ Form::label('unit', 'Unit:') }}
                        <select class="unit-total-select col-sm-12 form-control">
                            <option data-report='3' value="in">Inch</option>
                            <option data-report='1' value="mm">Millimeter</option>
                            <option data-report='5' value="cm">Centimeter</option>
                            <option data-report='2' value="m">Meter</option>
                        </select>
                    </div>

                    <div class="form-group col-sm-6">
                        {{ Form::label('total', 'Total') }}
                        {{ Form::text('total', '', array('class' => 'col-sm-12 form-control total-qty')) }}
                    </div>
                </div>

            </div>

            <div class="clearfix"></div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success calc-total">Calculate</button>
                <button type="button" class="btn btn-info set-quantity">Set Quantity</button>
            </div>

        </div>
    </div>

    <div class="hidden size-line row col-sm-12" id="list-temp">
        {{ Form::hidden('_', '', array('class' => 'unit')) }}
        <div class="col-sm-4 form-group">
            {{ Form::label('_', 'Size:') }}
            {{ Form::number('_', '', array('class' => 's1 form-control col-sm-12')) }}
        </div>
        <div class="col-sm-4 form-group">
            {{ Form::label('_', 'Size:') }}
            {{ Form::number('_', '', array('class' => 's2 form-control col-sm-12')) }}
        </div>
        <div class="col-sm-3 form-group">
            {{ Form::label('_', 'Qty:') }}
            {{ Form::number('_', '', array('class' => 'qty form-control col-sm-12')) }}
        </div>
        <div class="form-group col-sm-1">
            <br/>
            {!! Form::button('<i class="glyphicon glyphicon-remove"></i>', ['class' => 'btn btn-default pull-right remove-sheet-row']) !!}
        </div>
    </div>

</div>


@push('footer-script')
    <script type="text/javascript">
        (function(window, $) {
            var list = [];
            var current_row;

            function calc_total(){
                var total = 0;

                $(list).each(function(){
                    var sub = (this.size1 * this.size2) * this.qty;
                    console.log('From: ' + this.unit );
                    console.log('To: ' + $('#calculator-modal .unit-total-select').val());
                    if(this.unit != $('#calculator-modal .unit-total-select').val()){
                        var convert = Qty.swiftConverter(this.unit, $('#calculator-modal .unit-total-select').val());
                        var formatted = convert(sub);
                        sub = formatted;
                    }
                    console.log('Sub Total: '+sub);
                    total = total + sub;
                });

                console.log('Total: ' + total);
                $('#calculator-modal .total-qty').val(total);
            }

            function set_unit(){
                @if(isset($item->unit_id))
                    $('#calculator-modal .unit-select option[data-report={{ $item->unit_id }}]').prop('selected', true);
                    $('#calculator-modal .unit-total-select option[data-report={{ $item->unit_id }}]').prop('selected', true);
                    $('#calculator-modal .unit-total-select').attr('disabled', 'disabled');
                @else
                    if($('#calculator-modal .unit-total-select').attr('disabled')){
                        $('#calculator-modal .unit-total-select').removeAttr('disabled');
                    }
                @endif
            }

            function list_to_array(){
                list = [];
                $('#calculator-modal .item-list .size-line').each(function(){
                    var tmp = [];
                    tmp.unit = $(this).find('.unit').val();
                    tmp.size1 = $(this).find('.s1').val();
                    tmp.size2 = $(this).find('.s2').val();
                    tmp.qty = $(this).find('.qty').val();
                    list.push(tmp);
                });
            }

            function clear_inputs(){
                $('#calculator-modal .size-box-1').val('');
                $('#calculator-modal .size-box-2').val('');
                $('#calculator-modal .qty-box').val('');
                $('#calculator-modal .item-list').html('');
            }

            $('#calculator-modal .size-box-1').on('input', function() {
                disable_add();
            });
            $('#calculator-modal .size-box-2').on('input', function() {
                disable_add();
            });
            $('#calculator-modal .qty-box').on('input', function() {
                disable_add();
            });

            function disable_add(){
                if($('#calculator-modal .size-box-1').val() != '' && $('#calculator-modal .size-box-1').val() != '0'  && $('#calculator-modal .size-box-2').val() != '' && $('#calculator-modal .size-box-2').val() != '0' && $('#calculator-modal .qty-box').val() != '' && $('#calculator-modal .qty-box').val() != '0'){
                    if($('#calculator-modal .add-sheet-row').attr('disabled') !== undefined) {
                        $('#calculator-modal .add-sheet-row').removeAttr('disabled');
                    }
                }

                if($('#calculator-modal .size-box-1').val() == '' || $('#calculator-modal .size-box-1').val() == '0' || $('#calculator-modal .size-box-2').val() == '' || $('#calculator-modal .size-box-2').val() == '0' || $('#calculator-modal .qty-box').val() == '' || $('#calculator-modal .qty-box').val() == '0'){
                    if($('#calculator-modal .add-sheet-row').attr('disabled') == undefined) {
                        $('#calculator-modal .add-sheet-row').attr('disabled', 'disabled');
                    }
                }
            }

            $('body').on('click', '.qty-calc-item', function(e){
                disable_add();
                clear_inputs();
                set_unit();
                current_row = $(this).parent().parent();
            });

            $('body').on('click', '.remove-sheet-row', function(e){
                $(this).parent().parent().remove();
            });

            $('body').on('click', '.add-sheet-row', function(e){
                var row = $('#list-temp').clone();
                $(row).removeAttr('id');
                $(row).removeClass('hidden');
                $(row).find('.unit').val($('#calculator-modal .unit-select').val());
                $(row).find('.s1').val($('#calculator-modal .size-box-1').val());
                $(row).find('.s2').val($('#calculator-modal .size-box-2').val());
                $(row).find('.qty').val($('#calculator-modal .qty-box').val());
                $('#calculator-modal .item-list').append(row);

                $('#calculator-modal .unit-select').val('');
                $('#calculator-modal .size-box-1').val('');
                $('#calculator-modal .size-box-2').val('');
                $('#calculator-modal .qty-box').val('');
            });

            $('body').on('click', '.calc-total', function(e){
                list_to_array();
                calc_total();
            });

            $('body').on('click', '.set-quantity', function(e){
                console.log(current_row);
                current_row.find('[name$="qty]"]').val($('#calculator-modal .total-qty').val());
                $('#calculator-modal .close').click();
            });

        })(window, jQuery);
    </script>
@endpush
