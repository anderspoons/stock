<div class="modal fade bs-calc-cost-modal-sm" id="cost-calculator-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Area to Cost Calculator</h4>
            </div>

            <div class="modal-body">
                <div class="row col-sm-12">
                    <div class="form-group col-sm-4">
                        {{ Form::label('unit', 'Ordered As Unit:') }}
                        <select class="unit-select col-sm-12 form-control">
                            <option data-report='3' value="in">Inch</option>
                            <option data-report='1' value="mm">Millimeter</option>
                            <option data-report='5' value="cm">Centimeter</option>
                            <option data-report='2' value="m">Meter</option>
                        </select>
                    </div>

                    <div class="form-group col-sm-4">
                        {{ Form::label('size1', 'Size 1:') }}
                        {{ Form::number('size1', '', array('class' => 'col-sm-12 size-box-1 form-control')) }}
                    </div>

                    <div class="form-group col-sm-4">
                        {{ Form::label('size2', 'Size 2:') }}
                        {{ Form::number('size2', '', array('class' => 'col-sm-12 size-box-2 form-control')) }}
                    </div>

                    <div class="form-group col-sm-6">
                        {{ Form::label('qty', 'Qty:') }}
                        {{ Form::number('qty', '', array('class' => 'col-sm-12 qty-box form-control')) }}
                    </div>

                    <div class="form-group col-sm-6">
                        {{ Form::label('cost', 'Single Sheet Cost:') }}

                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-gbp"></i>
                            </span>
                            {{ Form::number('cost', '', array('class' => 'col-sm-12 cost-box form-control')) }}
                        </div>
                    </div>

                </div>

                <hr/>

                <div class="row col-sm-12">
                    <div class="form-group col-sm-6">
                        {{ Form::label('unit', 'Sold As Unit:') }}
                        <select class="unit-total-select col-sm-12 form-control">
                            <option data-report='3' value="in">Inch</option>
                            <option data-report='1' value="mm">Millimeter</option>
                            <option data-report='5' value="cm">Centimeter</option>
                            <option data-report='2' value="m">Meter</option>
                        </select>
                    </div>

                    <div class="form-group col-sm-6">
                        {{ Form::label('cost-per-unit', 'Cost Per Unit Squared') }}
                        {{ Form::text('cost-per-unit', '', array('class' => 'col-sm-12 form-control unit-cost')) }}
                    </div>
                </div>

            </div>

            <div class="clearfix"></div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success calc-total">Calculate</button>
                <button type="button" class="btn btn-info set-quantity">Set Cost</button>
            </div>

        </div>
    </div>
</div>

@push('footer-script')
<script type="text/javascript">
    (function(window, $) {
        var list = [];
        var current_row;

        function calc_total(){
            var total = 0;
            var sub = ($('#cost-calculator-modal .size-box-1').val() *  $('#cost-calculator-modal .size-box-2').val()) * $('#cost-calculator-modal .qty-box').val();

            if($('#cost-calculator-modal .unit-select').val() != $('#cost-calculator-modal .unit-total-select').val()){
                var convert = Qty.swiftConverter($('#cost-calculator-modal .unit-select').val(), $('#cost-calculator-modal .unit-total-select').val());
                var formatted = convert(sub);
                sub = formatted;
            }
            total = total + sub;

            var price =  $('#cost-calculator-modal .cost-box').val();
            //Convert to pence
            if(price > 0){
                price = price * 100;
            }
            total = roundUp(price / total, 10)
            $('#cost-calculator-modal #cost-per-unit').val((total/100).toFixed( 2 ));
        }

        function set_unit(){
            console.log('Within set unit');
            @if(isset($item->unit_id))
                $('#cost-calculator-modal .unit-total-select option[data-report={{ $item->unit_id }}]').prop('selected', true);
                $('#cost-calculator-modal .unit-total-select').attr('disabled', 'disabled');
            @else
                if($('#cost-calculator-modal .unit-total-select').attr('disabled')) {
                    $('#cost-calculator-modal .unit-total-select').removeAttr('disabled');
                }
            @endif

            $('#cost-calculator-modal .unit-select option[data-report='+ current_row.find('[name$="unit_id]"]').val() +']').prop('selected', true);
            $('#cost-calculator-modal .unit-select').attr('disabled', 'disabled');

            if(current_row.parent().find('.size-box').eq(0).length > 0){
                $('#cost-calculator-modal .size-box-1').val(current_row.parent().find('.size-box').eq(0).val());
            }

            if(current_row.parent().find('.size-box').eq(1).length > 0){
                $('#cost-calculator-modal .size-box-2').val(current_row.parent().find('.size-box').eq(1).val());
            }

            if(current_row.find('[name$="lots]"]').length > 0){
                $('#cost-calculator-modal .qty-box').val(current_row.find('[name$="lots]"]').val());
            }
        }

        function clear_inputs(){
            $('#cost-calculator-modal .size-box-1').val('');
            $('#cost-calculator-modal .size-box-2').val('');
            $('#cost-calculator-modal .qty-box').val('');
            $('#cost-calculator-modal .item-list').html('');
        }

        $('body').on('click', '.cost-calc-item', function(e){
            current_row = $(this).parent().parent();
            clear_inputs();
            set_unit();
        });

        $('body').on('click', '.calc-total', function(e){
            calc_total();
        });

        $('body').on('click', '.set-cost', function(e){
            current_row.find('[name$="price]"]').val($('#cost-calculator-modal #cost-per-unit').val());
            $('#cost-calculator-modal .close').click();
        });

        function roundUp(num, precision) {
            return Math.ceil(num * precision) / precision
        }
    })(window, jQuery);
</script>
@endpush
