<div class="modal fade bs-delete-modal-sm" id="delete-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirm Deletion</h4>
            </div>
            <div class="modal-body">
                {{-- Below we need to work out which thing were deleteling Here, item, supplier, customer --}}

                @if(Request::is('items*'))
                    {{ Form::open(['route' => ['items.destroy', $item->id], 'method' => 'delete', 'class' => 'delete-form']) }}
                    {!! Form::hidden('item_id', $item->id, array('class' => 'item-id')) !!}
                @elseif(Request::is('suppliers*'))
                    {{ Form::open(['route' => ['suppliers.destroy', $supplier->id], 'method' => 'delete', 'class' => 'delete-form']) }}
                    {!! Form::hidden('supplier_id', $supplier->id, array('class' => 'supplier-id')) !!}
                @elseif(Request::is('customers*'))
                    {{ Form::open(['route' => ['customers.destroy', $customer->id], 'method' => 'delete', 'class' => 'delete-form']) }}
                    {!! Form::hidden('customer_id', $customer->id, array('class' => 'customer-id')) !!}
                @elseif(Request::is('boats*'))
                    {{ Form::open(['route' => ['boats.destroy', $boat->id], 'method' => 'delete', 'class' => 'delete-form']) }}
                    {!! Form::hidden('boat_id', $boat->id, array('class' => 'boats-id')) !!}
                @elseif(Request::is('addresses*'))
                    {{ Form::open(['route' => ['addresss.destroy', $address->id], 'method' => 'delete', 'class' => 'delete-form']) }}
                    {!! Form::hidden('address_id', $address->id, array('class' => 'address-id')) !!}
                @elseif(Request::is('users*'))
                    {{ Form::open(['route' => ['users.destroy', $user->id], 'method' => 'delete', 'class' => 'delete-form']) }}
                    {!! Form::hidden('user_id', $user->id, array('class' => 'user-id')) !!}
                @elseif(Request::is('jobs*'))
                    {{ Form::open(['route' => ['jobs.destroy', $job->id], 'method' => 'delete', 'class' => 'delete-form']) }}
                    {!! Form::hidden('job_id', $job->id, array('class' => 'job-id')) !!}
                @elseif(Request::is('counters*'))
                    {{ Form::open(['route' => ['counters.destroy', $counter->id], 'method' => 'delete', 'class' => 'delete-form']) }}
                    {!! Form::hidden('counter_id', $counter->id, array('class' => 'counter-id')) !!}
                @elseif(Request::is('pincodes*'))
                    {{ Form::open(['route' => ['pincodes.destroy', $pincodes->id], 'method' => 'delete', 'class' => 'delete-form']) }}
                    {!! Form::hidden('pincode_id', $pincodes->id, array('class' => 'pincode-id')) !!}
                @endif

                <div class="row">
                    <div class="col-sm-12">
                        <p>Warning you are trying to delete this, are you sure?</p>
                        {{-- <p>Warning you are trying to delete this, to do this please select your name and enter your security pin below.</p> --}}
                    </div>
                </div>

                {{-- <div class="form-group">
                    <div class="col-sm-12 col-md-6 form-group">
                        {!! Form::label('pin_id', 'Name:') !!}
                        @if(isset($pins))
                            <select class="form-control" name="pin_id">
                                @foreach($pins as $user)
                                    <option value="{!! $user->id !!}">{!! $user->name !!}</option>
                                @endforeach
                            </select>
                        @endif
                    </div>
                    <div class="col-sm-12 col-md-6 form-group">
                        {!! Form::label('pin', 'Security Pin:') !!}
                        {!! Form::password('pin', array('class' => 'form-control')) !!}
                        <span class="code-error hidden">Wrong pin code entered.</span>
                    </div>
                </div> --}}

                {{ Form::close() }}
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger delete-item">Confirm Deletion</button>
            </div>
        </div>
    </div>
</div>


@section('scripts')
    <script type="text/javascript">
        (function(window, $) {
            $('body').on('click', '.delete-item', function(e){
                e.preventDefault();
                $('#pin').css({'border' : 'none'});
                if($('.code-error').is('visible')) {
                    $('.code-error').addClass('hidden');
                }
                var form = $(this).parent().parent().find('.modal-body form');
                var element = $(this);
                $.ajax({
                    type:"POST",
                    url:form.attr("action"),
                    data:form.serialize(),
                    success: function(response){
                        element.parent().parent().find('button[data-dismiss="modal"]').click();
                        var myStack = {"dir1":"down", "dir2":"right", "push":"top"};
                        new PNotify({
                            title: "Success",
                            text: response.data,
                            addclass: "stack-custom",
                            stack: myStack
                        })
                        setTimeout(function(e){
                            @if(Request::is('items*'))
                                window.location = '/items';
                            @elseif(Request::is('suppliers*'))
                                window.location = '/suppliers';
                            @elseif(Request::is('customers*'))
                                    window.location = '/customers';
                            @elseif(Request::is('boats*'))
                                    window.location = '/boats';
                            @elseif(Request::is('addresses*'))
                                window.location = '/addresses';
                            @elseif(Request::is('users*'))
                                window.location = '/users';
                            @elseif(Request::is('jobs*'))
                                    window.location = '/jobs';
                            @elseif(Request::is('counters*'))
                                    window.location = '/counters';
                            @elseif(Request::is('pincodes*'))
                                    window.location = '/pincodes';
                            @endif
                        }, 1000);
                    }
                });
            });

            $( document ).ajaxError(function(event, jqxhr, settings, thrownError ) {
                var object = JSON.parse(jqxhr.responseText);
                console.log(object);
                $('#pin').css({'border' : 'thin solid #ff3333'});
                if(!$('.code-error').is('visible')){
                    $('.code-error').removeClass('hidden');
                }
            });
        })(window, jQuery);
    </script>
@endsection