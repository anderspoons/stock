<div class="form-group col-md-6 col-sm-12">
    @if(Request::is('job*'))
        {!! Form::label('status_ids', 'Job Status:') !!}
    @elseif(Request::is('items*'))
        {!! Form::label('status_ids', 'Item Status:') !!}
    @elseif(Request::is('order*'))
        {!! Form::label('status_ids', 'Order Status:') !!}
    @elseif(Request::is('counter*'))
        {!! Form::label('status_ids', 'Sale Status:') !!}
    @endif
    <select name="status_id[]" id="statuses" class="form-control"

            @if(isset($billed))
                @if($billed)
                    disabled
                @endif
            @endif
            multiple="multiple">
        @if(isset($job))
            @foreach($job->statuses()->get() as $status)
                <option selected="selected" value="{!! $status->id !!}">{!! $status->name !!}</option>
            @endforeach
        @elseif(isset($counter))
            @foreach($counter->statuses()->get() as $status)
                <option selected="selected" value="{!! $status->id !!}">{!! $status->name !!}</option>
            @endforeach
        @elseif(isset($item))
            @foreach($item->statuses()->get() as $status)
                <option selected="selected" value="{!! $status->id !!}">{!! $status->name !!}</option>
            @endforeach
        @elseif(isset($order))
            @foreach($order->statuses()->get() as $status)
                <option selected="selected" value="{!! $status->id !!}">{!! $status->name !!}</option>
            @endforeach
        @endif
    </select>
</div>

@push('footer-script')
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            var last_results = [];
            $("#statuses").select2({
                tags: true,
                tokenSeparators: [','],
                multiple: true,
                ajax: {
                    url: "{!! url('statuses/json') !!}",
                    dataType: "json",
                    data: function (term) {
                        return {
                            q: term,
                            @if(Request::is('job*'))
                            type: 'job'
                            @elseif(Request::is('order*'))
                            type: 'order'
                            @elseif(Request::is('item*'))
                            type: 'item'
                            @elseif(Request::is('counter*'))
                            type: 'counter'
                            @endif
                        };
                    },
                    results: function (data) {
                        console.log(data);
                        return {
                            results: data.results
                        };
                    }
                },
                createSearchChoice: function (term, data) {
                    if ($(data.results).filter(function () {
                                return this.text.localeCompare(term) === 0;
                            }).length === 0) {
                        return {
                            id: term,
                            text: term
                        };
                    }
                },
                templateResult: function (data) {
                    var $result = $("<span></span>");
                    $result.text(data.text);
                    if (data.newOption) {
                        $result.append(" <em>(new)</em>");
                    }
                    return $result;
                }
            });
        });
    </script>
@endpush