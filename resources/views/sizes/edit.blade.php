@extends('layouts.app')

@section('content')
   <section class="content-header">
           <h1>
               Size
           </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">

           <div class="box-body">
               <div class="row">
                   {!! Form::model($size, ['route' => ['sizes.update', $size->id], 'method' => 'patch']) !!}

                    @include('sizes.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection