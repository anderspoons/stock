{!! $dataTable->table(['width' => '100%']) !!}

@section('scripts')
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
    <script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
    <script src="vendor/datatables/buttons.server-side.js"></script>
    <script src="/js/extjs.js"></script>
    {!! $dataTable->scripts() !!}

    <script>
        // Add event listener for opening and closing details
        $('body').on('click', '.child-expand', function () {
            var tr = $(this).closest('tr');
            var row = window.LaravelDataTables.dataTableBuilder.row( tr );

            if ( row.child.isShown() ) {
                row.child.hide();
                $(this).find('i').removeClass('glyphicon-minus');
                $(this).find('i').addClass('glyphicon-plus');
                tr.removeClass('shown');
            }
            else
            {
                var data = row.data();
                var address = '<div class="col-md-12"><table style="width:100%;" class="no-footer">' +
                        '<thead><th>Line 1</th><th>Line 2</th><th>Town</th><th>County</th><th>Postcode</th></thead>' +
                        '<tbody>' +
                        '<tr><td>' + data.address.line_1 + '</td>' +
                        '<td>' + data.address.line_2 + '</td>' +
                        '<td>' + data.address.town + '</td>' +
                        '<td>' + data.address.county + '</td>' +
                        '<td>' + data.address.postcode + '</td></tr>' +
                        '</tbody>' +
                        '<table>' +
                        '</div>';
                row.child($('<tr><td colspan="8"><div class="row">' + address + '</div></td></tr>')).show();



                tr.addClass('shown');
                $(this).find('i').addClass('glyphicon-minus');
                $(this).find('i').removeClass('glyphicon-plus');
            }
        });


    </script>
@endsection