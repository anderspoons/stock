<select name="store_id" id="store-select2" class="form-control">
    @if(isset($order))
        @foreach($order->store()->get() as $store)
            <option selected="selected" value="{!! $store->id !!}">{!! $store->code !!}</option>
        @endforeach
    @endif
</select>

@push('footer-script')
    <script type="text/javascript">
        jQuery( document ).ready(function( $ ) {
            var last_results = [];
            $("#store-select2").select2({
                ajax: {
                    url: "{!! url('stores/json') !!}",
                    dataType: "json",
                    data: function(term) {
                        return {
                            q: term
                        };
                    },
                    results: function(data) {
                        console.log(data);
                        return {
                            results: data.results
                        };
                    }
                },
                createSearchChoice:function(term, data) {
                    if ($(data.results).filter(function() {
                                return this.text.localeCompare(term) === 0;
                            }).length===0) {
                        return {
                            id:term,
                            text:term
                        };
                    }
                }
            });
        });
    </script>
@endpush