<!DOCTYPE html>

<html>
<head>
    <meta charset="UTF-8">
    <title>Macduff Group - Stock System</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    @stack('meta-tags')


    <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('/css/font-awesome.min.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('/css/select2.min.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('/css/AdminLTE.min.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('/css/skins/_all-skins.min.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('/css/awesome-bootstrap-checkbox.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('/css/bootstrap-toggle/bootstrap-toggle.min.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('/css/jquery-ui.min.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('/css/jquery-ui.theme.min.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('/css/jquery-ui.structure.min.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ url('css/blue.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('/css/ionicons.min.css') }}" type="text/css"/>
    {{--Pnotify--}}
    <link rel="stylesheet" href="{{ asset('/css/pnotify/pnotify.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('/css/pnotify/pnotify.buttons.css') }}" type="text/css"/>
</head>
<body class="skin-blue sidebar-mini">

@if (!Auth::guest())
    <div class="wrapper">
        <!-- Main Header -->
        <header class="main-header">

            <!-- Logo -->
            <a href="#" class="logo">
                <b>Macduff Group</b>
            </a>

            <!-- Header Navbar -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- Messages: style can be found in dropdown.less-->
                        <li class="dropdown messages-menu">
                            <!-- Menu toggle button -->

                            <ul class="dropdown-menu">
                                <li>
                                    <!-- inner menu: contains the messages -->
                                    <ul class="menu">
                                        <li><!-- start message -->
                                            <a href="#">
                                                <div class="pull-left">
                                                    <!-- User Image -->
                                                    <img src="{{ asset('/img/Logo-small.png') }}"
                                                         class="img-circle" alt="User Image"/>
                                                </div>
                                                <!-- Message title and timestamp -->
                                                <h4>
                                                    Support Team
                                                    <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                                </h4>
                                                <!-- The message -->
                                                <p>Why not buy a new awesome theme?</p>
                                            </a>
                                        </li><!-- end message -->
                                    </ul><!-- /.menu -->
                                </li>
                                <li class="footer"><a href="#">See All Messages</a></li>
                            </ul>
                        </li><!-- /.messages-menu -->

                        <!-- Notifications Menu -->
                        <li class="dropdown notifications-menu">
                            <!-- User Account Menu -->
                        <li class="dropdown user user-menu">
                            <!-- Menu Toggle Button -->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <!-- The user image in the navbar-->
                                <img src="{{ asset('/img/Logo-small.png') }}"
                                     class="user-image" alt="User Image"/>
                                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                <span class="hidden-xs">
                                    @if (Auth::guest())
                                          Guest
                                    @else
                                        {{ Auth::user()->name }}
                                    @endif
                                </span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- The user image in the menu -->
                                <li class="user-header">
                                    <img src="{{ asset('/img/Logo-small.png') }}"
                                         class="img-circle" alt="User Image"/>
                                    <p>
                                        @if (Auth::guest())
                                            Guest
                                        @else
                                            {{ Auth::user()->name}}
                                        @endif
                                        <small>Member since Apr. 2015</small>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        {{--<a href="#" class="btn btn-default btn-flat">Account</a>--}}
                                    </div>
                                    <div class="pull-right">
                                        <a href="{{ url('/logout') }}" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

        <!-- Left side column. contains the logo and sidebar -->
        @include('layouts.sidebar')


                <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            @yield('content')
        </div>

        <!-- Main Footer -->
        <footer class="main-footer" style="max-height: 100px;text-align: center">
            <strong>Copyright © 2016 <a href="{{ url('') }}">Macduff Group</a>.</strong> All rights reserved.
        </footer>

    </div>
@else
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    Macduff Shipyards
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/') }}">Home</a></li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>


    <div id="page-content-wrapper">

        <div class="container-fluid">


            <div class="row">
                <div class="col-lg-12">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
    @endif

    <!-- jQuery 2.1.4 -->
    <script src="{{ asset('/js/jquery.min.js') }}"></script>
    <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/js/select2.min.js') }}"></script>
    <script src="{{ asset('/js/script.js') }}"></script>
    <script src="{{ asset('/js/bootstrap-toggle/bootstrap-toggle.min.js') }}"></script>
    <script src="{{ url('/js/icheck.min.js') }}"></script>

    <!-- AdminLTE App -->
    <script src="{{ asset('/js/app.min.js') }}"></script>

    <!-- Datatables -->
    <script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/js/dataTables.bootstrap.min.js') }}"></script>

    {{--Pnotify--}}
    <script src="{{ asset('/js/tabtalk.js') }}"></script>
    <script src="{{ asset('/js/pnotify/pnotify.js') }}"></script>
    <script src="{{ asset('/js/pnotify/pnotify.buttons.js') }}"></script>

    {{-- Quantities --}}
    <script src="{{ asset('/js/quantities.js') }}"></script>

    @yield('scripts')
    @yield('extras')
    @yield('extras1')
    @stack('footer-script')
</body>
</html>