<li class="{{ Request::url() == url('') ? 'active' : '' }}">
    <a href="{!! url('') !!}"><i class="fa fa-home"></i><span>Home</span></a>
</li>

<li class="{{ Request::is('transfers*') ? 'active' : '' }}">
    <a href="{!! route('transfers.index') !!}"><i class="fa fa-random"></i><span>Internal Transfers</span></a>
</li>

<li class="{{ Request::is('items*') ? 'active' : '' }}">
    <a href="{!! route('items.index') !!}"><i class="fa fa-sitemap"></i><span>Items</span></a>
</li>

<li class="{{ Request::is('barcode*') ? 'active' : '' }}">
    <a href="{!! url('barcode') !!}"><i class="glyphicon glyphicon-barcode"></i><span>Barcode Labels</span></a>
</li>

<li class="{{ Request::is('orders*') ? 'active' : '' }}">
    <a href="{!! route('orders.index') !!}"><i class="fa fa-truck"></i><span>Orders</span></a>
</li>

<li class="{{ Request::is('counters*') ? 'active' : '' }}">
    <a href="{!! route('counters.index') !!}"><i class="fa fa-shopping-basket"></i><span>Counter Sales</span></a>
</li>

<li class="{{ Request::is('jobs*') ? 'active' : '' }}">
    <a href="{!! route('jobs.index') !!}"><i class="fa fa-legal"></i><span>Jobs</span></a>
</li>

<li class="{{ Request::is('reports*') ? 'active' : '' }}">
    <a href="{!! url('reports') !!}"><i class="fa fa-pie-chart"></i><span>Reports</span></a>
</li>


<li class="{{ Request::is('locations*', 'stores*') ? 'active' : '' }}">
    <a><i class="fa fa-building-o"></i><span>Stores / Locations</span></a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('stores*') ? 'active' : '' }}">
            <a href="{!! route('stores.index') !!}"><i class="fa fa-building-o"></i><span>Stores</span></a>
        </li>

        <li class="{{ Request::is('locations*') ? 'active' : '' }}">
            <a href="{!! route('locations.index') !!}"><i class="fa fa-list"></i><span>Locations</span></a>
        </li>
    </ul>
</li>

<li class="{{ Request::is('customers*', 'boats*', 'suppliers*') ? 'active' : '' }}">
    <a><i class="fa fa-building-o"></i><span>Customers / Suppliers</span></a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('customers*') ? 'active' : '' }}">
            <a href="{!! route('customers.index') !!}"><i class="fa fa-users"></i><span>Customers</span></a>
        </li>

        </li>
        <li class="{{ Request::is('boats*') ? 'active' : '' }}">
            <a href="{!! route('boats.index') !!}"><i class="fa fa-ship"></i><span>Boats</span></a>
        </li>

        <li class="{{ Request::is('suppliers*') ? 'active' : '' }}">
            <a href="{!! route('suppliers.index') !!}"><i class="fa fa-truck"></i><span>Suppliers</span></a>
        </li>
    </ul>
</li>


@if(Auth::user()->permission == 1)
    <li class="{{ Request::is('sexes*', 'types*', 'grades*', 'sizes*', 'materials*', 'partcodes*', 'prices*') ? 'active' : '' }}">
        <a><i class="fa fa-sitemap"></i><span>Item Properties</span></a>

        <ul class="treeview-menu">

            <li class="{{ Request::is('sexes*') ? 'active' : '' }}">
                <a href="{!! route('sexes.index') !!}"><i class="fa fa-transgender"></i><span>Sexes</span></a>
            </li>

            <li class="{{ Request::is('types*') ? 'active' : '' }}">
                <a href="{!! route('types.index') !!}"><i class="fa fa-filter"></i><span>Types</span></a>
            </li>

            <li class="{{ Request::is('grades*') ? 'active' : '' }}">
                <a href="{!! route('grades.index') !!}"><i class="fa fa-info"></i><span>Grades</span></a>
            </li>

            <li class="{{ Request::is('materials*') ? 'active' : '' }}">
                <a href="{!! route('materials.index') !!}"><i class="fa fa-edit"></i><span>Materials</span></a>
            </li>

            <li class="{{ Request::is('sizes*') ? 'active' : '' }}">
                <a href="{!! route('sizes.index') !!}"><i class="glyphicon glyphicon-resize-horizontal"></i><span>Sizes</span></a>
            </li>

            <li class="{{ Request::is('prices*') ? 'active' : '' }}">
                <a href="{!! route('prices.index') !!}"><i class="glyphicon glyphicon-gbp"></i><span>Prices</span></a>
            </li>

            <li class="{{ Request::is('partcodes*') ? 'active' : '' }}">
                <a href="{!! route('partcodes.index') !!}"><i class="fa fa-edit"></i><span>Part Codes</span></a>
            </li>

        </ul>
    </li>

    <li class="{{ Request::is('addresses*') ? 'active' : '' }}">
        <a href="{!! route('addresses.index') !!}"><i class="glyphicon glyphicon-envelope"></i><span>Addresses</span></a>
    </li>

    <li class="{{ Request::is('users*') ? 'active' : '' }}">
        <a href="{!! route('users.index') !!}"><i class="glyphicon glyphicon-user"></i><span>Users</span></a>
    </li>

    <li class="{{ Request::is('pincodes*') ? 'active' : '' }}">
        <a href="{!! route('pincodes.index') !!}"><i class="glyphicon glyphicon-lock"></i><span>Pincodes</span></a>
    </li>

    <li class="{{ Request::is('elements*') ? 'active' : '' }}">
        <a href="{!! route('elements.index') !!}"><i class="fa fa-edit"></i><span>Elements</span></a>
    </li>

    <li class="{{ Request::is('statuses*') ? 'active' : '' }}">
        <a href="{!! route('statuses.index') !!}"><i class="fa fa-edit"></i><span>Statuses</span></a>
    </li>
@endif

