<div class="modal fade bs-transfer-as-job-modal-sm" id="transfer-job-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Transfer as a Job.</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['url' => 'issue/job-as-counter', 'method' => 'post']) }}
                {{ Form::hidden('id', '', array('id' => 'counter-id')) }}
                {{ Form::hidden('type', 'job') }}
                <p>You are about to transfer this Counter Sale to a Job. Are you sure?</p>
                {{-- <div class="row ">
                    <div class="col-sm-12 form-group">
                        <div class="form-group">
                            <div class="col-sm-12 col-md-6 form-group">
                                {!! Form::label('pin_id', 'Name:') !!}
                                @if(isset($pincodes))
                                    <select class="form-control" name="pin_id">
                                        @foreach($pincodes as $user)
                                            <option value="{!! $user->id !!}">{!! $user->name !!}</option>
                                        @endforeach
                                    </select>
                                @endif
                            </div>
                            <div class="col-sm-12 col-md-6 form-group">
                                {!! Form::label('pin', 'Security Pin:') !!}
                                {!! Form::password('pin', array('class' => 'form-control')) !!}
                                <span class="code-error hidden">Wrong pin code entered.</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 form-group">
                        <div class="form-group">
                            <div class="col-sm-12 form-group">
                                {!! Form::label('reason', 'Transfer Reason:') !!}
                                {!! Form::text('reason', '', array('class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div> --}}
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Confirm', ['class' => 'btn btn-danger counter-to-job']) !!}
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

@push('footer-script')
<script type="text/javascript">
    jQuery( document ).ready(function($){
        $('body').on('click', '.transfer-as-job', function(){
            $('#transfer-job-modal form')[0].reset();
            $('#transfer-job-modal #counter-id').val($(this).attr('data-id'));
            $('#transfer-job-modal .counter-to-job').removeAttr('disabled')
        })

        //Submit the form by ajax
        $('body').on('click', '.counter-to-job', function(e){
            e.preventDefault();
            var form = $('#transfer-job-modal form');
            $('#transfer-job-modal .counter-to-job').attr('disabled', 'disabled')
            $.ajax({
                type:"POST",
                url:form.attr("action"),
                data:form.serialize(),
                success: function(response){
                    $('#transfer-job-modal .counter-to-job').removeAttr('disabled')
                    $('#transfer-job-modal .close').click();
                    var myStack = {"dir1":"down", "dir2":"right", "push":"top"};
                    new PNotify({
                        title: "Success",
                        text:  response.success.text,
                        addclass: "stack-custom",
                        stack: myStack
                    })
                    $('.buttons-reload').click();
                },
                error: function(response){
                    var myStack = {"dir1":"down", "dir2":"right", "push":"top"};
                    new PNotify({
                        title: "Problem",
                        text: response.text,
                        addclass: "stack-custom",
                        stack: myStack
                    })
                }
            });
        });
    });
</script>
@endpush

