<div class="modal fade bs-transfer-job-modal-sm" id="transfer-job-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Transfer to Job.</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['url' => 'issue/job-transfer', 'method' => 'post']) }}
                <div class="row ">
                    <div class="col-sm-12 form-group">
                        <div class="form-group col-sm-12">
                            {{ Form::hidden('counter_item_id', '', array('id' => 'item-id')) }}
                            <p>Make sure you have created a job before trying to transfer an item to it.</p>
                            {!! Form::label('job_id', 'Job:') !!}
                            @if(isset($jobs))
                                <select class="form-control" name="job_id">
                                    @foreach($jobs as $job)
                                        @php
                                            $name = '';
                                            if(isset($job->boat->name)){
                                                $name .= $job->boat->name;
                                            }
                                            if(isset($job->customer->name)){
                                                if(strlen($name) > 0){
                                                    $name .= ' : ';
                                                }
                                                $name .= $job->customer->name;
                                            }
                                            if(strlen($job->note) > 0){
                                                if(strlen($name) > 0){
                                                    $name .= ' : ';
                                                }
                                                $name .= $job->note;
                                            }
                                        @endphp
                                        <option value="{!! $job->id !!}">{!! $name !!}</option>
                                    @endforeach
                                </select>
                            @endif
                        </div>
                    </div>

                    <div class="col-sm-12 form-group">
                        <div class="form-group">
                            <div class="col-sm-12 col-md-6 form-group">
                                {!! Form::label('pin_id', 'Name:') !!}
                                @if(isset($pincodes))
                                    <select class="form-control" name="pin_id">
                                        @foreach($pincodes as $user)
                                            <option value="{!! $user->id !!}">{!! $user->name !!}</option>
                                        @endforeach
                                    </select>
                                @endif
                            </div>
                            <div class="col-sm-12 col-md-6 form-group">
                                {!! Form::label('pin', 'Security Pin:') !!}
                                {!! Form::password('pin', array('class' => 'form-control')) !!}
                                <span class="code-error hidden">Wrong pin code entered.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Confirm', ['class' => 'btn btn-danger item-transfer-button']) !!}
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

@push('footer-script')
<script type="text/javascript">
    jQuery( document ).ready(function($){
        $('body').on('click', '.transfer-job', function(){
            $('#transfer-job-modal form')[0].reset();
            $('#transfer-job-modal #item-id').val($(this).attr('line-id'));
        })

        //Submit the form by ajax
        $('body').on('click', '.item-transfer-button', function(e){
            e.preventDefault();
            var form = $('#transfer-job-modal form');
            $.ajax({
                type:"POST",
                url:form.attr("action"),
                data:form.serialize(),
                success: function(response){
                    $('#transfer-job-modal .close').click();
                    var myStack = {"dir1":"down", "dir2":"right", "push":"top"};
                    new PNotify({
                        title: "Success",
                        text: response.text,
                        addclass: "stack-custom",
                        stack: myStack
                    })
                    $('.buttons-reload').click();
                },
                error: function(response){
                    var myStack = {"dir1":"down", "dir2":"right", "push":"top"};
                    new PNotify({
                        title: "Problem",
                        text: response.text,
                        addclass: "stack-custom",
                        stack: myStack
                    })
                }
            });
        });
    });
</script>
@endpush

