<div class="modal fade bs-transfer-as-counter-modal-sm" id="transfer-counter-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Transfer as a Counter Sale.</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['url' => 'issue/counter-as-job', 'method' => 'post']) }}
                {{ Form::hidden('id', '', array('id' => 'job-id')) }}
                {{ Form::hidden('type', 'counter') }}
                <p>You are about to transfer this Job to a Counter Sale. Are you sure?</p>
                {{-- <div class="row ">
                    <div class="col-sm-12 form-group">
                        <div class="form-group">
                            <div class="col-sm-12 col-md-6 form-group">
                                {!! Form::label('pin_id', 'Name:') !!}
                                @if(isset($pincodes))
                                    <select class="form-control" name="pin_id">
                                        @foreach($pincodes as $user)
                                            <option value="{!! $user->id !!}">{!! $user->name !!}</option>
                                        @endforeach
                                    </select>
                                @endif
                            </div>
                            <div class="col-sm-12 col-md-6 form-group">
                                {!! Form::label('pin', 'Security Pin:') !!}
                                {!! Form::password('pin', array('class' => 'form-control')) !!}
                                <span class="code-error hidden">Wrong pin code entered.</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 form-group">
                        <div class="form-group">
                            <div class="col-sm-12 form-group">
                                {!! Form::label('reason', 'Transfer Reason:') !!}
                                {!! Form::text('reason', '', array('class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div> --}}
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Confirm', ['class' => 'btn btn-danger job-to-counter']) !!}
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

@push('footer-script')
<script type="text/javascript">
    jQuery( document ).ready(function($){
        $('body').on('click', '.transfer-as-counter', function(){
            $('#transfer-counter-modal #job-id').val($(this).attr('data-id'));
            $('#transfer-counter-modal form')[0].reset();
            $('#transfer-counter-modal .job-to-counter').removeAttr('disabled')
        })

        //Submit the form by ajax
        $('body').on('click', '.job-to-counter', function(e){
            e.preventDefault();
            var form = $('#transfer-counter-modal form');
            $('#transfer-counter-modal .job-to-counter').attr('disabled', 'disabled')
            $.ajax({
                type:"POST",
                url:form.attr("action"),
                data:form.serialize(),
                success: function(response){
                    $('#transfer-counter-modal .close').click();
                    var myStack = {"dir1":"down", "dir2":"right", "push":"top"};
                    new PNotify({
                        title: "Success",
                        text:  response.success.text,
                        addclass: "stack-custom",
                        stack: myStack
                    })
                    $('.buttons-reload').click();
                    $('#transfer-counter-modal .job-to-counter').removeAttr('disabled')
                },
                error: function(response){
                    var myStack = {"dir1":"down", "dir2":"right", "push":"top"};
                    new PNotify({
                        title: "Problem",
                        text: response.text,
                        addclass: "stack-custom",
                        stack: myStack
                    })
                }
            });
        });
    });
</script>
@endpush

