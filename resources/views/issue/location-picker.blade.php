{!! Form::label('back-location', 'Location:') !!}
<select class="back-location" name="item_return_location"></select>

@push('footer-script')
<script type="text/javascript">
    (function(window, $) {
        jQuery( document ).ready(function( $ ) {
            $('body').on('click', '.back-stock', function () {
                $('.back-location').val("").trigger("change")
            })

            $(".back-location").select2({
                ajax: {
                    url: "{!! url('location/item_lookup') !!}",
                    dataType: "json",
                    data: function(term) {
                        return {
                            q: term,
                            item_original: $('.bs-back-stock-modal-sm #item-location').val(),
                            item_id: $('.bs-back-stock-modal-sm #real-item-id').val()
                        };
                    },
                    results: function(data) {
                        console.log(data);
                        return {
                            results: data.results
                        };
                    }
                },
                createSearchChoice:function(term, data) {
                    if ($(data.results).filter(function() {
                            return this.text.localeCompare(term) === 0;
                        }).length===0) {
                        return {
                            id:term,
                            text:term
                        };
                    }
                }
            });
        });
    })(window, jQuery);
</script>
@endpush