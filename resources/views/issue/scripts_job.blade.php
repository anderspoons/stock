@section('scripts')
    <script type="text/javascript">
        var lastItem;
        (function(window, $) {
            $('#barcode-search').bind('input',function(){
                if($(this).val().length == 9){
                    lookupItem(true);
                }
                if ($(this).val().toUpperCase().indexOf("MS") == -1 && $(this).val().length == 7) {
                    lookupItem(false);
                }
            });

            $('#item-details-modal').find('#qty-box').on('keyup', function() {
                var maxval =  $('#item-details-modal').find('.loc-select').find(":selected").attr('data-qty');
                $(this).attr({
                    "max" : maxval,
                    "min" : 1
                });
                if(parseInt($(this).val()) > parseInt(maxval)){
                    $(this).val('');
                    showError('The qty entered is more than is currently in stock.');
                }
            });

            $('body').on('keyup', '#sizing .size-template .size-box input', function () {
                console.log('Called quantity check');
                if ($('#sizing .size-template .size-box').length == 2) {
                    //check both boxes are filled before trying to calculate / values.
                    var check = true;
                    $.each($('#sizing .size-template .size-box input'), function (e) {
                        if ($(this).val().length < 1) {
                            check = false;
                            return false;
                        }
                    });

                    if (check) {
                        if (check_area_val($('#item-details-modal').find('.loc-select option:selected').attr('data-qty')) == false) {
                            $(this).val('');
                            showError('The qty entered is more than is currently in stock.');
                        }
                    }
                } else {
                    if (checkVal($(this), $('#item-details-modal').find('.loc-select option:selected').attr('data-qty')) == false) {
                        $(this).val('');
                        showError('The qty entered is more than is currently in stock.');
                    }
                }
            });

            $('body').on('change', '.unit-select', function() {
                if ($('#sizing .size-template .size-box input').length == 2) {
                    //check both boxes are filled before trying to calculate / values.
                    var check = true;
                    $.each($('#sizing .size-template .size-box input'), function (e) {
                        if ($(this).val().length < 1) {
                            check = false;
                            return false;
                        }
                    });
                    if (check) {
                        if (check_area_val($('#item-details-modal').find('.loc-select option:selected').attr('data-qty')) == false) {
                            $.each($('#sizing .size-template .size-box input'), function (e) {
                                $(this).val('0');
                            });
                            showError('The unit chosen has less in stock when converted than the one changed from.');
                        }
                    }
                } else {
                    if(checkVal($('#item-details-modal #size').eq(0), $('#item-details-modal').find('.loc-select option:selected').attr('data-qty')) == false){
                        $(this).val('0');
                        showError('The unit chosen has less in stock when converted than the one changed from.');
                    }
                }
            })

            function check_area_val(qty_loc){
                var num_1 = $('#sizing .size-template .size-box input').eq(0).val();
                var num_2 = $('#sizing .size-template .size-box input').eq(1).val();
                var selected_unit = $('#sizing .unit-select option:selected').attr('data-alias');
                var reporting_unit = $("#sizing .unit-select option[value='" + $('.bs-details-modal-sm #unit-report').val() + "']").attr('data-alias');
                var num_1_converted = new Qty(parseFloat(num_1), selected_unit);
                var num_2_converted = new Qty(parseFloat(num_2), selected_unit);
                var location_qty = new Qty(parseFloat(qty_loc), reporting_unit);
                var converted_area = parseFloat(num_1_converted.to(reporting_unit)).toFixed(2) * parseFloat(num_2_converted.to(reporting_unit)).toFixed(2);

                if(converted_area > parseFloat(location_qty)){
                    return false;
                } else {
                    $('#item-details-modal #qty-box').val(parseFloat(converted_area).toFixed(2));
                    return true;
                }
            };

            function checkVal(element, qty) {
                console.log('Check Value called');
                var entered_amount = parseFloat($(element).val());
                var selected_unit = $('#sizing .unit-select option:selected').attr('data-alias');
                var reporting_unit = $(element).parent().parent().find(".unit-select option[value='" + $('.bs-details-modal-sm #unit-report').val() + "']").attr('data-alias');
                var entered_qty = new Qty(parseFloat(entered_amount), selected_unit);
                var location_qty = new Qty(parseFloat(qty), reporting_unit);
                if (entered_qty.gt(location_qty)) {
                    return false;
                } else {
                    $('#item-details-modal #qty-box').val(parseFloat(entered_qty.to(reporting_unit)).toFixed(2).toString());
                    return true;
                }
            }
            $('body').on('click', '.confirm-misc-button', function(e) {
                var template = $('#item-template').clone();
                var qty = 0;
                var cost = 0;
                if($('#misc-details-modal').find('#qty-box').val().length > 0) {
                    qty = parseFloat($('#misc-details-modal').find('#qty-box').val()).toFixed(2).toString();
                } else {
                    showError('Please specify a quantity.');
                    return true;
                }
                if($('#misc-details-modal').find('#cost').val().length > 0) {
                    cost = parseFloat($('#misc-details-modal').find('#cost').val()).toFixed(2).toString();
                } else {
                    cost = '0';
                }
                template.find('.item-id').val('misc');
                template.find('.item-quantity').val(qty);

                var markup = (parseFloat(cost) * 0.2) + parseFloat(cost);
                template.find('.item-unit-cost').val(parseFloat(markup).toFixed(2));
                template.find('.item-cost').val(parseFloat(markup * qty).toFixed(2));
                template.find('.item-real-cost').val(parseFloat(cost).toFixed(2));
                template.find('.item-cost-text').val(parseFloat(markup * qty).toFixed(2));

                template.find('.item-barcode').parent().addClass('hidden');
                template.find('.item-location').parent().addClass('hidden');
                template.find('.item-details').val($('#misc-details-modal .info-box').val());
                template.find('.item-details').parent().removeClass('hidden');
                template.find('.item-description').val($('#misc-details-modal #misc_name').val());

                var tempID = Date.now().toString();
                var elements = '';
                if($('#misc-details-modal .element-select').val() != null){
                    var elements = $('#misc-details-modal .element-select').val().toString();
                } else {
                    showError('Please specify a job element this is being used for.')
                    return true;
                }
                var element_names = $('#misc-details-modal .element-select').select2('data');
                template.find('.element-select-template').attr('identity', tempID)
                var element_text = '';
                $.each(element_names, function(e){
                    element_text = element_text + this.text + '| ';
                });
                template.find('.element-names').val(element_text.replace(/|\s*$/, ""));
                template.find('.element-ids').val(elements);
                //Remove the hidden class
                template.removeClass('hidden');
                //set ID to thew time so we don't accidently reuse this ID
                template.attr('id', tempID);
                template.find('.row-id').val(tempID);
                $('#item-row').append(template);
                $('#misc-details-modal').find('.close').click();
                reindex_rows();
                enableBeforeUnload('Adding Leave warning');
            });

            $('body').on('click', '.confirm-issue-button', function(e){
                var locationid = $('#item-details-modal').find('.loc-select').val();
                var qty = 0;
                //Checks for a 0 quantity value
                if($('#item-details-modal').find('#qty-box').val().length > 0) {
                    qty = $('#item-details-modal').find('#qty-box').val();
                } else {
                    showError('Please specify a quantity.');
                    return true;
                }

                var cost = 0;
                var real_cost = 0;
                if($('#item-details-modal').find('#cost').val().length > 0) {
                    cost = roundUp(parseFloat($('#item-details-modal').find('#cost').val()).toFixed(3), 100);
                    real_cost = roundUp(parseFloat($('#item-details-modal').find('#real-cost').val()).toFixed(3), 100);
                } else {
                    showError('Please specify a cost.');
                    return true;
                }

                var template = $('#item-template').clone();

                template.find('.item-id').val(lastItem[0].id);
                template.find('.item-quantity').val(qty);
                template.find('.item-unit-cost').val(parseFloat(cost).toFixed(2));
                template.find('.item-cost').val(roundUp(parseFloat(qty * cost).toFixed(2), 100));
                template.find('.item-cost-text').val(parseFloat(qty * cost).toFixed(2));
                template.find('.item-real-cost').val(parseFloat(real_cost).toFixed(2));
                template.find('.item-description').val(lastItem[0].description);

                $.each(lastItem[0].stores, function(i, loc){
                    if(loc.pivot.id == locationid) {
                        template.find('.item-location').val(loc.code + ' : ' + loc.pivot.room + loc.pivot.rack + loc.pivot.shelf + loc.pivot.bay);
                        template.find('.item-location-id').val(loc.pivot.id);
                    }
                });
                template.find('.item-barcode').val(lastItem[0].barcode);

                var tempID = Date.now().toString();
                var elements = '';
                if($('#item-details-modal .element-select').val() != null){
                    var elements = $('#item-details-modal .element-select').val().toString();
                } else {
                    showError('Please specify a job element this is being used for.');
                    return true;
                }

                var element_names = $('#item-details-modal .element-select').select2('data');
                template.find('.element-select-template').attr('identity', tempID)
                var element_text = '';
                $.each(element_names, function(e){
                    element_text = element_text + this.text;
                });
                template.find('.element-names').val(element_text.replace(/|\s*$/, ""));
                template.find('.element-ids').val(elements);

                if($('#item-details-modal .size-unit').is(":visible")){
                    var visible_count = 0;
                    $.each($('#item-details-modal .size-unit'), function(){
                        if($(this).is(':visible')){
                            visible_count++;
                        }
                    });
                    for (var i = template.find('.size-template').length; i < visible_count; i++) {
                        var size_box = template.find('.size-template').eq(0).clone();
                        template.find('.size-stuff div').eq(0).append(size_box);
                    }
                    var i = 0;
                    $.each($('#item-details-modal .size-unit'), function(){
                        if($(this).is(':visible')){
                            template.find('.size-box').eq(i).val($(this).val());
                            i++;
                        }
                    });
                    template.find('.size-stuff').removeClass('hidden');
                    template.find('.item-unit').val($.trim($('#item-details-modal .unit-select option:selected').text()));
                    template.find('.item-unit-id').val($('#item-details-modal .unit-select option:selected').val());
                } else {
                    template.find('.size-stuff').remove();
                }
                //Remove the hidden class
                template.removeClass('hidden');
                //set ID to thew time so we don't accidently reuse this ID
                template.attr('id', tempID);
                template.find('.row-id').val(tempID);
                $('#item-row').append(template);
                $('#item-details-modal').find('.close').click();
                $('#item-details-modal input').val("");
                reindex_rows();
                enableBeforeUnload('Loading the leave page warning.');
            })

            $('#search-item').click(function(e){
                OpenInNewTab("{!! url('/items?job_issue') !!}");
            })

            $('body').on('click', '.remove-row', function(e){
                $(this).parent().parent().parent().remove();
                reindex_rows();
            });

            function OpenInNewTab(url) {
                var win = window.open(url, '_blank');
                win.focus();
            }

            function doUnits(id, unit_id){
                var unit_box = $('#sizing').find('.unit-select');
                var all_units = $('#sizing').find('#unit-options');
                console.log(unit_box);
                console.log(all_units);
                if(id == 0){
                    $.each(all_units.find('option'), function(e){
                        unit_box.append($(this));
                    });
                } else {
                    $.each(all_units.find('option'), function(e){
                        if($(this).attr('data-cat') == id){
                            if($(this).val() == unit_id){
                                unit_box.append($(this));
                                $(this).attr('selected', 'selected');
                            } else {
                                unit_box.append($(this));
                            }
                        }
                    });
                }
                //unit_box.find('option').eq(0).attr('selected', 'selected');
            }

            TabTalk.setCallback(function(data) {
                if(data.message.event == 'issue-item'){
                    $('#barcode-search').val(data.message.barcode);
                    lookupItem(true);
                }
            });


            function openModal(data){
                $('#sizing').html('');
                var user_store = "{!! $user->store_id !!}";
                var totalQty = 0;
                $('.bs-details-modal-sm').find('.loc-select').html('');
                $.each(data[0].stores, function(i, store){
                    if(store.id == user_store){
                        totalQty += store.pivot.qty;
                        $('.bs-details-modal-sm').find('.loc-select').append('<option value="'+store.pivot.id+'" data-qty="'+store.pivot.qty+'">'+ store.code + ' : ' +store.pivot.room + store.pivot.rack + store.pivot.shelf + store.pivot.bay +' : Qty: '+store.pivot.qty+'</option>')
                    }
                });

                if(totalQty==0){
                    $(".confirm-issue-button").prop("disabled",true);
                } else {
                    $(".confirm-issue-button").prop("disabled",false);
                }

                var cost = 0;
                var real_cost = 0;
                try {
                    $.each(data[0].last_price, function(e){
                        if(this.cost > 0){
                            real_cost = this.cost;
                            return false;
                        }
                    });
                } catch (exp) {
                    console.log('No cost price known');
                }
                if(data[0].markup_fixed === 0 && data[0].markup_percent != 0){
                    if(real_cost > 0){
                        cost = ((real_cost / 100) * data[0].markup_percent) + real_cost;
                    }
                } else {
                    cost = data[0].markup_fixed;
                }
                $('.bs-details-modal-sm').find('#cost').val(roundUp(cost, 100));
                $('.bs-details-modal-sm').find('#real-cost').val(real_cost);

                if(data[0].unit_category != null){
                    $('.bs-details-modal-sm #unit-report').val(data[0].unit_id);
                    if(data[0].unit_category.id != '6' && data[0].unit_category.id != '7'){
                        HideQty(true);
                        //6 Meaning that its individual in which case qty box is used.
                        var sizes = $('.modal-content .size-template').clone();
                        var count = sizes.find('.size-box').length;
                        var size_row = sizes.find('.size-box').first().clone();
                        for (var i = count; i < data[0].unit_category.num_sizes; i++) {
                            sizes.find('.size-box').after(size_row);
                        }
                        $('#sizing').append(sizes);
                        //3 is squared which means these will be legnths which is 2 ... shit I know but hey ho...
                        if(data[0].unit_category.id == 3) {
                            doUnits(2, data[0].unit_id);
                        } else {
                            doUnits(data[0].unit_category.id, data[0].unit_id);
                        }
                    } else if(data[0].unit_category.id == '7') {
                        $('.bs-details-modal-sm').find('.qty-info').hide();
                        var sizes = $('.modal-content .size-template .unit-select').clone();
                        $('#sizing').append('<label for="units">Unit:</label>');
                        $('#sizing').append(sizes);
                        var units = $('.modal-content #unit-options').clone();
                        $('#sizing').append(units);
                        doUnits(0, '');
                    } else {
                        //Its a size meaning we need to hide quantity
                        HideQty(false);
                    }
                } else {
                    var sizes = $('.modal-content .size-template').clone();
                    $('#sizing').append(sizes);
                    $('#sizing').append('<p class="label-warning">The units this item is sold in has not been set.</p>');
                    doUnits(0, data[0].unit_id);
                }
                $('.element-select').each(function(){
                    $(this).select2("val", "");
                })
                reindex_rows();
                enableBeforeUnload('Adding Leave warning');
            }
            
            function lookupItem(normal){
                var barcode = '';
                if(normal){
                    barcode = $('#barcode-search').val().toUpperCase();
                } else {
                    barcode = 'MS'+$('#barcode-search').val();
                }
                $.ajax({
                    url: "{!! url('/lookup/barcode') !!}/"+barcode,
                    type: 'GET',
                    dataType: 'Json',
                    success: function (data) {
                        if(data.success == true){
                            lastItem = data.data;
                            openModal(data.data);
                            $('#item-details-modal').modal('show');
                            $('#item-details-modal').find('.loc-select').change(function(){
                                var $qty = $(this).find(':selected').data("qty");
                                if($qty==0){
                                    $(".confirm-issue-button").prop("disabled",true);
                                } else {
                                    $(".confirm-issue-button").prop("disabled",false);
                                }
                            });
                        } else {
                            lastItem = {};
                            showError("No item with that barcode found.");
                            $('#barcode-search').val('');
                        }
                    }
                });
            }

            $('body').on('click', '.confirm-pin-button', function(e){
                disableBeforeUnload();
                e.preventDefault();
                disableRows(false);
                $('.confirm-pin-button').attr('disabled', 'disabled');
                submitForm($('#issue-stock-form'));
            });

            function submitForm(element){
                $.ajax({
                    url: "{!! url('issue/job/process') !!}",
                    type: 'POST',
                    dataType: 'Json',
                    data: $(element).serialize(),
                    success: function(data) {
                        $('.bs-confirm-issue-modal-sm .close').click();
                        new PNotify({
                            title: "Success",
                            text: data.text,
                            type: 'success',
                            addclass: "alert alert-success",
                        });
                        setTimeout(function(e){
                            location.reload(false);
                        },3000);
                    },
                    error: function(data){
                        data = $.parseJSON(data.responseText);
                        disableRows(true);
                        $('.confirm-pin-button').removeAttr('disabled');
                        new PNotify({
                            title: "Problems",
                            text: data.text,
                            type: 'error',
                            addclass: "alert alert-warning",
                        });
                        if(data.rows.length != -1){
                            highlightRows(data.rows);
                        }
                    }
                });
            }

            function reindex_rows(){
                console.log('Re-indexing rows');
                var x = 0;
                $.each($('#item-row > div'), function(){
                    $.each($(this).find("[name*='item[]']"), function(){
                        $(this).attr('name', $(this).attr('name').replace('item[]', 'item['+x+']'));
                    });
                    x++;
                    var s = 0;
                    $.each($(this).find('.size-template > input'), function(){
                        $(this).attr('name', $(this).attr('name').replace('[size][]', '[size]['+s+']'));
                        s++;
                    })
                })

            }

            //Used when error returned because of reasons
            function highlightRows(rows){
                //foreach html row on page
                    //foreach row in "rows"
                        //check if this row ID is there if not remove
                        //if there add the error for this row
            }

            function showError(msg){
                var myStack = {
                    "dir1":"down",
                    "dir2":"right",
                    "push":"top"
                };
                new PNotify({
                    title: "Problem",
                    text: msg,
                    type: "error",
                    addclass: "alert alert-warning",
                    stack: myStack
                });
            }

            function disableRows(toggle){
                if(toggle == false){
                    $('#item-row [disabled=disabled]').removeAttr('disabled');
                } else {
                    $('#item-row input,#item-row select').attr('disabled', true);
                }
            }


            function HideQty(boool){
                if(boool == false){
                    $('.bs-details-modal-sm').find('.qty-box').show();
                    $('.bs-details-modal-sm').find('.qty-info').hide();
                } else {
                    $('.bs-details-modal-sm').find('.qty-box').hide();
                    $('.bs-details-modal-sm').find('.qty-info').show();
                }
            }

            function roundUp(num, precision) {
                return Math.ceil(num * precision) / precision
            }


            function enableBeforeUnload(string) {
                console.log(string);
                window.onbeforeunload = function (e) {
                    return "You are about to leave this page, you have not saved these changes are you sure you want to leave?";
                };
            }
            function disableBeforeUnload() {
                window.onbeforeunload = null;
            }
        })(window, jQuery);
    </script>
@endsection