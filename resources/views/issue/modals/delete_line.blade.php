<div class="modal fade bs-delete-modal-sm" id="delete-item-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirm Deletion</h4>
            </div>
            <div class="modal-body">
                @if(Request::is('counters/labour/*'))
                    {{ Form::open(['url' => 'labour/destroy', 'method' => 'post']) }}
                @elseif(Request::is('counters/carriage/*'))
                    {{ Form::open(['url' => 'carriage/destroy', 'method' => 'post']) }}
                @elseif(Request::is('counters/crane/*'))
                    {{ Form::open(['url' => 'crane/destroy', 'method' => 'post']) }}
                @elseif(Request::is('jobs/labour/*'))
                    {{ Form::open(['url' => 'labour/destroy', 'method' => 'post']) }}
                @elseif(Request::is('jobs/carriage/*'))
                    {{ Form::open(['url' => 'carriage/destroy', 'method' => 'post']) }}
                @elseif(Request::is('jobs/crane/*'))
                    {{ Form::open(['url' => 'crane/destroy', 'method' => 'post']) }}
                @endif

                @if(isset($job))
                    {!! Form::hidden('type', 'job', array('class' => 'id form-control')) !!}
                @elseif(isset($counter))
                    {!! Form::hidden('type', 'counter', array('class' => 'id form-control')) !!}
                @endif

                {{ Form::hidden('id', '', array('id' => 'delete-id')) }}

                <div class="row ">
                    @include('issue.modals.pin-confirm')
                </div>
                {{ Form::close() }}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger confirm-delete-button">Confirm</button>
            </div>
        </div>
    </div>
</div>

@push('footer-script')
<script type="text/javascript">
    (function (window, $) {
        $('body').on('click', '.delete-item', function () {
            $('#delete-item-modal #delete-id').val($(this).attr('line-id'));
            $('#delete-item-modal .confirm-delete-button').removeAttr('disabled')
        })

        $('body').on('click', '.confirm-delete-button', function (e) {
            $('#delete-item-modal .confirm-delete-button').attr('disabled', 'disabled')
            e.preventDefault();
            if ($('.code-error').is('visible')) {
                $('.code-error').addClass('hidden');
            }
            var form = $('#delete-item-modal form');
            $.ajax({
                type: "POST",
                url: form.attr("action"),
                data: form.serialize(),
                success: function (response) {
                    $('#delete-item-modal').find('button[data-dismiss="modal"]').click();
                    $('.buttons-reload').click();
                    $('#delete-item-modal .confirm-delete-button').removeAttr('disabled')
                    var myStack = {"dir1": "down", "dir2": "right", "push": "top"};
                    new PNotify({
                        title: "Success",
                        text: response.text,
                        addclass: "stack-custom",
                        stack: myStack
                    })
                },
                error: function (response) {
                    $('.code-error').removeClass('hidden');
                }
            });
        });

    })(window, jQuery);
</script>
@endpush