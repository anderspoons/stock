<div class="modal fade bs-add-labour-modal-sm" id="add-labour-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Labour</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['url' => 'labour/add', 'method' => 'post']) }}

                <div class="row">
                    @if(isset($job))
                        {!! Form::hidden('id', $job->id, array('class' => 'form-control')) !!}
                        {!! Form::hidden('type', 'job', array('class' => 'form-control')) !!}
                    @elseif(isset($counter))
                        {!! Form::hidden('id', $counter->id, array('class' => 'form-control')) !!}
                        {!! Form::hidden('type', 'counter', array('class' => 'form-control')) !!}
                    @endif

                    <div class="form-group">
                        <div class="col-sm-12 col-md-6 form-group">
                            {!! Form::label('name', 'Labour Name: ') !!}
                            {!! Form::text('name', '', array('class' => 'form-control name')) !!}
                        </div>

                        <div class="col-sm-12 col-md-6 form-group">
                            {!! Form::label('cost', 'Labour Cost (Excluding VAT): ') !!}
                            {!! Form::text('cost', '', array('class' => 'form-control cost')) !!}
                        </div>
                    </div>

                    @include('issue.modals.pin-confirm')
                    <div class="clearfix"></div>
                </div>

                {{ Form::close() }}
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary add-labour">Add Labour</button>
            </div>
        </div>
    </div>
</div>

@push('footer-script')
    <script type="text/javascript">
        jQuery( document ).ready(function($){
            $('body').on('click', '.add-labour', function(e) {
                console.log('Clicked');
                e.preventDefault();
                var form = $('#add-labour-modal').find('form');

                $.ajax({
                    type:"POST",
                    url:form.attr("action"),
                    data:form.serialize(),
                    success: function (response) {
                        $('#add-labour-modal').find('button[data-dismiss="modal"]').click();
                        $('#dataTableBuilder_wrapper .buttons-reload').click();
                        var myStack = {"dir1": "down", "dir2": "right", "push": "top"};
                        new PNotify({
                            title: "Success",
                            text: response.text,
                            addclass: "stack-custom",
                            stack: myStack
                        })
                    },
                    error: function (response) {
                        $('.code-error').removeClass('hidden');
                    }
                });
            });
        })
    </script>
@endpush
