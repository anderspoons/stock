<div class="modal fade bs-description-modal-sm" id="set-description-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Misc Description</h4>
            </div>
            @if(Request::is('counters/*'))
                {{ Form::open(['url' => 'counter/misc/description', 'method' => 'post']) }}
                {!! Form::hidden('type', 'counter', array('class' => 'form-control')) !!}
            @elseif(Request::is('jobs/*'))
                {{ Form::open(['url' => 'job/misc/description', 'method' => 'post']) }}
                {!! Form::hidden('type', 'job', array('class' => 'form-control')) !!}
            @endif

            <div class="modal-body">
                {!! Form::hidden('id', '', array('class' => 'form-control', 'id' => 'misc-id')) !!}
                {!! Form::hidden('item_id', '', array('class' => 'form-control', 'id' => 'item-id')) !!}

                <div class="row ">
                    <div class="col-sm-12 form-group">
                        <div class="form-group">
                            {{ Form::label('name', 'Name:') }}
                            {{ Form::text('name', '', array('id' => 'item-name', 'class' => 'form-control')) }}
                        </div>
                    </div>
                    <div class="col-sm-12 form-group">
                        <div class="form-group">
                            {{ Form::label('qty', 'QTY:') }}
                            {{ Form::number('qty', '', ['id' => 'item-qty', 'class' => 'form-control', 'min' => 1]) }}
                        </div>
                    </div>
                    <div class="col-sm-12 form-group">
                        <div class="form-group">
                            {{ Form::label('description', 'Description:') }}
                            {{ Form::textarea('description', '', array('id' => 'item-description', 'class' => 'form-control')) }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Confirm', ['class' => 'btn btn-primary item-desc']) !!}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>

@push('footer-script')
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('body').on('click', '.description-item', function () {
            console.log('Clicked to edit name and description');
            $('#set-description-modal #misc-id').val($(this).attr('line-id'));
            $('#set-description-modal #item-id').val($(this).attr('line-item-id'));
            $('#set-description-modal #item-description').val($(this).attr('line-desc'));
            $('#set-description-modal #item-name').val($(this).attr('line-name'));
            $('#set-description-modal #item-qty').val($(this).attr('line-qty'));
        })

        //Submit the form by ajax
        $('body').on('click', '.item-desc', function (e) {
            e.preventDefault();

            var form = $('#set-description-modal form');
            $.ajax({
                type: "POST",
                url: form.attr("action"),
                data: form.serialize(),
                success: function (response) {
                    $('#set-description-modal .close').click();
                    $('.buttons-reload').click();
                    var myStack = {"dir1": "down", "dir2": "right", "push": "top"};
                    new PNotify({
                        title: "Success",
                        text: response.text,
                        addclass: "stack-custom",
                        stack: myStack
                    })
                    $('.buttons-reload').click();
                },
                error: function (response) {
                    var myStack = {"dir1": "down", "dir2": "right", "push": "top"};
                    new PNotify({
                        title: "Problem",
                        text: response.text,
                        addclass: "stack-custom",
                        stack: myStack
                    })
                }
            });
        });
    });
</script>
@endpush

