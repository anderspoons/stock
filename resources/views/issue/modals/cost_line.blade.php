<div class="modal fade bs-set-cost-modal-sm" id="set-cost-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Line Cost</h4>
            </div>
            @if(Request::is('counters/labour/*') || Request::is('jobs/labour/*'))
                {{ Form::open(['url' => 'labour/update', 'method' => 'post']) }}
            @elseif(Request::is('counters/carriage/*') || Request::is('jobs/carriage/*'))
                {{ Form::open(['url' => 'carriage/update', 'method' => 'post']) }}
            @elseif(Request::is('counters/crane/*') || Request::is('jobs/crane/*'))
                {{ Form::open(['url' => 'crane/update', 'method' => 'post']) }}
            @endif

            <div class="modal-body">
                @if(isset($job))
                    {!! Form::hidden('type', 'job', array('class' => 'form-control')) !!}
                @elseif(isset($counter))
                    {!! Form::hidden('type', 'counter', array('class' => 'form-control')) !!}
                @endif

                {!! Form::hidden('id', '', array('class' => 'form-control', 'id' => 'item-cost-id')) !!}

                <div class="row ">
                    <div class="col-sm-12 form-group">
                        <div class="form-group">
                            {{ Form::label('cost', 'Line Cost (Excluding VAT):') }}
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-gbp"></i>
                                </span>
                                {{ Form::number('cost', '', array('id' => 'item-cost', 'class' => 'form-control')) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Confirm', ['class' => 'btn btn-primary item-cost']) !!}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>

@push('footer-script')
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('body').on('click', '.cost-btn', function () {
            $('#set-cost-modal #item-cost-id').val($(this).attr('line-id'));
            $('#set-cost-modal #item-cost').val($(this).attr('line-cost'));
        })

        //Submit the form by ajax
        $('body').on('click', '.item-cost', function (e) {
            e.preventDefault();

            var form = $('#set-cost-modal form');
            $.ajax({
                type: "POST",
                url: form.attr("action"),
                data: form.serialize(),
                success: function (response) {
                    $('#set-cost-modal .close').click();
                    $('.buttons-reload').click();
                    var myStack = {"dir1": "down", "dir2": "right", "push": "top"};
                    new PNotify({
                        title: "Success",
                        text: response.text,
                        addclass: "stack-custom",
                        stack: myStack
                    })
                    $('.buttons-reload').click();
                },
                error: function (response) {
                    var myStack = {"dir1": "down", "dir2": "right", "push": "top"};
                    new PNotify({
                        title: "Problem",
                        text: response.text,
                        addclass: "stack-custom",
                        stack: myStack
                    })
                }
            });
        });
    });
</script>
@endpush

