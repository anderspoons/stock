<div class="form-group">
    <div class="col-sm-12 col-md-6 form-group">
        {!! Form::label('pin_id', 'Name:') !!}
        @if(isset($pins))
            <select class="form-control" name="pin_id">
                @foreach($pins as $user)
                    <option value="{!! $user->id !!}">{!! $user->name !!}</option>
                @endforeach
            </select>
        @endif
    </div>
    <div class="col-sm-12 col-md-6 form-group">
        {!! Form::label('pin', 'Security Pin:') !!}
        {!! Form::password('pin', array('class' => 'form-control')) !!}
        <span class="code-error hidden">Wrong pin code entered.</span>
    </div>
</div>