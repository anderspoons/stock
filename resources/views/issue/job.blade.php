@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Issue Stock to Job</h1>
    </section>
    <div class="content">
        {{ Form::open(array('url' => 'issue/job/process', 'method' => 'post', 'id' => 'issue-stock-form')) }}
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    @if(isset($job->id))
                        {!! Form::hidden('id', $job->id) !!}
                    @endif

                    @if(isset($job->note))
                        <div class="col-sm-12 col-md-6">
                            {!! Form::label('note', 'Note:') !!}
                            {!! Form::textarea('note', $job->note, array('disabled' => 'disabled', 'class' => 'form-control')) !!}
                        </div>
                    @endif

                    @if(isset($job->work_scope))
                        <div class="col-sm-12 col-md-6">
                            {!! Form::label('work_scope', 'Work Scope:') !!}
                            {!! Form::textarea('work_scope', $job->work_scope, array('disabled' => 'disabled', 'class' => 'form-control')) !!}
                        </div>
                    @endif

                    @if(isset($job->boat))
                        <div class="col-sm-12 col-md-6">
                            {!! Form::label('boat', 'Boat:') !!}
                            {!! Form::text('boat', $job->boat->name.' : '.$job->boat->number, array('disabled' => 'disabled', 'class' => 'form-control')) !!}
                        </div>
                    @endif

                    @if(isset($job->customer))
                        <div class="col-sm-12 col-md-6">
                            {!! Form::label('customer', 'Customer:') !!}
                            {!! Form::text('customer', $job->customer->name, array('disabled' => 'disabled', 'class' => 'form-control')) !!}
                        </div>
                    @endif

                    @if(isset($job->id))
                        <div class="col-sm-12 col-md-6">
                            <br>
                            <a href="{{ route('jobs.edit', $job->id) }}" target="_blank" class='btn btn-default'>
                                Edit <i class="glyphicon glyphicon-edit"></i>
                            </a>
                        </div>
                    @endif

                    <div class="clearfix"></div>
                </div>

                <div class="row">
                    <br/>
                    <div class="clearfix"></div>
                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                            {!! Form::label('items[]', 'Item Search: (Enter Barcode)') !!}
                            {!! Form::text('barcode', '', array('maxlength' => '9', 'id' => 'barcode-search', 'class' => 'form-control' )) !!}
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                            {!! Form::label('', '&nbsp;') !!}
                            <div class="clearfix"></div>
                            {!! Form::button('<i class="glyphicon glyphicon-plus"></i>  Add Miscellaneous Item', array('data-toggle' => 'modal', 'data-target' => '.bs-misc-details-modal-sm', 'id' => 'issue-item', 'class' => 'btn btn-default')) !!}

                            {!! Form::button('<i class="glyphicon glyphicon-search"></i>  Search Items', array('id' => 'search-item', 'class' => 'btn btn-default')) !!}
                        </div>
                    </div>
                    <hr class="col-sm-12">
                    <div class="clearfix"></div>
                </div>

                <div class="" id="item-row">

                </div>

                <div class="row">
                    <!-- Submit Field -->
                    <div class="form-group col-sm-12">
                        <a href="{{ url('jobs') }}">
                            {!! Form::button('Back', array('class' => 'btn btn-default')) !!}
                        </a>
                        {!! Form::button('Issue Stock', array('data-toggle' => 'modal', 'data-target' => '.bs-confirm-issue-modal-sm', 'class' => 'btn btn-primary')) !!}
                        @include('issue.includes.confirm')
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>

    <div id="item-template" class="hidden">
        <div>
            <div class="row">
                {{-- Use the row id here so that if theres a problem with a particular row we can highlight this on the return for the ajax.--}}
                {!! Form::hidden('item[][row-id]', '', array('class' => 'row-id form-control')) !!}
                {!! Form::hidden('item[][id]', '', array('class' => 'item-id form-control')) !!}
                <div class="col-sm-12 col-md-3 form-group">
                    {!! Form::label('item[][name]', 'Description: ') !!}
                    {!! Form::text('item[][name]', '', array('class' => 'form-control item-description', 'disabled' => 'disabled')) !!}
                </div>

                <div class="col-sm-12 col-md-3 form-group">
                    {!! Form::hidden('item[][location]', '', array('class' => 'item-location-id form-control')) !!}
                    {!! Form::label('item[][location-desc]', 'Location: ') !!}
                    {!! Form::text('item[][location-desc]', '', array('class' => 'form-control item-location', 'disabled' => 'disabled')) !!}
                </div>

                <div class="col-sm-12 col-md-3 form-group hidden">
                    {!! Form::label('item[][item-details]', 'Details: ') !!}
                    {!! Form::text('item[][item-details]', '', array('class' => 'form-control item-details', 'disabled' => 'disabled')) !!}
                </div>

                <div class="col-sm-12 col-md-3 form-group">
                    {!! Form::label('item[][qty]', 'Quantity: ') !!}
                    {!! Form::text('item[][qty]', '', array('class' => 'form-control item-quantity', 'disabled' => 'disabled')) !!}
                </div>

                <div class="col-sm-12 col-md-3 form-group">
                    {!! Form::label('item[][cost]', 'Line Cost (Excluding VAT): ') !!}
                    {!! Form::hidden('item[][unit_cost]', '', array('class' => 'item-unit-cost')) !!}
                    {!! Form::hidden('item[][real_cost]', '', array('class' => 'item-real-cost')) !!}
                    {!! Form::hidden('item[][cost]', '', array('class' => 'item-cost')) !!}
                    {!! Form::text('', '', array('class' => 'form-control item-cost-text', 'disabled' => 'disabled')) !!}
                </div>

                <div class="col-sm-12 col-md-3 form-group">
                    {!! Form::label('item[][barcode]', 'Barcode') !!}
                    {!! Form::text('item[][barcode]', '', array('class' => 'form-control item-barcode', 'disabled' => 'disabled')) !!}
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="row">
                <div class="col-sm-12 col-md-3 form-group">
                    {!! Form::label('item[][element]', 'Job Elements: ') !!}
                    {{ Form::hidden('item[][element]', '', array('class' => 'element-ids')) }}
                    {{ Form::text('element_names', '', array('class'=> 'form-control element-names', 'disabled' => 'disabled')) }}
                </div>

                <div class="size-stuff hidden">
                    <div class="col-sm-12 col-md-3 form-group">
                        {!! Form::label('item[][size][]', 'Size: ') !!}
                        <div class="size-template">
                            {!! Form::text('item[][size][]', '', array('class' => 'form-control size-box', 'disabled' => 'disabled')) !!}
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-3 form-group">
                        {!! Form::label('item[][unit]', 'Size Unit: ') !!}
                        {!! Form::hidden('item[][unit]', null, array('class' => "item-unit-id form-control")) !!}
                        {!! Form::text('', null, array('class' => "item-unit form-control", 'disabled' => 'disabled')) !!}
                    </div>
                </div>

                <div class="col-sm-12 col-md-3 form-group">
                    <br>
                    <div class="clearfix"></div>
                    {!! Form::button('<i class="glyphicon glyphicon-remove"></i> Remove Item', ['class' => 'remove-row btn btn-default']) !!}
                </div>

                <hr class="col-sm-12"/>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection

@include('issue.includes.details_job')
@include('issue.includes.misc_job')
@include('issue.scripts_job')
