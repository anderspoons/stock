@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Issue Stock to Counter</h1>
    </section>
    <div class="content">
        {{ Form::open(array('url' => 'issue/counter/process', 'method' => 'post', 'id' => 'issue-stock-form')) }}
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    @if(isset($counter->id))
                        {!! Form::hidden('id', $counter->id) !!}
                    @endif

                    @if(isset($counter->note))
                        <div class="col-sm-12 col-md-6">
                            {!! Form::label('note', 'Note:') !!}
                            {!! Form::textarea('note', $counter->note, array('disabled' => 'disabled', 'class' => 'form-control')) !!}
                        </div>
                    @endif

                    @if(isset($counter->work_scope))
                        <div class="col-sm-12 col-md-6">
                            {!! Form::label('work_scope', 'Work Scope:') !!}
                            {!! Form::textarea('work_scope', $counter->work_scope, array('disabled' => 'disabled', 'class' => 'form-control')) !!}
                        </div>
                    @endif

                    @if(isset($counter->po))
                        <div class="col-sm-12 col-md-6">
                            {!! Form::label('po', 'PO:') !!}
                            {!! Form::text('po', $counter->po, array('disabled' => 'disabled', 'class' => 'form-control')) !!}
                        </div>
                    @endif

                    @if(isset($counter->sage_invoice))
                        <div class="col-sm-12 col-md-6">
                            {!! Form::label('sage_invoice', 'Sage Invoice:') !!}
                            {!! Form::text('sage_invoice', $counter->sage_invoice, array('disabled' => 'disabled', 'class' => 'form-control')) !!}
                        </div>
                    @endif

                    @if(isset($counter->boat))
                        <div class="col-sm-12 col-md-6">
                            {!! Form::label('boat', 'Boat:') !!}
                            {!! Form::text('boat', $counter->boat->name.' : '.$counter->boat->number, array('disabled' => 'disabled', 'class' => 'form-control')) !!}
                        </div>
                    @endif

                    @if(isset($counter->customer))
                        <div class="col-sm-12 col-md-6">
                            {!! Form::label('customer', 'Customer:') !!}
                            {!! Form::text('customer', $counter->customer->name, array('disabled' => 'disabled', 'class' => 'form-control')) !!}
                        </div>
                    @endif

                    @if(isset($counter->id))
                        <div class="col-sm-12 col-md-6">
                            <br>
                            <a href="{{ route('counters.edit', $counter->id) }}" target="_blank" class='btn btn-default'>
                                Edit <i class="glyphicon glyphicon-edit"></i>
                            </a>
                        </div>
                    @endif

                    <div class="clearfix"></div>
                </div>
                <div class="row">
                    <br/>
                    <div class="clearfix"></div>
                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                        <style>
                            #barcode-search {
                                text-transform: uppercase;
                            }
                        </style>
                            {!! Form::label('items[]', 'Item Search: (Enter Barcode)') !!}
                            {!! Form::text('barcode', '', array('maxlength' => '9', 'id' => 'barcode-search', 'class' => 'form-control' )) !!}
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                            {!! Form::label('', '&nbsp;') !!}
                            <div class="clearfix"></div>
                            {!! Form::button('<i class="glyphicon glyphicon-plus"></i>  Add Miscellaneous Item', array('data-toggle' => 'modal', 'data-target' => '.bs-misc-details-modal-sm', 'id' => 'issue-item', 'class' => 'btn btn-default', 'the-id' => $counter->id)) !!}
                            {!! Form::button('<i class="glyphicon glyphicon-search"></i>  Search Items', array('id' => 'search-item', 'class' => 'btn btn-default')) !!}
                        </div>
                    </div>
                    <hr class="col-sm-12">
                    <div class="clearfix"></div>
                </div>

                <div class="" id="item-row">

                </div>

                <div class="row">
                    <!-- Submit Field -->
                    <div class="form-group col-sm-12">
                        <a href="{{ url('counters') }}">
                            {!! Form::button('Back', array('class' => 'btn btn-default')) !!}
                        </a>
                        {!! Form::button('Issue Stock', array('data-toggle' => 'modal', 'data-target' => '.bs-confirm-issue-modal-sm', 'class' => 'btn btn-primary')) !!}
                        @include('issue.includes.confirm')
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>

    @include('issue.includes.issue-normal-row')
    @include('issue.includes.issue-misc-row')
    @include('issue.includes.issue-labour-row')
    @include('issue.includes.issue-carriage-row')

@endsection

@include('issue.includes.counter-details')
@include('issue.includes.counter-misc')
@include('issue.includes.counter-scripts')
