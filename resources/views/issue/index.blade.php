@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Issue Stock</h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">

                    <div class="col-sm-12 col-md-6">
                        {!! Form::label('info', 'Info:') !!}
                        {!! Form::text('info', '', array('disabled' => 'disabled', 'class' => 'form-control')) !!}
                    </div>

                    <div class="col-sm-12 col-md-6">
                        {!! Form::label('po', 'Overall PO: (Unless specified per item) ') !!}
                        {!! Form::text('po', '', array('class' => 'form-control')) !!}
                    </div>

                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <div class="col-sm-8 col-md-10">
                                {!! Form::label('customer_id', 'Customer:') !!}
                                @include('customers.select')
                            </div>

                            <div class="col-sm-4 col-md-2">
                                {!! Form::label('nothing', '&nbsp;') !!}
                                {!! Form::button('<i class="glyphicon glyphicon-plus"></i>  New Customer', array('class' => 'add-customer btn btn-default')) !!}
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-6">
                            <div class="col-sm-8 col-md-10">
                                {!! Form::label('boat_id', 'Boat:') !!}
                                @include('boats.select')
                            </div>
                            <div class="col-sm-4 col-md-2">
                                {!! Form::label('nothing', '&nbsp;') !!}
                                {!! Form::button('<i class="glyphicon glyphicon-plus"></i>  New Boat', array('class' => 'add-boat btn btn-default')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="row">
                    <div class="form-group col-sm-12">
                        {!! Form::label('items[]', 'Items:') !!}
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                            {!! Form::text('barcode', '', array('class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                            {!! Form::button('<i class="glyphicon glyphicon-plus"></i>  Add Item', array('class' => 'add-item btn btn-default', 'disabled' => 'disabled')) !!}
                        </div>
                    </div>
                    <hr class="col-sm-12">
                    <div class="clearfix"></div>
                </div>

                <div class="row" id="item-row">

                </div>

                <div class="item-template">
                    <div>
                        <div class="row">
                            {!! Form::hidden('item[][id]', '', array('class' => 'form-control')) !!}
                            <div class="col-sm-12 col-md-3 form-group">
                                {!! Form::label('item_name', 'Description: ') !!}
                                {!! Form::text('item_name', '', array('class' => 'form-control', 'disabled' => 'disabled')) !!}
                            </div>

                            <div class="col-sm-12 col-md-3 form-group">
                                {!! Form::label('item_qty', 'Location: ') !!}
                                @include('issue.includes.loc-select')
                            </div>

                            <div class="col-sm-12 col-md-3 form-group">
                                {!! Form::label('item[][qty]]', 'Quantity: ') !!}
                                {!! Form::text('item[][qty]', '', array('class' => 'form-control')) !!}
                            </div>

                            <div class="col-sm-12 col-md-3 form-group">
                                {!! Form::label('item[][po]', 'Custom PO: ') !!}
                                {!! Form::text('item[][po]', '', array('class' => 'form-control')) !!}
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-md-3 form-group">
                                {!! Form::label('unit[][type]', 'Types') !!}
                                {!! Form::textarea('unit[][type]', '', array('class' => 'form-control info-box', 'disabled' => 'disabled')) !!}
                            </div>

                            {{-- This could be hidden if the item doesn't have sizes as such --}}
                            <div class="size-row hidden">
                                <div class="col-sm-12 col-md-3 form-group">
                                    {!! Form::label('unit[][size]', 'Size') !!}
                                    {!! Form::textarea('unit[][size]', '', array('class' => 'form-control info-box', 'disabled' => 'disabled')) !!}
                                </div>

                                <div class="col-sm-12 col-md-3 form-group">
                                    {!! Form::label('unit[][sex]', 'Sex') !!}
                                    {!! Form::textarea('unit[][sex]', '', array('class' => 'form-control info-box', 'disabled' => 'disabled')) !!}
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-3 form-group">
                                {!! Form::label('unit[][barcode]', 'Barcode') !!}
                                {!! Form::text('unit[][barcode]', '', array('class' => 'form-control', 'disabled' => 'disabled')) !!}
                                <br/>
                                <div class="clearfix"></div>
                                {!! Form::button('<i class="glyphicon glyphicon-remove"></i> Remove Item', ['class' => 'remove-item btn btn-default']) !!}
                            </div>

                            <hr class="col-sm-12"/>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <!-- Submit Field -->
                    <div class="form-group col-sm-12">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                        <a href="{!! url('issue') !!}" class="btn btn-default">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@include('issue.scripts')
