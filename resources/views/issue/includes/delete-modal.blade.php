<div class="modal fade bs-delete-modal-sm" id="delete-item-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirm Deletion</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['url' => 'issue/destroy/misc', 'method' => 'post']) }}
                @if(Request::is('counters/issues/*'))
                    {{ Form::hidden('type', 'Counter') }}
                @elseif(Request::is('jobs/issues/*'))
                    {{ Form::hidden('type', 'Job') }}
                @endif
                <div class="row ">
                    <div class="form-group">
                        {{ Form::hidden('delete-id', '', array('id' => 'delete-id')) }}
                        <div class="col-sm-12 col-md-6 form-group">
                            {!! Form::label('pin_id', 'Name:') !!}
                            @if(isset($pincodes))
                                <select class="form-control" id="pin_id" name="pin_id">
                                    @foreach($pincodes as $user)
                                        <option value="{!! $user->id !!}">{!! $user->name !!}</option>
                                    @endforeach
                                </select>
                            @endif
                        </div>
                        <div class="col-sm-12 col-md-6 form-group">
                            {!! Form::label('code', 'Security Pin:') !!}
                            {!! Form::password('code', array('id' => 'pin_code', 'class' => 'form-control')) !!}
                            <span class="code-error hidden">Wrong code entered.</span>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger confirm-delete-button" data-dismiss="modal">Confirm</button>
            </div>
        </div>
    </div>
</div>

@push('footer-script')
<script type="text/javascript">
    (function(window, $) {
        $('body').on('click', '.delete-item', function(){
            $('#delete-item-modal #delete-id').val($(this).attr('line-id'));
        })

        $('body').on('click', '.confirm-delete-button', function(e){
            e.preventDefault();
            $('#pin').css({'border' : 'none'});
            if($('.code-error').is('visible')) {
                $('.code-error').addClass('hidden');
            }
            var form = $('#delete-item-modal form');
            $.ajax({
                type:"POST",
                url:form.attr("action"),
                data:form.serialize(),
                success: function(response){
                    $('#delete-item-modal').find('button[data-dismiss="modal"]').click();
                    var myStack = {"dir1":"down", "dir2":"right", "push":"top"};
                    new PNotify({
                        title: "Success",
                        text: response.data,
                        addclass: "stack-custom",
                        stack: myStack
                    })
                    $('.buttons-reload').click();
                },
                error: function(response){
                    $('.code-error').removeClass('hidden');
                }
            });
        });

    })(window, jQuery);
</script>
@endpush