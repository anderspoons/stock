<select class="form-control unit-select">

</select>

@if(isset($units))
    <div class="hidden" id="unit-options">
    @foreach($units as $unit)
        <option data-cat="{!! $unit->category_id !!}" data-alias="{!! $unit->short !!}" value="{!! $unit->id !!}">
            {!! $unit->name !!}
        </option>
    @endforeach
    </div>
@endif
