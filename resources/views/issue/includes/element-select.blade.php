
<div class="form-group col-md-6 col-sm-12">
    {!! Form::label('element_ids', 'Job Elements:') !!}
    <select name="element_id[]" class="form-control element-select" multiple="multiple">
        @if(Request::is('issue*'))
            {{-- Dunno what to use here but it means were on a issue page --}}
        @else
            @if(isset($job))
                @foreach($job->elements()->get() as $element)
                    <option selected="selected" value="{!! $element->id !!}">{!! $element->name !!}</option>
                @endforeach
            @endif
        @endif
    </select>
</div>

@section('extras')
    <script type="text/javascript">
        jQuery( document ).ready(function( $ ) {
            var last_results = [];
            jobElements('element-select');

            function jobElements(theclass){
                $("." + theclass).select2({
                    tags: true,
                    multiple: true,
                    ajax: {
                        url: "{!! url('elements/json') !!}",
                        dataType: "json",
                        data: function(term) {
                            return {
                                q: term
                            };
                        },
                        results: function(data) {
                            console.log(data);
                            return {
                                results: data.results
                            };
                        }
                    },
                    createTag: function (params) {
                        return {
                            id: params.term,
                            text: params.term,
                            newOption: true
                        }
                    },
                    templateResult: function (data) {
                        var $result = $("<span></span>");
                        $result.text(data.text);
                        if (data.newOption) {
                            $result.append(" <em>(new)</em>");
                        }
                        return $result;
                    },
                    createSearchChoice:function(term, data) {
                        if ($(data.results).filter(function() {
                                    return this.text.localeCompare(term) === 0;
                                }).length===0) {
                            return {
                                id:term,
                                text:term
                            };
                        }
                    },
                    width: '100%'
                });
            }
        });
    </script>
@endsection