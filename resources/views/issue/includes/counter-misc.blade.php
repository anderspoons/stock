<div class="modal fade bs-misc-details-modal-sm" id="misc-details-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Miscellaneous Item</h4>
            </div>
            <div class="modal-body">
                <div class="row ">
                    <div class="form-group col-md-6 col-sm-12">
                        {!! Form::label('misc_name', 'Item Name:') !!}
                        {!! Form::text('misc_name', '', array('class' => 'form-control')) !!}
                    </div>
                    <div class="form-group col-md-6 col-sm-12">
                        {!! Form::label('misc-qty', 'Quantity:') !!}
                        {!! Form::text('misc-qty', '', array('class' => 'form-control', 'id' => 'qty-box')) !!}
                    </div>

                    <div class="form-group col-md-6 col-sm-12">
                        {!! Form::label('misc-cost', 'Cost: (Excluding VAT)') !!}
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-gbp"></i>
                            </span>
                            {!! Form::number('misc-cost', '', array('class' => 'form-control', 'id' => 'cost')) !!}
                        </div>
                    </div>

                    <div class="form-group col-md-6 col-sm-12">
                        {!! Form::label('sales_price', 'Sales Price:') !!}
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-gbp"></i>
                            </span>
                            {!! Form::number('sales_price', '', array('class' => 'form-control', 'id' => 'sales_price')) !!}
                        </div>
                    </div>

                    <div id="details" class="form-group col-md-12 col-sm-12">
                        {!! Form::label('item-details', 'Description: (Please describe the item, its condition and size.)') !!}
                        {!! Form::textarea('item-details', '', array('class' => 'form-control info-box')) !!}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger confirm-misc-button">Confirm</button>
            </div>
        </div>
    </div>
</div>


@push('footer-script')
<script type="text/javascript">
    $(function() {
        $('button[data-target=".bs-misc-details-modal-sm"]').click(function(e){
            $('#qty').val('');
        });
    });
</script>
@endpush