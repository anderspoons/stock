<div class="modal fade bs-details-modal-sm" id="item-details-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Details</h4>
            </div>
            <div class="hidden">
                <div class="size-template">
                    {!! Form::label('size', 'Sizes:') !!}
                    @include('issue.includes.units')
                    {{ Form::hidden('unit-report', '', array('id' => 'unit-report')) }}
                    <div class="size-box">
                        {!! Form::text('size', '', array('class' => 'size-unit form-control')) !!}
                    </div>
                </div>
            </div>

            <div class="modal-body">
                <div class="row ">
                    <div class="form-group col-md-6 col-sm-12">
                        {!! Form::label('location_id', 'Location:') !!}
                        @include('issue.includes.loc-select')
                    </div>

                    <div class="form-group col-md-6 col-sm-12">
                        {!! Form::label('misc-cost', 'Cost: (Excluding VAT each)') !!}
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-gbp"></i>
                            </span>
                            {!! Form::number('cost', '', array('class' => 'form-control', 'id' => 'cost')) !!}
                            {!! Form::hidden('real-cost', '', array('id' => 'real-cost')) !!}
                        </div>
                    </div>

                    <div class="form-group col-md-6 col-sm-12 qty-box">
                        {!! Form::label('qty', 'Quantity:') !!}
                        {!! Form::number('qty', '', array('class' => 'form-control', 'id' => 'qty-box')) !!}
                    </div>

                    <div class="form-group col-md-6 col-sm-12 qty-info">
                        {!! Form::label('qty-info', 'Quantity not required here as sizes will be used to determine the actual quantity used.') !!}
                    </div>

                    <div id="sizing" class="form-group col-md-6 col-sm-12">

                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger confirm-issue-button">Confirm</button>
            </div>
        </div>
    </div>
</div>



@push('footer-script')
<script type="text/javascript">
    $(function() {
        $('button[data-target=".bs-details-modal-sm"]').click(function(e){
            $('#qty-box').val('')
        });
    });
</script>
@endpush