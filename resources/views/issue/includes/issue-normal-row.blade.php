<div id="item-template" class="hidden">
    <div>
        <div class="row">
            {{-- Use the row id here so that if theres a problem with a particular row we can highlight this on the return for the ajax.--}}
            {!! Form::hidden('item[][row-id]', '', array('class' => 'row-id form-control')) !!}
            {!! Form::hidden('item[][id]', '', array('class' => 'item-id form-control')) !!}
            <div class="col-sm-12 col-md-3 form-group">
                {!! Form::label('item[][name]', 'Description: ') !!}
                {!! Form::text('item[][name]', '', array('class' => 'form-control item-description', 'disabled' => 'disabled')) !!}
            </div>

            <div class="col-sm-12 col-md-3 form-group">
                {!! Form::hidden('item[][location]', '', array('class' => 'item-location-id form-control')) !!}
                {!! Form::label('item[][location-desc]', 'Location: ') !!}
                {!! Form::text('item[][location-desc]', '', array('class' => 'form-control item-location', 'disabled' => 'disabled')) !!}
            </div>

            <div class="col-sm-12 col-md-3 form-group hidden">
                {!! Form::label('item[][item-details]', 'Details: ') !!}
                {!! Form::text('item[][item-details]', '', array('class' => 'form-control item-details', 'disabled' => 'disabled')) !!}
            </div>

            <div class="col-sm-12 col-md-3 form-group">
                {!! Form::label('item[][qty]', 'Quantity: ') !!}
                {!! Form::text('item[][qty]', '', array('class' => 'form-control item-quantity', 'disabled' => 'disabled')) !!}
            </div>

            <div class="col-sm-12 col-md-3 form-group">
                {!! Form::label('', 'Line Cost (Excluding VAT): ') !!}
                {!! Form::hidden('item[][unit_cost]', '', array('class' => 'item-unit-cost')) !!}
                {!! Form::hidden('item[][real_cost]', '', array('class' => 'item-real-cost')) !!}
                {!! Form::hidden('item[][cost]', '', array('class' => 'item-cost')) !!}
                {!! Form::text('', '', array('class' => 'form-control item-cost-text', 'disabled' => 'disabled')) !!}
            </div>


            <div class="col-sm-12 col-md-3 form-group">
                {!! Form::label('item[][barcode]', 'Barcode') !!}
                {!! Form::text('item[][barcode]', '', array('class' => 'form-control item-barcode', 'disabled' => 'disabled')) !!}
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="row">
            <div class="size-stuff hidden">
                <div class="col-sm-12 col-md-3 form-group">
                    {!! Form::label('item[][size][]', 'Size: ') !!}
                    <div class="size-template">
                        {!! Form::text('item[][size][]', '', array('class' => 'form-control size-box', 'disabled' => 'disabled')) !!}
                    </div>
                </div>

                <div class="col-sm-12 col-md-3 form-group">
                    {!! Form::label('item[][unit]', 'Size Unit: ') !!}
                    {!! Form::hidden('item[][unit]', null, array('class' => "item-unit-id form-control")) !!}
                    {!! Form::text('', null, array('class' => "item-unit form-control", 'disabled' => 'disabled')) !!}
                </div>
            </div>

            <div class="col-sm-12 col-md-3 form-group">
                <br>
                <div class="clearfix"></div>
                {!! Form::button('<i class="glyphicon glyphicon-remove"></i> Remove Item', ['class' => 'remove-row btn btn-default']) !!}
            </div>

            <hr class="col-sm-12"/>
            <div class="clearfix"></div>
        </div>
    </div>
</div>