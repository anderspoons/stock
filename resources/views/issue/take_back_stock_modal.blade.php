<div class="modal fade bs-back-stock-modal-sm" id="back-stock-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Recieve Stock Back</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['url' => 'issue/back-stock', 'method' => 'post']) }}
                <div class="row ">
                    <div class="col-sm-12 form-group">
                        <div class="form-group col-sm-6">
                            {{ Form::hidden('item_id', '', array('id' => 'item-id')) }}
                            {{ Form::hidden('real_item_id', '', array('id' => 'real-item-id')) }}
                            {{ Form::hidden('type', '', array('id' => 'type')) }}
                            {{ Form::hidden('item_location', '', array('id' => 'item-location')) }}
                            {!! Form::label('qty', 'Qty:') !!}
                            {!! Form::number('qty', '', array('id' => 'qty-box', 'class' => 'form-control')) !!}
                        </div>
                        <div class="form-group col-sm-6">
                            {!! Form::label('reason', 'Reason:') !!}
                            {!! Form::text('reason', '', array('class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-sm-12 form-group">
                        <div class="form-group">
                            <div class="col-sm-12 col-md-6 form-group">
                                {!! Form::label('pin_id', 'Name:') !!}
                                @if(isset($pincodes))
                                    <select class="form-control" name="pin_id">
                                        @foreach($pincodes as $user)
                                            <option value="{!! $user->id !!}">{!! $user->name !!}</option>
                                        @endforeach
                                    </select>
                                @endif
                            </div>
                            <div class="col-sm-12 col-md-6 form-group">
                                {!! Form::label('pin', 'Security Pin:') !!}
                                {!! Form::password('pin', array('class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 form-group">
                        <div class="form-group">
                            <div class="col-sm-12 form-group">
                                @include('issue.location-picker')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Confirm', ['class' => 'btn btn-danger stock-back-button']) !!}
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

@push('footer-script')
<script type="text/javascript">
    jQuery( document ).ready(function($){
        function showError(msg){
            var myStack = {
                "dir1":"down",
                "dir2":"right",
                "push":"top"
            };
            new PNotify({
                title: "Problem",
                text: msg,
                type: "error",
                addclass: "alert alert-warning",
                stack: myStack
            });
        }

        $('#back-stock-modal #qty-box').on('keyup', function() {
            var maxval =  $('#back-stock-modal #qty-box').attr('max');
            $(this).attr({
                "max" : maxval,
                "min" : 1
            });
            if(parseInt($(this).val()) > parseInt(maxval)){
                $(this).val('');
                $(this).val(maxval);
                showError('The qty entered is more than is currently in stock.');
            }
        });

        $('body').on('click', '.back-stock', function(){
            $('#back-stock-modal form')[0].reset();
            $('#back-stock-modal #item-id').val($(this).attr('line-id'));
            $('#back-stock-modal #real-item-id').val($(this).attr('item-id'));

            @if(Request::is('counters*'))
                var type = 'counter';
            @endif

            @if(Request::is('jobs*'))
                var type = 'job';
            @endif

            $('#back-stock-modal #type').val(type);
            $('#back-stock-modal #qty-box').attr('max', $(this).attr('line-qty'));
            $('#back-stock-modal #item-location').val($(this).attr('line-location'));
        });

        //Submit the form by ajax
        $('body').on('click', '.stock-back-button', function(e){
            e.preventDefault();
            var form = $('#back-stock-modal form');
            $.ajax({
                type:"POST",
                url:form.attr("action"),
                data:form.serialize(),
                success: function(response){
                    $('#back-stock-modal .close').click();
                    var myStack = {"dir1":"down", "dir2":"right", "push":"top"};
                    new PNotify({
                        title: "Success",
                        text: response.text,
                        addclass: "stack-custom",
                        stack: myStack
                    })
                    $('.buttons-reload').click();
                },
                error: function(response){
                    var myStack = {"dir1":"down", "dir2":"right", "push":"top"};
                    new PNotify({
                        title: "Problem",
                        text: response.text,
                        addclass: "stack-custom",
                        stack: myStack
                    })
                }
            });
        });
    });
</script>
@endpush

