<div class="modal fade bs-set-cost-modal-sm" id="set-cost-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Line Cost</h4>
            </div>
            <div class="modal-body">
                {{ Form::open(['url' => 'issue/job/cost', 'method' => 'post']) }}
                <div class="row ">
                    <div class="col-sm-12 form-group">
                        <div class="form-group">
                            {{ Form::hidden('job_item_id', '', array('id' => 'item-cost-id')) }}
                            {{ Form::hidden('sale_real', '', array('id' => 'item-cost-switch')) }}
                            <div class="input-group">
                                <input type="checkbox" id="toggle-cost-sale" data-size="normal">
                            </div>

                            <div class="unit-cost">
                                {{ Form::label('line_cost', 'Unit Cost (Excluding VAT):') }}
                                <div class="input-group unit-cost">
                                <span class="input-group-addon">
                                    <i class="fa fa-gbp"></i>
                                </span>
                                    {{ Form::number('real_cost', '', array('id' => 'item-unit-cost', 'class' => 'form-control')) }}
                                </div>
                            </div>

                            <div class="sale-cost hidden">
                                {{ Form::label('line_cost', 'Sale Cost (Excluding VAT):') }}
                                <div class="input-group sale-cost hidden">
                                <span class="input-group-addon">
                                    <i class="fa fa-gbp"></i>
                                </span>
                                    {{ Form::number('sale_cost', '', array('id' => 'item-sale-cost', 'class' => 'form-control')) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Confirm', ['class' => 'btn btn-danger item-cost-button']) !!}
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

@push('footer-script')
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('#toggle-cost-sale').bootstrapToggle({
            on: 'Unit Sale Price',
            off: 'Unit Cost Price',
            width: 150
        });

        $('body').on('click', '.cost-btn', function () {
            $('#toggle-cost-sale').bootstrapToggle('off')
            $('#set-cost-modal #item-cost-id').val($(this).attr('line-id'));
            $('#set-cost-modal #item-cost-switch').val('cost');
            $('#set-cost-modal #item-unit-cost').val($(this).attr('real-cost'));
            $('#set-cost-modal #item-sale-cost').val($(this).attr('sale-cost'));
        })

        $('#toggle-cost-sale').change(function () {
            if ($(this).parent().hasClass('off')) {
                console.log('Toggled Sale Cost');
                if ($('.unit-cost').hasClass('hidden')) {
                    $('.unit-cost').removeClass('hidden');
                    $('#set-cost-modal #item-cost-switch').val('cost');
                }
                $('.sale-cost').addClass('hidden');
            } else {
                console.log('Toggled Unit Cost');
                $('.unit-cost').addClass('hidden');
                if ($('.sale-cost').hasClass('hidden')) {
                    $('.sale-cost').removeClass('hidden');
                    $('#set-cost-modal #item-cost-switch').val('sale');
                }
            }
        });

        //Submit the form by ajax
        $('body').on('click', '.item-cost-button', function (e) {
            e.preventDefault();
            console.log('Doing the item line price cost');

            var form = $('#set-cost-modal form');
            $.ajax({
                type: "POST",
                url: form.attr("action"),
                data: form.serialize(),
                success: function (response) {
                    $('#set-cost-modal .close').click();
                    var myStack = {"dir1": "down", "dir2": "right", "push": "top"};
                    new PNotify({
                        title: "Success",
                        text: response.text,
                        addclass: "stack-custom",
                        stack: myStack
                    })
                    $('.buttons-reload').click();
                },
                error: function (response) {
                    var myStack = {"dir1": "down", "dir2": "right", "push": "top"};
                    new PNotify({
                        title: "Problem",
                        text: response.text,
                        addclass: "stack-custom",
                        stack: myStack
                    })
                }
            });
        });
    });
</script>
@endpush
