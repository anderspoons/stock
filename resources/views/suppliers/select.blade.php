<select name="supplier_id" id="supplier-select2" class="form-control">
    @if(isset($order))
        @foreach($order->supplier()->get() as $supplier)
            <option selected="selected" value="{!! $supplier->id !!}">{!! $supplier->name !!}</option>
        @endforeach
    @endif
</select>

@push('footer-script')
    <script type="text/javascript">
        jQuery( document ).ready(function( $ ) {
            var last_results = [];
            $("#supplier-select2").select2({
                ajax: {
                    url: "{!! url('supplier/json') !!}",
                    dataType: "json",
                    data: function(term) {
                        return {
                            q: term
                        };
                    },
                    results: function(data) {
                        console.log(data);
                        return {
                            results: data.results
                        };
                    }
                },
                createSearchChoice:function(term, data) {
                    if ($(data.results).filter(function() {
                                return this.text.localeCompare(term) === 0;
                            }).length===0) {
                        return {
                            id:term,
                            text:term
                        };
                    }
                }
            });
        });
    </script>
@endpush