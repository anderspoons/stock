@extends('layouts.app')

@section('content')
   <section class="content-header">
           <h1>
               Supplier
           </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">

           <div class="box-body">
               {!! Form::model($supplier, ['route' => ['suppliers.update', $supplier->id], 'method' => 'patch']) !!}
               <div class="row">
                    @include('suppliers.fields')
               </div>

               <hr>

               <div class="row">
                   @include('addresses.shared.fields')
               </div>

               <div class="row">
                   <!-- Submit Field -->
                   <div class="form-group col-sm-12">
                       {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                       <a href="{!! route('suppliers.index') !!}" class="btn btn-default">Cancel</a>
                   </div>
               </div>

               {!! Form::close() !!}
           </div>
       </div>
   </div>

@endsection



@include('addresses.scripts')