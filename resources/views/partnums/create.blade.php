@extends('layouts.app')

@section('content')
 <section class="content-header">
        <h1>
            Partnum
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'partnums.store']) !!}

                              @include('partnums.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
