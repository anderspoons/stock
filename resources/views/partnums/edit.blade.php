@extends('layouts.app')

@section('content')
   <section class="content-header">
           <h1>
               Partnum
           </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">

           <div class="box-body">
               <div class="row">
                   {!! Form::model($partnum, ['route' => ['partnums.update', $partnum->id], 'method' => 'patch']) !!}

                    @include('partnums.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection