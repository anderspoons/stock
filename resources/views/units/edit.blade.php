@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Unit
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               {!! Form::model($unit, ['route' => ['units.update', $unit->id], 'method' => 'patch']) !!}
               <div class="row">
                   @include('units.fields')
               </div>
               {!! Form::close() !!}
           </div>
       </div>
   </div>
@endsection