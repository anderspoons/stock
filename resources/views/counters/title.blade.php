@if(isset($counter))
    @if(isset($counter->boat))
        &nbsp;Boat:{{ $counter->boat->name }}
    @elseif(isset($counter->customer))
        &nbsp;Customer:{{ $counter->customer->name }}
    @endif

    @if(strlen($counter->note) > 0)
        &nbsp;Note:{{ $counter->note }}
    @endif
@endif
