
@if(isset($boats))
    <div class="filter-set">
        <div class="header-bar">
            <div class="filter-search-box" style="padding:0 !important;">
                {!! Form::text('boat', '', array('class' => 'search-filters', 'placeholder' => 'Boat')) !!}
            </div>
            <button class="btn btn-xs pull-right filter-clear hidden"><i class="glyphicon glyphicon-remove"></i></button>
            <button class="btn btn-xs pull-right filter-expand"><i class="glyphicon glyphicon-plus"></i></button>
        </div>
        <div class="clearfix"></div>
        <ul>
            @foreach($boats as $i => $boat)
                <li>
                    {!! Form::checkbox('boat_filter', $boat->id, false, ['id' => 'boat_' . $i]) !!}
                    {!! Form::label('boat_' . $i, $boat->name) !!}
                </li>
            @endforeach
        </ul>
    </div>
@endif

@if(isset($customers))
    <div class="filter-set">
        <div class="header-bar">
            <div class="filter-search-box" style="padding:0 !important;">
                {!! Form::text('customer', '', array('class' => 'search-filters', 'placeholder' => 'Customer')) !!}
            </div>
            <button class="btn btn-xs pull-right filter-clear hidden"><i class="glyphicon glyphicon-remove"></i></button>
            <button class="btn btn-xs pull-right filter-expand"><i class="glyphicon glyphicon-plus"></i></button>
        </div>
        <div class="clearfix"></div>
        <ul>
            @foreach($customers as $i => $customer)
                <li>
                    {!! Form::checkbox('customer_filter', $customer->id, false, ['id' => 'customer_' . $i]) !!}
                    {!! Form::label('customer_' . $i, $customer->name) !!}
                </li>
            @endforeach
        </ul>
    </div>
@endif

@if(isset($statuses))
    <div class="filter-set">
        <div class="header-bar">
            <div class="filter-search-box" style="padding:0 !important;">
                {!! Form::text('status', '', array('class' => 'search-filters', 'placeholder' => 'Status')) !!}
            </div>
            <button class="btn btn-xs pull-right filter-clear hidden"><i class="glyphicon glyphicon-remove"></i></button>
            <button class="btn btn-xs pull-right filter-expand"><i class="glyphicon glyphicon-plus"></i></button>
        </div>
        <div class="clearfix"></div>
        <ul>
            @foreach($statuses as $i => $status)
                <li>
                    {!! Form::checkbox('status_filter', $status->id, false, ['id' => 'status_' . $i]) !!}
                    {!! Form::label('status_' . $i, $status->name) !!}
                </li>
            @endforeach
        </ul>
    </div>
@endif

@if(isset($stores))
    <div class="filter-set">
        <div class="header-bar">
            <div class="filter-search-box" style="padding:0 !important;">
                {!! Form::text('store', '', array('class' => 'search-filters', 'placeholder' => 'Store')) !!}
            </div>
            <button class="btn btn-xs pull-right filter-clear hidden"><i class="glyphicon glyphicon-remove"></i></button>
            <button class="btn btn-xs pull-right filter-expand"><i class="glyphicon glyphicon-plus"></i></button>
        </div>
        <div class="clearfix"></div>
        <ul>
            @foreach($stores as $store)
                <li>
                    {!! Form::checkbox('store_filter', $store->id, false, ['id' => 'store_' . $i]) !!}
                    {!! Form::label('store_' . $i, $store->name) !!}
                </li>
            @endforeach
        </ul>
    </div>
@endif

@if(isset($boats) || isset($customers) || isset($statuses) || isset($stores))
    <br/>
    <div class="clearfix"></div>
    {{ Form::checkbox('all_closed', null, false, array('class' => 'closed-filter', 'id' => 'all_closed')) }}
    {{ Form::label('all_closed', 'Only Closed Sales', array( 'style' => 'margin-bottom:10px;')) }}
    <br/>
    {{ Form::checkbox('all_billed', null, false, array('class' => 'billed-filter', 'id' => 'all_billed')) }}
    {{ Form::label('all_billed', 'Only Billed Sales', array( 'style' => 'margin-bottom:10px;')) }}
    <br/>
    {{ Form::button('Filter', array('class' => 'col-md-12 do-filter')) }}
    {{ Form::button('Reset Filters', array('class' => 'col-md-12 buttons-reset', 'style' => 'margin-top:10px;')) }}
@endif
