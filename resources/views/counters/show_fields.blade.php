<!-- Customer Id Field -->
<div class="form-group">
    {!! Form::label('customer_id', 'Customer:') !!}
    <p>{!! $counter->customer->name !!}</p>
</div>

<!-- Note Field -->
<div class="form-group">
    {!! Form::label('note', 'Note:') !!}
    <p>{!! $counter->note !!}</p>
</div>

<!-- Po Field -->
<div class="form-group">
    {!! Form::label('po', 'Po:') !!}
    <p>{!! $counter->po !!}</p>
</div>

<!-- Closed At Field -->
<div class="form-group">
    {!! Form::label('closed_at', 'Closed At:') !!}
    <p>{!! $counter->closed_at !!}</p>
</div>

<!-- Sage Invoice Field -->
<div class="form-group">
    {!! Form::label('sage_invoice', 'Sage Invoice:') !!}
    <p>{!! $counter->sage_invoice !!}</p>
</div>