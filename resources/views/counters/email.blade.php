<div class="modal fade bs-email-invoice-modal-sm" id="email-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Email Invoice</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                {{ Form::open(['url' => 'counters/email', 'method' => 'post', 'class' => 'email-invoice-form']) }}
                    <div class="col-sm-12">
                        {{ Form::label('email', 'Customer Email') }}
                        {{ Form::hidden('id', '', array('class' => 'form-control id-box')) }}
                        {{ Form::email('email', '', array('class' => 'form-control email-box')) }}
                    </div>

                    <div class="col-sm-12">
                        {{ Form::label('message', 'Custom Message:') }}
                        {{ Form::textarea('message', '', array('class' => 'form-control')) }}
                    </div>
                {{ Form::close() }}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary send-email">Send Invoice</button>
            </div>
        </div>
    </div>
</div>

@section('extras')
    <script>
        $(function(){
            $('body').on('click', '.btn-email', function(e){
                var id = $(this).attr('data-id');
                var email = $(this).attr('data-email');
                $('.email-invoice-form .id-box').val(id);
                $('.email-invoice-form .email-box').val(email);
            });

            $('body').on('click', '.send-email', function(e){
                var form = $('.email-invoice-form');
                $.ajax({
                    type:"POST",
                    url:form.attr("action"),
                    data:form.serialize(),
                    success: function(response){
                        $('#email-modal button[data-dismiss="modal"]').click();
                        var myStack = {"dir1":"down", "dir2":"right", "push":"top"};
                        new PNotify({
                            title: "Success",
                            text: response.data,
                            addclass: "stack-custom",
                            stack: myStack
                        })
                    },
                    error: function(response){
                        $('#email-modal button[data-dismiss="modal"]').click();
                        var myStack = {"dir1":"down", "dir2":"right", "push":"top"};
                        new PNotify({
                            title: "Error",
                            text: response.data,
                            addclass: "stack-custom",
                            stack: myStack
                        })
                    }
                });
            });
        })
    </script>
@endsection