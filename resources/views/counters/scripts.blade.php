@push('footer-script')
<script src="/js/extjs.js"></script>
<script type="text/javascript">
    (function (window, $) {
        var debug = true;
        var anyFilters = {};

        window.LaravelDataTables = window.LaravelDataTables || {};
        window.LaravelDataTables["dataTableBuilder"] = $("#dataTableBuilder").DataTable({
            "serverSide": true,
            "processing": true,
            "pagingType": "extStyle",
            "ajax": {
                url: '/counters',
                dataType: 'json',
                cache:false,
                type: 'GET',
                data: function (d) {
                    if(debug){
                        console.log(anyFilters);
                    }
                    $.each( anyFilters, function( key, value ) {
                        d[key] = value;
                    });
                }
            },
            stateSave: true,
            "order": [[ 1, "DESC" ]],
            "columns": [{
                "name": "checkbox",
                "data": "checkbox",
                "defaultContent": " ",
                "orderable": false,
                "searchable": false,
                "title": "<input type='checkbox' class='selectAll'>",
                "width": "1px"
            }, {
                "name": "created_date",
                "data": "created_date",
                "defaultContent": " ",
                "orderable": true,
                "searchable": false,
                "title": "Date"
            }, {
                "name": "boat_name",
                "data": "boat_name",
                "defaultContent": " ",
                "orderable": true,
                "searchable": true,
                "title": "Boat"
            }, {
                "name": "note",
                "data": "note",
                "title": "Note",
                "orderable": false,
                "searchable": true
            }, {
                "name": "customer_name",
                "data": "customer_name",
                "defaultContent": " ",
                "orderable": true,
                "searchable": true,
                "title": "Customer"
            }, {
                "name": "po",
                "data": "po",
                "title": "PO",
                "orderable": true,
                "searchable": true
            }, {
                "name": "acct_ref",
                "data": "acct_ref",
                "defaultContent": " ",
                "orderable": true,
                "title": "Sage Account",
                "searchable": true
            }, {
                "name": "ref",
                "data": "ref",
                "title": "Invoice No.",
                "orderable": true,
                "searchable": true
            }, {
                "name": "counter_status",
                "data": "counter_status",
                "searchable": false,
                "orderable": false,
                "title": "Status"
            }, {
                "defaultContent": "",
                "data": "action",
                "name": "action",
                "title": "Action",
                "render": null,
                "orderable": false,
                "searchable": false,
            }], 
            "dom": "Bfrtip",
            "scrollX": false,
            "buttons": ["print", "reset", "reload", {
                        "extend": "collection",
                        "text": "<i class=\"fa fa-download\"><\/i> Export",
                        "buttons": ["csv", "excel", "pdf"]
                        }
                    ],
            fnDrawCallback: function(settings, json) {
                $("td > .btn-group").each(function(){
                    $btngrpWidth = 0;
                    $(this).find("a, button").each(function(i){
                        $btngrpWidth += +$(this).outerWidth();
                    });
                    $(this).width($btngrpWidth);
                });
            }
        });

        $('body').on('change', 'select[name=customer_id]', function (e) {
            $.ajax({
                url: "{!! url('/lookup/customer') !!}/" + $(this).val(),
                type: 'GET',
                data: {'id': $(this).val()},
                dataType: 'Json',
                success: function (data) {
                    if (data.old == true) {
                        showError('This customer details have not been updated in over 12 months, please confirm ')
                    }
                }
            });

        })

        $('body').on('click', '.filter-expand', function (e) {
            if (debug) {
                console.log('expand filters clicked ken min');
            }
            var btn = $(this);
            expandFilters(btn, 'toggle');
        });

        function expandFilters(btn, open) {
            if (open == 'toggle') {
                if (btn.children('i').hasClass('glyphicon-plus')) {
                    btn.parent().parent().find('ul').slideDown('fast', function (e) {
                        btn.children('i').removeClass('glyphicon-plus');
                        btn.children('i').addClass('glyphicon-minus');
                    });
                } else {
                    btn.parent().parent().find('ul').slideUp('fast', function (e) {
                        btn.children('i').removeClass('glyphicon-minus');
                        btn.children('i').addClass('glyphicon-plus');
                    });
                }
            } else {
                if (btn.children('i').hasClass('glyphicon-plus')) {
                    btn.parent().parent().find('ul').slideDown('fast', function (e) {
                        btn.children('i').removeClass('glyphicon-plus');
                        btn.children('i').addClass('glyphicon-minus');
                    });
                }
            }
        }

        $('body').on('click', '.filter-clear', function (e) {
            if (debug) {
                console.log('Clear filters clicked ken min');
            }
            var btn = $(this);
            $(this).parent().parent().find('ul').each(function (i) {
                $(this).find('input').each(function (i) {
                    $(this).attr('checked', false);
                });
            })
            $('.search-filters').each(function (e) {
                $(this).val('');
                searchText($(this));
            })
            btn.addClass('hidden');
            doFilters();
        });

        function doFilters() {
            anyFilters = {};
            $('.filter-set ul').each(function (i) {
                var tmp = [];
                var name = '';
                $(this).find('input:checked').each(function (i) {
                    if ($(this).parent().parent().parent().find('.filter-clear').hasClass('hidden')) {
                        $(this).parent().parent().parent().find('.filter-clear').removeClass('hidden');
                    }
                    name = $(this).attr('name');
                    tmp.push($(this).val());
                });
                if ($(this).find('input:checked').length < 1) {
                    $(this).parent().find('.filter-clear').addClass('hidden')
                }
                if (tmp.length > 0) {
                    anyFilters[name] = tmp;
                }
            })
            if (debug) {
                console.log(anyFilters);
            }
        }

        $('body').on('change', '.filter-set input[type="checkbox"]', function (e) {
            if (debug) {
                console.log('OMG min! a filters been checked ... ken fit lyk.');
            }
            doFilters();
        });

        $('body').on('click', '.buttons-reset', function (e) {
            anyFilters = {};
            $('.filter-set ul').each(function (i) {
                $(this).find('input:checked').each(function (i) {
                    $(this).attr('checked', false);
                });
            })
            $('.buttons-reload').click();
        });

        $('body').on('click', '.closed-filter', function (e) {
            if($('.billed-filter').is(':checked')) {
                $('.billed-filter').prop('checked', false);
                anyFilters['show_billed'] = 'false';
            }
            if($(this).is(':checked')){
                anyFilters['show_closed'] = 'true';
            } else {
                anyFilters['show_closed'] = 'false';
            }
        });
        $('body').on('click', '.billed-filter', function (e) {
            if($('.closed-filter').is(':checked')) {
                $('.closed-filter').prop('checked', false);
                anyFilters['show_closed'] = 'false';
            }
            if($(this).is(':checked')){
                anyFilters['show_billed'] = 'true';
            } else {
                anyFilters['show_billed'] = 'false';
            }
        });

        $('body').on('click', '.do-filter', function (e) {
            anyFilters['search_type'] = 'broad';
            setTimeout(function () {
                $('a[data-dt-idx="1"]').click().delay(800);
            }, 1000);
            $('.paginate_button.first').click();
            $('.buttons-reload').click();
        });

        $('body').on('click', '.buttons-reset', function (e) {
            anyFilters = {};
            $('.filter-set ul').each(function (i) {
                $(this).find('input:checked').each(function (i) {
                    $(this).attr('checked', false);
                });
            })
            $('.all-filter').removeAttr('checked');
            $('.paginate_button.first').click();
            $('.buttons-reload').click();
        });

        $('body').on('keyup', '.search-filters', function (e) {
            searchText($(this));
        });

        function searchText(input) {
            var term = input.val();
            var btn = input.parent().parent().find('.filter-expand');
            if (term.length > 0) {
                expandFilters(btn, 'open');
            }
            if (debug) {
                console.log('Filter search text: ' + term + ' length: ' + term.length);
            }
            input.parent().parent().parent().find('ul li').each(function (i) {
                if ($(this).hasClass('hidden')) {
                    $(this).removeClass('hidden');
                }
                var text = $(this).text();
                if (text.toLowerCase().search(term.toLowerCase()) < 0 && $(this).find('input').prop('checked') == false) {
                    $(this).addClass('hidden');
                }
            })
        }

        $('[data-toggle="modal"][title]').tooltip();
        $('[data-toggle="tooltip"]').tooltip();

        function showError(msg) {
            var myStack = {
                "dir1": "down",
                "dir2": "right",
                "push": "top"
            };
            new PNotify({
                title: "Problem",
                text: msg,
                type: "error",
                addclass: "alert alert-warning",
                stack: myStack
            });
        }
    })(window, jQuery);
</script>
@endpush