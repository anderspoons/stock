@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Counter
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                {!! Form::open(['route' => 'counters.store']) !!}
                <div class="row">

                    @include('counters.fields')
                </div>
                <div class="row">
                    <div class="form-group col-sm-6">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                        <a href="{!! route('counters.index') !!}" class="btn btn-default">Cancel</a>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@include('counters.scripts');