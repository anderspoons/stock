<div class="item-row-full">
    <b>Work:</b> {{ $counter->work_scope }}
</div>
<br/>
@php
    $char_count = ceil((float)(strlen($counter->work_scope) / 37));

    $height_to_be_used = 0;
    if($char_count <= 1){
        $height_to_be_used = 40;
    } else {
        $extra = 20 * $char_count;
        $height_to_be_used = 40 + $extra;
    }
    $workscope = true;
@endphp