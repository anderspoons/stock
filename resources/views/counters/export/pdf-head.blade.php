<html>
@include('counters.export.pdf-style')
<div class="invoice-page">

<div class="grid head">
    <div class="grid__col grid__col--6-of-12">
        Macduff Shipyards Limited<br/>
        The Harbour<br/>
        Macduff<br/>
        Aberdeenshire<br/>
        AB44 1QT<br/>
        T: (01261) 832234 F: (01261) 833541<br/>
        VAT reg no: 265997200<br/>
    </div>
    <div class="grid__col grid__col--6-of-12">
        <img class="logo" src="img/Logo.jpg">
    </div>

</div>
<br>