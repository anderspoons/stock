<?php
    $page = 1;
    $max = 470;
    $height_used = 0;
    $sub_net = 0;
    $workscope = false;
    $page = 1;
?>
{{-- This is the first page obviously --}}
{{-- Header info, addresses etc.  --}}
@include('counters.export.pdf-head')
@include('counters.export.pdf-invoice-info')
{{-- Column names --}}
@if($workscope == false && strlen($counter->work_scope) > 0)
    @include('counters.export.pdf-work-scope')
@endif
@include('counters.export.pdf-column-names')


@foreach($list as $item)
    <?php
    //Calculate the height thats going to be used by the next row
    $char_count = ceil((float)(strlen($item['details']) / 37));

    $height_to_be_used = 0;
    if($char_count <= 1){
        $height_to_be_used = 40;
    } else {
        $extra = 20 * $char_count;
        $height_to_be_used = 40 + $extra;
    }
    ?>

    @if($height_used >= ($max - $height_to_be_used))
        {{-- We need to start a new page so sub total is needed --}}
        @include('counters.export.pdf-sub-total')
        <?php
        $page = $page + 1;
        ?>
        {{-- Header info, addresses etc.  --}}
        @include('counters.export.pdf-head')
        @include('counters.export.pdf-invoice-info')
        {{-- Column names --}}
        @include('counters.export.pdf-column-names')
        <?php
        $height_used = 0;
        ?>
    @endif


    @include('counters.export.pdf-line')
    <?php
    $sub_net = ($sub_net + $item['net_cost']);
    $height_used = $height_used + $height_to_be_used;
    ?>
@endforeach

@include('counters.export.pdf-total')
