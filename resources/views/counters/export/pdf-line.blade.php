<div class="items">
    <div class="item-row item-row-1">
        {{ $item['qty'] }}
    </div>
    <div class="item-row item-row-2">
        {{ $item['details'] }}
    </div>
    <div class="item-row item-row-3">
        {{ $item['unit_cost'] }}
    </div>
    <div class="item-row item-row-4">
        {{ $item['net_cost'] }}
    </div>
    <div class="item-row item-row-5">
        {{ $item['vat_rate'] }}
    </div>
    <div class="item-row item-row-6">
        {{ $item['vat'] }}
    </div>
</div>