<div class="item-head">
    <div class="item-row item-row-1">
        Quantity
    </div>
    <div class="item-row item-row-2">
        Details
    </div>
    <div class="item-row item-row-3">
        Unit Price
    </div>
    <div class="item-row item-row-4">
        Net Amount
    </div>
    <div class="item-row item-row-5">
        VAT Rate
    </div>
    <div class="item-row item-row-6">
        VAT
    </div>
</div>