<div class="grid info">
    <div class="grid__col grid__col--12-of-12 grid__col--centered centered-text">
        <u><b>Invoice</b></u>
    </div>
    <div class="address grid__col grid__col--6-of-12">
        @if(isset($boat->name))
            {{ $boat->name }}
            @if($boat->number)
                : {{ $boat->number }}
            @endif
            <br>
        @endif
        @if(isset($customer))
            @if($customer->name)
                {{ $customer->name }} <br>
            @endif
            @if($customer->address->line_1)
                {{ $customer->address->line_1 }} <br>
            @endif
            @if($customer->address->line_2)
                {{ $customer->address->line_2 }} <br>
            @endif
            @if($customer->address->line_3)
                {{ $customer->address->line_3 }} <br>
            @endif
            @if($customer->address->town)
                {{ $customer->address->town }} <br>
            @endif
            @if($customer->address->county)
                {{ $customer->address->county }} <br>
            @endif
            @if($customer->address->postcode)
                {{ $customer->address->postcode }} <br>
            @endif
        @else
            Problem getting customer info.
        @endif
    </div>
    <div class="grid__col grid__col--6-of-12">
        <div class="grid__col grid__col--7-of-12 right-text">
            <b>Invoice Number:</b> <br>
            <b>Invoice Date:</b> <br>
            <b>Purchase Order:</b> <br>
            <b>Account Reference:</b> <br>
        </div>
        <div class="grid__col grid__col--5-of-12">
            @if($counter->ref)
                {{ $counter->ref }}  <br>
            @else
                <br>
            @endif
            @if($counter->closed_at)
                {{ date('d/m/y', strtotime($counter->invoiced_at)) }}  <br>
            @else
                <br>
            @endif
            @if($counter->po)
                {{ $counter->po }}  <br>
            @else
                <br>
            @endif
            @if(isset($boat->sage_ref))
                @if(strlen($boat->sage_ref) > 0)
                    {{ $boat->sage_ref }}  <br>
                @endif
            @elseif(strlen($customer->sage_ref) > 0)
                {{ $customer->sage_ref }}  <br>
            @else
                <br>
            @endif
        </div>
    </div>
</div>