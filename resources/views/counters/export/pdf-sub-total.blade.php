<div class="grid bottom">
    <div class="grid__col grid__col--6-of-12 centered-text">
        All queries regarding this invoice must be raised within <b>30 days</b> from the invoice date.
        Seller retains the right of ownership until payment of goods has been received in full.
        Payment terms 30 Days
    </div>

    <div class="grid__col grid__col--5-of-12">
        <div class="grid__col grid__col--8-of-12">
            <b>Sub Total Net Amount</b><br>
        </div>
        <div class="grid__col grid__col--3-of-12 right-align">
            £{{ number_format((float)$sub_net, 2, '.', '')}}<br>
        </div>
    </div>

    <div class="grid__col grid__col--centered grid__col--12-of-12 centered-text">
        <a class="website_link" href="http://macduffshipyards.com/">www.macduffshipyards.com</a><br>
        <span class="italic">E: <a href="mailto:enquiries@macduffshipyards.com">enquiries@macduffshipyards.com</a></span><br>
        <span class="italic large-text">Combining Tradition, Experience, Craftsmanship & Technology</span>
    </div>

    <div class="grid__col grid__col--12-of-12 right-text">
        Page {{ $page }}
    </div>
</div>
<div class="page-break"></div>