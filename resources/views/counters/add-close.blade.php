<div class="modal fade bs-close-modal-sm" id="close-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Confirm Counter Sale Closure</h4>
            </div>
            <div class="modal-body">
                {{-- Below we need to work out which thing were deleteling Here, item, supplier, customer --}}
                {{ Form::open(['url' => 'counters/close', 'method' => 'post', 'class' => 'delete-form']) }}
                @if(isset($counter->id))
                    {!! Form::hidden('counter_id', $counter->id, array('class' => 'counter-id')) !!}
                @endif
                <div class="row">
                    <div class="col-sm-12">
                        @if(isset($counter))
                        <p>Warning you are about to close this sale off. Are you sure?</p>
                        @else
                        <p>Warning you are about to close these sales off. Are you sure?</p>
                        @endif
                    </div>
                </div>

{{--                 <div class="form-group">
                    <div class="col-sm-12 col-md-6 form-group">
                        {!! Form::label('pin_id', 'Name:') !!}
                        @if(isset($pins))
                            <select class="form-control" name="pin_id">
                                @foreach($pins as $user)
                                    <option value="{!! $user->id !!}">{!! $user->name !!}</option>
                                @endforeach
                            </select>
                        @endif
                    </div>
                    <div class="col-sm-12 col-md-6 form-group">
                        {!! Form::label('pin', 'Security Pin:') !!}
                        {!! Form::password('pin', array('class' => 'form-control', 'id' => 'pin-close')) !!}
                        <span class="code-close-error hidden">Wrong pin code entered.</span>
                    </div>
                </div> --}}

                {{ Form::close() }}
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger close-counter">Confirm Closure</button>
            </div>
        </div>
    </div>
</div>


@push('footer-script')
<script type="text/javascript">
    (function(window, $) {
    $('body').on('click', '.close-counter-btn', function(e){
        $('.close-counter').removeAttr('disabled');
    });

    $('body').on('click', '.close-counter', function(e){
        e.preventDefault();
        $('.close-counter').attr('disabled', 'disabled');
        $('#pin').css({'border' : 'none'});
        if($('.code-error').is('visible')) {
            $('.code-error').addClass('hidden');
        }
        var form = $('.bs-close-modal-sm form');
        var element = $(this);
        $formData = form.serializeArray();
        
        if ($(".counter-id").length<1&&$(".closeMulti").length>0) {
            var $counterIds = [];
            $("input.closeMulti:checked").each(function(){
                $counterIds.push($(this).val());
            });
            $formData.push({name: "counter_id", value: $counterIds});
        }

        console.log($formData);

        $.ajax({
            type:"POST",
            url:form.attr("action"),
            data:$formData,
            success: function(response){
                element.parent().parent().find('button[data-dismiss="modal"]').click();
                var myStack = {"dir1":"down", "dir2":"right", "push":"top"};
                new PNotify({
                    title: "Success",
                    text: response.data,
                    addclass: "stack-custom",
                    stack: myStack
                })
                setTimeout(function(e){
                    window.location = '/counters/';
                }, 1000);
            }
        }).error(function(jqxhr, settings, thrownError){
            var object = JSON.parse(jqxhr.responseText);
            console.log(object);
            $('#pin-close').css({'border' : 'thin solid #ff3333'});
            if(!$('.code-close-error').is('visible')) {
                $('.code-close-error').removeClass('hidden');
            }
        });
    });
    })(window, jQuery);
</script>
@endpush