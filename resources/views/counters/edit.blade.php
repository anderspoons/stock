@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Counter
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                {!! Form::model($counter, ['route' => ['counters.update', $counter->id], 'method' => 'patch']) !!}
                <div class="row">
                    @include('counters.fields')
                </div>
                <div class="row">
                    <!-- Submit Field -->
                    <div class="form-group col-sm-6">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                        <a href="{!! route('counters.index') !!}" class="btn btn-default">Cancel</a>
                    </div>
                    <div class="form-group col-sm-6 right">

                        @if($delete)
                        <a href="#" class="btn btn-danger" data-toggle="modal"
                           data-target=".bs-delete-modal-sm"><i
                                    class="delete-item glyphicon glyphicon-remove"></i>Delete Sale</a>
                        @endif

                        @if(!$closed && !$billed)
                            &nbsp;
                            <a href="#" class="btn btn-warning" data-toggle="modal" data-target=".bs-close-modal-sm"><i
                                        class="close-counter-btn glyphicon glyphicon-remove"></i>Close Sale</a>
                        @endif

                        @if($closed && !$billed)
                            &nbsp;
                            <a href="#" class="btn btn-warning" data-toggle="modal" data-target=".bs-remove-close-modal-sm"><i
                                        class="remove-close-job glyphicon glyphicon-remove"></i>Remove Closed Status</a>
                        @endif

                        @if(!$billed && $closed)
                            &nbsp;
                            <a href="#" class="btn btn-primary" data-toggle="modal"
                               data-target=".bs-add-billed-modal-sm"><i
                                        class="add-billed-job glyphicon glyphicon-remove"></i>Mark as Billed</a>
                        @endif

                        @if($billed)
                            &nbsp;
                            <a href="#" class="btn btn-warning" data-toggle="modal"
                               data-target=".bs-remove-billed-modal-sm"><i
                                        class="remove-billed-job glyphicon glyphicon-remove"></i>Remove Billed Lock</a>
                        @endif
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@if(!$billed && $closed)
    @include('counters.add-billed')
@endif

@if(!$closed && !$billed)
    @include('counters.add-close')
@endif

@if($closed)
    @include('counters.remove-close')
@endif

@if($billed)
    @include('counters.remove-billed')
@endif

@include('shared.modal.delete')
