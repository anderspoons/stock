<!-- Customer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('boat_id', 'Boat:') !!}
    @include('boats.select')
</div>

<!-- Customer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_id', 'Customer:') !!}
    @include('customers.select')
</div>

<!-- Note Field -->
<div class="form-group col-sm-6">
    {!! Form::label('note', 'Note:') !!}
    @if($billed)
        {!! Form::textarea('note', null, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
    @else
        {!! Form::textarea('note', null, ['class' => 'form-control']) !!}
    @endif
</div>

<!-- Note Field -->
<div class="form-group col-sm-6">
    {!! Form::label('work_scope', 'Work Scope:') !!}
    @if($billed)
        {!! Form::textarea('work_scope', null, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
    @else
        {!! Form::textarea('work_scope', null, ['class' => 'form-control']) !!}
    @endif
</div>

<!-- Po Field -->
<div class="form-group col-sm-6">
    {!! Form::label('po', 'Po:') !!}
    @if($billed)
        {!! Form::text('po', null, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
    @else
        {!! Form::text('po', null, ['class' => 'form-control']) !!}
    @endif
</div>

@include('shared.status')

<!-- Sage Invoice Field -->
<div class="form-group col-sm-6">
    @if(isset($ref))
        {!! Form::label('ref', 'Reference for Sage:') !!}
        {!! Form::text('', $ref, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
        {!! Form::hidden('ref', $ref) !!}
    @elseif(isset($counter->ref))
        {!! Form::label('ref', 'Reference for Sage:') !!}
        {!! Form::text('', $counter->ref, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
        {!! Form::hidden('ref', $counter->ref) !!}
    @endif
</div>
