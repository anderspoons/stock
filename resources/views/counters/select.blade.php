<select name="counter_id"
        @if(isset($billed))
        @if($billed)
        disabled
        @endif
        @endif
        class="form-control counter-select" id="counter-select">
</select>

@push('footer-script')
<script type="text/javascript">
    (function (window, $) {
        $(".counter-select").each(function(e) {
            $(this).select2({
                ajax: {
                    url: "{!! url('counter/json') !!}",
                    dataType: "json",
                    data: function (term) {
                        return {query: term.term}
                    },
                    results: function (data) {
                        return {
                            results: data.results
                        };
                    },
                },
                createSearchChoice: function (term, data) {
                    if ($(data.results).filter(function () {
                            return this.text.localeCompare(term) === 0;
                        }).length === 0) {
                        return {
                            id: term,
                            text: term
                        };
                    }
                },
                width: '100%',
            });
        });
    })(window, jQuery);
</script>
@endpush