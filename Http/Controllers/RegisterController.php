<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Validator;
use App\User;
use Redirect;
use Input;
use DebugBar;

class RegisterController extends Controller
{

	public function index() {
		return view('pages.register.index');
	}

	public function store(Request $request) {
		$validator = Validator::make( $request->all(), [
			'title'    => 'required',
			'fname'    => 'required',
			'sname'    => 'required',
			'email'    => 'required|email|Unique:users',
			'password' => 'required|confirmed|min:6'
		] );
		if ( $validator->fails() ) {
			Input::flash();
			return view('pages.register.index')->withErrors($validator->errors());
		} else {
			$user           = new User;
			$user->title    = $request->input( 'title' );
			$user->fname    = $request->input( 'fname' );
			$user->sname    = $request->input( 'sname' );
			$user->email    = $request->input( 'email' );
			$user->password = bcrypt( $request->input( 'password' ) );
			$user->active   = '0';
			if ( $user->save() ) {
				return view( 'pages.register.done' );
			} else {
				$message = 'There was an unforeseen issue with your registration please retry.';
				return view( 'pages.register.index', compact( 'message' ));
			}
		}
	}

}
