<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Excel;

class HomeController extends Controller
{

	public function index()
	{
		if ( Auth::check() ) {
			return view('pages.home.index');
		} else {
			return redirect('/login');
		}
	}

	public function excel() {
		ini_set( 'output_buffering', 0 );
		$stock = Excel::filter( 'chunk' )->load( 'stocklist.csv' )->chunk( 50, function ( $results ) {
			foreach ( $results as $k => $v ) {
				if ( is_array( $v ) ) {
					foreach ( $v as $k => $v ) {
						if ( is_array( $v ) ) {
							foreach ( $v as $k => $v ) {
								echo $k;
								echo $v;
							}
						}
					}
				}
			}
		});
	}

	public function row($row){
		foreach($row as $k => $v){
			if($k == 'LOCAT') {
				echo $v .'<br/>';
			}
		}
	}
}
