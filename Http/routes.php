<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group(['middleware' => ['web']], function () {
    Route::get( 'login', 'LoginController@index' );
    Route::post( 'login', 'LoginController@login' );
    Route::get( 'forgot', 'ForgotController@index' );
    Route::post( 'forgot', 'ForgotController@forgot' );
    Route::get( 'register', 'RegisterController@index' );
    Route::post( 'register', 'RegisterController@store' );
    Route::get('/import', 'HomeController@excel');

    Route::group(['middleware' => ['auth']], function () {
        Route::get( '/', 'HomeController@index' );
        //logout
        Route::get( 'logout', 'LoginController@logout' );
        Route::resource( 'user', 'UserController' );
    });

});
