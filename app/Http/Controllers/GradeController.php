<?php

namespace App\Http\Controllers;

use App\DataTables\gradeDataTable;
use App\Http\Requests;
use App\Http\Requests\CreategradeRequest;
use App\Http\Requests\UpdategradeRequest;
use App\Repositories\gradeRepository;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Response;

class GradeController extends AppBaseController
{
    /** @var  gradeRepository */
    private $gradeRepository;

    public function __construct(GradeRepository $gradeRepo)
    {
        $this->gradeRepository = $gradeRepo;
    }

    /**
     * Display a listing of the grade.
     *
     * @param gradeDataTable $gradeDataTable
     * @return Response
     */
    public function index(GradeDataTable $gradeDataTable)
    {
        return $gradeDataTable->render('grades.index');
    }

    /**
     * Show the form for creating a new grade.
     *
     * @return Response
     */
    public function create()
    {
        return view('grades.create');
    }

    /**
     * Store a newly created grade in storage.
     *
     * @param CreategradeRequest $request
     *
     * @return Response
     */
    public function store(CreateGradeRequest $request)
    {
        $input = $request->all();

        $grade = $this->gradeRepository->create($input);

        Flash::success('grade saved successfully.');

        return redirect(route('grades.index'));
    }

    /**
     * Display the specified grade.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $grade = $this->gradeRepository->findWithoutFail($id);

        if (empty($grade)) {
            Flash::error('grade not found');

            return redirect(route('grades.index'));
        }

        return view('grades.show')->with('grade', $grade);
    }

    /**
     * Show the form for editing the specified grade.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $grade = $this->gradeRepository->findWithoutFail($id);

        if (empty($grade)) {
            Flash::error('grade not found');

            return redirect(route('grades.index'));
        }

        return view('grades.edit')->with('grade', $grade);
    }

    /**
     * Update the specified grade in storage.
     *
     * @param  int              $id
     * @param UpdategradeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateGradeRequest $request)
    {
        $grade = $this->gradeRepository->findWithoutFail($id);

        if (empty($grade)) {
            Flash::error('grade not found');

            return redirect(route('grades.index'));
        }

        $grade = $this->gradeRepository->update($request->all(), $id);

        Flash::success('grade updated successfully.');

        return redirect(route('grades.index'));
    }

    /**
     * Remove the specified grade from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $grade = $this->gradeRepository->findWithoutFail($id);

        if (empty($grade)) {
            Flash::error('grade not found');

            return redirect(route('grades.index'));
        }

        $this->gradeRepository->delete($id);

        Flash::success('grade deleted successfully.');

        return redirect(route('grades.index'));
    }
}
