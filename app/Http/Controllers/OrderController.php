<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\OrderSize;
use App\Models\Boat;
use App\Models\Status;
use App\Models\Store;
use App\Models\Supplier;
use App\Models\JobItems;
use App\Models\JobMisc;
use App\Models\CounterItems;
use App\Models\CounterMisc;
use App\Models\Unit;
use Auth;
use Mail;
use PDF;
use Carbon\Carbon;
use Session;
use App\DataTables\OrderDataTable;
use App\DataTables\OrderItemDataTable;
use App\DataTables\OrderReceiveDataTable;
use App\Models\User;
use App\Models\Item;
use Illuminate\Http\Request;
use App\Http\Requests\CreateOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Repositories\OrderRepository;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Response;
use App\Models\Pincodes;
use App\Models\Order;
use App\Models\Location;
use App\Models\OrderCarriage;
use App\Models\OrderMisc;
use App\Models\OrderItems;
use App\Models\OrderSupplierNew;

class OrderController extends AppBaseController {
	/** @var  OrderRepository */
	private $orderRepository;

	public function __construct( OrderRepository $orderRepo ) {
		$this->orderRepository = $orderRepo;
	}

	/**
	 * Display a listing of the Order.
	 *
	 * @param OrderDataTable $orderDataTable
	 *
	 * @return Response
	 */
	public function index( OrderDataTable $orderDataTable ) {
		$supplier_ids = Order::distinct()->pluck( 'supplier_id' )->toArray();
		$suppliers    = Supplier::whereIn( 'id', $supplier_ids )->get();

		$store_ids = Order::distinct()->pluck( 'store_id' )->toArray();
		$stores    = Store::whereIn( 'id', $store_ids )->get();

		$yard_ids = Order::distinct()->pluck( 'yard_id' )->where( 'yard_id', 'NOT NULL' )->toArray();
		$boats    = Boat::whereIn( 'id', $yard_ids )->get();

		$status_ids = Order::with( 'statuses' )->get();
		$ids        = array();
		foreach ( $status_ids as $order ) {
			foreach ( $order->statuses as $status ) {
				$ids[] = $status->id;
			}
		}
		$statuses = Status::whereIn( 'id', $ids )->get();

		$pincodes = Pincodes::all();

		return $orderDataTable->render( 'orders.index', compact( 'pincodes', 'suppliers', 'boats', 'stores', 'statuses' ) );
	}

	/**
	 * Show the form for creating a new Order.
	 * @return Response
	 */
	public function create() {
		return view( 'orders.create' )->with( 'number', $this->get_order_number() );
	}

	public function create_yard() {
		return view( 'orders.yard.create' )->with( 'number', $this->get_yard_order_number() );
	}

	public function remove_item( Request $request ) {
		if ( CheckPin( $request->input( 'pin_id' ), $request->input( 'pin' ) ) ) {
			$item = OrderItems::where( 'id', '=', $request->input( 'id' ) )->first();
			if ( $item ) {
				if ( $item->delete() ) {
					check_order( $request->input( 'id' ) );

					return response()->json( [ 'text' => 'Item deleted successfully.' ], 200 );
				}
			}

			return response()->json( [ 'text' => 'Problem deleting item.' ], 500 );
		}

		return response()->json( [ 'text' => 'No pin or the wrong pin was entered.' ], 500 );
	}

	public function get_order_number() {
		$current = Order::select( 'id', 'po', 'custom_po' )->orderBy( 'id', 'DESC' )->limit( 10 )->first();
		//PO format
		//Year (2 digits), M, 4 numbers (9999) orders per year.

		//TODO only do year on or after march 1st

		$num    = substr( $current->po, - 4 );
		$number = Date( 'y' ) . 'M' . str_pad( ( $num + 1 ), 4, 0, STR_PAD_LEFT );
		$check  = Order::select( 'po' )->where( 'po', '=', $number )->first();
		if ( ! $check ) {
			return $number;
		}
		//redo this.
		$this->get_order_number();
	}

	//TODO  Yard Number - 000 is the format for yard based numbers
	public function get_yard_order_number() {
		$current = Order::select( 'id', 'po', 'custom_po' )->orderBy( 'id', 'DESC' )->first();
		//PO format
		//Year (2 digits), M, 4 numbers (9999) orders per year.
		//TODO only do year on or after march 1st
		$num    = substr( $current->po, - 4 );
		$number = Date( 'y' ) . 'M' . str_pad( ( $num + 1 ), 4, 0, STR_PAD_LEFT );
		$check  = Order::select( 'po' )->where( 'po', '=', $number )->first();
		if ( ! $check ) {
			return $number;
		}
		//redo this.
		$this->get_order_number();
	}


	/**
	 * Store a newly created Order in storage.
	 *
	 * @param CreateOrderRequest $request
	 *
	 * @return Response
	 */
	public function store( CreateOrderRequest $request ) {
		$input = $request->all();
		$order = $this->orderRepository->create( $input );
		//TODO a check needs to be performed on the PO number used to confirm its unique
		Flash::success( 'Order saved successfully.' );

		return redirect( route( 'orders.index' ) );
	}

	/**
	 * Display the specified Order.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show( $id ) {
		$order = $this->orderRepository->findWithoutFail( $id );
		if ( empty( $order ) ) {
			Flash::error( 'Order not found' );

			return redirect( route( 'orders.index' ) );
		}

		return view( 'orders.show' )->with( 'order', $order );
	}

	/**
	 * Show the form for editing the specified Order.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit( $id ) {
		$order = $this->orderRepository->findWithoutFail( $id );
		if ( empty( $order ) ) {
			Flash::error( 'Order not found' );

			return redirect( route( 'orders.index' ) );
		}

		return view( 'orders.edit' )->with( 'order', $order );
	}

	public function confirm( Request $request ) {
		if ( CheckPin( $request->input( 'pin_id' ), $request->input( 'pin' ) ) ) {
			$order = $this->orderRepository->findWithoutFail( $request->input( 'id' ) );
			if ( empty( $order ) ) {
				return response()->json( [ 'text' => 'Order not found.' ], 500 );
			}
			$order->expected_at = Carbon::parse( $request->input( 'expected_date' ) )->format( 'Y-m-d G:i:s' );
			$order->save();
			check_order( $request->input( 'id' ) );

			return response()->json( [ 'text' => 'Order confirmed succesfully.' ], 200 );
		} else {
			return response()->json( [ 'text' => 'No pin or the wrong pin was entered.' ], 500 );
		}
	}


	/**
	 * Update the specified Order in storage.
	 *
	 * @param  int $id
	 * @param UpdateOrderRequest $request
	 *
	 * @return Response
	 */
	public function update( $id, UpdateOrderRequest $request ) {
		$order = $this->orderRepository->findWithoutFail( $id );
		if ( empty( $order ) ) {
			Flash::error( 'Order not found' );

			return redirect( route( 'orders.index' ) );
		}
		$order = $this->orderRepository->update( $request->all(), $id );
		if ( $request->has( 'status_id' ) ) {
			$order->statuses()->sync( $request->input( 'status_id' ) );
		} else {
			$order->statuses()->sync( [] );
		}
		check_order( $id );
		Flash::success( 'Order updated successfully.' );

		return redirect( route( 'orders.index' ) );
	}

	/**
	 * Remove the specified Order from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function remove( $id ) {
		$order = $this->orderRepository->findWithoutFail( $id );
		if ( empty( $order ) ) {
			Flash::error( 'Order not found' );

			return redirect( route( 'orders.index' ) );
		}
		$this->orderRepository->delete( $id );
		Flash::success( 'Order deleted successfully.' );

		return redirect( route( 'orders.index' ) );
	}

	public function JsonLookup( Request $request ) {
		$orders = Order::orderBy( 'id', 'DESC' );
		if ( $request->has( 'q' ) ) {
			foreach ( $request->input( 'q' ) as $val ) {
				$orders->where( 'description', 'like', '%' . $val . '%' );
				$orders->orWhere( 'po', 'like', '%' . $val . '%' );
				$orders->with( array(
					'supplier' => function ( $query ) use ( $val ) {
						$query->where( 'supplier.name', $val );
					}
				) );
			}
		}
		$results        = $orders->get();
		$formatted_list = array();
		foreach ( $results as $order ) {
			$txt = '';
			if ( strlen( $order->po ) > 0 ) {
				$txt = 'PO: ' . $order->po;
			}
			$processed = false;
			if ( isset( $order->statuses ) ) {
				foreach ( $order->statuses as $status ) {
					if ( in_array( $status->id, [ 2, 6 ] ) ) {
						$processed = true;
					}
				}
			}
			if ( ! $processed ) {
				$formatted_list[] = array(
					'id'      => $order->id,
					'text'    => $txt . ' Supplier: ' . $order->supplier->name,
					'supp_id' => $order->supplier->id
				);
			}
		}

		return Response::json( array( 'results' => $formatted_list ) );
	}

	public function add( Request $request ) {
		if ( CheckPin( $request->input( 'pin_id' ), $request->input( 'pin' ) ) ) {
			$order = $this->orderRepository->findWithoutFail( $request->input( 'order_id' ) );
			$item  = Item::where( 'id', '=', $request->input( 'order_id' ) );
			if ( empty( $order ) ) {
				Flash::error( 'Order not found' );

				return redirect( route( 'orders.index' ) );
			}
			$processitem = $this->process_item_order( $item, $request->input( 'id' ), $request->input( 'pin_id' ) );
			if ( $processitem == true ) {
				check_order( $request->input( 'id' ) );

				return response()->json( [
					'text' => 'Item added succesfully.'
				], 200 );
			} else {
				return response()->json( [
					'text' => 'There were some a problem adding this to the order, try again and seek support if this issues persists.',
				], 500 );
			}
		} else {
			return response()->json( [
				'text' => 'No pin or the wrong pin was entered.'
			], 500 );
		}
	}

	public function items( OrderItemDataTable $dataTable, $id ) {
		$pincodes = Pincodes::all();

		return $dataTable->forOrder( $id )->render( 'orders.items', compact( 'pincodes', 'id' ) );
	}

	public function receive( OrderReceiveDataTable $dataTable, $id ) {
		$pincodes = Pincodes::all();

		return $dataTable->forOrder( $id )->render( 'orders.receive', compact( 'pincodes', 'id' ) );
	}

	public function line_qty( Request $request ) {
		if ( strlen( $request->input( 'reason' ) ) < 1 ) {
			return Response::json( [
				'error' => 'You must enter a reason for this change.',
			], 500 );
		}
		//check that the pin code is valid
		if ( CheckPin( $request->input( 'pin_id' ), $request->input( 'pin' ) ) ) {

			$line = OrderItems::where( 'id', '=', $request->input( 'line_id' ) )->first();
			if ( $line ) {
				$line->qty = $request->input( 'qty' );
				Session::set( 'pin_id', $request->input( 'pin_id' ) );
				Session::set( 'reason', $request->input( 'reason' ) );
				if ( $line->save() ) {
					return Response::json( [
						'text' => 'Quantity changed successfully.'
					], 200 );
				}
			}
			Session::remove( 'pin_id' );
			Session::remove( 'reason' );

			return Response::json( [
				'error' => 'There was a problem changing the quantity of this line item please try again and contact support if this continues.',
			], 500 );
		}

		return Response::json( [
			'error' => 'No pin or the wrong pin was entered.'
		], 500 );
	}


	public function email( Request $request ) {
		if(strlen($request->input('expected_date')) > 0){
			$order = Order::where('id', '=', $request->input('id'))->first();
			if (valid_email($request->input('email'))) {
				$pdf = $this->order_pdf($request->input('id'), true);
				$data = array(
					'message' => $request->input('message')
				);

				$mail = Mail::send('email.counter', ['data' => $data], function ($message) use ($order, $pdf, $request) {
					$message->from('stores@macduffshipyards.com', 'Macduff Shipyards');
					$message->sender('stores@macduffshipyards.com', 'Macduff Shipyards');
					$message->to($request->input('email'));
					$message->bcc('sam@boxportable.com');
					$message->attachData($pdf, $order->po . '_' . Date('d-m-y') . '_.pdf');
				});

				if ($mail) {
					$order->expected_at = Carbon::parse( $request->input( 'expected_date' ) )->format( 'Y-m-d G:i:s' );
					if ($order->save()) {
						check_order($order->id);
						return Response::json(array(
							'success' => true,
							'data'    => 'Order sent, the order has been marked as placed.'
						));
					}

					return Response::json([
						'error' => [
							'exception' => 'The email sent but we could not update the order to mark it as being processed.',
						]
					], 500);
				}
			}

			return Response::json([
				'error' => [
					'exception' => 'The suppliers email address is not valid.',
				]
			], 500);
		}

		return Response::json([
			'error' => [
				'exception' => 'The expected delivery date is required.',
			]
		], 500);


		//check the customer for an email.
		//check the counter sale is closed and not marked as
	}

	public function fax( $id ) {
		return $this->order_pdf( $id, false );
	}

	public function order_pdf( $id, $stream ) {
		$order = Order::where( 'id', '=', $id )->with( 'items' )->first();
		$title = 'Purchase Order ' . $order->po . '   Date: '. Date( 'd/m/y' );
		$pdf   = PDF::loadView( 'orders.layouts.pdf', array(
			'title' => $title,
			'order' => $order
		) )->setPaper( 'A4', 'landscape' );
		if($stream){
			return $pdf->stream();
		}
		return $pdf->download();
	}

	public function line_cost( Request $request ) {
		$line = OrderItems::where( 'id', '=', $request->input( 'line-id' ) )->first();
		if ( $line ) {
			$total                = number_format( number_format( $request->input( 'line_unit_cost' ), 2 ) * $line->qty, 2 );
			$line->line_cost      = $total;
			$line->line_unit_cost = number_format( $request->input( 'line_unit_cost' ), 2 );
			Session::set( 'pin_id', $request->input( 'pin_id' ) );
			Session::set( 'reason', $request->input( 'reason' ) );
			if ( $line->save() ) {
				return Response::json( [
					'text' => 'Line cost changed successfully.'
				], 200 );
			}
		}
		Session::remove( 'pin_id' );
		Session::remove( 'reason' );

		return Response::json( [
			'error' => 'There was a problem changing the cost of this line item please try again and contact support if this continues.',
		], 500 );
	}

	public function receive_items( Request $request ) {
		$inputs   = $request->all();
		$lines    = json_decode( $inputs['items'] );
		$problems = array();
		foreach ( $lines as $line ) {
			$the_line = $this->process_receive_line( $line );
			if ( $the_line != true ) {
				$problems[] = $the_line;
			}
		}

		if ( count( $problems ) > 0 ) {
			return Response::json( [
				'text' => json_encode( $problems )
			], 500 );
		} else {
			return Response::json( [
				'text' => 'All items processed successfully.'
			], 200 );
		}
	}

	public function process_receive_line( $line ) {
		if ( $line->for === 'stock' && $line->location === 0 ) {
			return [ 'line_id' => $line->line_id, 'problem' => 'location not set.' ];
		}
		//Get all the item details then so we can proceed with checks.
		//TODO if a location when the order was placed was set to somthing different from when it was marked as recieved and
		//TODO the location was not 0 so it was a real location you need to then go and change the totals for on order / on reserve of the location that was orginially used when ordering

		$order_item = OrderItems::where( 'id', '=', $line->line_id )->with( 'order', 'counter', 'job', 'misc', 'item', 'new_supplier' )->first();
		if ($line->qty === 0 || $line->qty > $order_item->qty || ( $line->qty + $order_item->qty_in > $order_item->qty)) {
			return [ 'line_id' => $line->line_id, 'problem' => 'Problem with the quantity' ];
		}

		if ( $line->for === 'job') {
			$issue = $this->issue_to_job($line->line_id, $line->qty);
			if(!$issue){
				return [ 'line_id' => $line->line_id, 'problem' => $issue ];
			}
		}

		if ( $line->for === 'counter') {
			$issue = $this->issue_to_counter($line->line_id, $line->qty);
			if($issue !== true){
				return [ 'line_id' => $line->line_id, 'problem' => $issue ];
			}
		}

		if ( $order_item->new_supplier === 1 ) {
			//this is a new supplier to this item. Add the supplier + details to the item record.
			$supplier = $this->add_supplier_details($line->line_id);
			if($supplier !== true){
				return [ 'line_id' => $line->line_id, 'problem' => $supplier ];
			}
		}


		if ($order_item->item_id === 0 &&  $line->for === 'stock' ) {
			//todo a item thats misc could still be for a job or counter
			$order_item->qty = $order_item->qty - $line->qty;
			if ( ! $order_item->save() ) {
				return [ 'line_id' => $line->line_id, 'problem' => 'Item would not save the updated qty.' ];
			}
		}

		if ($order_item->item_id !== 0 &&  $line->for === 'stock' ) {
			//update the location the item ended up in.
			$item               = Item::where( 'id', '=', $order_item->item_id )->first();
			$item->on_order     = $item->on_order - $line->qty;
			$location           = Location::where( 'id', '=', $line->location )->first();
			$location->qty      = $location->qty + $line->qty;
			$location->on_order = $location->on_order - $line->qty;
			$order_item->location = $line->location;
			if(!$item->save()){
				return [ 'line_id' => $line->line_id, 'problem' => 'Item would not save the updated due in qty.' ];
			}
			if(!$location->save()){
				return [ 'line_id' => $line->line_id, 'problem' => 'location would not save new qty and correct the due in numbers' ];
			}
		}

		if ( $line->qty + $order_item->qty_in === $order_item->qty ) {
			//items been fully recieved
			$order_item->recieved_at = date( "Y-m-d H:i:s" );
			$order_item->qty_in      = $order_item->qty;
		} else {
			$order_item->qty_in = $line->qty;
		}

		if ($order_item->save() ) {
			check_order($order_item->order_id);
			return true;
		}
		return [ 'line_id' => $line->line_id, 'problem' => 'Unknown error' ];
	}

	public function issue( $id ) {
		$pins     = Pincodes::where( 'active', '1' )->get();
		$order    = Order::where( 'id', $id )->first();
		$category = Category::all();
		$units    = Unit::all();

		return view( 'orders.order' )
			->with( 'user', Auth::user() )
			->with( 'pins', $pins )
			->with( 'category', $category )
			->with( 'units', $units )
			->with( 'order', $order );
	}

	public function process_items( Request $request ) {
		//check that the pin code is valid
		if ( CheckPin( $request->input( 'pin_id' ), $request->input( 'code' ) ) ) {
			if ($request->input( 'item' )) {
				$problem_items = array();
				foreach ( $request->input( 'item' ) as $item ) {
					$processitem = $this->process_item_order( $item, $request->input( 'id' ), $request->input( 'pin_id' ));
					if ( $processitem == true ) {
						//Thats fine the item should of been issued save this incase we need to do somthing here
					} else {
						//Lets take the problem row id and put it in the array, the problem description if there is one should be added at this point so we can add info to the row with a problem
						$problem_items[] = array( 'row-id' => $item['row-id'], 'problem' => $processitem );
					}
				}
				if ( count( $problem_items ) > 0 ) {
					return response()->json( [
						'text' => 'There were some problems with certain items, all successfully submitted rows have been removed.',
						'rows' => $problem_items
					], 400 );
				} else {
					return response()->json( [
						'success' => true,
						'text' => 'Stock issued succesfully.'
					], 200 );
				}
			} else {
				return response()->json( [
					'text' => 'No items to process.'
				], 400 );
			}
		} else {
			return response()->json( [
				'text' => 'No pin or the wrong pin was entered.'
			], 400 );
		}
	}

	public function process_item_order( $orderItem, $order_id, $pin_id) {
		//todo might be worth checking if an item is being ordered for stock / job / counter that if there is the same item on order just to update that item.
		//todo becuase if someone adds 2 or more items which are new with different supplier units they are going to end up with a problem of the same supplier being added to an item twice with
		Session::set( 'pin_id', 13 );
		if ( $orderItem['qty'] != 0 ) {
			switch ( $orderItem['id'] ) {
				//
				// Misc items on a order sale
				//
				case 'carriage':
					try {
						$order           = new OrderCarriage();
						$order->order_id = $order_id;
						$order->pin_id   = $pin_id;
						$order->cost     = $orderItem['cost'];
						$order->name     = $orderItem['name'];
						Session::set( 'pin_id', $pin_id );
						if ( $order->save() ) {
							return true;
						}
					} catch ( Exception $exp ) {
						return 'Unknown issue when trying to issue this item, please remove it and try again, if this problem continues contact support.';
					}
					break;

				case 'misc':
					try {
						$order                 = new OrderItems();
						$order->item_id        = '0';
						$order->order_id       = $order_id;
						$order->pin_id         = $pin_id;
						$order->line_unit_cost = $orderItem['unit_cost'];
						$order->line_cost      = $orderItem['real_cost'];
						$order->qty            = $orderItem['qty'];
						Session::set( 'pin_id', $pin_id );
						if ( $order->save() ) {
							try {
								$misc                = new OrderMisc();
								$misc->order_item_id = $order->id;
								$misc->name          = $orderItem['name'];
								$misc->description   = nl2br( $orderItem['item_description'] );
								if ( $misc->save() ) {
									return true;
								} else {
									throwException( 'Problem Saving' );
								}
							} catch ( Exception $exp ) {
								//need to delete the first job part
								$delete = OrderItems::find( $order->id );
								if ( $delete->delete() ) {
									Session::forget( 'pin_id' );

									return 'Unknown issue when trying to order this misc item, please remove it and try again, if this problem continues contact support.';
								} else {
									Session::forget( 'pin_id' );

									return 'Please contact support about this because there has been a problem which will leave orphaned data in the system if its not dealt with. Do not close this screen!';
								}
							}
						}
					} catch ( Exception $exp ) {
						return 'Unknown issue when trying to issue this item, please remove it and try again, if this problem continues contact support.';
					}
					break;

				default:
					$item = Item::where( 'id', $orderItem['id'] )->first();
					if ( $orderItem['location'] != 0 ) {
						$location = Location::where( 'id', $orderItem['location'] )->first();
						//Check item and location were both found
						if ( ! $location ) {
							return 'Location was not found on the system, remove the item and try again, if this problem continues contact support.';
						}
					}
					if ( ! $item ) {
						return 'Item was not found on the system, remove the item and try again, if this problem continues contact support.';
					}
					try {
						//try to do the issue
						$order                 = new OrderItems();
						$order->item_id        = $orderItem['id'];
						$order->order_id       = $order_id;
						$order->qty            = $orderItem['qty'];
						$order->line_unit_cost = $orderItem['unit_cost'];
						$order->line_cost      = $orderItem['cost'];
						$order->pin_id         = $pin_id;
						$order->location_id    = $orderItem['location'];

						if ( isset( $location ) ) {
							$location->on_order = ( $location->on_order + floatval( $orderItem['qty'] ) );
						}
						$item->on_order = $item->on_order + $orderItem['qty'];
						if ( ! empty( $orderItem['job_id'] ) || ! empty( $orderItem['counter_id'] ) ) {
							$item->on_reserve = $item->on_reserve + $orderItem['qty'];
						}
						//TODO add items on order status to job or counter when processing this item.
						if ( ! empty( $orderItem['job_id'] ) ) {
							$order->job_id = $orderItem['job_id'];
						}
						if ( ! empty( $orderItem['counter_id'] ) ) {
							$order->counter_id = $orderItem['counter_id'];
						}

						Session::set( 'pin_id', $pin_id );
						if ( $order->save() ) {
							if ( isset( $orderItem['size'] ) ) {
								foreach ( $orderItem['size'] as $size ) {
									try {
										$itemSize                = new OrderSize();
										$itemSize->order_item_id = $order->id;
										$itemSize->size_unit_id  = $orderItem['unit'];
										$itemSize->size          = $size;
										$itemSize->save();
									} catch ( Exception $exp ) {
										return 'Failed to save this items sizes, please contact support.';
									}
								}
							}

							if ( ! empty( $orderItem['ordered_as_cat'] ) && ! empty( $orderItem['lots_of'] ) ) {
								try {
									$newSupplier                = new OrderSupplierNew();
									$newSupplier->order_item_id = $order->id;
									$newSupplier->lots          = $orderItem['lots_of'];
									$newSupplier->unit_id       = $orderItem['ordered_as_unit'];
									$newSupplier->unit_cat_id   = $orderItem['ordered_as_cat'];
									$newSupplier->save();
								} catch ( Exception $exp ) {
									return 'Failed to save this items sizes, please contact support.';
								}
							}

							return true;
						} else {
							Session::forget( 'pin_id' );

							return 'Failed to save this issue, the quantity may have been reduced please contact support.';
						}
					} catch ( Exception $exp ) {
						Session::forget( 'pin_id' );

						return 'Unknown issue when trying to issue this item, please remove it and try again, if this problem continues contact support.';
					}
					break;
			}
		} else {
			return true;
		}
	}

	public function location_json( Request $request ) {
		$line_id = $request->input( 'line' );
		$id      = OrderItems::where( 'id', '=', $line_id )->first();

		$locations      = Location::with( 'store' )
		                          ->where( 'item_id', '=', $id->item_id )
		                          ->where( 'store_id', '=', $id->order->store_id )
		                          ->orderBy( 'qty', 'DESC' );
		$results        = $locations->get();
		$formatted_list = array();
		foreach ( $results as $location ) {
			$name = '';
			if ( isset( $location->store ) ) {
				$name .= $location->store->code . ' : ';
			}
			$name .= $location->room;
			$name .= $location->rack;
			$name .= $location->shelf;
			$name .= $location->bay;
			$name .= ' Qty: ' . $location->qty;
			$formatted_list[] = array(
				'id'   => $location->id,
				'text' => $name
			);
		}

		return Response::json( array( 'results' => $formatted_list ) );
	}

	public function issue_to_job($id, $qty){
		$order_item = OrderItems::where('id', '=', $id)->with('size', 'misc')->first();

		if ($order_item->item_id != '0') {
			$item = Item::where('id', $order_item->item_id)->first();
			try {
				//try to do the issue
				$issue = new JobItems();
				$issue->item_id = $item->id;
				$issue->job_id = $id;
				$issue->qty = $qty;
				$issue->line_cost = $order_item->line_cost;
				$issue->line_real_cost = $order_item->line_cost;
				$issue->line_unit_cost = $order_item->line_unit_cost;
				$issue->pin_id = 13;
				Session::set('pin_id', 13);
				if ($issue->save()) {
					if (isset($order_item->size)) {
						foreach ($order_item->size as $size) {
							try {
								$itemSize = new JobSize();
								$itemSize->job_item_id = $issue->id;
								$itemSize->size_unit_id = $size->size_unit_id;
								$itemSize->size = $size;
								$itemSize->save();
							} catch (Exception $exp) {
								return 'Failed to save this items sizes, please contact support.';
							}
						}
					}
					return true;
				} else {
					Session::forget('pin_id');
					return 'Failed to save this issue, the quantity may have been reduced please contact support.';
				}
			} catch (Exception $exp) {
				Session::forget('pin_id');
				return 'Unknown issue when trying to issue this item, please remove it and try again, if this problem continues contact support.';
			}
			//double check item stock level is still ok and someone didn't just check it out.
		} else {
			try {
				$issue = new JobItems();
				$issue->item_id = '0';
				$issue->job_id = $id;
				$issue->pin_id = 13;
				$issue->line_cost = $order_item->line_cost;
				$issue->line_real_cost = $order_item->line_cost;
				$issue->line_unit_cost = $order_item->line_unit_cost;
				$issue->qty = $qty;
				Session::set('pin_id', 13);
				if ($issue->save()) {
					try {
						$misc = new JobMisc();
						$misc->job_item_id = $issue->id;
						$misc->name = $order_item->misc->name;
						$misc->description = nl2br($order_item->misc->description);
						if ($misc->save()) {
							return true;
						}
					} catch (Exception $exp) {
						//need to delete the first job part
						$delete = JobItems::find($issue->id);
						if ($delete->delete()) {
							Session::forget('pin_id');
							return 'Unknown issue when trying to issue this misc item, please remove it and try again, if this problem continues contact support.';
						} else {
							Session::forget('pin_id');

							return 'Please contact support about this because there has been a problem which will leave orphaned data in the system if its not dealt with. Do not close this screen!';
						}
					}
				}
			} catch (Exception $exp) {
				return 'Unknown issue when trying to issue this item, please remove it and try again, if this problem continues contact support.';
			}
		}
	}

	public function issue_to_counter($id, $qty){
		$order_item = OrderItems::where('id', '=', $id)->with('size', 'misc')->first();

		if ($order_item->item_id == '0') {
			try {
				$issue                 = new CounterItems();
				$issue->item_id        = '0';
				$issue->counter_id     = $id;
				$issue->pin_id         = 13;
				$issue->line_cost = $order_item->line_cost;
				$issue->line_real_cost = $order_item->line_cost;
				$issue->line_unit_cost = $order_item->line_unit_cost;
				$issue->qty            = $qty;
				Session::set( 'pin_id', 13 );
				if ( $issue->save() ) {
					try {
						$misc                  = new CounterMisc();
						$misc->counter_item_id = $issue->id;
						$misc->name            = $order_item->misc->name;
						$misc->description     = nl2br($order_item->misc->description);
						if ( $misc->save() ) {
							return true;
						}
					} catch ( Exception $exp ) {
						//need to delete the first job part
						$delete = JobItems::find( $issue->id );
						if ( $delete->delete() ) {
							Session::forget( 'pin_id' );

							return 'Unknown issue when trying to issue this misc item, please remove it and try again, if this problem continues contact support.';
						} else {
							Session::forget( 'pin_id' );

							return 'Please contact support about this because there has been a problem which will leave orphaned data in the system if its not dealt with. Do not close this screen!';
						}
					}
				}
			} catch ( Exception $exp ) {
				return 'Unknown issue when trying to issue this item, please remove it and try again, if this problem continues contact support.';
			}
		} else {
			$item     = Item::where( 'id', $order_item->item_id )->first();
			//Check item and location were both found
			if (!$item) {
				return 'Item or Location was not found on the system, remove the item and try again, if this problem continues contact support.';
			}
			//Check stock level because there could be an instance where some mongo adds the item twice to avoid my JS validation check
			//Look out for the phrase "I will review it and give you my input".... that means you can expect to redo hours possibly days of work with no justification as to why other than "it should work like this" ...

			try {
				//try to do the issue
				$issue                 = new CounterItems();
				$issue->item_id        = $order_item->item_id;
				$issue->counter_id     = $id;
				$issue->qty            = $qty;
				$issue->line_cost = $order_item->line_cost;
				$issue->line_real_cost = $order_item->line_cost;
				$issue->line_unit_cost = $order_item->line_unit_cost;
				$issue->pin_id         = 13;
				Session::set( 'pin_id', 13 );
				if ( $issue->save()) {
					if ( isset( $issueItem['size'] ) ) {
						foreach ( $issueItem['size'] as $size ) {
							try {
								$itemSize                  = new CounterSize();
								$itemSize->counter_item_id = $issue->id;
								$itemSize->size_unit_id    = $size->unit_id;
								$itemSize->size            = $size;
								$itemSize->save();
							} catch ( Exception $exp ) {
								return 'Failed to save this items sizes, please contact support.';
							}
						}
					}
					return true;
				} else {
					Session::forget( 'pin_id' );
					return 'Failed to save this issue, the quantity may have been reduced please contact support.';
				}
			} catch ( Exception $exp ) {
				Session::forget( 'pin_id' );
				return 'Unknown issue when trying to issue this item, please remove it and try again, if this problem continues contact support.';
			}
		}
	}

	public function add_supplier_details($id){
		$order_item = OrderItems::where('id', '=', $id)->with('new_supplier', 'size')->first();
		$item = Item::where('id', '=', $order_item->item_id)->with('suppliers')->first();

		//TODO noticed partcode isn't actually part of the issuing items to orders for new suppliers so it really should be as part of this.
		if (isset($order_item->new_supplier)) {
			$item->supplier_units()->attach($order_item->new_supplier->unit_id, array(
				'unit_cat_id' => $order_item->new_supplier->category_id,
				'supplier_id' => $order_item->supplier_id,
				'lots'        => $order_item->new_supplier->lots
			));
		}
		//so this is the bit where say a supplier is 2 sizes area squared these are those 2 sizes
		if (isset($order_item->size)) {
			foreach ( $order_item->size as $sizing ) {
				$size = new UnitSize();
				$size->data = floatval( $sizing );
				$size->save();
				$item->supplier_sizing()->attach( $size->id, array( 'supplier_id' => $order_item->supplier_id ) );
			}
		}
		if(!$item->save()){
			return 'Could not save items new supplier details';
		}
		return true;
	}

	public function close($id){
		$order = Order::where('id', '=', $id)->first();
		$order->closed_at = Carbon::now()->format('Y-m-d G:i:s');
		$order->save();
	}
}
