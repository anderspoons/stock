<?php

namespace App\Http\Controllers;

use App\DataTables\PincodesDataTable;
use App\Http\Requests;
use Mail;
use Request;
use App\Http\Requests\CreatePincodesRequest;
use App\Http\Requests\UpdatePincodesRequest;
use App\Models\Pincodes;
use App\Repositories\PincodesRepository;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Response;

class PincodesController extends AppBaseController
{
    /** @var  PincodesRepository */
    private $pincodesRepository;

    public function __construct(PincodesRepository $pincodesRepo)
    {
        $this->pincodesRepository = $pincodesRepo;
    }

    /**
     * Display a listing of the Pincodes.
     *
     * @param PincodesDataTable $pincodesDataTable
     * @return Response
     */
    public function index(PincodesDataTable $pincodesDataTable)
    {
        return $pincodesDataTable->render('pincodes.index');
    }

    /**
     * Show the form for creating a new Pincodes.
     *
     * @return Response
     */
    public function create()
    {
        return view('pincodes.create');
    }

    /**
     * Store a newly created Pincodes in storage.
     *
     * @param CreatePincodesRequest $request
     *
     * @return Response
     */
    public function store(CreatePincodesRequest $request)
    {
        $input = $request->all();
        $pincodes = $this->pincodesRepository->create($input);
        Flash::success('Pincodes saved successfully.');
        return redirect(route('pincodes.index'));
    }

    /**
     * Display the specified Pincodes.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $pincodes = $this->pincodesRepository->findWithoutFail($id);

        if (empty($pincodes)) {
            Flash::error('Pincodes not found');

            return redirect(route('pincodes.index'));
        }

        return view('pincodes.show')->with('pincodes', $pincodes);
    }

    /**
     * Show the form for editing the specified Pincodes.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $pincodes = $this->pincodesRepository->findWithoutFail($id);
	    $pins  = Pincodes::get();
        
        if (empty($pincodes)) {
            Flash::error('Pincodes not found');
            return redirect(route('pincodes.index'));
        }

        return view('pincodes.edit')
	        ->with('pincodes', $pincodes)
	        ->with('pins', $pins);
    }

    /**
     * Update the specified Pincodes in storage.
     *
     * @param  int              $id
     * @param UpdatePincodesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePincodesRequest $request)
    {
        $pincodes = $this->pincodesRepository->findWithoutFail($id);

        if (empty($pincodes)) {
            Flash::error('Pincodes not found');

            return redirect(route('pincodes.index'));
        }

        $pincodes = $this->pincodesRepository->update($request->all(), $id);

        Flash::success('Pincodes updated successfully.');

        return redirect(route('pincodes.index'));
    }

    /**
     * Remove the specified Pincodes from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $pincodes = $this->pincodesRepository->findWithoutFail($id);
        if (empty($pincodes)) {
            Flash::error('Pincodes not found');
            return redirect(route('pincodes.index'));
        }
        $this->pincodesRepository->delete($id);
        Flash::success('Pincodes deleted successfully.');
        return redirect(route('pincodes.index'));
    }


	public function send($id){
		$pincodes = $this->pincodesRepository->findWithoutFail($id);
		if (empty($pincodes)) {
			Flash::error('Pincodes not found');
			return redirect(route('pincodes.index'));
		}
		if($this->codeGenerate($id)){
			Flash::success('Pincode was reset and sent successfully.');
			return redirect(route('pincodes.index'));
		} else {
			Flash::error('Pincode could not be generated.');
			return redirect(route('pincodes.index'));
		}
	}

	public function codeGenerate($id){
		$code = rand(1001 , 9999);
		$check = Pincodes::where('code', $code)->get();
		$row = Pincodes::where('id', $id)->first();
		if($check->count() > 0){
			$this->randomCode();
		} else {
			Mail::send('email.pincode', ['code' => $code, 'name' => $row->name], function($message) use ($row)
			{
				$message->to($row->email)->subject('Macduff Stock System - Security Pincode Generated');
			});
			$row->code = $code;
			if($row->save()){
				return true;
			} else {
				return false;
			}
		}
    }
}
