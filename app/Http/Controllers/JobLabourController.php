<?php

namespace App\Http\Controllers;

use App\DataTables\JobLabourDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateJobLabourRequest;
use App\Http\Requests\UpdateJobLabourRequest;
use App\Repositories\JobLabourRepository;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Response;

class JobLabourController extends AppBaseController
{
    /** @var  JobLabourRepository */
    private $jobLabourRepository;

    public function __construct(JobLabourRepository $jobLabourRepo)
    {
        $this->jobLabourRepository = $jobLabourRepo;
    }

    /**
     * Display a listing of the JobLabour.
     *
     * @param JobLabourDataTable $jobLabourDataTable
     * @return Response
     */
    public function index(JobLabourDataTable $jobLabourDataTable)
    {
        return $jobLabourDataTable->render('jobLabours.index');
    }

    /**
     * Show the form for creating a new JobLabour.
     *
     * @return Response
     */
    public function create()
    {
        return view('jobLabours.create');
    }

    /**
     * Store a newly created JobLabour in storage.
     *
     * @param CreateJobLabourRequest $request
     *
     * @return Response
     */
    public function store(CreateJobLabourRequest $request)
    {
        $input = $request->all();

        $jobLabour = $this->jobLabourRepository->create($input);

        Flash::success('JobLabour saved successfully.');

        return redirect(route('jobLabours.index'));
    }

    /**
     * Display the specified JobLabour.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $jobLabour = $this->jobLabourRepository->findWithoutFail($id);

        if (empty($jobLabour)) {
            Flash::error('JobLabour not found');

            return redirect(route('jobLabours.index'));
        }

        return view('jobLabours.show')->with('jobLabour', $jobLabour);
    }

    /**
     * Show the form for editing the specified JobLabour.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $jobLabour = $this->jobLabourRepository->findWithoutFail($id);

        if (empty($jobLabour)) {
            Flash::error('JobLabour not found');

            return redirect(route('jobLabours.index'));
        }

        return view('jobLabours.edit')->with('jobLabour', $jobLabour);
    }

    /**
     * Update the specified JobLabour in storage.
     *
     * @param  int              $id
     * @param UpdateJobLabourRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateJobLabourRequest $request)
    {
        $jobLabour = $this->jobLabourRepository->findWithoutFail($id);

        if (empty($jobLabour)) {
            Flash::error('JobLabour not found');

            return redirect(route('jobLabours.index'));
        }

        $jobLabour = $this->jobLabourRepository->update($request->all(), $id);

        Flash::success('JobLabour updated successfully.');

        return redirect(route('jobLabours.index'));
    }

    /**
     * Remove the specified JobLabour from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $jobLabour = $this->jobLabourRepository->findWithoutFail($id);

        if (empty($jobLabour)) {
            Flash::error('JobLabour not found');

            return redirect(route('jobLabours.index'));
        }

        $this->jobLabourRepository->delete($id);

        Flash::success('JobLabour deleted successfully.');

        return redirect(route('jobLabours.index'));
    }
}
