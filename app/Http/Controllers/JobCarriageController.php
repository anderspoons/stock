<?php

namespace App\Http\Controllers;

use App\DataTables\JobCarriageDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateJobCarriageRequest;
use App\Http\Requests\UpdateJobCarriageRequest;
use App\Repositories\JobCarriageRepository;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Response;

class JobCarriageController extends AppBaseController
{
    /** @var  JobCarriageRepository */
    private $jobCarriageRepository;

    public function __construct(JobCarriageRepository $jobCarriageRepo)
    {
        $this->jobCarriageRepository = $jobCarriageRepo;
    }

    /**
     * Display a listing of the JobCarriage.
     *
     * @param JobCarriageDataTable $jobCarriageDataTable
     * @return Response
     */
    public function index(JobCarriageDataTable $jobCarriageDataTable)
    {
        return $jobCarriageDataTable->render('jobCarriages.index');
    }

    /**
     * Show the form for creating a new JobCarriage.
     *
     * @return Response
     */
    public function create()
    {
        return view('jobCarriages.create');
    }

    /**
     * Store a newly created JobCarriage in storage.
     *
     * @param CreateJobCarriageRequest $request
     *
     * @return Response
     */
    public function store(CreateJobCarriageRequest $request)
    {
        $input = $request->all();

        $jobCarriage = $this->jobCarriageRepository->create($input);

        Flash::success('JobCarriage saved successfully.');

        return redirect(route('jobCarriages.index'));
    }

    /**
     * Display the specified JobCarriage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $jobCarriage = $this->jobCarriageRepository->findWithoutFail($id);

        if (empty($jobCarriage)) {
            Flash::error('JobCarriage not found');

            return redirect(route('jobCarriages.index'));
        }

        return view('jobCarriages.show')->with('jobCarriage', $jobCarriage);
    }

    /**
     * Show the form for editing the specified JobCarriage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $jobCarriage = $this->jobCarriageRepository->findWithoutFail($id);

        if (empty($jobCarriage)) {
            Flash::error('JobCarriage not found');

            return redirect(route('jobCarriages.index'));
        }

        return view('jobCarriages.edit')->with('jobCarriage', $jobCarriage);
    }

    /**
     * Update the specified JobCarriage in storage.
     *
     * @param  int              $id
     * @param UpdateJobCarriageRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateJobCarriageRequest $request)
    {
        $jobCarriage = $this->jobCarriageRepository->findWithoutFail($id);

        if (empty($jobCarriage)) {
            Flash::error('JobCarriage not found');

            return redirect(route('jobCarriages.index'));
        }

        $jobCarriage = $this->jobCarriageRepository->update($request->all(), $id);

        Flash::success('JobCarriage updated successfully.');

        return redirect(route('jobCarriages.index'));
    }

    /**
     * Remove the specified JobCarriage from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $jobCarriage = $this->jobCarriageRepository->findWithoutFail($id);

        if (empty($jobCarriage)) {
            Flash::error('JobCarriage not found');

            return redirect(route('jobCarriages.index'));
        }

        $this->jobCarriageRepository->delete($id);

        Flash::success('JobCarriage deleted successfully.');

        return redirect(route('jobCarriages.index'));
    }
}
