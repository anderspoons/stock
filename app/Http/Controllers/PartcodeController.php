<?php

namespace App\Http\Controllers;

use App\DataTables\PartcodeDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatePartcodeRequest;
use App\Http\Requests\UpdatePartcodeRequest;
use App\Repositories\PartcodeRepository;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Response;

class PartcodeController extends AppBaseController
{
    /** @var  PartcodeRepository */
    private $partcodeRepository;

    public function __construct(PartcodeRepository $partcodeRepo)
    {
        $this->partcodeRepository = $partcodeRepo;
    }

    /**
     * Display a listing of the Partcode.
     *
     * @param PartcodeDataTable $partcodeDataTable
     * @return Response
     */
    public function index(PartcodeDataTable $partcodeDataTable)
    {
        return $partcodeDataTable->render('partcodes.index');
    }

    /**
     * Show the form for creating a new Partcode.
     *
     * @return Response
     */
    public function create()
    {
        return view('partcodes.create');
    }

    /**\
     * Store a newly created Partcode in storage.
     *
     * @param CreatePartcodeRequest $request
     *
     * @return Response
     */
    public function store(CreatePartcodeRequest $request)
    {
        $input = $request->all();

        $partcode = $this->partcodeRepository->create($input);

        Flash::success('Partcode saved successfully.');

        return redirect(route('partcodes.index'));
    }

    /**
     * Display the specified Partcode.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $partcode = $this->partcodeRepository->findWithoutFail($id);

        if (empty($partcode)) {
            Flash::error('Partcode not found');

            return redirect(route('partcodes.index'));
        }

        return view('partcodes.show')->with('partcode', $partcode);
    }

    /**
     * Show the form for editing the specified Partcode.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $partcode = $this->partcodeRepository->findWithoutFail($id);

        if (empty($partcode)) {
            Flash::error('Partcode not found');

            return redirect(route('partcodes.index'));
        }

        return view('partcodes.edit')->with('partcode', $partcode);
    }

    /**
     * Update the specified Partcode in storage.
     *
     * @param  int              $id
     * @param UpdatePartcodeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePartcodeRequest $request)
    {
        $partcode = $this->partcodeRepository->findWithoutFail($id);

        if (empty($partcode)) {
            Flash::error('Partcode not found');

            return redirect(route('partcodes.index'));
        }

        $partcode = $this->partcodeRepository->update($request->all(), $id);

        Flash::success('Partcode updated successfully.');

        return redirect(route('partcodes.index'));
    }

    /**
     * Remove the specified Partcode from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $partcode = $this->partcodeRepository->findWithoutFail($id);

        if (empty($partcode)) {
            Flash::error('Partcode not found');

            return redirect(route('partcodes.index'));
        }

        $this->partcodeRepository->delete($id);

        Flash::success('Partcode deleted successfully.');

        return redirect(route('partcodes.index'));
    }
}
