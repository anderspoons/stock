<?php

namespace App\Http\Controllers;

use App\DataTables\PartcodeDataTable;
use App\Models\Dashboard;
use Illuminate\Support\Facades\DB;
use App\Models\Job;
use App\Models\Counter;
use App\Models\Order;
use App\Models\JobItems;
use App\Models\Item;
use App\Models\CounterItems;

class CronController extends Controller {

	public function doValues() {
		$this->itemCount();
		$this->stockValue();
		$this->check_item_max();
		$this->check_item_min();
		$this->check_orders();
        //$this->closed_statuses();
	}

	public function cron_tests(){
//        $this->fixPrices('Job', 19, '2016-11-16 15:00:00', '2016-11-17 16:00:00');
	}

    public function fixPrices($type, $id, $start, $end){
        $list = null;

        switch($type){
            case ('Job'):
                $list = JobItems::where('job_id', '=', $id)
                    ->where('created_at', '>', $start)
                    ->where('created_at', '<', $end)
                    ->where('item_id', '!=', 0)
                    ->get();
                break;

            case ('Counter'):
                $list = CounterItems::where('counter_id', '=', $id)
                    ->where('created_at', '>', $start)
                    ->where('created_at', '<', $end)
                    ->where('item_id', '!=', 0)
                    ->get();
                break;
        }

        foreach($list as $item){
            $price = Item::where('id', '=', $item->item_id)->with('last_price')->first();
            $cost = $price->last_price[0]->cost;

            $set = false;

            if($item->markup_fixed != 0) {
                $set = $item->markup_fixed;
            }

            if($item->markup_percent == 0 & $set == false){
                $set = ($cost * 0.20) + $cost;
                $set = $this->roundUpTo($set, 0.01);
            }
            //total sale cost
            $item->line_cost = $set * $item->qty;
            //unit sale cost
            $item->line_unit_cost = $set;
            //total cost cost
            $item->line_real_cost = $cost;

            echo 'Set: '. $set.'<br/>';
            echo 'Line Cost: '. $set * $item->qty.'<br/>';
            echo 'Line Unit Cost: '. $set.'<br/>';
            echo 'Line Real Cost: '. $cost * $item->qty.'<br/>';
            $item->save();
        }
    }

	public function itemCount(){
		$total_results = DB::select( DB::raw('SELECT FORMAT(SUM(number),0) as item_count FROM(
		SELECT DISTINCT prices.item_id, locations.qty as number FROM prices 
		LEFT JOIN locations on prices.item_id=locations.item_id 
		AND locations.qty != \'0\' 
		ORDER BY prices.created_at DESC) unique_table;'));
		$stock_total = Dashboard::where('name', 'inventory')->first();
		$stock_total->value = (string)$total_results[0]->item_count;
        $stock_total->touch();

		$total_items = DB::select( DB::raw('SELECT FORMAT(COUNT(items.id),0) as item_count FROM items WHERE items.deleted_at IS NULL;'));
		$item_total = Dashboard::where('name', 'inventory_items')->first();
		$item_total->value = (string)$total_items[0]->item_count;
		$item_total->touch();

		if($stock_total->save() && $item_total->save()){
			echo 'Inventory Count Updated';
		} else {
			echo 'Inventory Count Failed To Update';
		}
	}

	public function stockValue(){
		$results = DB::select( DB::raw('SELECT FORMAT(SUM(value),2) as total FROM(
			SELECT DISTINCT prices.item_id, prices.cost * locations.qty as value FROM prices
			LEFT JOIN locations on prices.item_id=locations.item_id
			WHERE prices.cost != \'0\'
	      	AND locations.qty != \'0\'
			ORDER BY prices.created_at DESC) unique_table;
		'));
		$stock_value = Dashboard::where('name', 'value')->first();
		$stock_value->value = (string)$results[0]->total;
		$stock_value->touch();
		if($stock_value->save()){
			echo 'Stock Value Updated';
		} else {
			echo 'Stock Value Failed To Update';
		}
	}

	//This will go through all items which are less than the minimum stock value set
	public function check_item_min(){
		$locations = DB::select( DB::raw('SELECT `item_id`, `min`, `max`, `qty` FROM `locations` WHERE `min` != 0 AND `qty` <= min AND `locations`.`deleted_at` IS NULL'));
		foreach($locations as $location){
			//check if this item_id already has a status of Under Stocked - ID = 10
			$check = DB::select(DB::raw('SELECT `item_id` FROM `items_statuses` WHERE `item_id` = '.$location->item_id.' AND `status_id` = 10'));
			if(count($check) == 0){
				DB::table('items_statuses')->insert(
					['item_id' => $location->item_id, 'status_id' => 10]
				);
			}
		}

		$under_count = DB::select( DB::raw('SELECT COUNT(item_id) as item_count FROM `items_statuses` WHERE `status_id` = 10'));
		$stock_min = Dashboard::where('name', 'under_stocked')->first();
		$stock_min->value = (string)$under_count[0]->item_count;
        $stock_min->touch();
		if($stock_min->save()){
			echo 'Under Stocked Updated';
		} else {
			echo 'Under Stocked Failed To Update';
		}
	}

	//This will go through all items which are less than the minimum stock value set
	public function check_item_max(){
		$locations = DB::select( DB::raw('SELECT `item_id`, `min`, `max`, `qty` FROM `locations` WHERE `max` != 0 AND `qty` > max AND `locations`.`deleted_at` IS NULL'));

		foreach($locations as $location) {
			//check if this item_id already has a status of Under Stocked - ID = 11
			$check = DB::select( DB::raw( 'SELECT `item_id` FROM `items_statuses` WHERE `item_id` = ' . $location->item_id . ' AND `status_id` = 11' ) );
			if ( count( $check ) == 0 ) {
				DB::table( 'items_statuses' )->insert(
					[ 'item_id' => $location->item_id, 'status_id' => 11 ]
				);
			}
		}

		$over_count = DB::select( DB::raw('SELECT COUNT(item_id) as item_count FROM `items_statuses` WHERE `status_id` = 11'));
		$item_max = Dashboard::where('name', 'over_stocked')->first();
		$item_max->value = (string)$over_count[0]->item_count;
		$item_max->touch();
		if($item_max->save()){
			echo 'Over Stocked Updated';
		} else {
			echo 'Over Stocked Failed To Update';
		}
	}

	public function remove_over_under(){
		DB::table('items_statuses')->whereRaw('status_id IN(10,11)')->delete();
	}

	public function check_item_order(){
		echo 'Items on order and status';
	}


	public function closed_statuses(){
	    $jobs = Job::whereNotNull('closed_at')->with('statuses')->get();
	    $counters = Counter::whereNotNull('closed_at')->with('statuses')->get();
        foreach($jobs as $job){
            if (!$job->statuses->contains(13)) {
                $job->statuses()->attach([13]);
            }
        }
        foreach($counters as $counter){
            if (!$counter->statuses->contains(13)) {
                $counter->statuses()->attach([13]);
            }
        }
    }

    function roundUpTo($number, $increments) {
        $increments = 1 / $increments;
        return (ceil($number * $increments) / $increments);
    }

    public function check_orders(){
	    $orders = Order::orderBy('supplier_id', 'desc')->whereNull('closed_at')->get();

	    $total_on_order = 0;
	    $total_open = 0;

	    foreach($orders as $order){
	    	check_order($order->id);

	    	if($order->expected_at !== null){
			    $total_on_order = $total_on_order + 1;
		    } else {
			    $total_open = $total_open + 1;
		    }
	    }

	    $orders_open = Dashboard::where('name', 'orders_open')->first();
	    $orders_open->value = (string)$total_open;
	    $orders_open->touch();

	    $orders_awaiting = Dashboard::where('name', 'orders_awaiting')->first();
	    $orders_awaiting->value = (string)$total_on_order;
	    $orders_awaiting->touch();

	    if($orders_open->save() && $orders_awaiting->save()){
		    echo 'Over Stocked Updated: '.$total_on_order.' '.$total_open;
	    } else {
		    echo 'Over Stocked Failed To Update '.$total_on_order.' '.$total_open;
	    }
    }
}
