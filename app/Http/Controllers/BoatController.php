<?php

namespace App\Http\Controllers;

use App\DataTables\BoatDataTable;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\CreateBoatRequest;
use App\Http\Requests\UpdateBoatRequest;
use App\Repositories\BoatRepository;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Response;
use App\Models\Customer;
use App\Models\Boat;

class BoatController extends AppBaseController {
	/** @var  BoatRepository */
	private $boatRepository;

	public function __construct( BoatRepository $boatRepo ) {
		$this->boatRepository = $boatRepo;
	}

	/**
	 * Display a listing of the Boat.
	 *
	 * @param BoatDataTable $boatDataTable
	 *
	 * @return Response
	 */
	public function index( BoatDataTable $boatDataTable ) {
		return $boatDataTable->render( 'boats.index' );
	}

	/**
	 * Show the form for creating a new Boat.
	 *
	 * @return Response
	 */
	public function create() {
		$customers = Customer::orderby( 'name', 'ASC' )->with( 'address' )->get();

		return view( 'boats.create' )->with( 'customers', $customers );
	}

	/**
	 * Store a newly created Boat in storage.
	 *
	 * @param CreateBoatRequest $request
	 *
	 * @return Response
	 */
	public function store( CreateBoatRequest $request ) {
		$input = $request->all();

		$boat = $this->boatRepository->create( $input );

		Flash::success( 'Boat saved successfully.' );

		return redirect( route( 'boats.index' ) );
	}

	/**
	 * Display the specified Boat.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show( $id ) {
		$boat = $this->boatRepository->findWithoutFail( $id );

		if ( empty( $boat ) ) {
			Flash::error( 'Boat not found' );

			return redirect( route( 'boats.index' ) );
		}

		return view( 'boats.show' )->with( 'boat', $boat );
	}

	/**
	 * Show the form for editing the specified Boat.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit( $id ) {
		$boat      = $this->boatRepository->findWithoutFail( $id );
		$customers = Customer::orderby( 'name', 'ASC' )->with( 'address' )->get();

		if ( empty( $boat ) ) {
			Flash::error( 'Boat not found' );

			return redirect( route( 'boats.index' ) );
		}

		return view( 'boats.edit' )->with( 'boat', $boat )->with( 'customers', $customers );
	}

	/**
	 * Update the specified Boat in storage.
	 *
	 * @param  int              $id
	 * @param UpdateBoatRequest $request
	 *
	 * @return Response
	 */
	public function update( $id, UpdateBoatRequest $request ) {
		$boat = $this->boatRepository->findWithoutFail( $id );

		if ( empty( $boat ) ) {
			Flash::error( 'Boat not found' );

			return redirect( route( 'boats.index' ) );
		}

		$boat = $this->boatRepository->update( $request->all(), $id );

		Flash::success( 'Boat updated successfully.' );

		return redirect( route( 'boats.index' ) );
	}

	/**
	 * Remove the specified Boat from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy( $id ) {
		$boat = $this->boatRepository->findWithoutFail( $id );

		if ( empty( $boat ) ) {
			Flash::error( 'Boat not found' );

			return redirect( route( 'boats.index' ) );
		}

		$this->boatRepository->delete( $id );

		Flash::success( 'Boat deleted successfully.' );

		return redirect( route( 'boats.index' ) );
	}


	public function JsonLookup( Request $request ) {
		$boats = Boat::orderBy( 'name', 'ASC' )->with('customer');
		if ( $request->has( 'query' ) ) {
            $boats->where('name', 'like', '%' .$request->input( 'query' ) . '%' );
            $boats->orWhere('notes', 'like', '%' .$request->input( 'query' ) . '%' );
            $boats->orWhere('sage_ref', 'like', '%' . $request->input( 'query' ) . '%' );
            $boats->orWhere('number', 'like', '%' . $request->input( 'query' ) . '%' );
		}
		$results = $boats->get();
		$formatted_list = array();
		foreach ( $results as $boat ) {
			$name = '';
			if ( strlen( $boat->sage_ref ) > 0 ) {
				$name .= $boat->sage_ref . ' : ';
			}
			if ( strlen( $boat->number ) > 0 ) {
				$name .= $boat->number. ' : ';
			}
			if ( strlen( $boat->note ) > 0 ) {
				$name .= $boat->note. ' : ';
			}
			$cust_id = '';
			$cust_name = '';
			if(isset($boat->customer->id)){
				$cust_id = $boat->customer->id;
                $cust_name = $boat->customer->name;
			}
			$formatted_list[] = array(
				'id'   => $boat->id,
				'text' => $name.$boat->name,
				'customer_id' => $cust_id,
				'customer_name' => $cust_name
			);
		}

		return Response::json( array( 'results' => $formatted_list ) );
	}

}