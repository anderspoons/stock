<?php

namespace App\Http\Controllers;

use App\Models\JobMisc;
use App\Models\CounterMisc;
use Illuminate\Http\Request;
use Response;
use App\Models\CounterLabour;
use App\Models\CounterCarriage;
use App\Models\CounterCrane;
use App\Models\CounterItems;
use App\Models\JobCarriage;
use App\Models\JobCrane;
use App\Models\JobLabour;
use App\Models\JobItems;

//Purpose of this controller is to handle both the job and counter requests for adding, removing and updating the carriage, labour and crane costs against jobs and counter sales

class IssueAjax extends Controller
{


    // -------------------
    // Carriage Stuff
    // -------------------
    public function update_cost_carriage(Request $request)
    {
        $carriage = null;
        $type = strtolower($request->input('type'));

        switch ($type) {
            case 'job':
                $carriage = JobCarriage::where('id', $request->input('id'))->first();
                if ($carriage) {
                    $carriage->cost = $request->input('cost');
                    check_prices($type, $carriage->job_id);
                }
                break;

            case 'counter':
                $carriage = CounterCarriage::where('id', $request->input('id'))->first();
                if ($carriage) {
                    $carriage->cost = $request->input('cost');
                    check_prices($type, $carriage->counter_id);
                }
                break;
        }

        if ($carriage->save()) {
            return Response::json(array(
                'text'    => 'Cost updated successfully.'
            ), 200);
        }

        return Response::json(array(
            'text'    => 'Problem finding the carriage to update.'
        ), 500);
    }


    public function add_carriage(Request $request)
    {
        if (CheckPin($request->input('pin_id'), $request->input('pin'))) {
            $carriage = null;

            $type = strtolower($request->input('type'));
            switch ($type) {
                case 'job':
                    $carriage         = new JobCarriage();
                    $carriage->name   = $request->input('name');
                    $carriage->job_id = $request->input('id');
                    $carriage->pin_id = $request->input('pin_id');
                    $carriage->cost   = $request->input('cost');
                    break;

                case 'counter':
                    $carriage             = new CounterCarriage();
                    $carriage->name       = $request->input('name');
                    $carriage->counter_id = $request->input('id');
                    $carriage->pin_id     = $request->input('pin_id');
                    $carriage->cost       = $request->input('cost');
                    break;
            }
            if ($carriage->save()) {
                check_prices($type, $request->input('id'));
                return Response::json(array(
                    'text'    => 'Carriage added.'
                ), 200);
            }
        }

        return Response::json([
            'error' => [
                'exception' => 'Wrong pin code entered.',
            ]
        ], 500);
    }


    public function destroy_carriage(Request $request)
    {
        if (CheckPin($request->input('pin_id'), $request->input('pin'))) {
            $deleted = null;

            $type = strtolower($request->input('type'));
            switch ($type) {
                case 'job':
                    $deleted = JobCarriage::where('id', $request->input('id'))->first();
                    break;
                case 'counter':
                    $deleted = CounterCarriage::where('id', $request->input('id'))->first();
                    break;
            }

            if ($deleted->delete()) {
                switch ($type) {
                    case 'job':
                        check_prices($type, $deleted->job_id);
                        break;
                    case 'counter':
                        check_prices($type, $deleted->counter_id);
                        break;
                }
                return Response::json(array(
                    'text'    => 'Carriage Deleted.'
                ), 200);
            }
        }

        return Response::json([
            'error' => [
                'text' => 'Wrong pin code entered.',
            ]
        ], 500);
    }

    public function misc_desc(Request $request)
    {
        $crane = null;

        $type = strtolower($request->input('type'));
        switch ($type) {
            case 'job':
                $job_item = JobItems::where('id', $request->input('item_id'))->first();
                if ($job_item) {
                    $job_item->qty = $request->input('qty');
                }
                $misc = JobMisc::where('id', $request->input('id'))->first();
                if ($misc) {
                    $misc->name = $request->input('name');
                    $misc->description = $request->input('description');
                }
                break;

            case 'counter':
                $counter_item = CounterItems::where('id', $request->input('item_id'))->first();
                if ($counter_item) {
                    $counter_item->qty = $request->input('qty');
                }
                $misc = CounterMisc::where('id', $request->input('id'))->first();
                if ($misc) {
                    $misc->name = $request->input('name');
                    $misc->description = $request->input('description');
                }
                break;
        }

        if (($counter_item->save()&&$misc->save())||($misc->save()&&$job_item->save())) {
            return Response::json(array(
                'text'    => 'Details updated successfully.'
            ), 200);
        }

        return Response::json(array(
            'text'    => 'Problem finding the misc item to update.'
        ), 500);
    }




    // -------------------
    // Crane Stuff
    // -------------------

    // -------------------
    // Carriage Stuff
    // -------------------
    public function update_cost_crane(Request $request)
    {
        $crane = null;

        $type = strtolower($request->input('type'));
        switch ($type) {
            case 'job':
                $crane = JobCrane::where('id', $request->input('id'))->first();
                if ($crane) {
                    $crane->cost = $request->input('cost');
                    check_prices($type, $crane->job_id);
                }
                break;

            case 'counter':
                $crane = CounterCrane::where('id', $request->input('id'))->first();
                if ($crane) {
                    $crane->cost = $request->input('cost');
                    check_prices($type, $crane->counter_id);
                }
                break;
        }

        if ($crane->save()) {
            return Response::json(array(
                'text'    => 'Cost updated successfully.'
            ), 200);
        }

        return Response::json(array(
            'text'    => 'Problem finding the crane to update.'
        ), 500);
    }


    public function add_crane(Request $request)
    {
        if (CheckPin($request->input('pin_id'), $request->input('pin'))) {
            $crane = null;

            $type = strtolower($request->input('type'));
            switch ($type) {
                case 'job':
                    $crane         = new JobCrane();
                    $crane->name   = $request->input('name');
                    $crane->job_id = $request->input('id');
                    $crane->pin_id = $request->input('pin_id');
                    $crane->cost   = $request->input('cost');
                    break;

                case 'counter':
                    $crane             = new CounterCrane();
                    $crane->name       = $request->input('name');
                    $crane->counter_id = $request->input('id');
                    $crane->pin_id     = $request->input('pin_id');
                    $crane->cost       = $request->input('cost');
                    break;
            }
            if ($crane->save()) {
                check_prices($type, $request->input('id'));
                return Response::json(array(
                    'text'    => 'Crane Added.'
                ), 200);
            }
        }

        return Response::json([
            'error' => [
                'exception' => 'Wrong pin code entered.',
            ]
        ], 500);
    }


    public function destroy_crane(Request $request)
    {
        if (CheckPin($request->input('pin_id'), $request->input('pin'))) {
            $deleted = null;

            $type = strtolower($request->input('type'));
            switch ($type) {
                case 'job':
                    $deleted = JobCrane::where('id', $request->input('id'))->first();
                    break;

                case 'counter':
                    $deleted = CounterCrane::where('id', $request->input('id'))->first();
                    break;
            }

            if ($deleted->delete()) {
                switch ($type) {
                    case 'job':
                        check_prices($type, $deleted->job_id);
                        break;
                    case 'counter':
                        check_prices($type, $deleted->counter_id);
                        break;
                }
                return Response::json(array(
                    'text'    => 'Crane deleted.'
                ), 200);
            }
        }

        return Response::json([
            'error' => [
                'exception' => 'Wrong pin code entered.',
            ]
        ], 500);
    }





    // -------------------
    // Labour Stuff
    // -------------------
    public function update_cost_labour(Request $request)
    {
        $labour = null;

            $type = strtolower($request->input('type'));
            switch ($type) {
            case 'job':
                $labour = JobLabour::where('id', $request->input('id'))->first();
                if ($labour) {
                    $labour->cost = $request->input('cost');
                }
                break;

            case 'counter':
                $labour = CounterLabour::where('id', $request->input('id'))->first();
                if ($labour) {
                    $labour->cost = $request->input('cost');
                }
                break;
        }
        if ($labour->save()) {
            check_prices($type, $request->input('id'));
            return Response::json(array(
                'success' => 'true',
                'text'    => 'Cost updated successfully.'
            ));
        }
        return Response::json(array(
            'success' => false,
            'text'    => 'Problem finding the labour to update.'
        ));
    }


    public function add_labour(Request $request)
    {
        if (CheckPin($request->input('pin_id'), $request->input('pin'))) {
            $labour = null;


            $type = strtolower($request->input('type'));
            switch ($type) {
                case 'job':
                    $labour         = new JobLabour();
                    $labour->name   = $request->input('name');
                    $labour->job_id = $request->input('id');
                    $labour->pin_id = $request->input('pin_id');
                    $labour->cost   = $request->input('cost');
                    break;

                case 'counter':
                    $labour             = new CounterLabour();
                    $labour->name       = $request->input('name');
                    $labour->counter_id = $request->input('id');
                    $labour->pin_id     = $request->input('pin_id');
                    $labour->cost       = $request->input('cost');
                    break;
            }
            if ($labour->save()) {
                check_prices($type, $request->input('id'));
                return Response::json(array(
                    'text'    => 'Labour Added.'
                ), 200);
            }
        }

        return Response::json([
            'error' => [
                'exception' => 'Wrong pin code entered.',
            ]
        ], 500);
    }


    public function destroy_labour(Request $request)
    {
        if (CheckPin($request->input('pin_id'), $request->input('pin'))) {
            $deleted = null;

            $type = strtolower($request->input('type'));
            switch ($type) {
                case 'job':
                    $deleted = JobLabour::where('id', $request->input('id'))->first();
                    break;

                case 'counter':
                    $deleted = CounterLabour::where('id', $request->input('id'))->first();
                    break;
            }

            if ($deleted->delete()) {
                check_prices($type, $request->input('id'));
                return Response::json(array(
                    'text'    => 'Labour Deleted.'
                ), 200);
            }
        }

        return Response::json([
            'error' => [
                'exception' => 'Wrong pin code entered.',
            ]
        ], 500);
    }


}
