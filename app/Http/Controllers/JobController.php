<?php

namespace App\Http\Controllers;

use App\Models\Counter;
use App\Models\Job;
use App\Models\JobItems;
use App\Models\Item;
use App\Models\JobMisc;
use App\Models\JobSize;
use Session;
use App\Models\Status;
use Illuminate\Http\Request;
use App\Models\Pincodes;
use App\Http\Requests\CreateJobRequest;
use App\Http\Requests\UpdateJobRequest;
use App\Repositories\JobRepository;
use App\DataTables\JobLabourDataTable;
use App\DataTables\JobCarriageDataTable;
use App\DataTables\JobCraneDataTable;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Response;
use App\Models\Customer;
use App\Models\Address;
use App\Models\Boat;
use App\Models\Element;
use Yajra\Datatables\Datatables;
use App\DataTables\JobDataTable;
use App\DataTables\JobIssueDataTable;
use Excel;
use Storage;

class JobController extends AppBaseController
{
    /** @var  JobRepository */
    private $jobRepository;

    public function __construct(JobRepository $jobRepo)
    {
        $this->jobRepository = $jobRepo;
    }

    /**
     * Display a listing of the Job.
     *
     * @param JobDataTable $jobDataTable
     *
     * @return Response
     */
    public function index(JobDataTable $jobDataTable)
    {
        $pincodes  = Pincodes::where('active', '1')->get();
        $statuses  = Status::where('job', '1')->get();
        $customers = Customer::orderby('name', 'ASC')->get();
        $boats     = Boat::orderby('name', 'ASC')->get();

        return $jobDataTable->render('jobs.index', array(
            'boats'     => $boats,
            'customers' => $customers,
            'pincodes'  => $pincodes,
            'statuses'  => $statuses
        ));
    }

    /**
     * Show the form for creating a new Job.
     *
     * @return Response
     */
    public function create()
    {
        $customers = Customer::orderBy('name', 'desc')->get();
        $boats     = Boat::orderBy('name', 'desc')->get();
        $billed    = false;

        return view('jobs.create')->with('customers', $customers)->with('boats', $boats)->with('billed', $billed);
    }

    /**
     * Store a newly created Job in storage.
     *
     * @param CreateJobRequest $request
     *
     * @return Response
     */
    public function store(CreateJobRequest $request)
    {
        $input = $request->all();
        $job   = $this->jobRepository->create($input);

        if ($request->has('element_id')) {
            $job->elements()->sync($request->input('element_id'));
        }

        if ($request->has('status_id')) {
            $job->statuses()->sync($request->input('status_id'));
        }

        Flash::success('Job saved successfully.');

        return redirect(route('jobs.index'));
    }

    /**
     * Display the specified Job.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $job          = $this->jobRepository->findWithoutFail($id);
        $customer     = Customer::where('id', $job->customer_id)->get();
        $boat         = Boat::findOrFail($job->boat_id);
        $cust_address = Address::where('id', $customer[0]->address_id)->get();
        if (empty($job)) {
            Flash::error('Job not found');

            return redirect(route('jobs.index'));
        }

        return view('jobs.show')
            ->with('job', $job)
            ->with('customer', $customer)
            ->with('boat', $boat)
            ->with('cust_address', $cust_address);
    }

    /**
     * Show the form for editing the specified Job.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $job       = Job::where('id', $id)->with('statuses', 'job_items', 'labour', 'carriage', 'crane')->first();
        $customers = Customer::orderBy('name', 'desc')->get();
        $boats     = Boat::orderBy('name', 'desc')->get();
        $pins      = Pincodes::get();
        $billed    = false;
        $closed    = false;
        $delete    = true;

        if (isset($job->statuses)) {
            foreach ($job->statuses as $status) {
                if ($status->id == 9) {
                    $billed = true;
                }
                if ($status->id == 13) {
                    $closed = true;
                }
            }
        }

        if (isset($job->job_items)) {
            foreach ($job->job_items as $item) {
                $delete = false;
            }
        }

        if (isset($job->carriage)) {
            foreach ($job->carriage as $carriage) {
                $delete = false;
            }
        }

        if (isset($job->labour)) {
            foreach ($job->labour as $labour) {
                $delete = false;
            }
        }

        if (isset($job->crane)) {
            foreach ($job->crane as $crane) {
                $delete = false;
            }
        }


        if (empty($job)) {
            Flash::error('Job not found');

            return redirect(route('jobs.index'));
        }

        return view('jobs.edit')
            ->with('customers', $customers)
            ->with('boats', $boats)
            ->with('job', $job)
            ->with('billed', $billed)
            ->with('closed', $closed)
            ->with('delete', $delete)
            ->with('pins', $pins);
    }

    /**
     * Update the specified Job in storage.
     *
     * @param  int             $id
     * @param UpdateJobRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateJobRequest $request)
    {
        $job = $this->jobRepository->findWithoutFail($id);
        if (empty($job)) {
            Flash::error('Job not found');

            return redirect(route('jobs.index'));
        }
        $job = $this->jobRepository->update($request->all(), $id);

        if ($request->has('element_id')) {
            $job->elements()->sync($request->input('element_id'));
        }

        if ($request->has('status_id')) {
            $job->statuses()->sync($request->input('status_id'));
        }

        check_prices('job', $id);

        Flash::success('Job updated successfully.');

        return redirect(route('jobs.index'));
    }





    public function loopElements($job, $request)
    {
        $job->elements()->detach();
        foreach ($request->input('element_id') as $element) {
            if (is_numeric($element)) {
                if ($ele_check = Element::find($element)->count() > 0) {
                    $this->attachElement($job, $element);
                }
            } else {
                $ele_check = Element::where('name', $element)->get();
                if ($ele_check->count() > 0) {
                    $this->attachElement($job, $ele_check[0]->id);
                } else {
                    $new       = new Element();
                    $new->name = $element;
                    $new->save();
                    $this->attachElement($job, $new->id);
                }
            }
        }
    }

    function attachElement($job, $id)
    {
        $job->elements()->attach($id);
    }

    public function loopStatuses($job, $request)
    {
        foreach ($job->statuses() as $status) {
            if ($status->auto_status == 0) {
                $status->detach();
            }
        }
        foreach ($request->input('status_id') as $status) {
            if (is_numeric($status)) {
                if ($ele_check = Element::find($status)->count() > 0) {
                    $this->attachStatus($job, $status);
                }
            } else {
                $ele_check = Status::where('name', $status)->get();
                if ($ele_check->count() > 0) {
                    $this->attachStatus($job, $ele_check[0]->id);
                } else {
                    $new       = new Status();
                    $new->name = $status;
                    $new->save();
                    $this->attachStatus($job, $new->id);
                }
            }
        }
    }

    function attachStatus($job, $id)
    {
        $job->statuses()->attach($id);
    }

    /**
     * Remove the specified Job from storage.
     *
     * @param  Request $request
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
        // if ($request->has('job_id') && $request->has('pin')) {
        if ($request->has('job_id')) {
            $job = $this->jobRepository->findWithoutFail($request->input('job_id'));
            if (empty($job)) {
                Flash::error('Job not found');

                return redirect(route('jobs.index'));
            }
            // $code = Pincodes::where('id', $request->input('pin_id'))->limit(1)->get();
            // if ($code[0]->code == $request->input('pin')) {
                // Session::set('pin_id', $request->input('pin_id'));
                $this->jobRepository->delete($request->input('job_id'));

                return Response::json(array(
                    'success' => true,
                    'data'    => 'Job deleted successfully, Redirecting.'
                ));
            // } else {
            //     return Response::json([
            //         'error' => [
            //             'exception' => 'Wrong pin code entered.',
            //         ]
            //     ], 500);
            // }
        } else {
            return Response::json([
                'error' => [
                    'exception' => 'Missing job id.',
                ]
            ], 500);
        }
    }

    public function remove_billed(Request $request)
    {
        // if ($request->has('job_id') && $request->has('pin') && $request->has('reason')) {
        if ($request->has('job_id')) {
            $job = Job::where('id', $request->input('job_id'))->with('statuses')->first();
            if (empty($job)) {
                Flash::error('Job Sale not found');

                return redirect(route('jobs.index'));
            }
            // $code = Pincodes::where('id', $request->input('pin_id'))->limit(1)->get();
            // if ($code[0]->code == $request->input('pin')) {

                $job->statuses()->detach('9');
                $job->statuses()->attach('13');

                if ($job->save()) {
                    // Session::set('pin_id', $request->input('pin_id'));
                    // Session::set('reason', $request->input('reason'));

                    return Response::json(array(
                        'success' => true,
                        'data'    => 'Job removed billed status successfully, Redirecting.'
                    ));
                }
            // }
        } else {
            return Response::json([
                'error' => [
                    'exception' => 'Missing pin code or reason.',
                ]
            ], 500);
        }
    }

    public function remove_close(Request $request)
    {
        $job = Job::where('id', $request->input('job_id'))->with('statuses')->first();
        if (empty($job)) {
            Flash::error('Job Sale not found');
            return redirect(route('jobs.index'));
        } else {
            $job->statuses()->detach('13');
            $job->closed_at = null;
            if ($job->save()) {
                return Response::json(array(
                    'success' => true,
                    'data'    => 'Job removed closed status successfully, reloading.'
                ));                
            }
        }
        // if ($request->has('job_id') && $request->has('pin') && $request->has('reason')) {
        //     $job = Job::where('id', $request->input('job_id'))->with('statuses')->first();
        //     if (empty($job)) {
        //         Flash::error('Job Sale not found');

        //         return redirect(route('jobs.index'));
        //     }
        //     $code = Pincodes::where('id', $request->input('pin_id'))->limit(1)->get();
        //     if ($code[0]->code == $request->input('pin')) {
        //         $job->statuses()->detach('13');
        //         $job->closed_at = null;
        //         if ($job->save()) {
        //             Session::set('pin_id', $request->input('pin_id'));
        //             Session::set('reason', $request->input('reason'));

        //             return Response::json(array(
        //                 'success' => true,
        //                 'data'    => 'Job removed closed status successfully, Redirecting.'
        //             ));
        //         }
        //     }
        // } else {
        //     return Response::json([
        //         'error' => [
        //             'exception' => 'Missing pin code or reason.',
        //         ]
        //     ], 500);
        // }
    }

    public function close(Request $request)
    {
        if ($request->has('job_id')/* && $request->has('pin')*/) {

            $success = [];
            $errors = [];
            foreach (explode(",",$request->input('job_id')) as $job_id) {



                $job = Job::where('id', $job_id)->with('statuses')->first();

                if (empty($job)) {
                    Flash::error('Job not found');
                    return redirect(route('jobs.index'));
                }

                // $code = Pincodes::where('id', $request->input('pin_id'))->limit(1)->get();
                // if ($code[0]->code == $request->input('pin')) {
                    $job->closed_at = date("Y-m-d H:i:s");

                    if (!$job->statuses->contains(13)) {
                        $job->statuses()->attach([13]);
                    }

                    check_prices('job', $request->input('job_id'));

                    if ($job->save()) {
                        // Session::set('pin_id', $request->input('pin_id'));
                        $success[] = $job_id;
                    } else {
                        $errors[] = $job_id;
                    }
                // } else {
                //     return Response::json([
                //         'error' => [
                //             'exception' => 'Wrong pin code entered.',
                //         ]
                //     ], 500);
                // }
            }
            if (count($errors) > 0) {
                return Response::json(array(
                    'success' => true,
                    'data'    => 'Some jobs could not be saved.'
                ));
            } else {
                return Response::json(array(
                    'success' => true,
                    'data'    => 'Job(s) closed successfully, Redirecting.'
                ));                
            }
        } else {
            return Response::json([
                'error' => [
                    'exception' => 'Missing job id.',
                ]
            ], 500);
        }
    }

    public function add_billed(Request $request)
    {
        // if ($request->has('job_id') && $request->has('pin')) {
        if ($request->has('job_id')) {
            $job = Job::where('id', $request->input('job_id'))->with('statuses')->first();
            if (empty($job)) {
                Flash::error('Job not found');

                return redirect(route('jobs.index'));
            }
            // $code = Pincodes::where('id', $request->input('pin_id'))->limit(1)->get();
            // if ($code[0]->code == $request->input('pin')) {

                $job->statuses()->detach('13');
                $job->statuses()->attach('9');

                check_prices('job', $request->has('job_id'));

                if ($job->save()) {
                    // Session::set('pin_id', $request->input('pin_id'));

                    return Response::json(array(
                        'success' => true,
                        'data'    => 'Job marked as billed successfully, Redirecting.'
                    ));
                } else {
                    return Response::json(array(
                        'success' => true,
                        'data'    => 'Job could not be saved.'
                    ));
                }
            // } else {
            //     return Response::json([
            //         'error' => [
            //             'exception' => 'Wrong pin code entered.',
            //         ]
            //     ], 500);
            // }
        } else {
            return Response::json([
                'error' => [
                    'exception' => 'Missing job id.',
                ]
            ], 500);
        }
    }

    public function jobElements(Request $request, $job)
    {
        $elements = array();
        foreach ($job->elements as $element) {

        }
    }

    public function invoice($id)
    {
        $job      = Job::where('id', $id)->with('customer')->with('customer.address')->first();
        $customer = Customer::where('id', $job->customer_id)->with('address')->first();
        $boat     = Boat::where('id', $job->boat_id)->first();

        if ($job && $customer) {
            $list      = array();
            $total_net = 0;

            foreach ($job->labour()->get() as $labour) {
                $cost   = number_format((float)$labour->cost, 2, '.', '');
                $vat    = number_format((float)($cost * 0.2), 2, '.', '');
                $list[] = array(
                    'type'      => 'labour',
                    'qty'       => number_format((float)1, 2, '.', ''),
                    'details'   => $labour->name,
                    'unit_cost' => $cost,
                    'net_cost'  => $cost,
                    'vat_rate'  => '20',
                    'vat'       => $vat,
                );

                $total_net = $total_net + $labour->cost;
            }

            foreach ($job->crane()->get() as $crane) {
                $cost   = number_format((float)$crane->cost, 2, '.', '');
                $vat    = number_format((float)($cost * 0.2), 2, '.', '');
                $list[] = array(
                    'type'      => 'crane',
                    'qty'       => number_format((float)1, 2, '.', ''),
                    'details'   => $crane->name,
                    'unit_cost' => $cost,
                    'net_cost'  => $cost,
                    'vat_rate'  => '20',
                    'vat'       => $vat,
                );

                $total_net = $total_net + $crane->cost;
            }

            foreach ($job->items()->orderBy('created_at', 'ASC')->get() as $item) {
                $name = '';
                if ($item->item_id == 0) {
                    $misc = JobMisc::where('job_item_id', $item->id)->first();
                    if (strlen($misc->name) > 0) {
                        $name .= $misc->name . ' ';
                    }
                    if (strlen($misc->description) > 0) {
                        $name .= $misc->description;
                    }
                } else {
                    $itm = Item::where('id', $item->item_id)->with('sizes')->first();
                    if(isset($itm->sizes)){
                        foreach($itm->sizes as $size){
                            $name .= $size->name.' x ';
                        }
                        $name = rtrim($name, 'x ');
                        $name .= ' ';
                    }
                    if($itm) $name .= $itm->description;
                }

                if ($item->line_unit_cost != null) {
                    $unit_cost = number_format((float)$item->line_unit_cost, 2, '.', '');
                } else {
                    $unit_cost = number_format((float)($item->line_cost / $item->qty), 2, '.', '');
                }

                $net_cost = number_format((float)$item->line_cost, 2, '.', '');
                $vat      = number_format((float)($net_cost * 0.2), 2, '.', '');

                $list[] = array(
                    'type'      => 'item',
                    'qty'       => number_format((float)$item->qty, 2, '.', ''),
                    'details'   => $name,
                    'unit_cost' => $unit_cost,
                    'net_cost'  => $net_cost,
                    'vat_rate'  => '20',
                    'vat'       => $vat,
                );

                $total_net = $total_net + $item->line_cost;
            }

            foreach ($job->carriage()->get() as $carriage) {
                $cost      = number_format((float)$carriage->cost, 2, '.', '');
                $vat       = number_format((float)($cost * 0.2), 2, '.', '');
                $list[]    = array(
                    'type'      => 'carriage',
                    'qty'       => number_format((float)1, 2, '.', ''),
                    'details'   => $carriage->name,
                    'unit_cost' => $cost,
                    'net_cost'  => $cost,
                    'vat_rate'  => '20',
                    'vat'       => $vat,
                );
                $total_net = $total_net + $carriage->cost;
            }
        }

        $name = 'job'.time();

        $file = Excel::create($name, function ($excel) use ($job, $customer, $boat, $list) {
            $excel->sheet('ItemList', function ($sheet) use ($job, $customer, $boat, $list) {
                $sheet->loadView('jobs.export.excel-invoice', array(
                    'list'     => $list,
                    'job'      => $job,
                    'customer' => $customer,
                    'boat'     => $boat
                ));
                $sheet->setAutoSize(true);

                $sheet->setWidth('A', 38);
                $sheet->setWidth('B', 8);
                $sheet->setWidth('C', 20);
            });
        })->store('xlsx', storage_path('app/tmp'), false, true);

        Excel::load(storage_path('/app/tmp/'.$file->filename.'.'.$file->ext), function ($file) {
            $sheet = $file->getExcel()->getActiveSheet();

            $pageMargins = $sheet->getPageMargins();
            $pageMargins->setTop(3.4 / 2.54);
            $pageMargins->setBottom(1.9 / 2.54);
            $pageMargins->setLeft(8 / 2.54);
            $pageMargins->setRight(0.5 / 2.54);

            $sheet->getPageSetup()->setScale(91);
            $sheet->getStyle('C1')->getAlignment()->setHorizontal('right');

            $highestRow = $sheet->getHighestRow();
            $last = $highestRow + 1;
            $sheet->getStyle('C7:C'.$last)->getNumberFormat()->setFormatCode('"£"#,##0.00_-');

            $sheet->setCellValue('C'.$last, '=SUM(C7:C'.$highestRow.')');
        })->download('xlsx');

    }

    public function issue(JobIssueDataTable $dataTable, $id)
    {
        $pincodes = Pincodes::all();
        //TODO check for a status on each and make sure its not got billed or closed.
        $counters = Counter::whereRaw('closed_at is null')->orderby('ref', 'ASC')->get();
        $job = Job::where('id', '=', $id)->with('boat')->with('customer')->first();
        $billed    = false;
        $closed    = false;

        if (isset($job->statuses)) {
            foreach ($job->statuses as $status) {
                if ($status->id == 9) {
                    $billed = true;
                }
                if ($status->id == 13) {
                    $closed = true;
                }
            }
        }

        return $dataTable->forJob($id)->render('jobs.issues', compact('pincodes', 'counters', 'job', 'closed', 'billed'));
    }

    public function labour(JobLabourDataTable $dataTable, $id)
    {
        $pins = Pincodes::where('active', '1')->get();
        $job  = Job::where('id', $id)->with('statuses')->with('boat')->with('customer')->first();

        $billed = false;

        if (isset($job->statuses)) {
            foreach ($job->statuses as $status) {
                if ($status->id == 9) {
                    $billed = true;
                }
            }
        }

        return $dataTable->forJob($id)->render('jobs.labour', array(
            'pins'   => $pins,
            'billed' => $billed,
            'job'    => $job
        ));
    }

    public function carriage(JobCarriageDataTable $dataTable, $id)
    {
        $pins = Pincodes::where('active', '1')->get();
        $job  = Job::where('id', $id)->with('statuses')->with('boat')->with('customer')->first();

        $billed = false;

        if (isset($job->statuses)) {
            foreach ($job->statuses as $status) {
                if ($status->id == 9) {
                    $billed = true;
                }
            }
        }

        return $dataTable->forJob($id)->render('jobs.carriage', array(
                'pins'   => $pins,
                'billed' => $billed,
                'job'    => $job
            )
        );
    }

    public function crane(JobCraneDataTable $dataTable, $id)
    {
        $pins = Pincodes::where('active', '1')->get();
        $job  = Job::where('id', $id)->with('statuses')->with('boat')->with('customer')->first();

        $billed = false;
        if (isset($job->statuses)) {
            foreach ($job->statuses as $status) {
                if ($status->id == 9) {
                    $billed = true;
                }
            }
        }

        return $dataTable->forJob($id)->render('jobs.crane', array(
                'pins'   => $pins,
                'billed' => $billed,
                'job'    => $job
            )
        );
    }


    public function JsonLookup( Request $request ) {
        //TODO lookup jobs by customer, boat, ref, customer PO, note
        //TODO populate the PO feild in an order when this job is selected if the customer has a PO set for that job
        $jobs = Job::orderBy( 'created_at', 'DESC' )->with('customer', 'boat');
        $results = $jobs->get();
        $formatted_list = array();
        foreach ( $results as $job ) {
            $name = '';
            if ( strlen( $job->boat ) > 0 ) {
                $name .= $job->boat->name;
            }
            if ( strlen( $job->customer ) > 0 ) {
                if(strlen($name) > 0){
                    $name .= ' : ';
                }
                $name .= $job->customer->name;
            }
            if(strlen($name) > 0 && strlen($job->note) > 0){
                $name .= ' : '.$job->note;
            }
            $formatted_list[] = array(
                'id'   => $job->id,
                'text' => $name,
            );
        }
        return Response::json( array( 'results' => $formatted_list ) );
    }

    public function JsonLookupYard( Request $request ) {
        $jobs = Job::orderBy( 'created_at', 'DESC' )->with('customer', 'boat');
        $results = $jobs->get();
        $formatted_list = array();
        foreach ( $results as $job ) {
        	if(isset($job->customer)){
        		if(strpos($job->customer->name, 'YARD')){
			        $name = '';
			        if ( strlen( $job->boat ) > 0 ) {
				        $name .= $job->boat->number;
			        }
			        if ( strlen( $job->customer ) > 0 ) {
				        if(strlen($name) > 0){
					        $name .= ' : ';
				        }
				        $name .= $job->customer->name;
			        }
			        if(strlen($name) > 0 && strlen($job->note) > 0){
				        $name .= ' : '.$job->note;
			        }
			        $formatted_list[] = array(
				        'id'   => $job->id,
				        'text' => $name,
			        );
		        }
	        }

        }
        return Response::json( array( 'results' => $formatted_list ) );
    }

}
