<?php

namespace App\Http\Controllers;

use App\DataTables\SexDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateSexRequest;
use App\Http\Requests\UpdateSexRequest;
use App\Repositories\SexRepository;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Response;

class SexController extends AppBaseController
{
    /** @var  SexRepository */
    private $sexRepository;

    public function __construct(SexRepository $sexRepo)
    {
        $this->sexRepository = $sexRepo;
    }

    /**
     * Display a listing of the Sex.
     *
     * @param SexDataTable $sexDataTable
     * @return Response
     */
    public function index(SexDataTable $sexDataTable)
    {
        return $sexDataTable->render('sexes.index');
    }

    /**
     * Show the form for creating a new Sex.
     *
     * @return Response
     */
    public function create()
    {
        return view('sexes.create');
    }

    /**
     * Store a newly created Sex in storage.
     *
     * @param CreateSexRequest $request
     *
     * @return Response
     */
    public function store(CreateSexRequest $request)
    {
        $input = $request->all();

        $sex = $this->sexRepository->create($input);

        Flash::success('Sex saved successfully.');

        return redirect(route('sexes.index'));
    }

    /**
     * Display the specified Sex.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $sex = $this->sexRepository->findWithoutFail($id);

        if (empty($sex)) {
            Flash::error('Sex not found');

            return redirect(route('sexes.index'));
        }

        return view('sexes.show')->with('sex', $sex);
    }

    /**
     * Show the form for editing the specified Sex.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $sex = $this->sexRepository->findWithoutFail($id);

        if (empty($sex)) {
            Flash::error('Sex not found');

            return redirect(route('sexes.index'));
        }

        return view('sexes.edit')->with('sex', $sex);
    }

    /**
     * Update the specified Sex in storage.
     *
     * @param  int              $id
     * @param UpdateSexRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSexRequest $request)
    {
        $sex = $this->sexRepository->findWithoutFail($id);

        if (empty($sex)) {
            Flash::error('Sex not found');

            return redirect(route('sexes.index'));
        }

        $sex = $this->sexRepository->update($request->all(), $id);

        Flash::success('Sex updated successfully.');

        return redirect(route('sexes.index'));
    }

    /**
     * Remove the specified Sex from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $sex = $this->sexRepository->findWithoutFail($id);

        if (empty($sex)) {
            Flash::error('Sex not found');

            return redirect(route('sexes.index'));
        }

        $this->sexRepository->delete($id);

        Flash::success('Sex deleted successfully.');

        return redirect(route('sexes.index'));
    }
}
