<?php

namespace App\Http\Controllers;

use App\DataTables\StoreDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateStoreRequest;
use App\Http\Requests\UpdateStoreRequest;
use App\Repositories\StoreRepository;
use Illuminate\Http\Request;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Response;
use App\Models\Store;
use App\Models\Address;

class StoreController extends AppBaseController
{
    /** @var  StoreRepository */
    private $storeRepository;

    public function __construct(StoreRepository $storeRepo)
    {
        $this->storeRepository = $storeRepo;
    }

    /**
     * Display a listing of the Store.
     *
     * @param StoreDataTable $storeDataTable
     * @return Response
     */
    public function index(StoreDataTable $storeDataTable)
    {
        return $storeDataTable->render('stores.index');
    }

    /**
     * Show the form for creating a new Store.
     *
     * @return Response
     */
    public function create()
    {
        return view('stores.create');
    }

    /**
     * Store a newly created Store in storage.
     *
     * @param CreateStoreRequest $request
     *
     * @return Response
     */
    public function store(CreateStoreRequest $request)
    {
        $input = $request->all();

        $store = $this->storeRepository->create($input);

        if(!empty($request->input('address_id')) && ($request->input('address_id') != $store->address_id)) {
            $store->address_id = $request->input('address_id');
            $store->save();
        } else {
            if(!empty($request->input('line_1')) || !empty($request->input('line_2')) || !empty($request->input('town')) || !empty($request->input('county')) || !empty($request->input('postcode'))){
                //Need somthing from the above to distinguish the address
                $address = new Address;
                if(!empty($request->input('line_1'))){
                    $address->line_1 = $request->input('line_1');
                }
                if(!empty($request->input('line_2'))) {
                    $address->line_2 = $request->input( 'line_2' );
                }
                if(!empty($request->input('town'))){
                    $address->town = $request->input('town');
                }
                if(!empty($request->input('county'))) {
                    $address->county = $request->input( 'county' );
                }
                if(!empty($request->input('postcode'))) {
                    $address->postcode = $request->input( 'postcode' );
                }
                if(!empty($request->input('lng'))) {
                    $address->lng = $request->input( 'lng' );
                }
                if(!empty($request->input('lat'))) {
                    $address->lat = $request->input( 'lat' );
                }
                $address->save();
                $store->address_id = $address->id;
                $store->save();
            }
        }

        Flash::success('Store saved successfully.');

        return redirect(route('stores.index'));
    }

    /**
     * Display the specified Store.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $store = $this->storeRepository->findWithoutFail($id);

        if (empty($store)) {
            Flash::error('Store not found');

            return redirect(route('stores.index'));
        }

        return view('stores.show')->with('store', $store);
    }

    /**
     * Show the form for editing the specified Store.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $store = $this->storeRepository->findWithoutFail($id);
        $address = Address::find($store->address_id);

        if (empty($store)) {
            Flash::error('Store not found');
            return redirect(route('stores.index'));
        }

        return view('stores.edit')->with('store', $store)->with('address', $address);
    }

    /**
     * Update the specified Store in storage.
     *
     * @param  int              $id
     * @param UpdateStoreRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStoreRequest $request)
    {
        $store = $this->storeRepository->findWithoutFail($id);

        if (empty($store)) {
            Flash::error('Store not found');

            return redirect(route('stores.index'));
        }

        if(!empty($request->input('address_id')) && ($request->input('address_id') != $store->address_id)) {
            $store->address_id = $request->input('address_id');
            $store->save();
        } else {
            if(!empty($request->input('line_1')) || !empty($request->input('line_2')) || !empty($request->input('town')) || !empty($request->input('county')) || !empty($request->input('postcode'))){
                //Need somthing from the above to distinguish the address
                $address = new Address;
                if(!empty($request->input('line_1'))){
                    $address->line_1 = $request->input('line_1');
                }
                if(!empty($request->input('line_2'))) {
                    $address->line_2 = $request->input( 'line_2' );
                }
                if(!empty($request->input('town'))){
                    $address->town = $request->input('town');
                }
                if(!empty($request->input('county'))) {
                    $address->county = $request->input( 'county' );
                }
                if(!empty($request->input('postcode'))) {
                    $address->postcode = $request->input( 'postcode' );
                }
                if(!empty($request->input('lng'))) {
                    $address->lng = $request->input( 'lng' );
                }
                if(!empty($request->input('lat'))) {
                    $address->lat = $request->input( 'lat' );
                }
                $address->save();
                $store->address_id = $address->id;
                $store->save();
            }
        }

        $store = $this->storeRepository->update($request->all(), $id);

        Flash::success('Store updated successfully.');

        return redirect(route('stores.index'));
    }

    /**
     * Remove the specified Store from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $store = $this->storeRepository->findWithoutFail($id);

        if (empty($store)) {
            Flash::error('Store not found');

            return redirect(route('stores.index'));
        }

        $this->storeRepository->delete($id);

        Flash::success('Store deleted successfully.');

        return redirect(route('stores.index'));
    }





	public function JsonLookup(Request $request) {
		$stores = Store::orderBy('name', 'ASC');
		if($request->has('q')){
			foreach($request->input('q') as $val){
				$stores->where('name', 'like', '%'.$val.'%');
			}
		}
		$results = $stores->get();
		$formatted_list = array();
		foreach($results as $store){
			$formatted_list[] = array(
				'id' => $store->id,
				'text' =>  $store->name
			);
		}
		return Response::json(array('results' => $formatted_list));
	}
}
