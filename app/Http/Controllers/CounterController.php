<?php

namespace App\Http\Controllers;

use App\DataTables\CounterDataTable;
use App\DataTables\CounterIssueDataTable;
use App\DataTables\CounterLabourDataTable;
use App\DataTables\CounterCarriageDataTable;
use App\DataTables\CounterCraneDataTable;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\CreateCounterRequest;
use App\Http\Requests\UpdateCounterRequest;
use App\Models\Counter;
use App\Models\CounterMisc;
use App\Models\JobItems;
use App\Models\Status;
use App\Repositories\CounterRepository;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Response;
use App\Models\Customer;
use App\Models\CounterLabour;
use App\Models\Pincodes;
use App\Models\Job;
use App\Models\Item;
use App\Models\Boat;
use PDF;
use Session;
use Mail;
use DB;


class CounterController extends AppBaseController
{
    /** @var  CounterRepository */
    private $counterRepository;

    public function __construct(CounterRepository $counterRepo)
    {
        $this->counterRepository = $counterRepo;
    }

    /**
     * Display a listing of the Counter.
     *
     * @param CounterDataTable $counterDataTable
     *
     * @return Response
     */
    public function index(CounterDataTable $counterDataTable)
    {
        $pincodes  = Pincodes::where('active', '1')->get();
        $statuses  = Status::where('counter', '1')->get();
        $customers = Customer::orderby('name', 'ASC')->get();
        $boats     = Boat::orderby('name', 'ASC')->get();

        return $counterDataTable->render('counters.index', array(
            'pincodes'  => $pincodes,
            'statuses'  => $statuses,
            'customers' => $customers,
            'boats'     => $boats
        ));
    }

    /**
     * Show the form for creating a new Counter.
     *
     * @return Response
     */
    public function create()
    {
        $customers = Customer::get();#
        $boats     = Boat::all();
        $billed    = false;

        return view('counters.create')->with('customers', $customers)->with('boats', $boats)->with('billed', $billed);
    }

    /**
     * Store a newly created Counter in storage.
     *
     * @param CreateCounterRequest $request
     *
     * @return Response
     */
    public function store(CreateCounterRequest $request)
    {
        $input   = $request->all();
        $counter = $this->counterRepository->create($input);
        if ($request->has('status_id')) {
            $counter->statuses()->sync($request->input('status_id'));
        }
        Flash::success('Counter saved successfully.');

        return redirect(route('counters.index'));
    }

    /**
     * Display the specified Counter.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $counter = $this->counterRepository->findWithoutFail($id);
        if (empty($counter)) {
            Flash::error('Counter not found');

            return redirect(route('counters.index'));
        }

        return view('counters.show')->with('counter', $counter);
    }

    /**
     * Show the form for editing the specified Counter.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $counter = Counter::where('id', $id)->with('statuses', 'items', 'labour', 'carriage', 'crane')->first();
        $pins    = Pincodes::where('active', '1')->get();
        $billed  = false;
        $closed = false;
        $delete  = true;


        if (isset($counter->statuses)) {
            foreach ($counter->statuses as $status) {
                if ($status->id == 9) {
                    $billed = true;
                }
                if ($status->id == 13) {
                    $closed = true;
                }
            }
        }


        if (isset($counter->items)) {
            foreach ($counter->items as $item) {
                $delete = false;
            }
        }

        if (isset($counter->carriage)) {
            foreach ($counter->carriage as $carriage) {
                $delete = false;
            }
        }

        if (isset($counter->labour)) {
            foreach ($counter->labour as $labour) {
                $delete = false;
            }
        }

        if (isset($counter->crane)) {
            foreach ($counter->crane as $crane) {
                $delete = false;
            }
        }


        if (empty($counter)) {
            return redirect(route('counters.index'));
        }

        return view('counters.edit', compact('counter', 'pins', 'billed', 'closed', 'delete'));
    }

    /**
     * Update the specified Counter in storage.
     *
     * @param  int                 $id
     * @param UpdateCounterRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCounterRequest $request)
    {
        $counter = $this->counterRepository->findWithoutFail($id);
        if (empty($counter)) {
            Flash::error('Counter not found');

            return redirect(route('counters.index'));
        }
        $counter = $this->counterRepository->update($request->all(), $id);
        if ($request->has('status_id')) {
            $counter->statuses()->sync($request->input('status_id'));
        }
        check_prices('counter', $id);
        Flash::success('Counter updated successfully.');

        return redirect(route('counters.index'));
    }

    public function destroy(Request $request)
    {
        // if ($request->has('counter_id') && $request->has('pin')) {
        if ($request->has('counter_id')) {
            $counter = $this->counterRepository->findWithoutFail($request->input('counter_id'));
            if (empty($counter)) {
                Flash::error('Job not found');

                return redirect(route('counters.index'));
            }
            // $code = Pincodes::where('id', $request->input('pin_id'))->limit(1)->get();
            // if ($code[0]->code == $request->input('pin')) {
                // Session::set('pin_id', $request->input('pin_id'));
                $this->counterRepository->delete($request->input('counter_id'));

                return Response::json(array(
                    'success' => true,
                    'data'    => 'Counter deleted successfully, Redirecting.'
                ));
            // } else {
            //     return Response::json([
            //         'error' => [
            //             'exception' => 'Wrong pin code entered.',
            //         ]
            //     ], 500);
            // }
        } else {
            return Response::json([
                'error' => [
                    'exception' => 'Missing counter id.',
                ]
            ], 500);
        }
    }

    public function remove_billed(Request $request)
    {
        // if ($request->has('counter_id') && $request->has('pin') && $request->has('reason')) {
        if ($request->has('counter_id')) {
            $counter = Counter::where('id', $request->input('counter_id'))->with('statuses')->first();
            if (empty($counter)) {
                Flash::error('Counter Sale not found');

                return redirect(route('counters.index'));
            }
            // $code = Pincodes::where('id', $request->input('pin_id'))->limit(1)->get();
            // if ($code[0]->code == $request->input('pin')) {

                if ($counter->statuses->contains(9)) {
                    $counter->statuses()->detach([9]);
                }
                if (!$counter->statuses->contains(13)) {
                    $counter->statuses()->attach([13]);
                }
                $counter->batch_reported = null;

                if ($counter->save()) {
                    // Session::set('pin_id', $request->input('pin_id'));
                    // Session::set('reason', $request->input('reason'));

                    return Response::json(array(
                        'success' => true,
                        'data'    => 'Counter Sale removed billed status successfully, Redirecting.'
                    ));
                }
            // } else {
            //     return Response::json([
            //         'error' => [
            //             'exception' => 'Wrong pin code.',
            //         ]
            //     ], 500);
            // }
        } else {
            return Response::json([
                'error' => [
                    'exception' => 'Missing counter ID.',
                ]
            ], 500);
        }
    }

    public function remove_close(Request $request)
    {
        // if ($request->has('counter_id') && $request->has('pin') && $request->has('reason')) {
        if ($request->has('counter_id')) {
            $counter = Counter::where('id', $request->input('counter_id'))->with('statuses')->first();
            if (empty($counter)) {
                Flash::error('Counter Sale not found');
                return redirect(route('counters.index'));
            }
            $code = Pincodes::where('id', $request->input('pin_id'))->limit(1)->get();
            // if ($code[0]->code == $request->input('pin')) {

                if ($counter->statuses->contains(13)) {
                    $counter->statuses()->detach([13]);
                }

                $counter->closed_at = null;
                $counter->batch_reported = null;
                if ($counter->save()) {
                    Session::set('pin_id', $request->input('pin_id'));
                    Session::set('reason', $request->input('reason'));

                    return Response::json(array(
                        'success' => true,
                        'data'    => 'Counter Sale removed closed status successfully, Redirecting.'
                    ));
                }
            // } else {
            //     return Response::json([
            //         'error' => [
            //             'exception' => 'Wrong pin code.',
            //         ]
            //     ], 500);
            // }
        } else {
            return Response::json([
                'error' => [
                    'exception' => 'Missing counter ID.',
                ]
            ], 500);
        }
    }

    public function add_billed(Request $request)
    {
        // if ($request->has('counter_id') && $request->has('pin')) {
        if ($request->has('counter_id')) {
            $counter = Counter::where('id', $request->input('counter_id'))->with('statuses')->first();
            if (empty($counter)) {
                Flash::error('Counter Sale not found');

                return redirect(route('counters.index'));
            }
            // $code = Pincodes::where('id', $request->input('pin_id'))->limit(1)->get();
            // if ($code[0]->code == $request->input('pin')) {

                if ($counter->statuses->contains(13)) {
                    $counter->statuses()->detach([13]);
                }
                if (!$counter->statuses->contains(9)) {
                    $counter->statuses()->attach([9]);
                }

                if ($counter->save()) {
                    Session::set('pin_id', $request->input('pin_id'));

                    return Response::json(array(
                        'success' => true,
                        'data'    => 'Counter Sale added billed status successfully, Redirecting.'
                    ));
                }
            // } else {
            //     return Response::json([
            //         'error' => [
            //             'exception' => 'Wrong pin code.',
            //         ]
            //     ], 500);
            // }
        } else {
            return Response::json([
                'error' => [
                    'exception' => 'Missing counter ID.',
                ]
            ], 500);
        }
    }

    public function close(Request $request)
    {
        if ($request->has('counter_id') /*&& $request->has('pin')*/) {

            $success = [];
            $errors = [];
            foreach (explode(",",$request->input('counter_id')) as $counter_id) {

                $counter = Counter::where('id', $counter_id)
                                  ->with('statuses', 'items', 'labour', 'carriage', 'crane')
                                  ->first();

                if (empty($counter)) {
                    Flash::error('Counter Sale not found');

                    return redirect(route('counters.index'));
                }
                // $code = Pincodes::where('id', $request->input('pin_id'))->limit(1)->get();
                // if ($code[0]->code == $request->input('pin')) {
                    $counter->closed_at = date("Y-m-d H:i:s");
                    if (strlen($counter->ref) < 1) {
                        $counter->ref = $this->makeRef();
                    }

                    if (!$counter->statuses->contains(13)) {
                        $counter->statuses()->attach([13]);
                    }


                    check_prices('counter', $request->input('counter_id'));

                    if ($counter->save()) {
                        // Session::set('pin_id', $request->input('pin_id'));
                        $success[] = $counter_id;
                    } else {
                        $errors[] = $counter_id;
                    }
                // } else {
                //     return Response::json([
                //         'error' => [
                //             'exception' => 'Wrong pin code entered.',
                //         ]
                //     ], 500);
                // }
            }
            if (count($errors) > 0) {
                return Response::json(array(
                    'success' => true,
                    'data'    => 'Some Counter Sales could not be saved.'
                ));
            } else {
                return Response::json(array(
                    'success' => true,
                    'data'    => 'Counter sale(s) closed successfully, Redirecting.'
                ));                
            }
        } else {
            return Response::json([
                'error' => [
                    'exception' => 'Missing counter sale id.',
                ]
            ], 500);
        }
    }

    public function makeRef()
    {
        $last = DB::select('SELECT REPLACE(ref, \'MCS\', \'\') as ref FROM counter ORDER BY ref DESC LIMIT 1');
        if ( ! $last) {
            $ref = 0;
        } else {
            $ref = $last[0]->ref;
        }

        return 'MCS' . sprintf('%04d', $ref + 1);
    }

    public function issue(CounterIssueDataTable $dataTable, $id)
    {
        $pincodes = Pincodes::where('active', '=', '1')->get();
        $jobs     = Job::whereNull('closed_at')->orderby('created_at', 'DESC')->get();
        $counter  = Counter::where('id', '=', $id)->with('boat')->with('customer')->first();
        $billed  = false;
        $closed = false;

        if (isset($counter->statuses)) {
            foreach ($counter->statuses as $status) {
                if ($status->id == 9) {
                    $billed = true;
                }
                if ($status->id == 13) {
                    $closed = true;
                }
            }
        }


        return $dataTable->forCounter($id)->render('counters.issues', compact('pincodes', 'jobs', 'counter', 'closed', 'billed'));
    }

    public function labour(CounterLabourDataTable $dataTable, $id)
    {
        $pins    = Pincodes::where('active', '1')->get();
        $counter = Counter::where('id', $id)->with('statuses')->first();

        $billed = false;
        if (isset($counter->statuses)) {
            foreach ($counter->statuses as $status) {
                if ($status->id == 9) {
                    $billed = true;
                }
            }
        }

        return $dataTable->forCounter($id)->render('counters.labour', array(
            'billed'    => $billed,
            'pins'    => $pins,
            'counter' => $counter
        ));
    }

    public function carriage(CounterCarriageDataTable $dataTable, $id)
    {
        $pins    = Pincodes::where('active', '1')->get();
        $counter = Counter::where('id', $id)->with('statuses')->first();

        $billed = false;
        if (isset($counter->statuses)) {
            foreach ($counter->statuses as $status) {
                if ($status->id == 9) {
                    $billed = true;
                }
            }
        }

        return $dataTable->forCounter($id)->render('counters.carriage', array(
                'pins'    => $pins,
                'billed'    => $billed,
                'counter' => $counter
            )
        );
    }

    public function crane(CounterCraneDataTable $dataTable, $id)
    {
        $pins    = Pincodes::where('active', '1')->get();
        $counter = Counter::where('id', $id)->with('statuses')->first();

        $billed = false;
        if (isset($counter->statuses)) {
            foreach ($counter->statuses as $status) {
                if ($status->id == 9) {
                    $billed = true;
                }
            }
        }
        return $dataTable->forCounter($id)->render('counters.crane', array(
                'pins'    => $pins,
                'billed'    => $billed,
                'counter' => $counter
            )
        );
    }

    public function destroy_item(Request $request)
    {
        if (CheckPin($request->input('pin_id'), $request->input('code'))) {
            $item = CounterItems::where('id', $request->input('delete-id'))->first();
            if ($item->delete()) {
                return Response::json(array(
                    'success' => true,
                    'data' => 'Item deleted from sale.'
                ));
            }
        }

        return Response::json([
            'error' => [
                'exception' => 'Wrong pin code entered.',
            ]
        ], 500);
    }

    public function destroy_labour(Request $request)
    {
        if (CheckPin($request->input('pin_id'), $request->input('code'))) {
            $deleted = CounterLabour::where('id', $request->input('delete-id')) - first();
            if ($deleted->delete()) {
                return Response::json(array(
                    'success' => true,
                    'data'    => 'Labour deleted from sale.'
                ));
            }
        }

        return Response::json([
            'error' => [
                'exception' => 'Wrong pin code entered.',
            ]
        ], 500);
    }

    public function destroy_carriage(Request $request)
    {
        if (CheckPin($request->input('pin_id'), $request->input('code'))) {
            $deleted = CounterLabour::where('id', $request->input('delete-id')) - first();
            if ($deleted->delete()) {
                return Response::json(array(
                    'success' => true,
                    'data'    => 'Carriage deleted from sale.'
                ));
            }
        }

        return Response::json([
            'error' => [
                'exception' => 'Wrong pin code entered.',
            ]
        ], 500);
    }

    public function email_invoice(Request $request)
    {
        $counter = Counter::where('id', $request->input('id'))->with('statuses')->first();
//        $counter->statuses()->detach('13');
//        $counter->statuses()->attach('9');

        if (valid_email($request->input('email'))) {
            $pdf = $this->invoice($request->input('id'), true);

            $data = array(
                'message' => $request->input('message')
            );

            $mail = Mail::send('email.counter', ['data' => $data], function ($message) use ($counter, $pdf, $request) {
                $message->from('Mikaela@macduffshipyards.com', 'Macduff Shipyards');
                $message->sender('Mikaela@macduffshipyards.com', 'Macduff Shipyards');
                $message->to($request->input('email'))->bcc('sam@boxportable.com');
                $message->attachData($pdf['pdf'], $counter->ref . '_' . Date('d-m-y') . '_.pdf');
            });

            if ($mail) {
                $counter->invoiced_at = Date('Y-m-d H:i:s');
                $counter->net_total   = $pdf['net_total'];
                if ($counter->save()) {
                    return Response::json(array(
                        'success' => true,
                        'data'    => 'Counter sale invoice sent, sale will appear on next batch report.'
                    ));
                } else {
                    return Response::json([
                        'error' => [
                            'exception' => 'The email sent but we could not update the counter sale invoice date',
                        ]
                    ], 500);
                }
            }

        } else {
            return Response::json([
                'error' => [
                    'exception' => 'The customers email address is not valid.',
                ]
            ], 500);
        }
        //check the customer for an email.
        //check the counter sale is closed and not marked as

    }

    public function invoice($id, $email = false)
    {
        $counter  = Counter::where('id', $id)->with('statuses')->first();
        $customer = Customer::where('id', $counter->customer_id)->with('address')->first();
        $boat     = Boat::where('id', $counter->boat_id)->first();
        if ($counter && $customer) {
            $list      = array();
            $total_net = 0;
            $total_vat = 0;
            $total     = 0;

            foreach ($counter->labour()->get() as $labour) {
                $cost   = number_format((float)$labour->cost, 2, '.', '');
                $vat    = number_format((float)($cost * 0.2), 2, '.', '');
                $list[] = array(
                    'qty'       => number_format((float)1, 2, '.', ''),
                    'details'   => $labour->name,
                    'unit_cost' => $cost,
                    'net_cost'  => $cost,
                    'vat_rate'  => '20',
                    'vat'       => $vat,
                );

                $total_net = $total_net + $labour->cost;
                $total_vat = $total_vat + $cost * 0.2;
            }

            foreach ($counter->crane()->get() as $crane) {
                $cost   = number_format((float)$crane->cost, 2, '.', '');
                $vat    = number_format((float)($cost * 0.2), 2, '.', '');
                $list[] = array(
                    'qty'       => number_format((float)1, 2, '.', ''),
                    'details'   => $crane->name,
                    'unit_cost' => $cost,
                    'net_cost'  => $cost,
                    'vat_rate'  => '20',
                    'vat'       => $vat,
                );

                $total_net = $total_net + $crane->cost;
                $total_vat = $total_vat + $cost * 0.2;
            }

            foreach ($counter->items()->get() as $item) {
                $name = '';
                if ($item->item_id == 0) {
                    $misc = CounterMisc::where('counter_item_id', $item->id)->first();
                    if (strlen($misc->name) > 0) {
                        $name .= $misc->name . ' ';
                    }
                    if (strlen($misc->description) > 0) {
                        $name .= $misc->description;
                    }
                } else {
                    $itm = Item::where('id', $item->item_id)->with('sizes')->first();
                    if (isset($itm->sizes)) {
                        foreach ($itm->sizes as $size) {
                            $name .= $size->name . ' x ';
                        }
                        $name = rtrim($name, 'x ');
                        $name .= ' ';
                    }
                    $name .= $itm->description;
                }

                if ($item->line_unit_cost != null) {
                    $unit_cost = number_format((float)$item->line_unit_cost, 2, '.', '');
                } else {
                    $unit_cost = number_format((float)($item->line_cost / $item->qty), 2, '.', '');
                }

                $net_cost = number_format((float)$item->line_cost, 2, '.', '');
                $vat      = number_format((float)($net_cost * 0.2), 2, '.', '');

                $list[] = array(
                    'qty'       => number_format((float)$item->qty, 2, '.', ''),
                    'details'   => $name,
                    'unit_cost' => $unit_cost,
                    'net_cost'  => $net_cost,
                    'vat_rate'  => '20',
                    'vat'       => $vat,
                );

                $total_vat = $total_vat + $net_cost * 0.2;
                $total_net = $total_net + $item->line_cost;
            }

            foreach ($counter->carriage()->get() as $carriage) {
                $cost   = number_format((float)$carriage->cost, 2, '.', '');
                $vat    = number_format((float)($cost * 0.2), 2, '.', '');
                $list[] = array(
                    'qty'       => number_format((float)1, 2, '.', ''),
                    'details'   => $carriage->name,
                    'unit_cost' => $cost,
                    'net_cost'  => $cost,
                    'vat_rate'  => '20',
                    'vat'       => $vat,
                );

                $total_net = $total_net + $carriage->cost;
                $total_vat = $total_vat + $cost * 0.2;
            }

            $total = $total_net + $total_vat;

            if (count($list) > 0) {
                $counter->invoiced_at = Date('Y-m-d H:i:s');
                $counter->net_total   = $total_net;
                $counter->save();

                $pdf = PDF::loadView('counters.export.pdf', array(
                    'customer'  => $customer,
                    'boat'      => $boat,
                    'counter'   => $counter,
                    'list'      => $list,
                    'total_net' => $total_net,
                    'total_vat' => $total_vat,
                    'total'     => $total
                ))->setPaper('A4', 'portrait');

                if ($email) {
                    return array('pdf' => $pdf->stream(), 'net_total' => $total_net);
                }

                return $pdf->stream();
            } else {
                Flash::success('Counter sale has no items on it to invoice.');

                return redirect(route('counters.index'));
            }
        } else {
            Flash::success('Counter sale not found.');

            return redirect(route('counters.index'));
        }
        //check number of items issues > 0
        //check that all of those items have a sale
    }



    public function JsonLookup( Request $request ) {
        //TODO lookup counters by customer, boat, ref, customer PO, note
        //TODO populate the PO field in an order when this job is selected if the customer has a PO set for that job

        $counters = Counter::orderBy( 'created_at', 'DESC' )->with('customer', 'boat');
        $results = $counters->get();
        $formatted_list = array();
        foreach ( $results as $counter ) {
            $name = '';
            if ( strlen( $counter->boat ) > 0 ) {
                $name .= $counter->boat->number;
            }
            if ( strlen( $counter->customer ) > 0 ) {
                if(strlen($name) > 0){
                    $name .= ' : ';
                }
                $name .= $counter->customer->name;
            }
            if(strlen($name) > 0 && strlen($counter->note) > 0){
                $name .= ' : '.$counter->note;
            }
            $formatted_list[] = array(
                'id'   => $counter->id,
                'text' => $name,
            );
        }
        return Response::json( array( 'results' => $formatted_list ) );
    }

}
