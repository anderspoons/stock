<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Item;
use App;

class BarcodeController extends Controller
{
	public function index($code = '', $style = ''){
		if(strlen($code) > 0){
			return view('barcode.index')->with('code', $code)->with('style', $style);
		} else {
			return view('barcode.index');
		}
	}

	public function pdf($code = ''){
		$item = Item::where('barcode', $code)->with('sizes')->with('types')->with('materials')->get();
		$pdf = App::make('dompdf.wrapper');
		if($item->count() == 1){
			$html = view('barcode.print')->with('item', $item);
			$pdf->loadHTML($html);
			$width = floor(2.21*72);
			$height = floor(1.26*72);
			$pdf->setPaper(array(0,0,$width,$height), 'portrait');
		} else {
			$pdf->loadHTML('<h1>Not Found</h1>');
		}
		return $pdf->stream();
	}

	public function generate(Request $request){
		if(!empty($request->input('code')) && !empty($request->input('style'))) {
			if($request->input('style') == 'zebra'){
				$code = $request->input('code');
				if(strpos($code, 'MS') === false){
					$code = 'MS'.$code;
				}
				$item = Item::where('barcode', $code)->with('sizes')->with('types')->with('materials')->get();
				if($item->count() == 1){
					return view('barcode.index')
						->with('item', $item)
						->with('code', $request->input('code'))
						->with('style', $request->input('style'));
				} else {
					return view('barcode.index')
						->with('code', $request->input('code'))
						->with('style', $request->input('style'));
				}
			} else {
				return view('barcode.index')
					->with('code', $request->input('code'))
					->with('style', $request->input('style'));
			}
		}
	}
}
