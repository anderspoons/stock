<?php

namespace App\Http\Controllers;

use App\DataTables\ItemDataTable;
use App\Http\Requests;
use App\Models\Category;
use App\Models\Order;
use App\Models\Supplier;
use Illuminate\Http\Request;
use App\Http\Requests\CreateItemRequest;
use App\Http\Requests\ItemRequest;
use App\Repositories\ItemRepository;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Response;
use App\Models\Item;
use App\Models\Store;
use App\Models\Type;
use App\Models\Sex;
use App\Models\Location;
use App\Models\Grade;
use App\Models\Size;
use App\Models\Material;
use App\Models\Price;
use App\Models\Pincodes;
use App\Models\Unit;
use App\Models\UnitSize;
use Session;
use DNS1D;
use Debugbar;
use App\Models\Status;
use Auditing;
use App\Models\CounterItems;
use App\Models\JobItems;
use App\Models\OrderItems;
use Collect;


class ItemController extends AppBaseController
{
    /** @var  ItemRepository */
    private $itemRepository;

    public function __construct(ItemRepository $itemRepo)
    {
        $this->itemRepository = $itemRepo;
    }

    /**
     * Display a listing of the Item.
     *
     * @param ItemDataTable $itemDataTable
     *
     * @return Response
     */
    public function index(ItemDataTable $itemDataTable)
    {
        $sexes     = Sex::orderBy('name', 'ASC')->get();
        $types     = Type::orderBy('name', 'ASC')->get();
        $materials = Material::orderBy('name', 'ASC')->get();
        $grades    = Grade::orderBy('name', 'ASC')->get();
        $suppliers = Supplier::orderBy('name', 'ASC')->get();
        $units     = Unit::orderBy('name', 'ASC')->get();
        $sizes     = Size::orderBy('name', 'ASC')->get();
        $pins      = Pincodes::orderBy('name', 'ASC')->get();
        $stores    = Store::orderBy('name', 'ASC')->get();
        $category  = Category::orderBy('name', 'ASC')->get();
        $statuses  = Status::orderBy('name', 'ASC')->where('item', 1)->get();
        $orders    = Order::orderBy('supplier_id', 'desc')->whereNull('closed_at')->whereNull('expected_at')->get();

        $viewData = array(
            'types'     => $types,
            'grades'    => $grades,
            'materials' => $materials,
            'sizes'     => $sizes,
            'sexes'     => $sexes,
            'suppliers' => $suppliers,
            'pins'      => $pins,
            'category'  => $category,
            'statuses'  => $statuses,
            'stores'    => $stores,
            'orders'    => $orders,
            'units'     => $units
        );

        return $itemDataTable->render('items.index', $viewData);
    }

    /**
     * Show the form for creating a new Item.
     *
     * @return Response
     */
    public function create()
    {
        $barcode   = $this->barcode();
        $stores    = Store::select('id', 'code')->get();
        $suppliers = Supplier::orderBy('name', 'ASC')->get();
        $category  = Category::orderBy('name', 'ASC')->get();
        $units     = Unit::orderBy('name', 'ASC')->get();
        $sexes     = Sex::select('id', 'name')->get();

        return view('items.create')
            ->with('barcode', $barcode)
            ->with('stores', $stores)
            ->with('category', $category)
            ->with('units', $units)
            ->with('suppliers', $suppliers)
            ->with('sexes', $sexes);
    }

    public function barcode()
    {
        $rnd  = str_shuffle(mt_rand(0, 9999999));
        $code = str_pad($rnd, 7, "0", STR_PAD_LEFT);;
        if (count(Item::where('barcode', 'MS' . $code)->get()) > 0) {
            $this->barcode();
        } else {
            return 'MS' . $code;
        }
    }

    public function lookup($barcode)
    {
        $item = Item::where('barcode', strtoupper($barcode))->with('types', 'sizes', 'sexes', 'stores', 'suppliers', 'unit_category', 'last_price', 'supplier_units')->limit('1')->get();
        if ($item->count() == 0) {
            return Response::json(array(
                'success' => false,
                'data'    => 'Could not find the item you were looking for.'
            ));
        } else {
            return Response::json(array(
                'success' => true,
                'data'    => $item
            ));
        }
    }


    public function lookup_id($id)
    {
        $item = Item::where('id', '=', $id)
	        ->with('types')
	        ->with('sizes')
	        ->with('sexes')
	        ->with('stores')
	        ->with('suppliers')
	        ->with('unit_category')
	        ->with('last_price')
	        ->limit('1')->first();
        if ($item->count() == 0) {
            return Response::json(array(
                'success' => false,
                'data'    => 'Could not find the item you were looking for.'
            ));
        } else {
            return Response::json(array(
                'success' => true,
                'data'    => $item
            ));
        }
    }


    /**
     * Store a newly created Item in storage.
     *
     * @param ItemRequest $request
     *
     * @return Response
     */
    public function store(ItemRequest $request)
    {
        $input = $request->all();
        $item  = $this->itemRepository->create($input);
        $id    = $item['id'];
        //This item, this will be used several times below for relationship stuff
        $item = Item::find($id);

        if ($request->has('types')) {
            $this->LoopTypes($request, $item);
        } else {
            $this->removeTypes($item);
        }
        if ($request->has('grades')) {
            $this->LoopGrades($request, $item);
        } else {
            $this->removeGrades($item);
        }
        if ($request->has('materials')) {
            $this->LoopMaterials($request, $item);
        } else {
            $this->removeMaterials($item);
        }
        if ($request->has('sexes')) {
            $this->LoopSexes($request, $item);
        } else {
            $this->removeSexes($item);
        }
        //MUST BE AFTER SEXES DON'T FUCK WITH ME!!!!
        //... I can't even remember why this is, PEPE is sad.
        if ($request->has('sizes')) {
            $this->LoopSizes($request, $item);
        } else {
            $this->removeSizes($item);
        }
        if ($request->has('location')) {
            $this->LoopStoreNew($request, $item);
        } else {
            $this->removeStores($item);
        }
        if ($request->has('supplier')) {
            $this->LoopSupplier($request, $item);
        } else {
            $this->removeSupplier($item);
        }

        Flash::success('Item saved successfully.');

        return redirect(route('items.index'));
    }

    /**
     * @param ItemRequest       $request
     * @param                   $item
     */
    public function LoopTypes(ItemRequest $request, $item)
    {
        $this->removeTypes($item);
        $ids = array();
        foreach ($request->input('types') as $key => $val) {
            if (strlen($val) > 0) {
                $type_id = Type::find($key);
                if (count($type_id) > 0) {
                    if ($type_id->id == $key && $type_id->name == $val) {
                        $ids[] = $type_id->id;
                        continue;
                    }
                }
                $type_id = Type::where('name', $val)->get();
                if (count($type_id) > 0) {
                    $ids[] = $type_id[0]->id;
                } else {
                    $new       = new Type();
                    $new->name = $val;
                    $new->save();
                    $ids[] = $new->id;
                }
            }
        }
        $item->types()->attach($ids);
    }

    /**
     * @param $item
     */
    public function removeTypes($item)
    {
        foreach ($item->types()->get() as $type) {
            $item->types()->detach($type->id);
        }
    }

    /**
     * @param ItemRequest       $request
     * @param                   $item
     */
    public function LoopGrades(ItemRequest $request, $item)
    {
        $this->removeGrades($item);
        $ids = array();
        foreach ($request->input('grades') as $key => $val) {
            if (strlen($val) > 0) {
                $grade_id = Grade::find($key);
                if (count($grade_id) > 0) {
                    if ($grade_id->id == $key && $grade_id->name == $val) {
                        $ids[] = $grade_id->id;
                        continue;
                    }
                }
                $grade_id = Grade::where('name', $val)->get();
                if (count($grade_id) > 0) {
                    $ids[] = $grade_id[0]->id;
                } else {
                    $new       = new Grade();
                    $new->name = $val;
                    $new->save();
                    $ids[] = $new->id;
                }
            }
        }
        $item->grades()->attach($ids);
    }

    /**
     * @param $item
     */
    public function removeGrades($item)
    {
        foreach ($item->grades()->get() as $grade) {
            $item->grades()->detach($grade->id);
        }
    }

    /**
     * @param ItemRequest       $request
     * @param                   $item
     */
    public function LoopMaterials(ItemRequest $request, $item)
    {
        $this->removeMaterials($item);
        $ids = array();
        foreach ($request->input('materials') as $key => $val) {
            if (strlen($val) > 0) {
                $mat_id = Material::find($key);
                if (count($mat_id) > 0) {
                    if ($mat_id->id == $key && $mat_id->name == $val) {
                        $ids[] = $mat_id->id;
                        continue;
                    }
                }
                $mat_id = Material::where('name', $val)->get();
                if (count($mat_id) > 0) {
                    $ids[] = $mat_id[0]->id;
                } else {
                    $new       = new Material();
                    $new->name = $val;
                    $new->save();
                    $ids[] = $new->id;
                }
            }
        }
        $item->materials()->attach($ids);
    }

    /**
     * @param $item
     */
    public function removeMaterials($item)
    {
        foreach ($item->materials()->get() as $mat) {
            $item->materials()->detach($mat->id);
        }
    }

    /**
     * @param ItemRequest       $request
     * @param                   $item
     */
    public function LoopSexes(ItemRequest $request, $item)
    {
        $this->removeSexes($item);
        foreach ($request->input('sexes') as $key => $val) {
            if ( ! empty($val)) {
                $sex_id = Sex::find($key);
                if (count($sex_id) > 0) {
                    if ($sex_id->id == $key && $sex_id->name == $val) {
                        $item->sexes()->attach($sex_id->id, array('size_id' => '0'));
                        continue;
                    }
                }
                $sex_id = Sex::where('name', $val)->get();
                if (count($sex_id) > 0) {
                    $item->sexes()->attach($sex_id[0]->id, array('size_id' => '0'));
                } else {
                    $new       = new Sex();
                    $new->name = $val;
                    $new->save();
                    $item->sexes()->attach($new->id, array('size_id' => '0'));
                }
            }
        }
    }

    /**
     * @param $item
     */
    public function removeSexes($item)
    {
        foreach ($item->sexes()->get() as $sex) {
            if ($sex->pivot->size_id == 0) {
                $item->sexes()->detach($sex->id);
            }
        }
    }

    /**
     * @param ItemRequest       $request
     * @param                   $item
     */
    public function LoopSizes(ItemRequest $request, $item)
    {
        $this->removeSizes($item);
        foreach ($request->input('sizes') as $siz) {
            if (is_array($siz)) {
                if ( ! empty($siz['size'])) {
                    $size_id = Size::where('name', $siz['size'])->get();
                    if (count($size_id) > 0) {
                        $item->sizes()->attach($size_id[0]->id, array('sex_id' => $siz['sex']));
                    } else {
                        $new       = new Size();
                        $new->name = $siz['size'];
                        $new->save();
                        $ids[] = $new->id;
                        $item->sizes()->attach($new->id, array('sex_id' => $siz['sex']));
                    }
                } else {
                    $item->sizes()->attach(0, array('sex_id' => $siz['sex']));
                }
            }
        }
    }

    /**
     * @param $item
     */
    public function removeSizes($item)
    {
        foreach ($item->sizes()->get() as $size) {
            $item->sizes()->detach($size->id);
        }
    }

    public function LoopStoreNew(ItemRequest $request, $item)
    {
        $ids = array();

        foreach ($request->input('location') as $location) {
            if (isset($location['loc_id'])) {
                //Existing location
                $loc = Location::where('id', '=', $location['loc_id'])->first();
                if (!$loc) {
                    continue;
                } else {
                    if (!isset($location['store'])) {
                        //have to skip this if they missed this out.
                        continue;
                    } else {
                        $store = $location['store'];
                    }
                    if ( ! isset($location['room'])) {
                        //have to skip this if they missed this out need at least a store & room
                        continue;
                    } else {
                        $room = $location['room'];
                    }
                    if (isset($location['rack'])) {
                        $rack = $location['rack'];
                    } else {
                        $rack = '';
                    }
                    if (isset($location['shelf'])) {
                        $shelf = $location['shelf'];
                    } else {
                        $shelf = '';
                    }
                    if (isset($location['bay'])) {
                        $bay = $location['bay'];
                    } else {
                        $bay = '';
                    }
                    if (isset($location['min'])) {
                        $min = $location['min'];
                    } else {
                        $min = '';
                    }
                    if (isset($location['max'])) {
                        $max = $location['max'];
                    } else {
                        $max = '';
                    }
                    if (isset($location['qty'])) {
                        $qty = $location['qty'];
                    } else {
                        $qty = 0;
                    }

                    if ($loc->store_id !== $store) {
                        $loc->store_id = $store;
                    }
                    if ($loc->min !== $min) {
                        $loc->min = $min;
                    }
                    if ($loc->max !== $max) {
                        $loc->max = $max;
                    }
                    if ($loc->qty !== $qty) {
                        $loc->qty = $qty;
                    }
                    if ($loc->rack !== $rack) {
                        $loc->rack = $rack;
                    }
                    if ($loc->shelf !== $shelf) {
                        $loc->shelf = $shelf;
                    }
                    if ($loc->room !== $room) {
                        $loc->room = $room;
                    }
                    if ($loc->bay !== $bay) {
                        $loc->bay = $bay;
                    }
                    $loc->save();
                }
            } else {
                    //new location
                    if ( ! isset($location['store'])) {
                        //have to skip this if they missed this out.
                        continue;
                    } else {
                        $store = $location['store'];
                    }
                    if ( ! isset($location['room'])) {
                        //have to skip this if they missed this out need at least a store & room
                        continue;
                    } else {
                        $room = $location['room'];
                    }
                    if (isset($location['rack'])) {
                        $rack = $location['rack'];
                    } else {
                        $rack = '';
                    }
                    if (isset($location['shelf'])) {
                        $shelf = $location['shelf'];
                    } else {
                        $shelf = '';
                    }
                    if (isset($location['bay'])) {
                        $bay = $location['bay'];
                    } else {
                        $bay = '';
                    }
                    if (isset($location['min'])) {
                        $min = $location['min'];
                    } else {
                        $min = '';
                    }
                    if (isset($location['max'])) {
                        $max = $location['max'];
                    } else {
                        $max = '';
                    }
                    if (isset($location['qty'])) {
                        $qty = $location['qty'];
                    } else {
                        $qty = 0;
                    }
                    $location           = new Location();
                    $location->item_id  = $item->id;
                    $location->store_id = $store;
                    $location->min      = $min;
                    $location->max      = $max;
                    $location->qty      = $qty;
                    $location->rack     = $rack;
                    $location->shelf    = $shelf;
                    $location->room     = $room;
                    $location->bay      = $bay;
                    $location->save();
                    $ids[] = $location->id;
            }
        }

        //get the existing locations which are being sent back for an update this is used to cross check against the locations already set to see if any where deleted.
        foreach ($request->input('location') as $location) {
            if (isset($location['loc_id'])) {
                $ids[] = $location['loc_id'];
            }
        }
        //get locations currently before updating them
        $item_locs = Location::where('item_id', '=', $item->id)->get();
        foreach($item_locs as $loc){
            if(!in_array($loc->id, $ids)){
                $loc->deleted_at = date("Y-m-d H:i:s");
                $loc->save();
            }
        }

    }

    /**
     * @param $item
     */
    public function removeStores($item)
    {
        foreach ($item->stores()->get() as $store) {
            $item->stores()->detach($store->id);
        }
    }

    public function LoopSupplier(ItemRequest $request, $item)
    {
        //need to see if this is a new store or if this is
        $item->suppliers()->detach();
        $item->supplier_sizing()->detach();
        $item->supplier_units()->detach();
        foreach ($request->input('supplier') as $supplier) {

            if ( ! empty($supplier['code'])) {
                $code = $supplier['code'];
            } else {
                $code = '';
            }
            $item->suppliers()->attach($supplier['supplier'], array('code' => $code));

            //need to do a price check if its changed then we should add
            $price_check = Price::where('supplier_id', $supplier['supplier'])
                ->where('item_id', $item->id)
                ->orderBy('created_at', 'desc')
                ->limit(1)
                ->get();

            if (!empty($supplier['price'])) {
                $new_cost = $supplier['price'];
            } else {
                $new_cost = '0';
            }

            if (count($price_check) < 1 || ($price_check[0]->cost != $new_cost) && $new_cost != '') {
                $price = new Price;
                if ( ! empty($supplier['supplier'])) {
                    $price->supplier_id = $supplier['supplier'];
                }
                $price->item_id = $item->id;
                $price->cost    = $new_cost;
                $price->save();
            }

            if (isset($supplier['unit_id'])) {
                $item->supplier_units()->attach($supplier['unit_id'], array(
                    'unit_cat_id' => $supplier['category_id'],
                    'supplier_id' => $supplier['supplier'],
                    'lots'        => $supplier['lots']
                ));
            }

            if (isset($supplier['unit-size'])) {
                foreach ($supplier['unit-size'] as $sizing) {
                    $size       = new UnitSize();
                    $size->data = floatval($sizing);
                    $size->save();
                    $item->supplier_sizing()->attach($size->id, array('supplier_id' => $supplier['supplier']));
                }
            }
        }
    }

    /**
     * @param $item
     */
    public function removeSupplier($item)
    {
        foreach ($item->suppliers()->get() as $supplier) {
            $item->suppliers()->detach($supplier->id);
        }
    }

    /**
     * Display the specified Item.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $transactions = collect();

        $item = Item::where('id', $id)->with('stores')->first();

        //location transaction history
        if (isset($item->stores)) {
            foreach ($item->stores as $store) {
                $location       = Location::where('id', '=', $store->pivot->id)->first();
                $transactions[] = $location->logs;
            }
        }


        if (empty($item)) {
            Flash::error('Item not found');

            return redirect(route('items.index'));
        }

        return view('items.show')->with('item', $item)->with('logs', $transactions);
    }

    /**
     * Show the form for editing the specified Item.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        //$item = $this->itemRepository->findWithoutFail($id);
        $item      = Item::with('stores')
                         ->with('types')
                         ->with('sizes')
                         ->with('sexes')
                         ->with('suppliers')
                         ->with('materials')
                         ->with('grades')
                         ->with('prices')
                         ->with('supplier_units')
                         ->find($id);
        $stores    = Store::select('id', 'code')->get();
        $sexes     = Sex::select('id', 'name')->get();
        $suppliers = Supplier::select('id', 'code')->orderBy('name', 'ASC')->get();
        $category  = Category::orderBy('name', 'ASC')->get();
        $pins      = Pincodes::get();
        $units     = Unit::with('category')->get();

        if (empty($item)) {
            Flash::error('Item not found');
            return redirect(route('items.index'));
        }

        return view('items.edit')
            ->with('item', $item)
            ->with('stores', $stores)
            ->with('sexes', $sexes)
            ->with('pins', $pins)
            ->with('suppliers', $suppliers)
            ->with('units', $units)
            ->with('category', $category);
    }

    /**
     * Update the specified Item in storage.
     *
     * @param  int        $id
     * @param ItemRequest $request
     *
     * @return Response
     */
    public function update($id, ItemRequest $request)
    {
        $item = $this->itemRepository->findWithoutFail($id);
        if (empty($item)) {
            Flash::error('Item not found');

            return redirect(route('items.index'));
        }
        $item = $this->itemRepository->update($request->all(), $id);
        //This item, this will be used several times below for relationship stuff
        $item = Item::find($id);

        if ($request->has('types')) {
            $this->LoopTypes($request, $item);
        } else {
            $this->removeTypes($item);
        }
        if ($request->has('grades')) {
            $this->LoopGrades($request, $item);
        } else {
            $this->removeGrades($item);
        }
        if ($request->has('materials')) {
            $this->LoopMaterials($request, $item);
        } else {
            $this->removeMaterials($item);
        }
        if ($request->has('sexes')) {
            $this->LoopSexes($request, $item);
        } else {
            $this->removeSexes($item);
        }
        //MUST BE AFTER SEXES DON'T FUCK WITH ME!!!!
        if ($request->has('sizes')) {
            $this->LoopSizes($request, $item);
        } else {
            $this->removeSizes($item);
        }
        if ($request->has('location')) {
            $this->LoopStoreNew($request, $item);
        } else {
            $this->removeStores($item);
        }
        if ($request->has('supplier')) {
            $this->LoopSupplier($request, $item);
        } else {
            $this->removeSupplier($item);
        }

        Flash::success('Item updated successfully.');

        return redirect(route('items.index'));
    }

    public function LoopStore(ItemRequest $request, $item)
    {
        //need to see if this is a new store or if this is
        $this->removeStores($item);

        foreach ($request->input('location') as $location) {
            if ( ! isset($location['store'])) {
                //have to skip this if they missed this out.
                continue;
            } else {
                $store = $location['store'];
            }
            if ( ! isset($location['room'])) {
                //have to skip this if they missed this out need at least a store & room
                continue;
            } else {
                $room = $location['room'];
            }
            if (isset($location['rack'])) {
                $rack = $location['rack'];
            } else {
                $rack = '';
            }
            if (isset($location['shelf'])) {
                $shelf = $location['shelf'];
            } else {
                $shelf = '';
            }
            if (isset($location['bay'])) {
                $bay = $location['bay'];
            } else {
                $bay = '';
            }
            if (isset($location['min'])) {
                $min = $location['min'];
            } else {
                $min = '';
            }
            if (isset($location['max'])) {
                $max = $location['max'];
            } else {
                $max = '';
            }
            if (isset($location['qty'])) {
                $qty = $location['qty'];
            } else {
                $qty = 0;
            }
            $location           = new Location();
            $location->item_id  = $item->id;
            $location->store_id = $store;
            $location->min      = $min;
            $location->max      = $max;
            $location->qty      = $qty;
            $location->rack     = $rack;
            $location->shelf    = $shelf;
            $location->room     = $room;
            $location->bay      = $bay;
            $location->save();
        }
    }

    /**
     * Remove the specified Item from storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
        if ($request->has('item_id') && $request->has('pin')) {
            $item = $this->itemRepository->findWithoutFail($request->input('item_id'));

            if (empty($item)) {
                Flash::error('Item not found');

                return redirect(route('items.index'));
            }
            $code = Pincodes::where('id', $request->input('pin_id'))->limit(1)->get();

            if ($code[0]->code == $request->input('pin')) {
                Session::set('pin_id', $request->input('pin_id'));
                $this->itemRepository->delete($request->input('item_id'));

                return Response::json(array(
                    'success' => true,
                    'data'    => 'Item deleted successfully, Redirecting.'
                ));
            } else {
                return Response::json([
                    'error' => [
                        'exception' => 'Wrong Code Entered.',
                    ]
                ], 500);
            }
        } else {
            return Response::json([
                'error' => [
                    'exception' => 'Missing item id or pin code.',
                ]
            ], 500);
        }
    }

    public function qty(Request $request)
    {
        // if (strlen($request->input('reason')) < 1) {
        //     return Response::json([
        //         'error' => [
        //             'exception' => 'You must enter a reason for this change.',
        //         ]
        //     ], 500);
        // }

        // $processitem = CheckPin($request->input('pin_id'), $request->input('pin'));
        // if ($processitem == true) {
            $item = Item::find($request->input('item_id'));
            if (empty($item)) {
                Flash::error('Item not found');

                return redirect(route('items.index'));
            }

            foreach ($request->input('location') as $loc) {
                $id         = $loc['id'];
                $qty        = $loc['qty'];
                $table      = Location::find($id);
                $table->qty = $qty;
                // Session::set('pin_id', $request->input('pin_id'));
                // Session::set('reason', $request->input('reason'));
                $table->save();
            }

            return Response::json(array(
                'success' => true,
                'data'    => 'Quantities updated successfully.'
            ));
        // } else {
        //     return Response::json([
        //         'error' => [
        //             'exception' => 'Wrong Code Entered.',
        //         ]
        //     ], 500);
        // }
    }

    public function price(Request $request)
    {
        $item = Item::find($request->input('item_id'));
        if (empty($item)) {
            Flash::error('Item not found');
            return redirect(route('items.index'));
        }

        $price = new Price;
        if ( ! empty($request->input('supplier'))) {
            $price->supplier_id = $request->input('supplier');
        }
        $price->item_id = $request->input('item_id');
        $price->cost    = $request->input('price');
        $price->save();

        return Response::json(array(
            'success' => true,
            'data'    => 'Price updated successfully.'
        ));
    }

}
