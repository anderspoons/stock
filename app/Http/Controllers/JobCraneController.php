<?php

namespace App\Http\Controllers;

use App\DataTables\JobCraneDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateJobCraneRequest;
use App\Http\Requests\UpdateJobCraneRequest;
use App\Repositories\JobCraneRepository;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Response;

class JobCraneController extends AppBaseController
{
    /** @var  JobCraneRepository */
    private $jobCraneRepository;

    public function __construct(JobCraneRepository $jobCraneRepo)
    {
        $this->jobCraneRepository = $jobCraneRepo;
    }

    /**
     * Display a listing of the JobCrane.
     *
     * @param JobCraneDataTable $jobCraneDataTable
     * @return Response
     */
    public function index(JobCraneDataTable $jobCraneDataTable)
    {
        return $jobCraneDataTable->render('jobCranes.index');
    }

    /**
     * Show the form for creating a new JobCrane.
     *
     * @return Response
     */
    public function create()
    {
        return view('jobCranes.create');
    }

    /**
     * Store a newly created JobCrane in storage.
     *
     * @param CreateJobCraneRequest $request
     *
     * @return Response
     */
    public function store(CreateJobCraneRequest $request)
    {
        $input = $request->all();

        $jobCrane = $this->jobCraneRepository->create($input);

        Flash::success('JobCrane saved successfully.');

        return redirect(route('jobCranes.index'));
    }

    /**
     * Display the specified JobCrane.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $jobCrane = $this->jobCraneRepository->findWithoutFail($id);

        if (empty($jobCrane)) {
            Flash::error('JobCrane not found');

            return redirect(route('jobCranes.index'));
        }

        return view('jobCranes.show')->with('jobCrane', $jobCrane);
    }

    /**
     * Show the form for editing the specified JobCrane.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $jobCrane = $this->jobCraneRepository->findWithoutFail($id);

        if (empty($jobCrane)) {
            Flash::error('JobCrane not found');

            return redirect(route('jobCranes.index'));
        }

        return view('jobCranes.edit')->with('jobCrane', $jobCrane);
    }

    /**
     * Update the specified JobCrane in storage.
     *
     * @param  int              $id
     * @param UpdateJobCraneRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateJobCraneRequest $request)
    {
        $jobCrane = $this->jobCraneRepository->findWithoutFail($id);

        if (empty($jobCrane)) {
            Flash::error('JobCrane not found');

            return redirect(route('jobCranes.index'));
        }

        $jobCrane = $this->jobCraneRepository->update($request->all(), $id);

        Flash::success('JobCrane updated successfully.');

        return redirect(route('jobCranes.index'));
    }

    /**
     * Remove the specified JobCrane from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $jobCrane = $this->jobCraneRepository->findWithoutFail($id);

        if (empty($jobCrane)) {
            Flash::error('JobCrane not found');

            return redirect(route('jobCranes.index'));
        }

        $this->jobCraneRepository->delete($id);

        Flash::success('JobCrane deleted successfully.');

        return redirect(route('jobCranes.index'));
    }
}
