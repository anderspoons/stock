<?php

namespace App\Http\Controllers;

use App\DataTables\CounterCraneDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateCounterCraneRequest;
use App\Http\Requests\UpdateCounterCraneRequest;
use App\Repositories\CounterCraneRepository;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Response;

class CounterCraneController extends AppBaseController
{
    /** @var  CounterCraneRepository */
    private $counterCraneRepository;

    public function __construct(CounterCraneRepository $counterCraneRepo)
    {
        $this->counterCraneRepository = $counterCraneRepo;
    }

    /**
     * Display a listing of the CounterCrane.
     *
     * @param CounterCraneDataTable $counterCraneDataTable
     * @return Response
     */
    public function index(CounterCraneDataTable $counterCraneDataTable)
    {
        return $counterCraneDataTable->render('counterCranes.index');
    }

    /**
     * Show the form for creating a new CounterCrane.
     *
     * @return Response
     */
    public function create()
    {
        return view('counterCranes.create');
    }

    /**
     * Store a newly created CounterCrane in storage.
     *
     * @param CreateCounterCraneRequest $request
     *
     * @return Response
     */
    public function store(CreateCounterCraneRequest $request)
    {
        $input = $request->all();

        $counterCrane = $this->counterCraneRepository->create($input);

        Flash::success('CounterCrane saved successfully.');

        return redirect(route('counterCranes.index'));
    }

    /**
     * Display the specified CounterCrane.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $counterCrane = $this->counterCraneRepository->findWithoutFail($id);

        if (empty($counterCrane)) {
            Flash::error('CounterCrane not found');

            return redirect(route('counterCranes.index'));
        }

        return view('counterCranes.show')->with('counterCrane', $counterCrane);
    }

    /**
     * Show the form for editing the specified CounterCrane.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $counterCrane = $this->counterCraneRepository->findWithoutFail($id);

        if (empty($counterCrane)) {
            Flash::error('CounterCrane not found');

            return redirect(route('counterCranes.index'));
        }

        return view('counterCranes.edit')->with('counterCrane', $counterCrane);
    }

    /**
     * Update the specified CounterCrane in storage.
     *
     * @param  int              $id
     * @param UpdateCounterCraneRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCounterCraneRequest $request)
    {
        $counterCrane = $this->counterCraneRepository->findWithoutFail($id);

        if (empty($counterCrane)) {
            Flash::error('CounterCrane not found');

            return redirect(route('counterCranes.index'));
        }

        $counterCrane = $this->counterCraneRepository->update($request->all(), $id);

        Flash::success('CounterCrane updated successfully.');

        return redirect(route('counterCranes.index'));
    }

    /**
     * Remove the specified CounterCrane from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $counterCrane = $this->counterCraneRepository->findWithoutFail($id);

        if (empty($counterCrane)) {
            Flash::error('CounterCrane not found');

            return redirect(route('counterCranes.index'));
        }

        $this->counterCraneRepository->delete($id);

        Flash::success('CounterCrane deleted successfully.');

        return redirect(route('counterCranes.index'));
    }
}
