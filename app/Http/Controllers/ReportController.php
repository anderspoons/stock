<?php

namespace App\Http\Controllers;

use App\Models\Counter;
use App\Models\Job;
use App\Models\Item;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Store;
use App\Models\Location;
use App\Models\Supplier;
use PDF;
use Session;
use Excel;
use Carbon\Carbon;

class ReportController extends Controller
{
    public function index(){
    	$stores = Store::get();
    	$suppliers = Supplier::get();
	    $locations = $this->store_locations();
	    $dates = array();
	    for($i = 2016; $i <= 2060; $i++){
		    $dt = new Carbon('last day of February '.$i);
	    	$dates[] = $dt->format('d/m/Y');
	    }

	    return view('reports.index', compact('stores', 'suppliers', 'locations', 'dates'));
    }

    public function store_locations(){
	    $room_array = $rack_array = $shelf_array = $bay_array = array();
	    $stores = Store::with('locations')->get();
		foreach($stores as $store){
			$rooms = Location::select('store_id', 'room')->where('store_id', $store->id)->distinct('room')->orderby('room', 'ASC')->get();
			foreach($rooms as $room){
				if(strlen($room->room) > 0){
					$room_array[] = array('store_id' => $room->store_id, 'room' => $room->room);
				}
			}
			$racks = Location::select('store_id', 'rack')->where('store_id', $store->id)->distinct('rack')->orderby('rack', 'ASC')->get();
			foreach($racks as $rack){
				if(strlen($rack->rack) > 0) {
					$rack_array[] = array('store_id' => $rack->store_id, 'rack' => $rack->rack);
				}
			}
			$shelves = Location::select('store_id', 'shelf')->where('store_id', $store->id)->distinct('shelf')->orderby('shelf', 'ASC')->get();
			foreach($shelves as $shelf){
				if(strlen($shelf->shelf) > 0) {
					$shelf_array[] = array('store_id' => $shelf->store_id, 'shelf' => $shelf->shelf);
				}
			}
			$bays = Location::select('store_id', 'bay')->where('store_id', $store->id)->distinct('bay')->orderby('bay', 'ASC')->get();
			foreach($bays as $bay){
				if(strlen($bay->bay) > 0) {
					$bay_array[] = array('store_id' => $bay->store_id, 'bay' => $bay->bay);
				}
			}
		}
	    $locations = array('rooms' => $room_array, 'racks' => $rack_array, 'shelves' => $shelf_array, 'bays' => $bay_array);
    	return $locations;
    }


    public function generator(Request $request){
    	switch($request->input('report_name')){
		    case 'stock-check':
		    	return $this->stock_check_report($request);
		    case 'stock-value':
		    	return $this->stock_value_report($request);
		    case 'supplier-spend':
			    return $this->supplier_spending($request);
			case 'year-end':
				return $this->year_end($request);
		    case 'all-wip':
			    return $this->all_wip($request);
		    case 'batch-counter':
			    return $this->counter_batch($request);
	    }
    }

	public function counter_batch(Request $request){
		$counters = Counter::whereRaw('batch_reported is null')->whereRaw('invoiced_at is not null')->whereRaw('closed_at is not null')->with('customer')->get();
		$title = 'Batch Counter Sale Report '.Date('d/m/y');

	    $pdf = PDF::loadView( 'reports.pdf.pdf-batch-counters', array(
		    'title'     => $title,
		    'counters' => $counters
	    ))->setPaper( 'A4', 'landscape' );

		foreach($counters as $counter){
			$counter->batch_reported = Date('Y-m-d H:i:s');
            if($counter->statuses->contains(13)){
                $counter->statuses()->detach([13]);
            }
            if(!$counter->statuses->contains(9)){
                $counter->statuses()->attach([9]);
            }
			$counter->save();
		}

	    return $pdf->download();
	}

	public function supplier_spending(Request $request){
		print_r($request->all());


		return 'working on this';
	}

	public function year_end(Request $request){
	    //TODO This was said not to be required after I said it was still to do.
        //If this is ever needed it basically works like this:
        //Select all jobs and counters which are not billed
        //foreach get all items assigned to them within 2 dates (the one specified and the one a year ago)
        //Get the cost price of all these items and then output each on a sheet on the spreadsheet.

        $closed_jobs = DB::select('select job_id from job_statuses where status_id = ?', [13]);
        $arry = '';
        foreach ($closed_jobs as $id){
            $arry .= $id->job_id.',';
        }

        $closed_jobs = explode(',', rtrim($arry, ','));

        $closed_counters = DB::select('select counter_id from counter_statuses where status_id = ?', [13]);
        $arry = '';
        foreach ($closed_counters as $id){
            $arry .= $id->counter_id.',';
        }
        $closed_counters = explode(',', rtrim($arry, ','));


        $jobs = Job::whereIn('id', $closed_jobs);
        $counters = Counter::whereIn('id', $closed_jobs);



        Excel::create('Stock_Take'.date('d-m-Y-h-i-s'), function($excel) use ($jobs, $counters) {
            $excel->sheet('WIP Jobs', function($sheet) use ($jobs, $counters) {
                $sheet->loadView('reports.excel.excel-jobs-wip', array(
                        'jobs' => $jobs
                    )
                );
            });
            $excel->sheet('WIP Counters', function($sheet) use ($jobs, $counters) {
                $sheet->loadView('reports.excel.excel-counters-wip', array(
                        'counters' => $counters
                    )
                );
            });
        })->download('xlsx');
	}

	public function all_wip(Request $request){
        $jobs = Jobs::all();
        $counters = Counter::all();

        Excel::create('Stock_Take'.date('d-m-Y-h-i-s'), function($excel) use ($jobs, $counters) {
            $excel->sheet('WIP Jobs', function($sheet) use ($jobs, $counters) {
                $sheet->loadView('reports.excel.excel-jobs-wip', array(
                        'jobs' => $jobs
                    )
                );


            });
            $excel->sheet('WIP Counters', function($sheet) use ($jobs, $counters) {
                $sheet->loadView('reports.excel.excel-counters-wip', array(
                        'counters' => $counters
                    )
                );
            });
        })->download('xlsx');
	}

    public function stock_check_report(Request $request) {
	    $title     = 'Stock Check Report - ';
	    $locations = Location::orderBy( 'store_id', 'ASC' )
		    ->orderBy( 'room' , 'ASC')
		    ->orderBy( 'rack', 'ASC' )
		    ->orderBy( 'shelf', 'ASC' )
		    ->orderBy( 'bay', 'ASC' )
		    ->with('item')
		    ->with('item.materials')
		    ->with('item.types')
		    ->with('item.sizes')
		    ->with('item.sexes')
		    ->with('item.last_price');

	    if ( $request->input('store_id') != 'all') {
		    $locations->where('store_id', $request->input('store_id'));
		    $store = Store::select( 'name' )->where( 'id', $request->input( 'store_id' ) )->first();
		    $title .= 'Store: ' . $store->name;
	    } else {
		    $title .= 'Store: All';
	    }
	    if ( $request->input( 'room' ) != 'all' ) {
		    $title .= ' Room:' . $request->input( 'room' );
		    $locations->where( 'room', $request->input( 'room' ) );
	    } else {
		    $title .= ' Room: All';
	    }
	    if ( $request->input( 'rack' ) != 'all' ) {
		    $title .= ' Rack:' . $request->input( 'rack' );
		    $locations->where( 'rack', $request->input( 'rack' ) );
	    } else {
		    $title .= ' Rack: All';
	    }
	    if ( $request->input( 'shelf' ) != 'all' ) {
		    $title .= ' Shelf:' . $request->input( 'shelf' );
		    $locations->where( 'shelf', $request->input( 'shelf' ) );
	    } else {
		    $title .= ' Shelf: All';
	    }

	    if ( $request->input( 'bay' ) != 'all' ) {
		    $title .= ' Bay:' . $request->input( 'bay' );
		    $locations->where( 'bay', $request->input( 'bay' ) );
	    } else {
		    $title .= ' Bay: All';
	    }
	    $locations = $locations->get();

	    Excel::create('Stock_Take'.date('d-m-Y-h-i-s'), function($excel) use ($locations) {
		    $excel->sheet('New sheet', function($sheet) use ($locations) {
			    $sheet->loadView('reports.excel.excel-stock-take', array(
		            'locations' => $locations
				    )
			    );
			    $sheet->setAutoSize(true);
			    $sheet->setWidth('A', 10);
			    $sheet->setWidth('B', 10);
			    $sheet->setWidth('C', 10);
			    $sheet->setWidth('D', 10);
			    $sheet->setWidth('E', 20);
			    $sheet->setWidth('F', 20);
			    $sheet->setWidth('G', 15);
			    $sheet->setWidth('H', 15);
			    $sheet->setWidth('I', 15);
			    $sheet->setWidth('J', 15);
			    $sheet->setWidth('K', 15);
			    $sheet->setWidth('L', 15);
			    $sheet->setWidth('M', 15);
			    $sheet->setWidth('N', 15);

			    $sheet->setColumnFormat(array(
				    "M" => "£#,##0.00",
				    "N" => "£#,##0.00",
			    ));

		    });
	    })->download('xlsx');
    }

    public function stock_value_report(Request $request){
	    $stores = Store::orderBy( 'id', 'ASC' );
                                // ->with('locations')
                                // ->with('locations.item')
                                // ->with('locations.item.materials')
                                // ->with('locations.item.types')
                                // ->with('locations.item.sizes')
                                // ->with('locations.item.sexes')
                                // ->with('locations.item.last_price');
	    if ( $request->input( 'store_id' ) != 'all' ) {
		    $stores->where( 'id', $request->input( 'store_id' ) );
	    }

	    $stores = $stores->get();

	    // foreach ($stores as $store) {
	    // 	echo "<h3>".$store->name."</h3>";
	    // 		// echo "<pre>";
	    // 		// print_r($store);
	    // 		// echo "</pre>";
	    // 	foreach ($store->locations as $location) {
	    // 		echo "<pre>";
	    // 		print_r($location->item->last_price->first());
	    // 		echo "</pre>";
	    // 	}
	    // }

	    // exit;

	    Excel::create('Stock_Value_'.Date('d/m/y'), function($excel) use ($stores) {
	    	foreach($stores as $store) {
				    $excel->sheet($store->name, function($sheet) use ($store) {
					    $sheet->loadView('reports.excel.excel-store-value', array('store' => $store));
				    });
		    }
	    })->download('xls');
    }
}
