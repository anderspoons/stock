<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\Dashboard;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home')
	        ->with('inventory', $this->inventoryCount())
	        ->with('under_stocked', $this->get_value('under_stocked'))
	        ->with('over_stocked', $this->get_value('over_stocked'))
	        ->with('orders_awaiting', $this->get_value('orders_awaiting'))
	        ->with('orders_open', $this->get_value('orders_open'))
	        ->with('value', $this->get_value('value'));
    }

    public function get_value($name){
	    $value = Dashboard::where('name', $name)->first();
	    if($value){
		    return array($name => $value->value, 'updated' => timeago($value->updated_at));
	    }
	}

	public function inventoryCount(){
		$total = Dashboard::where('name', 'inventory')->get();
		$item_items = Dashboard::where('name', 'inventory_items')->get();
		return array(
			'total_count' => $total['0']->value,
			'total_items' => $item_items['0']->value,
			'updated' => timeago($total['0']->updated_at)
		);
	}
}
