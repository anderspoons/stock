<?php

namespace App\Http\Controllers;

use App\DataTables\AddressDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateAddressRequest;
use App\Http\Requests\UpdateAddressRequest;
use App\Repositories\AddressRepository;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Response;
use Illuminate\Http\Request;
use App\Models\Address;

class AddressController extends AppBaseController
{
    /** @var  AddressRepository */
    private $addressRepository;

    public function __construct(AddressRepository $addressRepo)
    {
        $this->addressRepository = $addressRepo;
    }

    /**
     * Display a listing of the Address.
     *
     * @param AddressDataTable $addressDataTable
     * @return Response
     */
    public function index(AddressDataTable $addressDataTable)
    {
        return $addressDataTable->render('addresses.index');
    }

    /**
     * Show the form for creating a new Address.
     *
     * @return Response
     */
    public function create()
    {
        return view('addresses.create');
    }

    /**
     * Store a newly created Address in storage.
     *
     * @param CreateAddressRequest $request
     *
     * @return Response
     */
    public function store(CreateAddressRequest $request)
    {
        $input = $request->all();

        $address = $this->addressRepository->create($input);

        Flash::success('Address saved successfully.');

        return redirect(route('addresses.index'));
    }

    /**
     * Display the specified Address.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $address = $this->addressRepository->findWithoutFail($id);

        if (empty($address)) {
            Flash::error('Address not found');

            return redirect(route('addresses.index'));
        }

        return view('addresses.show')->with('address', $address);
    }

    /**
     * Show the form for editing the specified Address.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $address = $this->addressRepository->findWithoutFail($id);

        if (empty($address)) {
            Flash::error('Address not found');

            return redirect(route('addresses.index'));
        }

        return view('addresses.edit')->with('address', $address);
    }

    /**
     * Update the specified Address in storage.
     *
     * @param  int              $id
     * @param UpdateAddressRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAddressRequest $request)
    {
        $address = $this->addressRepository->findWithoutFail($id);

        if (empty($address)) {
            Flash::error('Address not found');

            return redirect(route('addresses.index'));
        }

        $address = $this->addressRepository->update($request->all(), $id);

        Flash::success('Address updated successfully.');

        return redirect(route('addresses.index'));
    }

    /**
     * Remove the specified Address from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $address = $this->addressRepository->findWithoutFail($id);

        if (empty($address)) {
            Flash::error('Address not found');

            return redirect(route('addresses.index'));
        }

        $this->addressRepository->delete($id);

        Flash::success('Address deleted successfully.');

        return redirect(route('addresses.index'));
    }

    public function JsonLookup(Request $request){
        $addresses = Address::select('*');
        if(!empty($request->input( 'query' ))){
            $query = $request->input('query');
            $addresses->orWhere('line_1', 'like', '%'.$query.'%');
            $addresses->orWhere('line_2', 'like', '%'.$query.'%');
            $addresses->orWhere('line_3', 'like', '%'.$query.'%');
            $addresses->orWhere('town', 'like', '%'.$query.'%');
            $addresses->orWhere('county', 'like', '%'.$query.'%');
            $addresses->orWhere('postcode', 'like', '%'.$query.'%');
        }
        $results = $addresses->get();

        $formatted_list = array();

        foreach($results as $address){
            $formatted_list[] = array(
                'id' => $address->id,
                'text' => $this->formatAddress(array($address->line_1, $address->line_2, $address->line_3, $address->town, $address->county, $address->postcode)),
                'line_1' => $address->line_1,
                'line_2' => $address->line_2,
                'line_3' => $address->line_3,
                'town' => $address->town,
                'county' => $address->county,
                'postcode' => $address->postcode,
                'lng' => $address->lng,
                'lat' => $address->lat,
            );
        }

        return Response::json(array(
            'results'   => $formatted_list,
            'more' => false
        ));
    }

    public function formatAddress($address){
        $line = '';
        foreach($address as $val){
            if(!empty($val)){
                $line .= $val;
            }
            if(strlen($line) > 1 && strlen($val) > 1){
                $line .= ', ';
            }
        }
        return rtrim($line,',');
    }
}
