<?php

namespace App\Http\Controllers;

use App\DataTables\TypeDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateTypeRequest;
use App\Http\Requests\UpdateTypeRequest;
use App\Repositories\TypeRepository;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Response;

class TypeController extends AppBaseController
{
    /** @var  typeRepository */
    private $typeRepository;

    public function __construct(TypeRepository $typeRepo)
    {
        $this->typeRepository = $typeRepo;
    }

    /**
     * Display a listing of the type.
     *
     * @param typeDataTable $typeDataTable
     * @return Response
     */
    public function index(TypeDataTable $typeDataTable)
    {
        return $typeDataTable->render('types.index');
    }

    /**
     * Show the form for creating a new type.
     *
     * @return Response
     */
    public function create()
    {
        return view('types.create');
    }

    /**
     * Store a newly created type in storage.
     *
     * @param CreatetypeRequest $request
     *
     * @return Response
     */
    public function store(CreatetypeRequest $request)
    {
        $input = $request->all();

        $type = $this->typeRepository->create($input);

        Flash::success('type saved successfully.');

        return redirect(route('types.index'));
    }

    /**
     * Display the specified type.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $type = $this->typeRepository->findWithoutFail($id);

        if (empty($type)) {
            Flash::error('type not found');

            return redirect(route('types.index'));
        }

        return view('types.show')->with('type', $type);
    }

    /**
     * Show the form for editing the specified type.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $type = $this->typeRepository->findWithoutFail($id);

        if (empty($type)) {
            Flash::error('type not found');

            return redirect(route('types.index'));
        }

        return view('types.edit')->with('type', $type);
    }

    /**
     * Update the specified type in storage.
     *
     * @param  int              $id
     * @param UpdatetypeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTypeRequest $request)
    {
        $type = $this->typeRepository->findWithoutFail($id);

        if (empty($type)) {
            Flash::error('type not found');

            return redirect(route('types.index'));
        }

        $type = $this->typeRepository->update($request->all(), $id);

        Flash::success('type updated successfully.');

        return redirect(route('types.index'));
    }

    /**
     * Remove the specified type from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $type = $this->typeRepository->findWithoutFail($id);

        if (empty($type)) {
            Flash::error('type not found');

            return redirect(route('types.index'));
        }

        $this->typeRepository->delete($id);

        Flash::success('type deleted successfully.');

        return redirect(route('types.index'));
    }
}
