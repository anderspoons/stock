<?php

namespace App\Http\Controllers;

use App\DataTables\ElementDataTable;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\CreateElementRequest;
use App\Http\Requests\UpdateElementRequest;
use App\Repositories\ElementRepository;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Response;
use App\Models\Element;

class ElementController extends AppBaseController
{
    /** @var  ElementRepository */
    private $elementRepository;

    public function __construct(ElementRepository $elementRepo)
    {
        $this->elementRepository = $elementRepo;
    }

    /**
     * Display a listing of the Element.
     *
     * @param ElementDataTable $elementDataTable
     * @return Response
     */
    public function index(ElementDataTable $elementDataTable)
    {
        return $elementDataTable->render('elements.index');
    }

    /**
     * Show the form for creating a new Element.
     *
     * @return Response
     */
    public function create()
    {
        return view('elements.create');
    }

    /**
     * Store a newly created Element in storage.
     *
     * @param CreateElementRequest $request
     *
     * @return Response
     */
    public function store(CreateElementRequest $request)
    {
        $input = $request->all();

        $element = $this->elementRepository->create($input);

        Flash::success('Element saved successfully.');

        return redirect(route('elements.index'));
    }

    /**
     * Display the specified Element.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $element = $this->elementRepository->findWithoutFail($id);

        if (empty($element)) {
            Flash::error('Element not found');

            return redirect(route('elements.index'));
        }

        return view('elements.show')->with('element', $element);
    }

    /**
     * Show the form for editing the specified Element.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $element = $this->elementRepository->findWithoutFail($id);

        if (empty($element)) {
            Flash::error('Element not found');

            return redirect(route('elements.index'));
        }

        return view('elements.edit')->with('element', $element);
    }

    /**
     * Update the specified Element in storage.
     *
     * @param  int              $id
     * @param UpdateElementRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateElementRequest $request)
    {
        $element = $this->elementRepository->findWithoutFail($id);

        if (empty($element)) {
            Flash::error('Element not found');

            return redirect(route('elements.index'));
        }

        $element = $this->elementRepository->update($request->all(), $id);

        Flash::success('Element updated successfully.');

        return redirect(route('elements.index'));
    }

    /**
     * Remove the specified Element from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $element = $this->elementRepository->findWithoutFail($id);

        if (empty($element)) {
            Flash::error('Element not found');

            return redirect(route('elements.index'));
        }

        $this->elementRepository->delete($id);

        Flash::success('Element deleted successfully.');

        return redirect(route('elements.index'));
    }

    public function JsonLookup(Request $request) {
        $elements = Element::orderBy('name', 'ASC');
        if($request->has('q')){
            foreach($request->input('q') as $term){
                $elements->whereRaw('(REPLACE(`elements`.`name`, \' \', \'\') LIKE REPLACE(\'%'.$term.'%\', \' \', \'\'))');
            }
        }
        $results = $elements->get();
        $formatted_list = array();
        foreach($results as $element){
            $formatted_list[] = array(
                'id' => $element->id,
                'text' => $element->name,
                'newOption' => false
            );
        }
        return Response::json(array('results' => $formatted_list));
    }
}
