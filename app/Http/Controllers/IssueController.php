<?php

namespace App\Http\Controllers;

use App\Models\Counter;
use App\Models\CounterCrane;
use App\Models\CounterLabour;
use App\Models\CounterCarriage;
use App\Models\CounterItems;
use App\Models\CounterMisc;
use App\Models\CounterSize;

use App\Models\Job;
use App\Models\JobCarriage;
use App\Models\JobLabour;
use App\Models\JobCrane;
use App\Models\JobItems;
use App\Models\JobMisc;
use App\Models\JobSize;

use Illuminate\Http\Request;
use Mockery\CountValidator\Exception;
use Response;
use App\Http\Requests;
use Auth;
use App\Models\Pincodes;
use App\Models\Customer;
use App\Models\Boat;
use App\Models\Location;
use App\Models\Element;
use App\Models\Item;
use App\Models\Unit;
use Session;
use Illuminate\Support\Facades\Log;

class IssueController extends Controller
{
    public function index()
    {
        $customers = Customer::get();
        $boats = Boat::get();
        $elements = Element::get();

        return view('issue.index')
            ->with('customers', $customers)
            ->with('boats', $boats)
            ->with('elements', $elements);
    }

    public function job($id)
    {
        $job = Job::find($id);
        $job->with('boat')->with('customer')->first();
        $customers = Customer::get();
        $boats = Boat::get();
        $elements = Element::get();
        $units = Unit::get();
        $user = Auth::user();
        $pins = Pincodes::get();

        return view('issue.job')
            ->with('job', $job)
            ->with('customers', $customers)
            ->with('boats', $boats)
            ->with('units', $units)
            ->with('user', $user)
            ->with('pins', $pins)
            ->with('elements', $elements);
    }

    public function counter($id)
    {
        $counter = Counter::find($id);
        $counter->with('customer')->with('boat')->first();
        $units = Unit::get();
        $user = Auth::user();
        $pins = Pincodes::get();

        return view('issue.counter')
            ->with('counter', $counter)
            ->with('units', $units)
            ->with('user', $user)
            ->with('pins', $pins);
    }

    public function search(Request $request)
    {
        $item = Item::select('*');
        if (!empty($request->input('query'))) {
            $query = $request->input('query');
            $item->orWhere('barcode', 'like', '%' . $query . '%');
        }
        $results = $item->get();

        $formatted_list = array();

        foreach ($results as $item) {
            $formatted_list[] = array(
                'id' => $item->barcode,
                'text' => $item->barcode . ' : ' . $item->description
            );
        }

        return Response::json(array(
            'results' => $formatted_list,
            'more' => false
        ));
    }

    public function process_job(Request $request)
    {
        //check that the pin code is valid
        if ($this->check_code($request->input('pin_id'), $request->input('code'))) {
            $problem_items = array();
            foreach ($request->input('item') as $item) {
                $processitem = $this->process_item_job($item, $request->input('id'), $request->input('pin_id'));
                if ($processitem == true) {
                    //Thats fine the item should of been issued save this incase we need to do somthing here
                } else {
                    //Lets take the problem row id and put it in the array, the problem description if there is one should be added at this point so we can add info to the row with a problem
                    $problem_items[] = array('row-id' => $item['row-id'], 'problem' => $processitem);
                }
            }

            if (count($problem_items) > 0) {
                return response()->json([
                    'text' => 'There were some problems with certain items, all successfully submitted rows have been removed.',
                    'rows' => $problem_items
                ], 500);
            } else {
                check_prices('job', $request->input('id'));
                return response()->json([
                    'text' => 'Stock issued succesfully.'
                ], 200);
            }
        } else {
            return response()->json([
                'text' => 'No pin or the wrong pin was entered.'
            ], 500);
        }
    }

    public function check_code($user_id, $pin_code)
    {
        $lookup = Pincodes::where('id', $user_id)
            ->where('code', $pin_code)
            ->where('active', '1')
            ->get();
        if ($lookup->count() == 0) {
            return false;
        } else {
            return true;
        }
    }

    public function process_item_job($issueItem, $id, $pin_id)
    {
        if($issueItem['qty'] != 0) {
            if ($issueItem['id'] != 'misc') {
                $item = Item::where('id', $issueItem['id'])->first();
                $location = Location::where('id', $issueItem['location'])->first();
                //Check item and location were both found
                if (!$item || !$location) {
                    return 'Item or Location was not found on the system, remove the item and try again, if this problem continues contact support.';
                }
                //Check stock level because there could be an instance where some mongo adds the item twice to avoid my JS validation check
                //ooooor! at the same time on 2 or more machines because people are fuckers especially bosses who nit pick all day long over validation because they have nothing better to do.
                if ($location->qty < $issueItem['qty']) {
                    return 'The quantity you have in stock is less than the amount you tried to issue, remove the item and try again.';
                }
                try {
                    //try to do the issue
                    $issue = new JobItems();
                    $issue->item_id = $issueItem['id'];
                    $issue->job_id = $id;
                    if (preg_match("/([A-Z]|[a-z])/", $issueItem['element'])) {
                        $ele = new Element();
                        $ele->name = ucfirst($issueItem['element']);
                        $ele->save();
                        $issue->element_id = $ele->id;
                    } else {
                        $issue->element_id = $issueItem['element'];
                    }
                    $issue->qty = $issueItem['qty'];
                    $issue->line_cost = $issueItem['cost'];
                    $issue->line_real_cost = $issueItem['real_cost'];
                    $issue->line_unit_cost = $issueItem['unit_cost'];
                    $issue->pin_id = $pin_id;
                    $issue->location_id = $issueItem['location'];

                    $location->qty = ($location->qty - floatval($issueItem['qty']));

                    Session::set('pin_id', $pin_id);
                    if ($issue->save() && $location->save()) {
                        if (isset($issueItem['size'])) {
                            foreach ($issueItem['size'] as $size) {
                                try {
                                    $itemSize = new JobSize();
                                    $itemSize->job_item_id = $issue->id;
                                    $itemSize->size_unit_id = $issueItem['unit'];
                                    $itemSize->size = $size;
                                    $itemSize->save();
                                } catch (Exception $exp) {
                                    return 'Failed to save this items sizes, please contact support.';
                                }
                            }
                        }

                        return true;
                    } else {
                        Session::forget('pin_id');

                        return 'Failed to save this issue, the quantity may have been reduced please contact support.';
                    }
                } catch (Exception $exp) {
                    Session::forget('pin_id');

                    return 'Unknown issue when trying to issue this item, please remove it and try again, if this problem continues contact support.';
                }
                //double check item stock level is still ok and someone didn't just check it out.
            } else {
                try {
                    $issue = new JobItems();
                    $issue->item_id = '0';
                    if (preg_match("/([A-Z]|[a-z])/", $issueItem['element'])) {
                        $ele = new Element();
                        $ele->name = ucfirst($issueItem['element']);
                        $ele->save();
                        $issue->element_id = $ele->id;
                    } else {
                        $issue->element_id = $issueItem['element'];
                    }
                    $issue->job_id = $id;
                    $issue->pin_id = $pin_id;
                    $issue->line_cost = $issueItem['cost'];
                    $issue->line_real_cost = $issueItem['real_cost'];
                    $issue->qty = $issueItem['qty'];
                    Session::set('pin_id', $pin_id);
                    if ($issue->save()) {
                        try {
                            $misc = new JobMisc();
                            $misc->job_item_id = $issue->id;
                            $misc->name = $issueItem['name'];
                            $misc->description = nl2br($issueItem['item-details']);
                            if ($misc->save()) {
                                return true;
                            }
                        } catch (Exception $exp) {
                            //need to delete the first job part
                            $delete = JobItems::find($issue->id);
                            if ($delete->delete()) {
                                Session::forget('pin_id');

                                return 'Unknown issue when trying to issue this misc item, please remove it and try again, if this problem continues contact support.';
                            } else {
                                Session::forget('pin_id');

                                return 'Please contact support about this because there has been a problem which will leave orphaned data in the system if its not dealt with. Do not close this screen!';
                            }
                        }
                    }
                } catch (Exception $exp) {
                    return 'Unknown issue when trying to issue this item, please remove it and try again, if this problem continues contact support.';
                }
            }
        } else {
            return true;
        }
    }

    public function process_counter(Request $request)
    {
        //check that the pin code is valid
        if ($this->check_code($request->input('pin_id'), $request->input('code'))) {
            $problem_items = array();
            foreach ($request->input('item') as $item) {
                $processitem = $this->process_item_counter($item, $request->input('id'), $request->input('pin_id'));
                if ($processitem == true) {
                    //Thats fine the item should of been issued save this incase we need to do somthing here
                } else {
                    //Lets take the problem row id and put it in the array, the problem description if there is one should be added at this point so we can add info to the row with a problem
                    $problem_items[] = array('row-id' => $item['row-id'], 'problem' => $processitem);
                }
            }

            if (count($problem_items) > 0) {
                return response()->json([
                    'text' => 'There were some problems with certain items, all successfully submitted rows have been removed.',
                    'rows' => $problem_items
                ], 500);
            } else {
                check_prices('counter', $request->input('id'));
                return response()->json([
                    'text' => 'Stock issued succesfully.'
                ], 200);
            }
        } else {
            return response()->json([
                'text' => 'No pin or the wrong pin was entered.'
            ], 500);
        }
    }

    public function process_item_counter($issueItem, $id, $pin_id)
    {
        Session::set('pin_id', $pin_id);
        if($issueItem['qty'] != 0){
            switch ($issueItem['id']) {
                //
                // Misc items on a counter sale
                //
                case 'misc':
                    try {
                        $issue = new CounterItems();
                        $issue->item_id = '0';
                        $issue->counter_id = $id;
                        $issue->pin_id = $pin_id;
                        $issue->line_cost = $issueItem['cost'];
                        $issue->line_unit_cost = $issueItem['unit_cost'];
                        $issue->line_real_cost = $issueItem['real_cost'];
                        $issue->qty = $issueItem['qty'];
                        Session::set('pin_id', $pin_id);
                        if ($issue->save()) {
                            try {
                                $misc = new CounterMisc();
                                $misc->counter_item_id = $issue->id;
                                $misc->name = $issueItem['name'];
                                $misc->description = nl2br($issueItem['item-details']);
                                if ($misc->save()) {
                                    return true;
                                }
                            } catch (Exception $exp) {
                                //need to delete the first job part
                                $delete = JobItems::find($issue->id);
                                if ($delete->delete()) {
                                    Session::forget('pin_id');

                                    return 'Unknown issue when trying to issue this misc item, please remove it and try again, if this problem continues contact support.';
                                } else {
                                    Session::forget('pin_id');

                                    return 'Please contact support about this because there has been a problem which will leave orphaned data in the system if its not dealt with. Do not close this screen!';
                                }
                            }
                        }
                    } catch (Exception $exp) {
                        return 'Unknown issue when trying to issue this item, please remove it and try again, if this problem continues contact support.';
                    }
                    break;
                //
                // Default would be barcoded items on a counter sale
                //
                default:
                    $item = Item::where('id', $issueItem['id'])->first();
                    $location = Location::where('id', $issueItem['location'])->first();
                    //Check item and location were both found
                    if (!$item || !$location) {
                        return 'Item or Location was not found on the system, remove the item and try again, if this problem continues contact support.';
                    }
                    //Check stock level because there could be an instance where some mongo adds the item twice to avoid my JS validation check
                    //Look out for the phrase "I will review it and give you my input".... that means you can expect to redo hours possibly days of work with no justification as to why other than "it should work like this" ...

                    if ($location->qty < $issueItem['qty']) {
                        return 'The quantity you have in stock is less than the amount you are trying to issue, remove the item and try again.';
                    }
                    try {
                        //try to do the issue
                        $issue = new CounterItems();
                        $issue->item_id = $issueItem['id'];
                        $issue->counter_id = $id;
                        $issue->qty = $issueItem['qty'];
                        $issue->line_cost = $issueItem['cost'];
                        $issue->line_unit_cost = $issueItem['unit_cost'];
                        $issue->line_real_cost = $issueItem['real_cost'];
                        $issue->pin_id = $pin_id;
                        $issue->location_id = $issueItem['location'];

                        $location->qty = ($location->qty - floatval($issueItem['qty']));

                        Session::set('pin_id', $pin_id);
                        if ($issue->save() && $location->save()) {
                            if (isset($issueItem['size'])) {
                                foreach ($issueItem['size'] as $size) {
                                    try {
                                        $itemSize = new CounterSize();
                                        $itemSize->counter_item_id = $issue->id;
                                        $itemSize->size_unit_id = $issueItem['unit'];
                                        $itemSize->size = $size;
                                        $itemSize->save();
                                    } catch (Exception $exp) {
                                        return 'Failed to save this items sizes, please contact support.';
                                    }
                                }
                            }

                            return true;
                        } else {
                            Session::forget('pin_id');

                            return 'Failed to save this issue, the quantity may have been reduced please contact support.';
                        }
                    } catch (Exception $exp) {
                        Session::forget('pin_id');

                        return 'Unknown issue when trying to issue this item, please remove it and try again, if this problem continues contact support.';
                    }
                    break;
            }
        } else {
            return true;
        }
    }

    public function destroy(Request $request)
    {

        if (!$request->has('type')) {
            return Response::json([
                'error' => [
                    'exception' => 'Type not defined',
                ]
            ], 500);
        }

        $code = Pincodes::where('id', $request->input('pin_id'))->limit(1)->get();
        if ($code[0]->code == $request->input('code')) {
            Session::set('pin_id', $request->input('pin_id'));
            Session::set('reason', 'Item returning to stock from job: ' . $request->input('reason'));

            $item = false;
            $misc = false;
            switch ($request->input('type')) {
                case 'Counter':
                    $item = CounterItems::where('id', $request->input('delete-id'))->first();
                    $misc = CounterMisc::where('counter_item_id', $request->input('delete-id'))->first();
                    break;
                case 'Job':
                    $item = JobItems::where('id', $request->input('delete-id'))->first();
                    $misc = JobMisc::where('job_item_id', $request->input('delete-id'))->first();
                    break;
            }

            if ($item != false && $misc != false) {
                $misc->delete();
                $item->delete();

                return Response::json(array(
                    'success' => true,
                    'data' => 'Misc item deleted successfully'
                ));
            } else {
                return Response::json([
                    'error' => [
                        'exception' => 'Couldn\'t find item.',
                    ]
                ], 500);
            }
        } else {
            return Response::json([
                'error' => [
                    'exception' => 'Wrong Code Entered.',
                ]
            ], 500);
        }
    }

    public function takeback(Request $request)
    {
        if (!$request->has('type')) {
            return Response::json([
                'error' => [
                    'exception' => 'Type not defined',
                ]
            ], 500);
        }
        $code = Pincodes::where('id', $request->input('pin_id'))->limit(1)->get();
        if (!$request->has('reason')) {
            return Response::json([
                'error' => [
                    'exception' => 'Reason for return to stock required.',
                ]
            ], 500);
        }

        if ($code[0]->code == $request->input('pin')) {
            Session::set('pin_id', $request->input('pin_id'));
            Session::set('reason', 'Item returning to stock from job: ' . $request->input('reason'));

            $takeback = false;

            switch ($request->input('type')) {
                case 'counter':
                    $takeback = CounterItems::where('id', $request->input('item_id'))->first();
                    break;

                case 'job':
                    $takeback = JobItems::where('id', $request->input('item_id'))->first();
                    break;
            }

            if (!$takeback) {
                return Response::json([
                    'error' => [
                        'exception' => 'Item not found.',
                    ]
                ], 500);
            }

            if ($takeback->item_id == '0') {
                //Item is misc, only option is to remove it instead of increating the qty back to stock

                switch ($request->input('type')) {
                    case 'counter':
                        $misc = CounterMisc::where('job_item_id', $request->input('item_id'))->first();
                        break;

                    case 'job':
                        $misc = JobMisc::where('job_item_id', $request->input('item_id'))->first();
                        break;
                }
                if (!$misc) {
                    Session::forget('pin_id');
                    Session::forget('reason');

                    return Response::json([
                        'error' => [
                            'exception' => 'Item not found.',
                        ]
                    ], 500);
                }


                if ($misc->delete() && $takeback->delete()) {
                    switch ($request->input('type')) {
                        case 'counter':
                            $item = CounterItems::where('id', '=', $misc->counter_item_id)->first();
                            check_prices('counter', $item->counter_id);
                            break;

                        case 'job':
                            $item = JobItems::where('id', '=', $misc->job_item_id)->first();
                            check_prices('counter', $item->job_id);
                            break;
                    }
                    return Response::json(array(
                        'success' => true,
                        'data' => 'Item removed from job successfully, Redirecting.'
                    ));
                } else {
                    Session::forget('pin_id');
                    Session::forget('reason');

                    return Response::json([
                        'error' => [
                            'exception' => 'Problem removing this item, please contact support.'
                        ]
                    ], 500);
                }
            } else {
                $location = Location::where('id', $request->input('item_return_location'))->first();
                if (!$location) {
                    Session::forget('pin_id');
                    Session::forget('reason');

                    return Response::json([
                        'error' => [
                            'exception' => 'Problem finding the location this item came from, please contact support.'
                        ]
                    ], 500);
                }

                if ($request->input('qty') == $takeback->qty) {
                    $location->qty = ($location->qty + (int)$request->input('qty'));
                    if ($location->save() && $takeback->delete()) {

                        switch ($request->input('type')) {
                            case 'counter':
                                check_prices('counter', $takeback->counter_id);
                                break;

                            case 'job':
                                check_prices('counter', $takeback->job_id);
                                break;
                        }

                        return Response::json(array(
                            'success' => true,
                            'data' => 'Item returned to stock successfully, Redirecting.'
                        ));
                    }
                } elseif($request->input('qty') < $takeback->qty) {
                    if ($takeback->qty == $request->input('qty')) {
                        $takeback->deleted_at = date("Y-m-d H:i:s");
                    }
                    $takeback->qty = ($takeback->qty - (int)$request->input('qty'));
                    $takeback->line_cost = number_format((float)$takeback->line_unit_cost * $takeback->qty, 2, '.', '');
                    if ($location->save() && $takeback->save()) {

                        switch ($request->input('type')) {
                            case 'counter':
                                check_prices('counter', $takeback->counter_id);
                                break;

                            case 'job':
                                check_prices('counter', $takeback->job_id);
                                break;
                        }

                        return Response::json(array(
                            'success' => true,
                            'data' => 'Item returned to stock successfully, Redirecting.'
                        ));
                    }
                } else {
                    Session::forget('pin_id');
                    Session::forget('reason');

                    return Response::json([
                        'error' => [
                            'exception' => 'Problem returning item to stock, please check and try again.'
                        ]
                    ], 500);
                }
            }
        } else {
            Session::forget('pin_id');
            Session::forget('reason');

            return Response::json([
                'error' => [
                    'exception' => 'Wrong Code Entered.',
                ]
            ], 500);
        }
    }

    public function tocounter(Request $request)
    {
        $saved = false;
        Session::set('pin_id', $request->input('pin_id'));
        $jobItem = JobItems::where('id', '=', $request->input('job_item_id'))->first();

        if (!$jobItem) {
            return Response::json([
                'error' => [
                    'exception' => 'Item not found.',
                ]
            ], 500);
        }

        $counterItem = new CounterItems();
        $counterItem->pin_id = $request->input('pin_id');
        $counterItem->item_id = $jobItem->item_id;
        $counterItem->qty = $jobItem->qty;
        $counterItem->counter_id = $request->input('counter_id');
        $counterItem->order_id = $jobItem->order_id;
        $counterItem->line_cost = $jobItem->line_cost;
        $counterItem->location_id = $jobItem->location_id;
        $counterItem->line_real_cost = $jobItem->line_real_cost;
        if ($counterItem->save()) {

            if (isset($jobItem->misc)) {
                //Item is misc, only option is to remove it instead of increating the qty back to stock
                $counterMisc = new CounterMisc();
                $counterMisc->counter_item_id = $counterItem->id;
                $counterMisc->name = $jobItem->misc->name;
                $counterMisc->description = $jobItem->misc->description;
                if ($counterMisc->save()) {
                    $jobItem->misc->delete();
                    $jobItem->delete();
                    return Response::json(array(
                        'success' => true,
                        'data' => 'Item moved to counter sale successfully, Redirecting.'
                    ));
                }
            } else {
                $jobItem->delete();
                return Response::json(array(
                    'success' => true,
                    'data' => 'Item moved to counter sale successfully, Redirecting.'
                ));
            }
        } else {
            return Response::json([
                'error' => [
                    'exception' => 'Failed to transfer item.',
                ]
            ], 500);
        }
    }

    public function tojob(Request $request)
    {
        Session::set('pin_id', $request->input('pin_id'));
        $counterItem = CounterItems::where('id', '=', $request->input('counter_item_id'))->with('misc')->first();

        if (!$counterItem) {
            return Response::json([
                'error' => [
                    'exception' => 'Item not found.',
                ]
            ], 500);
        }

        $jobItem = new JobItems();
        $jobItem->pin_id = $request->input('pin_id');
        $jobItem->item_id = $counterItem->item_id;
        $jobItem->qty = $counterItem->qty;
        $jobItem->job_id = $request->input('job_id');
        $jobItem->order_id = $counterItem->order_id;
        $jobItem->line_cost = $counterItem->line_cost;
        $jobItem->location_id = $counterItem->location_id;
        $jobItem->line_real_cost = $counterItem->line_real_cost;

        if ($jobItem->save()) {
            if (isset($counterItem->misc)) {
                $jobMisc = new JobMisc();
                $jobMisc->job_item_id = $jobItem->id;
                $jobMisc->name = $counterItem->misc->name;
                $jobMisc->description = $counterItem->misc->description;
                if ($jobMisc->save()) {
                    $counterItem->misc->delete();
                    $counterItem->delete();

                    return Response::json(array(
                        'success' => true,
                        'data' => 'Item moved to job successfully, Redirecting.'
                    ));
                }
            } else {
                $counterItem->delete();

                return Response::json(array(
                    'success' => true,
                    'data' => 'Item moved to job successfully, Redirecting.'
                ));
            }
        } else {
            return Response::json([
                'error' => [
                    'exception' => 'Failed to transfer item.',
                ]
            ], 500);
        }

    }


    public function cost_job_update(Request $request)
    {
        $job_item = JobItems::where('id', $request->input('job_item_id'))->first();
        if ($job_item) {

            if ($job_item->item_id == 0) {
                //misc item

                //sale or cost
                switch($request->input('sale_real')){
                    case 'sale':
                        $sale_cost = number_format((float)$request->input('sale_cost'), 2, '.', '');
                        $job_item->line_unit_cost = $sale_cost;
                        $job_item->line_cost = number_format((float)$job_item->qty * $sale_cost, 2, '.', '');
                        break;

                    case 'cost':
                        $unit_cost = number_format((float)$request->input('real_cost'), 2, '.', '');
                        $unit_markup = ($unit_cost * 0.2) + $unit_cost;
                        $job_item->line_real_cost = $unit_cost;
                        $job_item->line_unit_cost = $unit_markup;
                        $job_item->line_cost = number_format((float)$job_item->qty * $unit_markup, 2, '.', '');
                        break;
                }

            } else {
                $item = Item::where('id', '=', $job_item->item_id)->with('last_price')->first();
                if (!$item) {
                    return Response::json(array(
                        'success' => false,
                        'text' => 'Couldn\'t find item.'
                    ));
                }

                //sale or cost
                switch($request->input('sale_real')){
                    case 'sale':
                        $sale_cost = number_format((float)$request->input('sale_cost'), 2, '.', '');
                        $job_item->line_unit_cost = $sale_cost;
                        $job_item->line_cost = number_format((float)$job_item->qty * $sale_cost, 2, '.', '');
                        break;

                    case 'cost':
                        $unit_cost = number_format((float)$request->input('real_cost'), 2, '.', '');
                        $markup = 0;

                        if($item->markup_percent != 0){
                            $markup = (($unit_cost / 100) * $item->markup_percent) + $unit_cost;
                        }

                        if($item->markup_fixed != 0){
                            $markup = $item->markup_fixed;
                        }

                        $job_item->line_real_cost = $unit_cost;
                        $job_item->line_unit_cost = $markup;
                        $job_item->line_cost = number_format((float)$job_item->qty * $markup, 2, '.', '');
                        break;
                }
            }

            if ($job_item->save()) {
                check_prices('counter', $job_item->job_id);
                return Response::json(array(
                    'success' => true,
                    'text' => 'Cost updated successfully.'
                ));
            }
        }

        return Response::json(array(
            'success' => false,
            'text' => 'Problem finding the issued item.'
        ));
    }

    public function cost_counter_update(Request $request)
    {
        $counter_item = CounterItems::where('id', '=', $request->input('counter_item_id'))->first();
        if ($counter_item) {
            if ($counter_item->item_id == 0) {
                //misc item

                //sale or cost
                switch($request->input('sale_real')){
                    case 'sale':
                        $sale_cost = number_format((float)$request->input('sale_cost'), 2, '.', '');
                        $counter_item->line_unit_cost = $sale_cost;
                        $counter_item->line_cost = number_format((float)$counter_item->qty * $sale_cost, 2, '.', '');
                        break;

                    case 'cost':
                        $unit_cost = number_format((float)$request->input('real_cost'), 2, '.', '');
                        $unit_markup = ($unit_cost * 0.2) + $unit_cost;
                        $counter_item->line_real_cost = $unit_cost;
                        $counter_item->line_unit_cost = $unit_markup;
                        $counter_item->line_cost = number_format((float)$counter_item->qty * $unit_markup, 2, '.', '');
                        break;
                }

            } else {
                $item = Item::where('id', '=', $counter_item->item_id)->with('last_price')->first();
                if (!$item) {
                    return Response::json(array(
                        'success' => false,
                        'text' => 'Couldn\'t find item.'
                    ));
                }

                //sale or cost
                switch($request->input('sale_real')){
                    case 'sale':
                        $sale_cost = number_format((float)$request->input('sale_cost'), 2, '.', '');
                        $counter_item->line_unit_cost = $sale_cost;
                        $counter_item->line_cost = number_format((float)$counter_item->qty * $sale_cost, 2, '.', '');
                        break;

                    case 'cost':
                        $unit_cost = number_format((float)$request->input('real_cost'), 2, '.', '');
                        $markup = 0;

                        if($item->markup_percent != 0){
                            $markup = (($unit_cost / 100) * $item->markup_percent) + $unit_cost;
                        }

                        if($item->markup_fixed != 0){
                            $markup = $item->markup_fixed;
                        }

                        $counter_item->line_real_cost = $unit_cost;
                        $counter_item->line_unit_cost = $markup;
                        $counter_item->line_cost = number_format((float)$counter_item->qty * $markup, 2, '.', '');
                        break;
                }
            }
            if ($counter_item->save()) {
                check_prices('counter', $counter_item->counter_id);
                return Response::json(array(
                    'success' => true,
                    'text' => 'Cost updated successfully.'
                ));
            }
        }

        return Response::json(array(
            'success' => false,
            'text' => 'Problem finding the issued item.'
        ));
    }


    public function transfer_counter_job(Request $request)
    {
        // $pin_id = $request->input('pin_id');
        // $code = Pincodes::where('id', $request->input('pin_id'))->limit(1)->get();
        // if (!$request->has('reason')) {
        //     return Response::json([
        //         'error' => [
        //             'text' => 'Reason for transfer is required.',
        //         ]
        //     ], 500);
        // }

        // if ($code[0]->code == $request->input('pin')) {
            // Session::set('pin_id', $pin_id);
            // Session::set('reason', 'Transfering Counter / Job: ' . $request->input('reason'));

            switch ($request->input('type')) {
                //a job to a counter sale
                case('counter'):
                    $job = Job::where('id',
                        $request->input('id'))
                        ->with('job_items')
                        ->with('labour')
                        ->with('carriage')
                        ->with('crane')
                        ->first();

                    //transfer over the counter to a job
                    $counter = new Counter();
                    $counter->customer_id = $job->customer_id;
                    $counter->boat_id = $job->boat_id;
                    $counter->note = $job->note;
                    $counter->work_scope = $job->work_scope;
                    $counter->po = $job->po;
                    $counter->closed_at = $job->closed_at;
                    $counter->created_at = $job->created_at;

                    if ($counter->save()) {
                        //loop items to transfer them
                        if (isset($job->job_items)) {
                            foreach ($job->job_items as $item) {
                                if (!$this->item_to_counter($counter->id, $item->id)) {
                                    return Response::json([
                                        'error' => [
                                            'text' => 'Transfer failed while transferring items, contact support!',
                                        ]
                                    ], 500);
                                }
                            }
                        }

                        //loop labour

                        if (isset($job->labour)) {
                            foreach ($job->labour as $labour) {
                                if (!$this->labour_to_counter($counter->id, $labour->id)) {
                                    return Response::json([
                                        'error' => [
                                            'text' => 'Transfer failed while transferring labour items, contact support!',
                                        ]
                                    ], 500);
                                }
                            }
                        }

                        //loop carriage
                        if (isset($job->carriage)) {
                            foreach ($job->carriage as $carriage) {
                                if (!$this->carriage_to_counter($counter->id, $carriage->id)) {
                                    return Response::json([
                                        'error' => [
                                            'text' => 'Transfer failed while transferring carriage items, contact support!',
                                        ]
                                    ], 500);
                                }
                            }
                        }

                        //loop crane works
                        if (isset($job->crane)) {
                            foreach ($job->crane as $crane) {
                                if (!$this->crane_to_counter($counter->id, $crane->id)) {
                                    return Response::json([
                                        'error' => [
                                            'text' => 'Transfer failed while transferring crane items, contact support!',
                                        ]
                                    ], 500);
                                }
                            }
                        }

                        if ($job->delete()) {
                            return Response::json([
                                'success' => [
                                    'text' => 'Transfered counter sale to job successfully.',
                                ]
                            ], 200);
                        }
                    }
                    break;


                //Transfering a counter sale to a job
                case('job'):
                    $counter = Counter::where('id',
                        $request->input('id'))
                        ->with('items')
                        ->with('labour')
                        ->with('carriage')
                        ->with('crane')->first();

                    //transfer over the counter to a job
                    $job = new Job();
                    $job->customer_id = $counter->customer_id;
                    $job->boat_id = $counter->boat_id;
                    $job->note = $counter->note;
                    $job->work_scope = $counter->work_scope;
                    $job->po = $counter->po;
                    $job->closed_at = $counter->closed_at;
                    $job->created_at = $counter->created_at;

                    if ($job->save()) {
                        //loop items to transfer them
                        if (isset($counter->items)) {
                            foreach ($counter->items as $item) {
                                if (!$this->item_to_job($job->id, $item->id)) {
                                    return Response::json([
                                        'error' => [
                                            'text' => 'Transfer failed while transferring items, contact support!',
                                        ]
                                    ], 500);
                                }
                            }
                        }

                        //loop labour
                        if (isset($counter->labour)) {
                            foreach ($counter->labour as $labour) {
                                if (!$this->labour_to_job($job->id, $labour->id)) {
                                    return Response::json([
                                        'error' => [
                                            'text' => 'Transfer failed while transferring labour items, contact support!',
                                        ]
                                    ], 500);
                                }
                            }
                        }

                        //loop carriage
                        if (isset($counter->carriage)) {
                            foreach ($counter->carriage as $carriage) {
                                if (!$this->carriage_to_job($job->id, $carriage->id)) {
                                    return Response::json([
                                        'error' => [
                                            'text' => 'Transfer failed while transferring carriage items, contact support!',
                                        ]
                                    ], 500);
                                }
                            }
                        }

                        //loop crane works
                        if (isset($counter->crane)) {
                            foreach ($counter->crane as $crane) {
                                if (!$this->crane_to_job($job->id, $crane->id)) {
                                    return Response::json([
                                        'error' => [
                                            'text' => 'Transfer failed while transferring crane items, contact support!',
                                        ]
                                    ], 500);
                                }
                            }
                        }

                        if ($counter->delete()) {
                            return Response::json([
                                'success' => [
                                    'text' => 'Transfered counter sale to job successfully.',
                                ]
                            ], 200);
                        }
                    } else {
                        return Response::json([
                            'error' => [
                                'text' => 'Failed to save counter sale as job.',
                            ]
                        ], 500);
                    }
                    break;

                default:
                    return Response::json([
                        'error' => [
                            'text' => 'Sale / Job type is undefined.',
                        ]
                    ], 500);
                    break;
            }
        // }
    }


    public function item_to_counter($counter_id, $item_id)
    {
        $jobItem = JobItems::where('id', $item_id)->with('misc')->first();
        if (!$jobItem) {
            return false;
        }
        $counterItem = new CounterItems();
        $counterItem->pin_id = $jobItem->pin_id;
        $counterItem->item_id = $jobItem->item_id;
        $counterItem->qty = $jobItem->qty;
        $counterItem->counter_id = $counter_id;
        $counterItem->order_id = $jobItem->order_id;
        $counterItem->location_id = $jobItem->location_id;
        $counterItem->line_cost = $jobItem->line_cost;
        $counterItem->line_unit_cost = $jobItem->line_unit_cost;
        $counterItem->line_real_cost = $jobItem->line_real_cost;
        $counterItem->created_at = $jobItem->created_at;


        if ($counterItem->save()) {
            if (isset($jobItem->misc) && !empty($jobItem->misc)) {
                $counterMisc = new CounterMisc();
                $counterMisc->counter_item_id = $counterItem->id;
                $counterMisc->name = $jobItem->misc->name;
                $counterMisc->description = $jobItem->misc->description;
                if ($counterMisc->save()) {
                    $jobItem->misc->delete();
                    $jobItem->delete();
                    return true;
                }
            } else {
                //because there is no misc item
                return true;
            }
        }

        print_r($counterItem);
        dd('Save Failed');

        return false;
    }

    public function labour_to_counter($counter_id, $labour_id)
    {
        $jobLabour = JobLabour::where('id', $labour_id)->first();
        if (!$jobLabour) {
            return false;
        }
        $counterLabour = new CounterLabour();
        $counterLabour->pin_id = $jobLabour->pin_id;
        $counterLabour->counter_id = $counter_id;
        $counterLabour->cost = $jobLabour->cost;
        $counterLabour->name = $jobLabour->name;
        $counterLabour->created_at = $jobLabour->created_at;
        if ($counterLabour->save()) {
            if ($jobLabour->delete()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function carriage_to_counter($counter_id, $carriage_id)
    {
        $jobCarriage = JobCarriage::where('id', $carriage_id)->first();
        if (!$jobCarriage) {
            return false;
        }
        $counterCarriage = new CounterCarriage();
        $counterCarriage->pin_id = $jobCarriage->pin_id;
        $counterCarriage->counter_id = $counter_id;
        $counterCarriage->cost = $jobCarriage->cost;
        $counterCarriage->name = $jobCarriage->name;
        $counterCarriage->created_at = $jobCarriage->created_at;
        if ($counterCarriage->save()) {
            if ($jobCarriage->delete()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function crane_to_counter($counter_id, $crane_id)
    {
        $jobCrane = JobCrane::where('id', $crane_id)->first();
        if (!$jobCrane) {
            return false;
        }
        $counterCrane = new CounterCrane();
        $counterCrane->pin_id = $jobCrane->pin_id;
        $counterCrane->counter_id = $counter_id;
        $counterCrane->cost = $jobCrane->cost;
        $counterCrane->name = $jobCrane->name;
        $counterCrane->created_at = $jobCrane->created_at;
        if ($counterCrane->save()) {
            if ($jobCrane->delete()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function item_to_job($job_id, $item_id)
    {
        $counterItem = CounterItems::where('id', $item_id)->with('misc')->first();
        if (!$counterItem) {
            return false;
        }
        $jobItem = new JobItems();
        $jobItem->pin_id = $counterItem->pin_id;
        $jobItem->item_id = $counterItem->item_id;
        $jobItem->qty = $counterItem->qty;
        $jobItem->job_id = $job_id;
        $jobItem->order_id = $counterItem->order_id;
        $jobItem->location_id = $counterItem->location_id;
        $jobItem->line_cost = $counterItem->line_cost;
        $jobItem->line_unit_cost = $counterItem->line_unit_cost;
        $jobItem->line_real_cost = $counterItem->line_real_cost;
        $jobItem->created_at = $counterItem->created_at;
        if ($jobItem->save()) {
            if (isset($counterItem->misc)) {
                $jobMisc = new JobMisc();
                $jobMisc->job_item_id = $jobItem->id;
                $jobMisc->name = $counterItem->misc->name;
                $jobMisc->description = $counterItem->misc->description;
                if ($jobMisc->save()) {
                    $counterItem->misc->delete();
                    $counterItem->delete();

                    return true;
                }
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    public function labour_to_job($job_id, $labour_id)
    {
        $counterLabour = CounterLabour::where('id', $labour_id)->first();
        if (!$counterLabour) {
            return false;
        }
        $jobLabour = new JobLabour();
        $jobLabour->pin_id = $counterLabour->pin_id;
        $jobLabour->job_id = $job_id;
        $jobLabour->cost = $counterLabour->cost;
        $jobLabour->name = $counterLabour->name;
        $jobLabour->created_at = $counterLabour->created_at;
        if ($jobLabour->save()) {
            if ($counterLabour->delete()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function carriage_to_job($job_id, $carriage_id)
    {
        $counterCarriage = CounterCarriage::where('id', $carriage_id)->first();
        if (!$counterCarriage) {
            return false;
        }
        $jobCarriage = new JobCarriage();
        $jobCarriage->pin_id = $counterCarriage->pin_id;
        $jobCarriage->job_id = $job_id;
        $jobCarriage->cost = $counterCarriage->cost;
        $jobCarriage->name = $counterCarriage->name;
        $jobCarriage->created_at = $counterCarriage->created_at;
        if ($jobCarriage->save()) {
            if ($counterCarriage->delete()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function crane_to_job($job_id, $crane_id)
    {
        $counterCrane = CounterCrane::where('id', $crane_id)->first();
        if (!$counterCrane) {
            return false;
        }
        $jobCrane = new JobCrane();
        $jobCrane->pin_id = $counterCrane->pin_id;
        $jobCrane->job_id = $job_id;
        $jobCrane->cost = $counterCrane->cost;
        $jobCrane->name = $counterCrane->name;
        $jobCrane->created_at = $counterCrane->created_at;
        if ($jobCrane->save()) {
            if ($counterCrane->delete()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
