<?php

namespace App\Http\Controllers;

use App\DataTables\CustomerDataTable;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\CreateCustomerRequest;
use App\Http\Requests\UpdateCustomerRequest;
use App\Models\Customer;
use App\Repositories\CustomerRepository;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Response;
use App\Models\Address;

class CustomerController extends AppBaseController
{
    /** @var  CustomerRepository */
    private $customerRepository;

    public function __construct(CustomerRepository $customerRepo)
    {
        $this->customerRepository = $customerRepo;
    }

    /**
     * Display a listing of the Customer.
     *
     * @param CustomerDataTable $customerDataTable
     * @return Response
     */
    public function index(CustomerDataTable $customerDataTable)
    {
        return $customerDataTable->render('customers.index');
    }

    /**
     * Show the form for creating a new Customer.
     *
     * @return Response
     */
    public function create()
    {
        return view('customers.create');
    }

    /**
     * Store a newly created Customer in storage.
     *
     * @param CreateCustomerRequest $request
     *
     * @return Response
     */
    public function store(CreateCustomerRequest $request)
    {
        $input = $request->all();

        $customer = $this->customerRepository->create($input);

        if (!empty($request->input('address_id')) && ($request->input('address_id') != $customer->address_id)) {
            $customer->address_id = $request->input('address_id');
            $customer->address_id = $request->input('address_id');
            $customer->save();
            $customer->save();
        } else {
            if (!empty($request->input('line_1')) || !empty($request->input('line_2')) || !empty($request->input('town')) || !empty($request->input('county')) || !empty($request->input('postcode'))) {
                //Need somthing from the above to distinguish the address
                $address = new Address;
                if (!empty($request->input('line_1'))) {
                    $address->line_1 = $request->input('line_1');
                }
                if (!empty($request->input('line_2'))) {
                    $address->line_2 = $request->input('line_2');
                }
                if (!empty($request->input('town'))) {
                    $address->town = $request->input('town');
                }
                if (!empty($request->input('county'))) {
                    $address->county = $request->input('county');
                }
                if (!empty($request->input('postcode'))) {
                    $address->postcode = $request->input('postcode');
                }
                if (!empty($request->input('lng'))) {
                    $address->lng = $request->input('lng');
                }
                if (!empty($request->input('lat'))) {
                    $address->lat = $request->input('lat');
                }
                $address->save();
                $customer->address_id = $address->id;
                $customer->address_id = $address->id;
                $customer->save();
                $customer->save();
            }
        }
        Flash::success('Customer saved successfully.');

        return redirect(route('customers.index'));
    }

    /**
     * Display the specified Customer.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $customer = $this->customerRepository->findWithoutFail($id);

        if (empty($customer)) {
            Flash::error('Customer not found');

            return redirect(route('customers.index'));
        }

        return view('customers.show')->with('customer', $customer);
    }

    /**
     * Show the form for editing the specified Customer.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $customer = $this->customerRepository->findWithoutFail($id);
        $address = Address::find($customer->address_id);

        if (empty($customer)) {
            Flash::error('Customer not found');

            return redirect(route('customers.index'));
        }

        return view('customers.edit')->with('customer', $customer)->with('address', $address);
    }

    /**
     * Update the specified Customer in storage.
     *
     * @param  int $id
     * @param UpdateCustomerRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCustomerRequest $request)
    {
        $customer = $this->customerRepository->findWithoutFail($id);
        if (empty($customer)) {
            Flash::error('Customer not found');

            return redirect(route('customers.index'));
        }
        $customer = $this->customerRepository->update($request->all(), $id);
        if (!empty($request->input('address_id')) && ($request->input('address_id') != $customer->address_id)) {
            $customer->address_id = $request->input('address_id');
            $customer->save();
        } else {
            if (!empty($request->input('line_1')) || !empty($request->input('line_2')) || !empty($request->input('town')) || !empty($request->input('county')) || !empty($request->input('postcode'))) {
                //Need somthing from the above to distinguish the address
                $address = new Address;
                if (!empty($request->input('line_1'))) {
                    $address->line_1 = $request->input('line_1');
                }
                if (!empty($request->input('line_2'))) {
                    $address->line_2 = $request->input('line_2');
                }
                if (!empty($request->input('town'))) {
                    $address->town = $request->input('town');
                }
                if (!empty($request->input('county'))) {
                    $address->county = $request->input('county');
                }
                if (!empty($request->input('postcode'))) {
                    $address->postcode = $request->input('postcode');
                }
                if (!empty($request->input('lng'))) {
                    $address->lng = $request->input('lng');
                }
                if (!empty($request->input('lat'))) {
                    $address->lat = $request->input('lat');
                }
                $address->save();
                $customer->address_id = $address->id;
                $customer->save();
            }
        }
        Flash::success('Customer updated successfully.');

        return redirect(route('customers.index'));
    }

    public function lookup($id)
    {

    }


    /**
     * Remove the specified Customer from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $customer = $this->customerRepository->findWithoutFail($id);
        if (empty($customer)) {
            Flash::error('Customer not found');
            return redirect(route('customers.index'));
        }
        $this->customerRepository->delete($id);
        Flash::success('Customer deleted successfully.');
        return redirect(route('customers.index'));
    }


    public function JsonLookup(Request $request)
    {
        // $customers = Customer::with('address')->orderBy('name', 'ASC');
        // $customers = Customer::with(['address'=>function($query) use ($request) {
        //     if ($request->has('q')) {
        //         $val = $request->input('q')['term'];
        //         $query->where('line_1', 'like', '%'.$val.'%');
        //         $query->orWhere('line_2', 'like', '%'.$val.'%');
        //         $query->orWhere('town', 'like', '%'.$val.'%');
        //         $query->orWhere('county', 'like', '%'.$val.'%');
        //         $query->orWhere('postcode', 'like', '%'.$val.'%');
        //     }
        // }])->orderBy('name', 'ASC');
        if ($request->has('q')&&strlen($request->input('q')['term'])>=3) {
            $customers = Customer::query()->select('customer.*')->orderBy('customer.name', 'ASC');
            $customers->leftJoin('address','customer.address_id', '=', 'address.id');
            // foreach ($request->input('q') as $val) {
            $val = $request->input('q')['term'];
            $customers->where('name', 'like', '%'.$val.'%');
            $customers->orWhere('sage_ref', 'like', '%'.$val.'%');
            $customers->orWhere('phone', 'like', '%'.$val.'%');
            $customers->orWhere('address.line_1', 'like', '%'.$val.'%');
            $customers->orWhere('address.line_2', 'like', '%'.$val.'%');
            $customers->orWhere('address.town', 'like', '%'.$val.'%');
            $customers->orWhere('address.county', 'like', '%'.$val.'%');
            $customers->orWhere('address.postcode', 'like', '%'.$val.'%');
            // }
        } else {
            $customers = Customer::with('address')->orderBy('name', 'ASC');
        }
        $results = $customers->get();
        $formatted_list = array();
        foreach ($results as $customer) {
            $name = '';
            if (strlen($customer->sage_ref) > 0) {
                $name .= $customer->sage_ref . ' : ';
            }
            $name .= $customer->name . ' : ';
            if ($customer->address&&strlen($customer->address->postcode) > 0) {
                $name .= $customer->address->line_1;
                if(!empty($customer->address->line_2)) $name .= ", ".$customer->address->line_2;
                if(!empty($customer->address->town)) $name .= ", ".$customer->address->town;
                if(!empty($customer->address->county)) $name .= ", ".$customer->address->county;
                if(!empty($customer->address->postcode)) $name .= ", ".$customer->address->postcode;
                $name .= ' : ';
            }
            $formatted_list[] = array(
                'id' => $customer->id,
                'text' => $name
            );
        }
        return Response::json(array('results' => $formatted_list));
    }
}
