<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class LoginController extends Controller
{

	public function index() {
		return view('pages.login.index');
	}

	public function login( Request $request ) {
		// validate the info, create rules for the inputs
		$this->validate( $request, [
			'email'    => 'required|email',
			'password' => 'required|alphaNum|min:3'
		] );

		$credentials = array(
			'email' => $request->only( 'email'),
			'password' =>$request->all()['password'],
			'active' => 1
		);

		if (Auth::attempt( $credentials, $request->has( 'remember' ) ) ) {
			return redirect('/');
		}

		return redirect('/login') 
			->withInput($request->only('email'))
			->withErrors([
				'email' =>  'The login credentials do not match our records'
			]);
	}

	public function logout() {
		Auth::logout();
		return redirect('/login');
	}

}
