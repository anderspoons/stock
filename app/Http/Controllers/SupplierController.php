<?php

namespace App\Http\Controllers;

use App\DataTables\SupplierDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateSupplierRequest;
use App\Http\Requests\UpdateSupplierRequest;
use App\Repositories\SupplierRepository;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Response;
use App\Models\Supplier;
use App\Models\Address;
use Illuminate\Http\Request;

class SupplierController extends AppBaseController
{
    /** @var  SupplierRepository */
    private $supplierRepository;

    public function __construct(SupplierRepository $supplierRepo)
    {
        $this->supplierRepository = $supplierRepo;
    }

    /**
     * Display a listing of the Supplier.
     *
     * @param SupplierDataTable $supplierDataTable
     * @return Response
     */
    public function index(SupplierDataTable $supplierDataTable)
    {
        return $supplierDataTable->render('suppliers.index');
    }

    /**
     * Show the form for creating a new Supplier.
     *
     * @return Response
     */
    public function create()
    {
        return view('suppliers.create');
    }

    /**
     * Store a newly created Supplier in storage.
     *
     * @param CreateSupplierRequest $request
     *
     * @return Response
     */
    public function store(CreateSupplierRequest $request)
    {
        $input = $request->all();

        $supplier = $this->supplierRepository->create($input);

        if(!empty($request->input('address_id'))) {
            $supplier->address_id = $request->input('address_id');
            $supplier->save();
        } else {
            if(!empty($request->input('line_1')) || !empty($request->input('line_2')) || !empty($request->input('town')) || !empty($request->input('county')) || !empty($request->input('postcode'))){
                //Need somthing from the above to distinguish the address
                $address = new Address;
                if(!empty($request->input('line_1'))){
                    $address->line_1 = $request->input('line_1');
                }
                if(!empty($request->input('line_2'))) {
                    $address->line_2 = $request->input( 'line_2' );
                }
                if(!empty($request->input('town'))){
                    $address->town = $request->input('town');
                }
                if(!empty($request->input('county'))) {
                    $address->county = $request->input( 'county' );
                }
                if(!empty($request->input('postcode'))) {
                    $address->postcode = $request->input( 'postcode' );
                }
                if(!empty($request->input('lng'))) {
                    $address->lng = $request->input( 'lng' );
                }
                if(!empty($request->input('lat'))) {
                    $address->lat = $request->input( 'lat' );
                }
                $address->save();
                $supplier->address_id = $address->id;
                $supplier->save();
            }
        }
        Flash::success('Supplier saved successfully.');

        return redirect(route('suppliers.index'));
    }

    /**
     * Display the specified Supplier.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $supplier = Supplier::with('address')->find($id);

        if (empty($supplier)) {
            Flash::error('Supplier not found');

            return redirect(route('suppliers.index'));
        }

        return view('suppliers.show')->with('supplier', $supplier);
    }

    /**
     * Show the form for editing the specified Supplier.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $supplier = Supplier::find($id);
        $address = Address::find($supplier->address_id);

        if (empty($supplier)) {
            Flash::error('Supplier not found');

            return redirect(route('suppliers.index'));
        }

        return view('suppliers.edit')->with('supplier', $supplier)->with('address', $address);
    }

    /**
     * Update the specified Supplier in storage.
     *
     * @param  int              $id
     * @param UpdateSupplierRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSupplierRequest $request)
    {
        $supplier = $this->supplierRepository->findWithoutFail($id);

        if (empty($supplier)) {
            Flash::error('Supplier not found');
            return redirect(route('suppliers.index'));
        }

        $supplier = $this->supplierRepository->update($request->all(), $id);

        if(!empty($request->input('address_id')) && ($request->input('address_id') != $supplier->address_id)) {
            $supplier->address_id = $request->input('address_id');
            $supplier->save();
        } else {
            if(!empty($request->input('line_1')) || !empty($request->input('line_2')) || !empty($request->input('town')) || !empty($request->input('county')) || !empty($request->input('postcode'))){
                //Need somthing from the above to distinguish the address
                $address = new Address;
                if(!empty($request->input('line_1'))){
                    $address->line_1 = $request->input('line_1');
                }
                if(!empty($request->input('line_2'))) {
                    $address->line_2 = $request->input( 'line_2' );
                }
                if(!empty($request->input('town'))){
                    $address->town = $request->input('town');
                }
                if(!empty($request->input('county'))) {
                    $address->county = $request->input( 'county' );
                }
                if(!empty($request->input('postcode'))) {
                    $address->postcode = $request->input( 'postcode' );
                }
                if(!empty($request->input('lng'))) {
                    $address->lng = $request->input( 'lng' );
                }
                if(!empty($request->input('lat'))) {
                    $address->lat = $request->input( 'lat' );
                }
                $address->save();
                $supplier->address_id = $address->id;
                $supplier->save();
            }
        }

        Flash::success('Supplier updated successfully.');

        return redirect(route('suppliers.index'));
    }

    /**
     * Remove the specified Supplier from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $supplier = $this->supplierRepository->findWithoutFail($id);

        if (empty($supplier)) {
            Flash::error('Supplier not found');

            return redirect(route('suppliers.index'));
        }

        $this->supplierRepository->delete($id);

        Flash::success('Supplier deleted successfully.');

        return redirect(route('suppliers.index'));
    }



	public function JsonLookup(Request $request) {
		$suppliers = Supplier::orderBy('name', 'ASC');
		if($request->has('q')){
			foreach($request->input('q') as $val){
				$suppliers->where('name', 'like', '%'.$val.'%');
				$suppliers->orWhere('code', 'like', '%'.$val.'%');
			}
		}
		$results = $suppliers->get();
		$formatted_list = array();
		foreach($results as $supplier){
			$text = '';
			if(strlen($supplier->code) > 0){
				$text .= $supplier->code .' : ';
			}
			if(strlen($supplier->name) > 0){
				$text .= $supplier->name;
			}
			$formatted_list[] = array(
				'id' => $supplier->id,
				'text' => $text
			);
		}
		return Response::json(array('results' => $formatted_list));
	}
}
