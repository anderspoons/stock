<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.


/*
|--------------------------------------------------------------------------
| API routes
|--------------------------------------------------------------------------
*/
//
//Route::group(['prefix' => 'api', 'namespace' => 'API'], function () {
//    Route::group(['prefix' => 'v1'], function () {
//        require config('infyom.laravel_generator.path.api_routes');
//    });
//});
//

Route::get('cron', 'CronController@doValues');
Route::get('cron_test', 'CronController@cron_tests');

Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', 'Auth\AuthController@postLogin');
Route::get('logout', 'Auth\AuthController@logout');

// Registration Routes...
Route::get('register', 'Auth\AuthController@getRegister');
Route::post('register', 'Auth\AuthController@postRegister');

// Password Reset Routes...
Route::get('password/reset', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');

Route::group(['middleware' => ['auth']], function () {
    Route::post('item/qty', 'ItemController@qty');
    Route::post('item/price', 'ItemController@price');

    Route::post('barcode', 'BarcodeController@generate');


	Route::get('barcode/pdf/{code}/', 'BarcodeController@pdf');
	Route::get('barcode/{code}/{style}', 'BarcodeController@index');
	Route::get('barcode', 'BarcodeController@index');

    Route::get('lookup/barcode/{code}', 'ItemController@lookup');
    Route::get('lookup/id/{code}', 'ItemController@lookup_id');
    Route::get('lookup/customer/{id}', 'CustomerController@lookup');

	Route::post('check_code', 'PincodesController@check_code');

	Route::get('pincodes/send/{id}', 'PincodesController@send');

	Route::get('jobs/issues/{id}', 'JobController@issue');
    Route::get('jobs/labour/{id}', 'JobController@labour');
    Route::get('jobs/crane/{id}', 'JobController@crane');
    Route::get('jobs/carriage/{id}', 'JobController@carriage');
	Route::get('jobs/excel/{id}', 'JobController@invoice');

	Route::post('jobs/close', 'JobController@close');
	Route::post('jobs/remove_close', 'JobController@remove_close');
    Route::post('jobs/remove_billed', 'JobController@remove_billed');
    Route::post('jobs/add_billed', 'JobController@add_billed');

	Route::post('issue/back-stock', 'IssueController@takeback');
	Route::post('issue/destroy/misc', 'IssueController@destroy');

	Route::post('issue/counter-transfer', 'IssueController@tocounter');
	Route::post('issue/job-transfer', 'IssueController@tojob');
	Route::post('issue/job-as-counter', 'IssueController@transfer_counter_job');
	Route::post('issue/counter-as-job', 'IssueController@transfer_counter_job');

    Route::post('order/process', 'OrderController@process_items');
    Route::post('order/receive/process', 'OrderController@receive_items');
    Route::post('order/confirm', 'OrderController@confirm');
    Route::post('orders/location/json', 'OrderController@location_json');
    Route::post('order/qty', 'OrderController@line_qty');
    Route::post('order/cost', 'OrderController@line_cost');
    Route::post('orders/add', 'OrderController@add');
	Route::post('order/item/remove', 'OrderController@remove_item');
	Route::post('order/send/email', 'OrderController@email');

    Route::get('orders/close/{id}', 'OrderController@close');
    Route::get('orders/issue/{id}', 'OrderController@issue');
    Route::get('orders/items/{id}', 'OrderController@items');
	Route::get('order/send/fax/{id}', 'OrderController@fax');
    Route::get('orders/receive/{id}', 'OrderController@receive');


    Route::get('counters/issues/{id}', 'CounterController@issue');
    Route::get('counters/labour/{id}', 'CounterController@labour');
    Route::get('counters/crane/{id}', 'CounterController@crane');
	Route::get('counters/carriage/{id}', 'CounterController@carriage');
    Route::get('counters/pdf/{id}', 'CounterController@invoice');

    Route::post('counters/close', 'CounterController@close');
    Route::post('counters/remove_close', 'CounterController@remove_close');
    Route::post('counters/remove_billed', 'CounterController@remove_billed');
    Route::post('counters/add_billed', 'CounterController@add_billed');
    Route::post('counters/email', 'CounterController@email_invoice');

    Route::post('labour/update', 'IssueAjax@update_cost_labour');
    Route::post('crane/update', 'IssueAjax@update_cost_crane');
    Route::post('carriage/update', 'IssueAjax@update_cost_carriage');

    Route::post('counter/misc/description', 'IssueAjax@misc_desc');
    Route::post('job/misc/description', 'IssueAjax@misc_desc');

    Route::post('labour/add', 'IssueAjax@add_labour');
    Route::post('crane/add', 'IssueAjax@add_crane');
    Route::post('carriage/add', 'IssueAjax@add_carriage');

    Route::post('labour/destroy', 'IssueAjax@destroy_labour');
    Route::post('crane/destroy', 'IssueAjax@destroy_crane');
    Route::post('carriage/destroy', 'IssueAjax@destroy_carriage');

    Route::get('location/item_lookup', 'LocationController@JsonLookup_ByItem');
    Route::get('addresses/json', 'AddressController@JsonLookup');
    Route::get('elements/json', 'ElementController@JsonLookup');
    Route::get('statuses/json', 'StatusController@JsonLookup');
    Route::get('supplier/json', 'SupplierController@JsonLookup');
    Route::get('stores/json', 'StoreController@JsonLookup');
    Route::get('customers/json', 'CustomerController@JsonLookup');
    Route::get('boats/json', 'BoatController@JsonLookup');
    Route::get('order/json', 'OrderController@JsonLookup');
    Route::get('job/json', 'JobController@JsonLookup');
    Route::get('counter/json', 'CounterController@JsonLookup');
    Route::get('jobs/yard_json', 'JobController@JsonLookupYard');

    //custom Yard order shite.
    Route::get('yard/create', 'OrderController@create_yard');


    Route::get('issue', 'IssueController@index');
	Route::get('issue/job/{id}', 'IssueController@job');
	Route::get('issue/counter/{id}', 'IssueController@counter');
	Route::get('issue/search', 'IssueController@search');
	Route::post('issue/job/process', 'IssueController@process_job');
	Route::post('issue/counter/process', 'IssueController@process_counter');
    Route::post('issue/counter/cost', 'IssueController@cost_counter_update');
    Route::post('issue/job/cost', 'IssueController@cost_job_update');

	route::get('reports', 'ReportController@index');
	route::post('report/generate', 'ReportController@generator');

	Route::resource('items', 'ItemController');
    Route::resource('suppliers', 'SupplierController');
    Route::resource('users', 'UserController');
    Route::resource('materials', 'MaterialController');
    Route::resource('stores', 'StoreController');
    Route::resource('sexes', 'SexController');
    Route::resource('types', 'TypeController');
    Route::resource('grades', 'GradeController');
    Route::resource('sizes', 'SizeController');
    Route::resource('prices', 'PriceController');
    Route::resource('partcodes', 'PartcodeController');
    Route::resource('locations', 'LocationController');
    Route::resource('addresses', 'AddressController');
    Route::resource('users', 'UserController');
    Route::resource('pincodes', 'PincodesController');
    Route::resource('customers', 'CustomerController');
    Route::resource('units', 'UnitController');
    Route::resource('boats', 'BoatController');
    Route::resource('jobs', 'JobController');
	Route::resource('orders', 'OrderController');
	Route::resource('counters', 'CounterController');
	Route::resource('statuses', 'StatusController');
	Route::resource('transfers', 'TransferController');
    Route::resource('elements', 'ElementController');

//    Route::get('generator_builder', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@builder');
//    Route::get('field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@fieldTemplate');
//    Route::post('generator_builder/generate', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generate');
});











