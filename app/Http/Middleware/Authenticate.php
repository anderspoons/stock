<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate {
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure                 $next
	 * @param  string|null              $guard
	 *
	 * @return mixed
	 */
	public function handle( $request, Closure $next, $guard = null ) {
		if ( Auth::guard( $guard )->guest() ) {
			if ( $request->ajax() || $request->wantsJson() ) {
				return response( 'Unauthorized.', 401 );
			} else {
				return redirect()->guest( 'login' );
			}
		}

		#logout if user not active
		if (  Auth::check() &&  Auth::user()->active !== 1 ) {
			Auth::logout();
			return redirect( 'login' )->withErrors( 'sorry, this user account is deactivated' );
		}

		return $next( $request );
	}
}
