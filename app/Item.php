<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model {

	protected $table = 'items';
	public $timestamps = true;

	public function sizes()
	{
		return $this->hasMany('Size', 'item_id');
	}

	public function sexes()
	{
		return $this->hasMany('Sex', 'item_id');
	}

	public function types()
	{
		return $this->hasMany('Type', 'item_id');
	}

	public function prices()
	{
		return $this->hasMany('App\Price', 'item_id');
	}

	public function partnum()
	{
		return $this->hasOne('App\Partnum', 'item_id');
	}

	public function partcode()
	{
		return $this->hasOne('App\Partcode', 'item_id');
	}

	public function locations()
	{
		return $this->hasMany('App\Location', 'item_id');
	}

	public function suppliers()
	{
		return $this->hasMany('App\Supplier_items', 'item_id');
	}

	public function barcode()
	{
		return $this->hasOne('App\Barcode', 'item_id');
	}

}