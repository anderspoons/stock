<?php
namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderSupplierNew extends Model
{
    use SoftDeletes;
    public $table = 'order_supplier_item_new';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $dates = ['deleted_at'];

    public $fillable = [
        'id',
        'order_item_id',
        'lots',
        'unit_id',
        'unit_cat_id',
        'deleted_at',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'order_item_id' => 'integer',
        'lots' => 'integer',
        'unit_id' => 'integer',
        'unit_cat_id' => 'integer',
        'deleted_at' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function order_item(){
        return $this->belongsTo('App\Models\OrderItems', 'id', 'order_item_id');
    }

    public function unit(){
        return $this->hasOne('App\Models\Unit', 'id', 'unit_id');
    }
}
