<?php
namespace App\Models;

use Eloquent as Model;

class Dashboard extends Model
{
	public $table = 'dashboard';

	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';

	public $fillable = [
		'name',
		'value'
	];
	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id' => 'integer',
		'name' => 'string',
		'value' => 'string'
	];

	public static $rules = [

	];
}

