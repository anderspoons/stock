<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="UnitSize",
 *      required={},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="data",
 *          description="data",
 *          type="number",
 *          format="float"
 *      )
 * )
 *
 *
 *
 * THIS IS TO DO WITH BOOKING OUT AND ORDERING NOTHING TO DO WITH DIMENSIONS OF A ITEM
 *
 */
class UnitSize extends Model {
    use SoftDeletes;
    public $table = 'unit_sizing';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $dates = [ 'deleted_at' ];

    public $fillable = [
        'data',
        'deleted_at'
    ];

    /*
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'         => 'integer',
        'data'       => 'float',
        'deleted_at' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];
}
