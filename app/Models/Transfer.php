<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transfer extends Model
{
    use SoftDeletes;

    public $table = 'transfers';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'item_id',
        'location_from',
        'location_to',
        'qty',
        'note',
        'pin_id_request',
        'for_type',
        'for_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'item_id' => 'integer',
        'location_from' => 'integer',
        'location_to' => 'integer',
        'qty' => 'float',
        'note' => 'string',
        'pin_id_request' => 'integer',
        'for_type' => 'string',
        'for_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    ];

	public function item(){
		return $this->hasOne('App\Models\Item', 'id', 'item_id');
	}

	public function location_to_object(){
		return $this->hasOne('App\Models\Location', 'id', 'location_to');
	}

	public function location_from_object(){
		return $this->hasOne('App\Models\Location', 'id', 'location_from');
	}

	public function requester(){
		return $this->hasOne('App\Models\Pincode', 'id', 'pin_id_request');
	}
}
