<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\AuditingTrait;
use Carbon\Carbon;

class Order extends Model
{
    use SoftDeletes;
    use AuditingTrait;

    public $table = 'order';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $dates = ['deleted_at'];

    protected $appends = array(
        'supplier_name'
    );

    public function getSupplierNameAttribute(){
        $supplier = Supplier::where('id', '=', $this->supplier_id)->value('name');
        return $supplier;
    }

    public $fillable = [
        'id',
        'supplier_id',
        'store_id',
        'yard_id',
        'description',
        'po',
        'custom_po',
        'closed_at',
        'expected_at',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'supplier_id' => 'integer',
        'store_id' => 'integer',
        'yard_id' => 'integer',
        'description' => 'string',
        'po' => 'string',
        'custom_po' => 'string',
        'closed_at' => 'datetime',
        'expected_at' => 'datetime',
        'deleted_at' => 'datetime'
    ];

    /**
     * Validation rules
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'supplier_id' => 'required',
        'store_id' => 'required',
    ];

    public function getExpectedAtAttribute($date){
    	if($date == null){
    		return '';
	    }
	    $date = new Carbon($date);
	    return $date->format('Y-m-d');
    }


    public function supplier(){
        return $this->hasOne('App\Models\Supplier', 'id', 'supplier_id');
    }

	public function store(){
		return $this->hasOne('App\Models\Store', 'id', 'store_id');
	}

	public function statuses(){
		return $this->belongsToMany('App\Models\Status', 'order_statuses', 'order_id', 'status_id');
	}

	public function items(){
		return $this->hasMany('App\Models\OrderItems', 'order_id', 'id');
	}

	public function carriage(){
		return $this->hasMany('App\Models\OrderCarriage', 'id', 'order_id');
	}
}
