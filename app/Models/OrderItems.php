<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\AuditingTrait;


class OrderItems extends Model
{
    use SoftDeletes;
    use AuditingTrait;

    public $table = 'order_items';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $dates = ['deleted_at'];

    public $fillable = [
        'id',
        'item_id',
        'order_id',
        'location_id',
        'job_id',
        'counter_id',
        'qty',
        'qty_in',
        'note',
        'custom_po',
        'new_supplier',
        'pin_id',
        'deleted_at',
        'received_at',
        'line_cost',
        'line_unit_cost',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'item_id' => 'integer',
        'order_id' => 'integer',
        'location_id' => 'integer',
        'job_id' => 'integer',
        'line_cost' => 'float',
        'line_unit_cost' => 'float',
        'counter_id' => 'integer',
        'qty' => 'float',
        'qty_in' => 'float',
        'note' => 'string',
        'custom_po' => 'string',
        'new_supplier' => 'integer',
        'pin_id' => 'integer',
        'received_at' => 'datetime',
        'deleted_at' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function counter(){
        return $this->hasOne('App\Models\Counter', 'id', 'counter_id');
    }

    public function job(){
        return $this->hasOne('App\Models\Job', 'id', 'job_id');
    }

    public function misc(){
        return $this->hasOne('App\Models\OrderMisc', 'order_item_id');
    }

    public function order(){
        return $this->hasOne('App\Models\Order', 'id', 'order_id');
    }

    public function item(){
        return $this->hasOne('App\Models\Item', 'id', 'item_id')->withTrashed();
    }

    public function size(){
        return $this->hasMany('App\Models\OrderSize', 'order_item_id', 'id');
    }

    public function location(){
        return $this->hasOne('App\Models\Location', 'id', 'location_id');
    }

    public function pin(){
        return $this->hasOne('App\Models\Pincodes', 'id', 'pin_id');
    }

    public function new_supplier(){
        return $this->hasOne('App\Models\OrderSupplierNew', 'order_item_id', 'id');
    }

}
