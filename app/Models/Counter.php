<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\AuditingTrait;

class Counter extends Model
{
    use SoftDeletes;
    use AuditingTrait;

    public $table = 'counter';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $dates = ['deleted_at'];

    public $fillable = [
        'customer_id',
        'boat_id',
        'note',
        'work_description',
        'po',
        'work_scope',
        'closed_at',
        'invoiced_at',
        'net_total',
        'batch_reported',
        'ref',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'boat_id' => 'integer',
        'customer_id' => 'integer',
        'note' => 'string',
        'work_description' => 'string',
        'po' => 'string',
        'work_scope' => 'string',
        'closed_at' => 'datetime',
        'invoiced_at' => 'datetime',
        'net_total' => 'float',
        'batch_reported' => 'datetime',
        'ref' => 'string',
        'deleted_at' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'customer_id' => 'required',
    ];

	public function customer() {
		return $this->hasOne('App\Models\Customer', 'id', 'customer_id');
	}
	public function boat() {
		return $this->hasOne('App\Models\Boat', 'id', 'boat_id');
	}
	public function items(){
		return $this->hasMany('App\Models\CounterItems', 'counter_id', 'id');
	}
    public function labour(){
        return $this->hasMany('App\Models\CounterLabour', 'counter_id', 'id');
    }
    public function crane(){
        return $this->hasMany('App\Models\CounterCrane', 'counter_id', 'id');
    }
	public function carriage(){
		return $this->hasMany('App\Models\CounterCarriage', 'counter_id', 'id');
	}
	public function statuses(){
		return $this->belongsToMany('App\Models\Status', 'counter_statuses', 'counter_id', 'status_id');
	}


}
