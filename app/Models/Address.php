<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\AuditingTrait;

/**
 * @SWG\Definition(
 *      definition="Address",
 *      required={},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="line_1",
 *          description="line_1",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="line_2",
 *          description="line_2",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="town",
 *          description="town",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="county",
 *          description="county",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="postcode",
 *          description="postcode",
 *          type="string"
 *      )
 * )
 */
class Address extends Model
{
    use SoftDeletes;
    use AuditingTrait;

    public $table = 'address';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'line_1',
        'line_2',
        'line_3',
        'town',
        'county',
        'postcode',
        'lng',
        'lat'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'line_1' => 'string',
        'line_2' => 'string',
        'line_3' => 'string',
        'town' => 'string',
        'county' => 'string',
        'postcode' => 'string',
        'lng' => 'float',
        'lat' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    ];


    public function supplier()
    {
        return $this->belongsTo('App\Models\Supplier', 'address_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'address_id');
    }

    public function store()
    {
        return $this->belongsTo('App\Models\Store', 'address_id');
    }
}
