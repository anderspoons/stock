<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CounterItems extends Model
{
    use SoftDeletes;

    public $table = 'counter_items';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'item_id',
        'counter_id',
        'qty',
        'location_id',
        'pin_id',
        'order_id',
        'deleted_at',
        'line_cost',
        'line_unit_cost',
        'line_real_cost'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'item_id' => 'integer',
        'counter_id' => 'integer',
        'qty' => 'float',
        'location_id' => 'integer',
        'pin_id' => 'integer',
        'order_id' => 'integer',
        'deleted_at' => 'datetime',
        'line_cost' => 'float',
        'line_unit_cost' => 'float',
        'line_real_cost' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

	public function misc(){
		return $this->hasOne('App\Models\CounterMisc', 'counter_item_id');
	}

	public function counter(){
		return $this->hasOne('App\Models\Counter', 'id', 'counter_id');
	}

	public function item(){
		return $this->hasOne('App\Models\Item', 'id', 'item_id')->withTrashed();;
	}

	public function size(){
		return $this->hasMany('App\Models\CounterSize', 'counter_item_id', 'id');
	}

	public function location(){
		return $this->hasOne('App\Models\Location', 'id', 'location_id');
	}

	public function pin(){
		return $this->hasOne('App\Models\Pincodes', 'id', 'pin_id');
	}

}
