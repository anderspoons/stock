<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\AuditingTrait;

/**
 * @SWG\Definition(
 *      definition="Location",
 *      required={},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="store_id",
 *          description="store_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="item_id",
 *          description="item_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="room",
 *          description="room",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="rack",
 *          description="rack",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="shelf",
 *          description="shelf",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="bay",
 *          description="bay",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="qty",
 *          description="qty",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Location extends Model
{
    use SoftDeletes;
    use AuditingTrait;

    public $table = 'locations';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'store_id',
        'item_id',
        'min',
        'max',
        'room',
        'rack',
        'shelf',
        'bay',
        'qty',
        'on_order',
        'on_reserve',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'store_id' => 'integer',
        'min' => 'integer',
        'max' => 'integer',
        'item_id' => 'integer',
        'room' => 'string',
        'rack' => 'string',
        'shelf' => 'string',
        'bay' => 'string',
        'qty' => 'float',
        'on_order' => 'float',
        'on_reserve' => 'float',
        'deleted_at' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

	public function store()
	{
		return $this->hasOne('App\Models\Store', 'id', 'store_id');
	}

	public function item()
	{
		return $this->hasOne('App\Models\Item', 'id', 'item_id');
	}
}
