<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\AuditingTrait;

/**
 * @SWG\Definition(
 *      definition="Item",
 *      required={},
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="description",
 *          description="description",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="markup_percent",
 *          description="markup_percent",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="markup_fixed",
 *          description="markup_fixed",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="barcode",
 *          description="barcode",
 *          type="string"
 *      )
 * )
 */
class Item extends Model
{
    use SoftDeletes;
    use AuditingTrait;

    public $table = 'items';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'description',
        'partnumber',
        'markup_percent',
        'markup_fixed',
	    'unit_category_id',
	    'unit_id',
        'barcode',
        'on_order',
        'on_reserve'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'description' => 'string',
        'partnumber' => 'string',
        'markup_percent' => 'integer',
        'markup_fixed' => 'float',
        'on_order' => 'float',
        'on_reserve' => 'float',
        'unit_category_id' => 'integer',
        'unit_id' => 'integer',
        'barcode' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function sizes()
    {
        return $this->belongsToMany('App\Models\Size', 'size_items', 'item_id', 'size_id')->withPivot('sex_id')->orderBy('size_items.id', 'asc');
    }

    public function sexes()
    {
        return $this->belongsToMany('App\Models\Sex', 'size_items', 'item_id', 'sex_id')->withPivot('size_id');
    }

    public function types()
    {
        return $this->belongsToMany('App\Models\Type', 'type_items', 'item_id', 'type_id')->orderBy('type_id', 'desc');
    }

    public function prices()
    {
        return $this->hasMany('App\Models\Price', 'item_id')->orderBy('prices.id', 'desc');
    }

    public function last_price()
    {
        return $this->hasMany('App\Models\Price', 'item_id', 'id')
	        ->select('*')
//	        ->where('prices.cost', '>', 0)
	        ->orderBy('prices.id', 'desc');
    }

    public function unit_cost()
    {
        return $this->hasMany('App\Models\Price', 'item_id', 'id')
	        ->select('*')
	        ->where('prices.cost', '>', 0)
	        ->orderBy('prices.id', 'desc');
    }

    public function stores()
    {
        return $this->belongsToMany('App\Models\Store', 'locations', 'item_id', 'store_id')->withPivot('id', 'qty', 'rack', 'bay', 'shelf', 'room', 'min', 'max', 'on_order')->whereNull('locations.deleted_at');
    }

    public function suppliers()
    {
        return $this->belongsToMany('App\Models\Supplier', 'supplier_items', 'item_id', 'supplier_id')->withPivot('code');
    }

    public function materials()
    {
        return $this->belongsToMany('App\Models\Material', 'material_items', 'item_id', 'material_id');
    }

    public function grades()
    {
        return $this->belongsToMany('App\Models\Grade', 'grade_items', 'item_id', 'grade_id');
    }

    public function unit_category()
    {
        return $this->hasOne('App\Models\Category', 'id', 'unit_category_id');
    }

    //TODO, because I have included the unit_cat_id here its prevented me from using that within its own relationship to reference the name of the category for this supplier
    //Not sure if this will become an issue down the line but I will output the unit_categories on the page as a Json array to be referneced.
    public function supplier_units() {
        return $this->belongsToMany('App\Models\Unit', 'unit_supplier', 'item_id', 'unit_id')->withPivot('supplier_id', 'unit_cat_id', 'lots');
    }

    //TODO: use same thing for orders and jobs and counter sales
    public function supplier_sizing() {
        return $this->belongsToMany('App\Models\UnitSize', 'unit_supplier_sizing', 'item_id', 'sizing_id')->withPivot('supplier_id');
    }

	public function statuses(){
		return $this->belongsToMany('App\Models\Status', 'items_statuses', 'item_id', 'status_id');
	}
}
