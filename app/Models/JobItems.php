<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\AuditingTrait;

class JobItems extends Model
{
    use SoftDeletes;
	use AuditingTrait;

    public $table = 'job_items';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'element_id',
        'item_id',
        'job_id',
        'qty',
        'line_cost',
        'line_real_cost',
        'line_unit_cost',
        'location_id',
        'order_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'element_id' => 'integer',
        'item_id' => 'integer',
        'job_id' => 'integer',
        'qty' => 'float',
        'line_cost' => 'float',
        'line_real_cost' => 'float',
        'line_unit_cost' => 'float',
        'location_id' => 'integer',
        'order_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

	public function misc(){
		return $this->hasOne('App\Models\JobMisc', 'job_item_id');
	}

	public function job(){
		return $this->hasOne('App\Models\Job', 'id', 'job_id');
	}

	public function item(){
		return $this->hasOne('App\Models\Item', 'id', 'item_id')->withTrashed();
	}

	public function size(){
		return $this->hasMany('App\Models\JobSize', 'job_item_id', 'id');
	}

	public function location(){
		return $this->hasOne('App\Models\Location', 'id', 'location_id');
	}

	public function element(){
		return $this->hasOne('App\Models\Element', 'id', 'element_id');
	}

	public function pin(){
		return $this->hasOne('App\Models\Pincodes', 'id', 'pin_id');
	}

}
