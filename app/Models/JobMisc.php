<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\AuditingTrait;

class JobMisc extends Model
{
    use SoftDeletes;
    use AuditingTrait;

    public $table = 'job_items_misc';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $dates = ['deleted_at'];

    public $fillable = [
	    'job_item_id',
	    'name',
	    'description',
    ];

    protected $casts = [
	    'id' => 'integer',
	    'job_item_id' => 'integer',
	    'name' => 'string',
	    'description' => 'string',
    ];

    public static $rules = [
        
    ];
}
