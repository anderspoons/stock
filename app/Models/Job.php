<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\AuditingTrait;

/**
 * @SWG\Definition(
 *      definition="Job",
 *      required={},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="address_id",
 *          description="address_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="customer_id",
 *          description="customer_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="boat_id",
 *          description="boat_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="note",
 *          description="note",
 *          type="string"
 *      )
 * )
 */
class Job extends Model
{
    use SoftDeletes;
	use AuditingTrait;

    public $table = 'job';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $dates = ['deleted_at', 'closed_at'];


    public $fillable = [
        'customer_id',
        'boat_id',
        'note',
        'work_scope',
        'po',
        'sage_invoice',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'customer_id' => 'integer',
        'boat_id' => 'integer',
        'note' => 'string',
        'work_scope' => 'string',
        'po' => 'string',
        'sage_invoice' => 'string',
        'deleted_at' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'customer_id' => 'required'
    ];

	public function elements(){
		return $this->belongsToMany('App\Models\Element', 'job_elements', 'job_id', 'element_id');
	}

	public function statuses(){
		return $this->belongsToMany('App\Models\Status', 'job_statuses', 'job_id', 'status_id');
	}

    public function boat(){
        return $this->hasOne('App\Models\Boat', 'id', 'boat_id');
    }

    public function items(){
        return $this->hasMany('App\Models\JobItems', 'job_id', 'id');
    }

    public function job_items(){
		return $this->hasMany('App\Models\JobItems', 'job_id', 'id');
	}

	public function customer(){
		return $this->hasOne('App\Models\Customer', 'id', 'customer_id');
	}

    public function labour(){
        return $this->hasMany('App\Models\JobLabour', 'job_id', 'id');
    }

    public function crane(){
        return $this->hasMany('App\Models\JobCrane', 'job_id', 'id');
    }

    public function carriage(){
        return $this->hasMany('App\Models\JobCarriage', 'job_id', 'id');
    }


}
