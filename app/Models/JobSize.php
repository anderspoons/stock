<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobSize extends Model
{
    use SoftDeletes;
    public $table = 'job_items_sizes';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $dates = ['deleted_at'];

    public $fillable = [
	    'job_item_id',
	    'size_unit_id',
	    'size',
    ];

    protected $casts = [
	    'id' => 'integer',
	    'job_item_id' => 'integer',
	    'size_unit_id' => 'integer',
	    'size' => 'float',
    ];

    public static $rules = [
        
    ];

	public function job_item(){
		return $this->belongsTo('App\Models\JobItems', 'id', 'job_item_id');
	}

	public function unit(){
		return $this->hasOne('App\Models\Unit', 'id', 'size_unit_id');
	}
}
