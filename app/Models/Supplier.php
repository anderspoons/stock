<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\AuditingTrait;

/**
 * @SWG\Definition(
 *      definition="Supplier",
 *      required={},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="code",
 *          description="code",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="address_1",
 *          description="address_1",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="address_2",
 *          description="address_2",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="town",
 *          description="town",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="county",
 *          description="county",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="postcode",
 *          description="postcode",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="phone",
 *          description="phone",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="fax",
 *          description="fax",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="email",
 *          description="email",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="min_order",
 *          description="min_order",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class Supplier extends Model
{
    use SoftDeletes;
    use AuditingTrait;

    public $table = 'suppliers';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'name',
        'code',
        'address_id',
        'phone',
        'fax',
        'email',
        'min_order'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'code' => 'string',
        'address_id' => 'integer',
        'phone' => 'string',
        'fax' => 'string',
        'email' => 'string',
        'min_order' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     *
     */
    public static $rules = [
//        'code' => 'unique:suppliers',
//        'name' => 'unique:suppliers',
    ];


    public function address()
    {
        return $this->hasOne('App\Models\Address', 'id', 'address_id');
    }

    public function getFormattedAddressAttribute(){
    	$string = '';
	    if(strlen($this->address->line_1) > 0){
		    $string .= $this->address->line_1.'&#013;';
	    }
	    if(strlen($this->address->line_2) > 0){
		    $string .= $this->address->line_2.'&#013;';
	    }
	    if(strlen($this->address->line_3) > 0){
		    $string .= $this->address->line_3.'&#013;';
	    }
	    if(strlen($this->address->town) > 0){
		    $string .= $this->address->town.'&#013;';
	    }
	    if(strlen($this->address->county) > 0){
		    $string .= $this->address->county.'&#013;';
	    }
	    if(strlen($this->address->postcode) > 0){
		    $string .= $this->address->postcode;
	    }
	    return $string;
    }
}
