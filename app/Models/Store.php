<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\AuditingTrait;

class Store extends Model
{
    use SoftDeletes;
    use AuditingTrait;
    public $table = 'stores';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'name',
        'code',
        'address_id',
        'phone',
        'fax',
        'email'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'code' => 'string',
        'address_id' => 'id',
        'phone' => 'string',
        'fax' => 'string',
        'email' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


    public function address()
    {
        return $this->hasOne('App\Models\Address', 'id', 'address_id');
    }

	public function getFormattedAddressAttribute(){
		$string = '';
		if(strlen($this->address->line_1) > 0){
			$string .= $this->address->line_1.'&#013;';
		}
		if(strlen($this->address->line_2) > 0){
			$string .= $this->address->line_2.'&#013;';
		}
		if(strlen($this->address->line_3) > 0){
			$string .= $this->address->line_3.'&#013;';
		}
		if(strlen($this->address->town) > 0){
			$string .= $this->address->town.'&#013;';
		}
		if(strlen($this->address->county) > 0){
			$string .= $this->address->county.'&#013;';
		}
		if(strlen($this->address->postcode) > 0){
			$string .= $this->address->postcode;
		}
		return $string;
	}

    public function locations()
    {
        return $this->hasMany('App\Models\Location', 'store_id', 'id');
    }
}
