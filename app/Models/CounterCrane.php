<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\AuditingTrait;

/**
 * @SWG\Definition(
 *      definition="CounterCrane",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="counter_id",
 *          description="counter_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="cost",
 *          description="cost",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="pin_id",
 *          description="pin_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class CounterCrane extends Model
{
    use SoftDeletes;
    use AuditingTrait;

    public $table = 'counter_crane';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'counter_id',
        'cost',
        'name',
        'pin_id',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'counter_id' => 'integer',
        'cost' => 'float',
        'name' => 'string',
        'pin_id' => 'integer',
        'deleted_at' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];


    public function pin() {
        return $this->hasOne( 'App\Models\Pincodes', 'id', 'pin_id' );
    }
}
