<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CounterSize extends Model
{
    use SoftDeletes;

    public $table = 'counter_items_sizes';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'counter_item_id',
        'size_unit_id',
        'size',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'counter_item_id' => 'integer',
        'size_unit_id' => 'integer',
        'size' => 'float',
        'deleted_at' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

	public function order_item(){
		return $this->belongsTo('App\Models\OrderItems', 'id', 'order_item_id');
	}

	public function unit(){
		return $this->hasOne('App\Models\Unit', 'id', 'size_unit_id');
	}
}
