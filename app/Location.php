<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model {

	protected $table = 'locations';
	public $timestamps = true;

    public function store(){
        return $this->hasOne('App\Store', 'store_id', 'id');
    }

    public function item(){
        return $this->hasOne('App\Item', 'item_id', 'id');
    }
}