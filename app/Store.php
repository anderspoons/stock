<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model {

	protected $table = 'stores';
	public $timestamps = true;

	public function locations()
	{
		return $this->hasMany('App\Location', 'store_id');
	}

}