<?php

namespace App\Repositories;

use App\Models\Sex;
use InfyOm\Generator\Common\BaseRepository;

class SexRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Sex::class;
    }
}
