<?php

namespace App\Repositories;

use App\Models\CounterLabour;
use InfyOm\Generator\Common\BaseRepository;

class CounterLabourRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'counter_id',
        'cost',
        'name',
        'deleted_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CounterLabour::class;
    }
}
