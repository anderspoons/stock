<?php

namespace App\Repositories;

use App\Models\OrderItems;
use InfyOm\Generator\Common\BaseRepository;

class OrderItemsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'item_id',
        'order_id',
        'location_id',
        'job_id',
        'note',
        'customer_id',
        'po',
        'deleted_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrderItems::class;
    }
}
