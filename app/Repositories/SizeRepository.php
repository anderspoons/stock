<?php

namespace App\Repositories;

use App\Models\Size;
use InfyOm\Generator\Common\BaseRepository;

class SizeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'item_id',
        'size',
        'deleted_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Size::class;
    }
}
