<?php

namespace App\Repositories;

use App\Models\JobMisc;
use InfyOm\Generator\Common\BaseRepository;

class JobMiscRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return JobMisc::class;
    }
}
