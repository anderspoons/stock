<?php

namespace App\Repositories;

use App\Models\CounterItems;
use InfyOm\Generator\Common\BaseRepository;

class CounterItemsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'item_id',
        'counter_id',
        'qty',
        'location_id',
        'pin_id',
        'order_id',
        'deleted_at',
        'line_cost'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CounterItems::class;
    }
}
