<?php

namespace App\Repositories;

use App\Models\Type;
use InfyOm\Generator\Common\BaseRepository;

class typeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return type::class;
    }
}
