<?php

namespace App\Repositories;

use App\Models\CounterCrane;
use InfyOm\Generator\Common\BaseRepository;

class CounterCraneRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'counter_id',
        'cost',
        'name',
        'pin_id',
        'deleted_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CounterCrane::class;
    }
}
