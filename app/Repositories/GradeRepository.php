<?php

namespace App\Repositories;

use App\Models\grade;
use InfyOm\Generator\Common\BaseRepository;

class gradeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'item_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return grade::class;
    }
}
