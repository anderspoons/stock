<?php

namespace App\Repositories;

use App\Models\Boat;
use InfyOm\Generator\Common\BaseRepository;

class BoatRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'sage_ref',
        'name',
        'number',
        'customer_id',
        'deleted_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Boat::class;
    }
}
