<?php

namespace App\Repositories;

use App\Models\Counter;
use InfyOm\Generator\Common\BaseRepository;

class CounterRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'customer_id',
        'note',
        'name',
        'po',
        'closed_at',
        'ref',
        'deleted_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Counter::class;
    }
}
