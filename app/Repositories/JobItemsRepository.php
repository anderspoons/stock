<?php

namespace App\Repositories;

use App\Models\JobItems;
use InfyOm\Generator\Common\BaseRepository;

class JobItemsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'element_id',
        'item_id',
        'job_id',
        'qty',
        'location_id',
        'order_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return JobItems::class;
    }
}
