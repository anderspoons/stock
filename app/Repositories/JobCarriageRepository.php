<?php

namespace App\Repositories;

use App\Models\JobCarriage;
use InfyOm\Generator\Common\BaseRepository;

class JobCarriageRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'job_id',
        'cost',
        'name',
        'pin_id',
        'deleted_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return JobCarriage::class;
    }
}
