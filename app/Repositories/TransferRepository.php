<?php

namespace App\Repositories;

use App\Models\Transfer;
use InfyOm\Generator\Common\BaseRepository;

class TransferRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'item_id',
        'location_from',
        'location_to',
        'qty',
        'note',
        'pin_id_request',
        'for_type',
        'for_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Transfer::class;
    }
}
