<?php

namespace App\Repositories;

use App\Models\CounterCarriage;
use InfyOm\Generator\Common\BaseRepository;

class CounterCarriageRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'counter_id',
        'cost',
        'name',
        'deleted_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CounterCarriage::class;
    }
}
