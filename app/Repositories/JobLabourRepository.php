<?php

namespace App\Repositories;

use App\Models\JobLabour;
use InfyOm\Generator\Common\BaseRepository;

class JobLabourRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'job_id',
        'cost',
        'name',
        'pin_id',
        'deleted_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return JobLabour::class;
    }
}
