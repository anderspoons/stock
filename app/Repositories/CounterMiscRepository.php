<?php

namespace App\Repositories;

use App\Models\CounterMisc;
use InfyOm\Generator\Common\BaseRepository;

class CounterMiscRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'counter_item_id',
        'name',
        'description',
        'deleted_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CounterMisc::class;
    }
}
