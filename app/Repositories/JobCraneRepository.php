<?php

namespace App\Repositories;

use App\Models\JobCrane;
use InfyOm\Generator\Common\BaseRepository;

class JobCraneRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'job_id',
        'cost',
        'name',
        'pin_id',
        'deleted_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return JobCrane::class;
    }
}
