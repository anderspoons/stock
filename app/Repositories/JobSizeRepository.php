<?php

namespace App\Repositories;

use App\Models\JobSize;
use InfyOm\Generator\Common\BaseRepository;

class JobSizeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return JobSize::class;
    }
}
