<?php

namespace App\Repositories;

use App\Models\Element;
use InfyOm\Generator\Common\BaseRepository;

class ElementRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'deleted_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Element::class;
    }
}
