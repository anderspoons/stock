<?php

namespace App\Repositories;

use App\Models\Pincodes;
use InfyOm\Generator\Common\BaseRepository;

class PincodesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'email'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Pincodes::class;
    }
}
