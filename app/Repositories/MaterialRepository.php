<?php

namespace App\Repositories;

use App\Models\Material;
use InfyOm\Generator\Common\BaseRepository;

class MaterialRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Material::class;
    }
}
