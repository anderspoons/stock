<?php

namespace App\Repositories;

use App\Models\CounterSize;
use InfyOm\Generator\Common\BaseRepository;

class CounterSizeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'counter_item_id',
        'size_unit_id',
        'size',
        'deleted_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CounterSize::class;
    }
}
