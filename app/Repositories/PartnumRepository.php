<?php

namespace App\Repositories;

use App\Models\Partcode;
use InfyOm\Generator\Common\BaseRepository;

class PartcodeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'item_id',
        'supplier_id',
        'number',
        'deleted_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Partcode::class;
    }
}
