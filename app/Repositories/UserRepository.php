<?php

namespace App\Repositories;

use App\Models\User;
use InfyOm\Generator\Common\BaseRepository;

class UserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'password',
        'email',
        'remember_token',
        'deleted_at',
        'active'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return User::class;
    }
}
