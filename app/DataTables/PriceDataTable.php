<?php

namespace App\DataTables;

use App\Models\Price;
use Form;
use Yajra\Datatables\Services\DataTable;

class PriceDataTable extends DataTable
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('the_cost', function ($data) {
                return '£'.number_format($data->cost, 2);
            })
            ->addColumn('actions', function ($data) {
                            return '
                            <div class=\'btn-group\'>
                                <a href="' . route('prices.edit', [$data->id]) . '" class=\'btn btn-default btn-xs\'><i data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class=\'action-tooltip\'>Edit Price</span>" class="glyphicon glyphicon-edit"></i></a>
                            </div>
                            ';
                        })
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $prices = Price::query()->with('item');

        return $this->applyScopes($prices);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns(array_merge(
                $this->getColumns(),
                [
                    'actions' => [
                        'orderable' => false,
                        'searchable' => false,
                        'printable' => false,
                        'exportable' => false
                    ]
                ]
            ))
            ->parameters([
                'dom' => 'Bfrtip',
                'scrollX' => true,
                'pagingType' => 'extStyle',
                'buttons' => [
                    'csv',
                    'excel',
                    'pdf',
                    'print',
                    'reset',
                    'reload'
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'cost' => ['name' => 'the_cost', 'data' => 'the_cost', 'orderable' => false],
            'item' => ['name' => 'item.description', 'data' => 'item.description'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'prices';
    }
}
