<?php

namespace App\DataTables;

use App\Models\Transfer;
use Form;
use Yajra\Datatables\Services\DataTable;

class TransferDataTable extends DataTable
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('item_description', function($data){
				if(is_object($data->item)){
					return $data->item->description.' | '.$data->item->barcode;
				} else {
					return 'Could not generate description';
				}
            })
            ->addColumn('location_from_formatted', function($data){
				if(is_object($data->location_to_object)){
					$store = $data->location_to_object->store->name;
					$room = $data->location_to_object->room;
					$rack = $data->location_to_object->rack;
					$shelf = $data->location_to_object->shelf;
					$bay = $data->location_to_object->bay;

					return $store.' '.$room.' '.$rack.' '.$shelf.' '.$bay;
				}
            })
            ->addColumn('location_from_formatted', function($data){
	            if(is_object($data->location_from_object)){
		            $store = $data->location_from_object->store->name;
		            $room = $data->location_from_object->room;
		            $rack = $data->location_from_object->rack;
		            $shelf = $data->location_from_object->shelf;
		            $bay = $data->location_from_object->bay;
		            return $store.' '.$room.' '.$rack.' '.$shelf.' '.$bay;
	            }
            })
	        ->addColumn('transfer_purpose', function($data){
				if(isset($data->for_type) && isset($data->for_id)){
					return $this->purpose_text($data->for_type, $data->for_id);
				} else {
					return 'Not Set';
				}
	        })
            ->addColumn('user_requested', function($data){
	            if(is_object($data->requester)){
		            return $data->requester->name;
	            }
            })
            ->addColumn('action', 'transfers.datatables_actions')
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $transfers = Transfer::query();

        return $this->applyScopes($transfers);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->addAction(['width' => '10%'])
            ->ajax('')
            ->parameters([
                'dom' => 'Bfrtip',
                'scrollX' => false,
                'pagingType' => 'extStyle',
                'buttons' => [
                    'print',
                    'reset',
                    'reload',
                    [
                         'extend'  => 'collection',
                         'text'    => '<i class="fa fa-download"></i> Export',
                         'buttons' => [
                             'csv',
                             'excel',
                             'pdf',
                         ],
                    ]
                ]
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
	        'item' => ['name' => 'item_description', 'data' => 'item_description'],
	        'location_from' => ['name' => 'location_from', 'data' => 'location_from'],
	        'location_to' => ['name' => 'location_to', 'data' => 'location_to'],
            'qty' => ['name' => 'qty', 'data' => 'qty'],
            'note' => ['name' => 'note', 'data' => 'note'],
            'user_requested' => ['name' => 'user_requested', 'data' => 'user_requested'],
	        'transfer_purpose'  => ['name' => 'transfer_purpose', 'data' => 'transfer_purpose'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'transfers';
    }



    public function purpose_text($type, $id){

    }
}
