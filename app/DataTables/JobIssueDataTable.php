<?php

namespace App\DataTables;

use App\Models\JobItems;
use App\User;
use App\Models\Job;
use Yajra\Datatables\Services\DataTable;

class JobIssueDataTable extends DataTable
{
	protected $job_id;

	public function forJob($id) {
		$this->job_id = $id;
		return $this;
	}



    /**
     * Build excel file and prepare for export.
     *
     * @return \Maatwebsite\Excel\Writers\LaravelExcelWriter
     */
    protected function buildExcelFile()
    {
        /** @var \Maatwebsite\Excel\Excel $excel */
        $excel = app('excel');

        $spreadsheet = $excel->create($this->filename(), function ($excel) {
            $excel->sheet('exported-data', function ($sheet) {
                $data = $this->getDataForExport();
                foreach($data as $key => $val){
                    $tmp = str_replace('£', '', $val);
                    $tmp = str_replace('N/A', '', $tmp);
                    $data[$key] = $tmp;
                }
                $data[count($data) + 2] = array(
                    'Issued' => '',
                    'Name' => '',
                    'Description/ Partnumber' => '',
                    'Quantity' => '',
                    'PO' => '',
                    'Cost Unit Price' => '=SUM(E1:E'.count($data).')',
                    'Cost Total Price' => '=SUM(F1:F'.count($data).')',
                );
                $sheet->rows($data);
            });
        });

        return $spreadsheet;
    }


    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
	    $urlquery = proper_parse_str(urldecode($_SERVER['QUERY_STRING']));
        $table = $this->datatables
            ->eloquent($this->query())
            ->addColumn('expand', function($data) {
            	if($data->misc){
            		//Nothing
		            return 'N/A';
	            } else {
		            return '<button type="submit" data-toggle="tooltip" data-placement="right" data-original-title="Show / Hide Details" title="" class="btn btn-success btn-xs child-expand"><i class="glyphicon glyphicon-plus"></i></button>';
	            }
            })
            ->addColumn('issue_date', function($data){
	            return date('d/m/Y H:i', strtotime($data->created_at));
            })
            ->addColumn('the_name', function($data){
	            if($data->item) {
		            return $data->item->description;
	            } elseif($data->misc) {
		            return $data->misc->name;
	            }
            })
            ->addColumn('the_desc', function($data){
                $str = '';
                if(isset($data->item->sizes)) {
                    foreach($data->item->sizes as $size){
                        $str .= ' '.$size->name.' x';
                    }
                    $str = rtrim($str, "x");
                }

                if(isset($data->item)) {
                    if(strlen($str) > 0 && strlen($data->item->partnumber) > 0){
                        $str .= ' : ';
                    }
                    $str .= $data->item->partnumber;
                }

                if(isset($data->misc)) {
                    $str = $data->misc->description;
                }
                return $str;
            })



//            'Cost Unit Price' => ['name' => 'real_unit_cost', 'data' => 'real_unit_cost', 'orderable' => false],
//            'Cost Total Price' => ['name' => 'real_total_cost', 'data' => 'real_total_cost', 'orderable' => false],
//            'Sale Unit Price' => ['name' => 'sale_unit_cost', 'data' => 'sale_unit_cost', 'orderable' => false],
//            'Sale Total Price' => ['name' => 'sale_total_cost', 'data' => 'sale_total_cost', 'orderable' => false],


            ->addColumn('real_unit_cost', function($data){
                if($data->line_real_cost != 0){
                    return '£'.number_format((float)$data->line_real_cost, 2, '.', '');
                } else {
                    return 'N/A';
                }
            })
            ->addColumn('real_total_cost', function($data) {
                return '£'.number_format((float)$data->line_real_cost*$data->qty, 2, '.', '');
            })
            ->addColumn('sale_unit_cost', function($data){
                if($data->line_unit_cost != null){
                    return '£'.number_format((float)$data->line_unit_cost, 2, '.', '');
                } else {
                    return 'N/A';
                }
            })
            ->addColumn('sale_total_cost', function($data){
                if($data->line_cost != null){
                    return '£'.number_format((float)$data->line_cost, 2, '.', '');
                } else {
                    return 'N/A';
                }
            })
            ->addColumn('the_sizing', function($data){
	            if($data->size) {
		            $txt = '';
	            	foreach($data->size as $size){
		           	    $txt .= $size->size.' ';
		            }
		            return $txt;
	            } else {
	            	return '';
	            }
            })
            ->addColumn('the_unit', function($data){
	            if($data->size->first()['unit']['name']) {
		            return $data->size->first()['unit']['name'];
	            } else {
		            return '';
	            }
            })
            ->addColumn('the_location', function($data){
	            $local = '';
	            if(isset($data->location)){
		            $local .= $data->location['room'];
		            $local .= $data->location['rack'];
		            $local .= $data->location['shelf'];
		            $local .= $data->location['bay'];
	            }
	            return $local;
            })
            ->addColumn('the_element', function($data){
	            if(isset($data->element)){
		            return $data->element->name;
	            } else {
	            	return '';
	            }
            })
            ->addColumn('the_issuer', function($data){
	            return $data->pin['name'];
            })
            ->addColumn('action', function ($data) {
                $billed = false;
                $job = Job::where('id', $data->job_id)->with('statuses')->first();
                if(isset($job->statuses)){
                    foreach($job->statuses as $status){
                        if($status->id == 9){
                            $billed = true;
                        }
                    }
                }
            	$item = '';
	            if(isset($data->item->id)){
		            $item = '<a target="_blank" href="'.url('/items/'.$data->item->id).'"  data-toggle="tooltip" data-placement="right" data-original-title="Show item details" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-eye-open"></i></a>';
	            }
                if(!$billed){
                    $item .= ' <a href="#" class="transfer-counter btn btn-default btn-xs" data-toggle="modal" line-qty="'.$data->qty.'" line-id="'.$data->id.'" data-target=".bs-transfer-counter-modal-sm"><i data-toggle="tooltip" data-placement="right" data-original-title="Transfer item to counter sale" class="glyphicon glyphicon-share-alt"></i></a>
                    <a href="#" class="cost-btn btn btn-default btn-xs" data-toggle="modal" line-id="'.$data->id.'" real-cost="' . $data->line_real_cost . '" sale-cost="' . $data->line_unit_cost . '" data-target=".bs-set-cost-modal-sm"><i data-toggle="tooltip" data-placement="right" data-original-title="Set sale cost" class="fa fa-gbp"></i></a>';
                }
                if (isset($data->misc->id) && $billed == false) {
                    $item .= '<a href="#" class="btn btn-default btn-xs description-item" data-toggle="modal" line-id="' .$data->misc->id . '"  line-item-id="' .$data->id . '"  line-name="' . htmlspecialchars($data->misc->name) . '"  line-qty="'.$data->qty.'"  line-desc="' . htmlspecialchars($data->misc->description) . '" data-target=".bs-description-modal-sm"><i data-toggle="tooltip" data-placement="right" data-original-title="Misc item details" class="glyphicon glyphicon-edit"></i></a>';
                    $item .= '<a href="#" class="btn btn-danger btn-xs delete-item" data-toggle="modal" line-id="' . $data->id . '" data-target=".bs-delete-modal-sm"><i data-toggle="tooltip" data-placement="right" data-original-title="Delete misc item" class="glyphicon glyphicon-trash"></i></a>';
                }

                if($data->misc == null && $billed == false){
                    $item .= '<a href="#" class="back-stock btn btn-default btn-xs" data-toggle="modal"  item-id="' . $data->item_id . '"   line-id="' . $data->id . '" line-qty="' . $data->qty . '" line-location="' . $data->location_id . '" data-target=".bs-back-stock-modal-sm"><i data-toggle="tooltip" data-placement="right" data-original-title="Receive back into stock" class="fa fa-cart-arrow-down"></i></a>';
                }
	            return '<div class="btn-group">'.$item.'</div>';
            })
	        ->filter(function($query) use ($urlquery){
	            if ( ! empty($urlquery['search[value]'])) {
                    $query->leftJoin('job_items_misc as misc', 'job_items.id', '=', 'misc.job_item_id')
                          ->leftJoin('items as itm', 'job_items.item_id', '=', 'itm.id')
                          ->leftJoin('elements as ele', 'job_items.element_id', '=', 'ele.id')
                          ->whereRaw('(REPLACE(`misc`.`description`, \' \', \'\') LIKE REPLACE(\'%' . $urlquery['search[value]'] . '%\', \' \', \'\') or 
                                REPLACE(`misc`.`name`, \' \', \'\') LIKE REPLACE(\'%' . $urlquery['search[value]'] . '%\', \' \', \'\') or
                                REPLACE(`itm`.`description`, \' \', \'\') LIKE REPLACE(\'%' . $urlquery['search[value]'] . '%\', \' \', \'\') or
                                REPLACE(`itm`.`partnumber`, \' \', \'\') LIKE REPLACE(\'%' . $urlquery['search[value]'] . '%\', \' \', \'\') or 
                                REPLACE(`itm`.`barcode`, \' \', \'\') LIKE REPLACE(\'%' . $urlquery['search[value]'] . '%\', \' \', \'\') or
                                REPLACE(`ele`.`name`, \' \', \'\') LIKE REPLACE(\'%' . $urlquery['search[value]'] . '%\', \' \', \'\'))');
                }
	        })
            ->order(function($query) {
                $query->orders = null;
                $dir = request()->get('order')[0]['dir'];
                switch (request()->get('order')[0]['column']) {
                    // case '0':
                    //     $col = 'job.created_at';
                    //     break;
                    
                    // case '1':
                    //     $col = 'bt.name';
                    //     break;
                    
                    // case '3':
                    //     $col = 'cus.name';
                    //     break;
                    
                    case '8':
                        $col = 'job_items.line_cost';
                        break;
                    
                    default:
                        $col = 'job_items.created_at';
                        $dir = 'DESC';
                        break;
                }
                $query->orderBy($col, $dir);
            })
            ->make(true);

            return $table;
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
    	// $items = JobItems::selectRaw('DISTINCT job_items.id, job_items.job_id, job_items.item_id, job_items.location_id, job_items.element_id, job_items.qty, job_items.line_real_cost, job_items.line_unit_cost, job_items.line_cost, job_items.pin_id, job_items.created_at, job_items.updated_at, job_items.deleted_at');
        // $items->where('job_items.job_id', $this->job_id);
        $items = JobItems::query()->select("job_items.*")->where('job_items.job_id', $this->job_id)->distinct();
	    $items->with('size');
	    $items->with('size.unit');
	    $items->with('misc');
	    $items->with('item', 'item.sizes', 'item.sexes', 'item.types', 'item.materials', 'item.grades');
	    $items->with('element');
	    $items->with('pin');
	    $items->with('job');
	    $items->with('location');
        $items->Leftjoin('elements', 'elements.id', '=', 'job_items.element_id');
        $items->orderBy('job_items.created_at', 'DESC');
//        $items->groupByRaw('elements.name ASC');
        return $this->applyScopes($items);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
	        ->columns($this->getColumns())
            ->ajax('')
            ->addAction(['width' => '80px'])
            ->parameters([
                'dom' => 'Bfrtip',
                "order" => [[ 1, "desc" ]],
                'scrollX' => true,
                'pagingType' => 'extStyle',
                'buttons' => [
                    'csv',
                    'excel',
                    'pdf',
                    'print',
                    'reset',
                    'reload',
                ]
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
		    'Expand' => ['name' => 'expand', 'data' => 'expand', 'orderable' => false, 'searchable' => false, 'printable', false, 'exportable' => false],
            'Issued' => ['name' => 'created_at', 'data' => 'issue_date'],
            'Name' => ['name' => 'the_name', 'data' => 'the_name', 'orderable' => false],
		    'Description / Partnumber' => ['name' => 'the_desc', 'data' => 'the_desc', 'orderable' => false],
		    'Quantity' => ['name' => 'qty', 'data' => 'qty', 'orderable' => false],
            'Cost Unit Price' => ['name' => 'real_unit_cost', 'data' => 'real_unit_cost', 'orderable' => false],
            'Cost Total Price' => ['name' => 'real_total_cost', 'data' => 'real_total_cost', 'orderable' => false],
            'Sale Unit Price' => ['name' => 'sale_unit_cost', 'data' => 'sale_unit_cost', 'orderable' => false],
            'Sale Total Price' => ['name' => 'sale_total_cost', 'data' => 'sale_total_cost', 'orderable' => true],
		    'Job Element' => ['name' => 'the_element', 'data' => 'the_element', 'orderable' => false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'job_issues' . time();
    }
}
