<?php

namespace App\DataTables;

use App\Models\User;
use Form;
use Yajra\Datatables\Services\DataTable;

class UserDataTable extends DataTable
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('is_active', function($data) {
                if($data->active == '1'){
                    return '<i class="fa fa-check green"></i>';
                } else {
                    return '<i class="fa fa-times red"></i>';
                }
            })
            ->addColumn('actions', function ($data) {
                            return '
                            <div class=\'btn-group\'>
                                <a href="' . route('users.show', [$data->id]) . '" class=\'btn btn-default btn-xs\'><i data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class=\'action-tooltip\'>View User</span>" class="glyphicon glyphicon-eye-open"></i></a>
                                <a href="' . route('users.edit', [$data->id]) . '" class=\'btn btn-default btn-xs\'><i data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class=\'action-tooltip\'>Edit User</span>" class="glyphicon glyphicon-edit"></i></a>
                            </div>
                            ';
                        })
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $users = User::query();

        return $this->applyScopes($users);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns(array_merge(
                $this->getColumns(),
                [
                    'actions' => [
                        'orderable' => false,
                        'searchable' => false,
                        'printable' => false,
                        'exportable' => false
                    ]
                ]
            ))
            ->parameters([
                'dom' => 'Bfrtip',
                'scrollX' => true,
                'pagingType' => 'extStyle',
                'buttons' => [
                    'csv',
                    'excel',
                    'pdf',
                    'print',
                    'reset',
                    'reload'
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'name' => ['name' => 'name', 'data' => 'name'],
            'email' => ['name' => 'email', 'data' => 'email'],
            'updated_at' => ['name' => 'updated_at', 'data' => 'updated_at'],
            'active' => ['name' => 'is_active', 'data' => 'is_active']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'users';
    }
}
