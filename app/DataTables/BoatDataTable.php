<?php

namespace App\DataTables;

use App\Models\Boat;
use Form;
use Yajra\Datatables\Services\DataTable;
use App\Models\Customer;

class BoatDataTable extends DataTable
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
	    $urlquery = proper_parse_str(urldecode($_SERVER['QUERY_STRING']));

        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('customer', function($data) {
                return $data->customer['name'];
            })
            ->addColumn('action', 'boats.datatables_actions')
	        ->filter(function($query) use ($urlquery){
                if (!empty(request()->get('search')['value'])) {
                    $term = '%'.$this->request()->input('search')['value'].'%';
                    // $query->leftJoin('customer as cus', 'boats.customer_id', '=', 'cus.id');
                    $query->where('boats.number', 'like', $term)
                          ->orWhere('cus.name', 'like', $term)
                          ->orWhere('boats.name', 'like', $term)
                          ->orWhere('boats.sage_ref', 'like', $term)
                          ->orWhere('boats.note', 'like', $term);
                }
	        })
            // ->order(function($query){
            //     if (request()->input('order')[0]['column']=="4") {
            //         $query->orders = null;
            //         $query->orderBy('customer.name', request()->input('order')[0]['dir']);
            //     }
            // })
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
	    $boats = Boat::query()->select('boats.*')->distinct()->leftJoin('customer as cus', 'boats.customer_id', '=', 'cus.id');;
//	    $boats->with('stores');
        return $this->applyScopes($boats);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->addAction(['width' => '10%'])
            ->ajax('')
            ->parameters([
                'dom' => 'Bfrtip',
                'pagingType' => 'extStyle',
                'scrollX' => false,
                'buttons' => [
                    'create',
                    'print',#
                    'reset',
                    'reload',
                    [
                         'extend'  => 'collection',
                         'text'    => '<i class="fa fa-download"></i> Export',
                         'buttons' => [
                             'csv',
                             'excel',
                             'pdf',
                         ],
                    ]
                ]
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'sage_ref' => ['name' => 'sage_ref', 'data' => 'sage_ref'],
            'name' => ['name' => 'name', 'data' => 'name'],
            'note' => ['name' => 'note', 'data' => 'note', 'defaultContent' => ''],
            'number' => ['name' => 'number', 'data' => 'number'],
            'customer' => ['name' => 'customer', 'data' => 'customer', 'searchable' => false, 'orderable' => true]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'boats';
    }
}
