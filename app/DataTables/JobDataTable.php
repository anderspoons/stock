<?php

namespace App\DataTables;

use App\Models\Job;
use Form;
use Yajra\Datatables\Services\DataTable;
use App\Models\Customer;
use App\Models\Address;
use App\Models\Boat;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class JobDataTable extends DataTable
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('checkbox', function ($data) {
                $is_closed = false;
                if($data->statuses()->get()){
                    foreach($data->statuses as $status){
                        if($status->id == '13'||$status->id == '9'){
                            $is_closed = true;
                        }
                    }
                }
                $elem = "<input type=\"checkbox\" name=\"closeMulti\" class=\"closeMulti\" value=\"{$data->id}\"";
                if ($is_closed) $elem .= " disabled";
                $elem .= ">";
                return $elem;
            })
            ->addColumn('created_date', function ($data) {
                $dt = Carbon::parse($data->created_at);
                return $dt->format('d/m/y');
            })
            // ->addColumn('note', function ($data) {
            //     return print_r(request()->session()->get('key'),true);
            // })
            ->addColumn('job_statuses', function ($data) {
                $str = '';
                foreach ($data->statuses as $status) {
                    $str .= '<span class="btn-xs ' . $status->style . '">' . $status->name . '</span>&nbsp;';
                }

                return $str;
            })
            ->addColumn('customer', function ($data) {
                $var      = ' ';
                $customer = Customer::where('id', $data->customer_id)->first();
                if (isset($customer->name)) {
                    $var = $customer->name;
                }

                return $var;
            })
            ->addColumn('boat_name', function ($data) {
                if (isset($data->boat->name)) {
                    return $data->boat->name . ' : ' . $data->boat->number;
                } else {
                    return '';
                }
            })
            ->addColumn('action', function ($data) {
                $is_closed = false;

                if($data->statuses()->get()){
                    foreach($data->statuses as $status){
                        if($status->id == '13'||$status->id == '9'){
                            $is_closed = true;
                        }
                    }
                }



                if (!$is_closed) {
                    $closed = '<a data-id="'.$data->id.'" data-toggle="modal" data-target=".bs-transfer-as-counter-modal-sm" class="btn btn-default btn-xs transfer-as-counter"><span data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class=\'action-tooltip\'>Transfer to Counter Sale</span>"><i class="fa fa-exchange"></i></span></a>
                        <a href="issue/job/' . $data->id . '" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class=\'action-tooltip\'>Issue Stock</span>">
                        <i class="glyphicon glyphicon-shopping-cart"></i>
                        </a>';
                } else {
                    $closed = '<a href="/jobs/excel/' . $data->id . '" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class=\'action-tooltip\'>Export XLS</span>">
                        <i class="fa fa-file-excel-o"></i>
                        </a>';
                }

                return '<div class="btn-group">
                    <a href="' . url('jobs/issues', $data->id) . '" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class=\'action-tooltip\'>Issues</span>">
                        <i class="glyphicon glyphicon-th-list"></i>
                    </a>
                    <a href="' . route('jobs.edit', $data->id) . '" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class=\'action-tooltip\'>Edit</span>">
                        <i class="glyphicon glyphicon-edit"></i>
                    </a>'/*
                    <a href="/jobs/labour/' . $data->id . '" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class=\'action-tooltip\'>Labour List</span>">
                        <i class="glyphicon glyphicon-user"></i>
                    </a>
                    <a href="/jobs/crane/' . $data->id . '" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class=\'action-tooltip\'>Crane Work List</span>">
                        <i class="fa fa-level-up"></i>
                    </a>
                    <a href="/jobs/carriage/' . $data->id . '" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class=\'action-tooltip\'>Carriage List</span>">
                        <i class="fa fa-truck"></i>
                    </a>*/
                     . $closed  . '
                </div>';
            })
            ->filter(function ($query) {
                $jobFilters = [];
                if (!empty(request()->get('search')['value'])) {
                    $jobFilters['search'] = request()->get('search')['value'];
                    $term = '%'.request()->get('search')['value'].'%';
                    $query->where('bt.number', 'like', $term)
                          ->orWhere('bt.name', 'like', $term)
                          ->orWhere('job.po', 'like', $term)
                          ->orWhere('cus.name', 'like', $term);
                }

                $closed = DB::select('select job_id from job_statuses where status_id = ?', [13]);
                $arry = '';
                foreach ($closed as $id){
                    $arry .= $id->job_id.',';
                }
                $closed = explode(',', rtrim($arry, ','));

                $billed = DB::select('select job_id from job_statuses where status_id = ?', [9]);
                $arry = '';
                foreach ($billed as $id){
                    $arry .= $id->job_id.',';
                }
                $billed = explode(',', rtrim($arry, ','));

                if (!empty(request()->get('customer_filter'))) {
                    $customers = request()->get('customer_filter');
                    $query->whereIn('job.customer_id', $customers);
                    $query->with('customer');
                } else {
                    $query->with('customer');
                }

                if (!empty(request()->get('boat_filter'))) {
                    $boats = request()->get('boat_filter');
                    $query->whereIn('job.boat_id', $boats);
                    $query->with('boat');
                } else {
                    $query->with('boat');
                }

                if (!empty(request()->get('status_filter'))) {
                    $statuses = request()->get('status_filter');
                    $query->leftJoin('job_statuses', 'job.id', '=', 'job_statuses.job_id')
                          ->whereIn('job_statuses.status_id', $statuses);
                          // ->leftJoin('status', 'job_statuses.status_id', '=', 'status.id')
                          // ->with('statuses');
                } else {
                    $query->with('statuses');
                }

                if ( !empty(request()->get('show_closed'))||!empty(request()->session()->get('filters.jobs.show_closed')) ) {
                    $jobFilters['show_closed'] = request()->get('show_closed');
                    if (request()->get('show_closed') == "true") {
                        $query->whereIn('job.id', $closed);
                    } else {
                        $query->whereNotIn('job.id', $closed);
                    }
                } else {
                    $query->whereNotIn('job.id', $closed);
                }

                if (!empty(request()->get('show_billed'))||request()->session()->has('filters.jobs.show_billed')) {
                    $jobFilters['show_billed'] = (request()->get('show_billed')!=request()->session()->get('filters.jobs.show_billed')) ? request()->get('show_billed') : request()->session()->get('filters.jobs.show_billed');
                    if (request()->get('show_billed') == "true") {
                        $query->whereIn('job.id', $billed);
                    } else {
                        $query->whereNotIn('job.id', $billed);
                    }
                } else {
                    $query->whereNotIn('job.id', $billed);
                }

                request()->session()->put('filters.jobs', $jobFilters);

            })
            ->order(function($query) {
                $query->orders = null;
                $dir = request()->get('order')[0]['dir'];
                switch (request()->get('order')[0]['column']) {
                    case '1':
                        $col = 'job.created_at';
                        break;
                    
                    case '2':
                        $col = 'bt.name';
                        break;
                    
                    case '4':
                        $col = 'cus.name';
                        break;
                    
                    case '5':
                        $col = 'job.po';
                        break;
                    
                    default:
                        $col = 'job.created_at';
                        break;
                }
                $query->orderBy($col, $dir);
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $jobs = Job::query()->select('job.*')->distinct();
        $jobs->leftJoin('boats as bt', 'job.boat_id', '=', 'bt.id');
        $jobs->leftJoin('customer as cus', 'job.customer_id', '=', 'cus.id');
        // $jobs->leftJoin('job_statuses', 'job.id', '=', 'job_statuses.job_id');
        // $jobs->where('job_statuses.status_id',"9");
        // $jobs->where('job.closed_at', 'NOT' ,NULL);
        // $jobs->orderBy('bt.name', 'ASC');
        // $jobs->orderBy('po', 'ASC');
        $jobs->orderBy('job.created_at', 'ASC');

        return $this->applyScopes($jobs);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
        ->columns($this->getColumns())
        ->addAction(['width' => '100%'])
        ->ajax('')
        ->parameters([
            'dom'     => 'Bfrtip',
            'scrollX' => false,
            "order" => [[ 1, "DESC" ]],
            'buttons' => [
                'print',
                'reset',
                'reload',
                [
                    'extend'  => 'collection',
                    'text'    => '<i class="fa fa-download"></i> Export',
                    'buttons' => [
                        'csv',
                        'excel',
                        'pdf',
                    ],
                ]
            ],
        ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'checkbox'        => ['name' => 'checkbox', 'data' => 'checkbox','orderable' => false],
            'date'         => [
                'name'           => 'created_date',
                'data'           => 'created_date',
                'defaultContent' => ' ',
                'orderable'      => true,
                'searchable'     => false,
            ],
            'boat'        => ['name'       => 'boat_name',
                              'data'       => 'boat_name',
                              'searchable' => true,
                              'orderable'  => true
            ],
            'note'        => ['name' => 'note', 'data' => 'note'],
            'customer'    => ['name' => 'customer', 'data' => 'customer', 'searchable' => false, 'orderable' => true],
            'customer_po' => ['name' => 'po', 'data' => 'po', 'searchable' => false],
            'status'      => ['name'       => 'job_statuses',
                              'data'       => 'job_statuses',
                              'searchable' => false,
                              'orderable'  => false
            ],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'jobs';
    }

}
