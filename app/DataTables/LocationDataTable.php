<?php

namespace App\DataTables;

use App\Models\Location;
use Form;
use Yajra\Datatables\Services\DataTable;

class LocationDataTable extends DataTable
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('actions', function ($data) {
                            return '<div class=\'btn-group\'>
                                <a href="' . route('locations.show', [$data->id]) . '" class=\'btn btn-default btn-xs\'><i class="glyphicon glyphicon-eye-open"></i></a>
                                <a href="' . route('locations.edit', [$data->id]) . '" class=\'btn btn-default btn-xs\'><i class="glyphicon glyphicon-edit"></i></a>
                            </div>';
                        })
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $locations = Location::query()->with('store', 'item');

        return $this->applyScopes($locations);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns(array_merge(
                $this->getColumns(),
                [
                    'actions' => [
                        'orderable' => false,
                        'searchable' => false,
                        'printable' => false,
                        'exportable' => false
                    ]
                ]
            ))
            ->parameters([
                'dom' => 'Bfrtip',
                'pagingType' => 'extStyle',
                'scrollX' => true,
                'buttons' => [
                    'csv',
                    'excel',
                    'pdf',
                    'print',
                    'reset',
                    'reload'
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'store' => ['name' => 'store.name', 'data' => 'store.name', 'orderable' => false, 'seracheable' => false],
            'item' => ['name' => 'item.description', 'data' => 'item.description', 'orderable' => false, 'seracheable' => false],
            'room' => ['name' => 'room', 'data' => 'room'],
            'rack' => ['name' => 'rack', 'data' => 'rack'],
            'shelf' => ['name' => 'shelf', 'data' => 'shelf'],
            'bay' => ['name' => 'bay', 'data' => 'bay'],
            'qty' => ['name' => 'qty', 'data' => 'qty'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'locations';
    }
}
