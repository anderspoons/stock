<?php

namespace App\DataTables;

use App\Models\Supplier;
use Form;
use Yajra\Datatables\Services\DataTable;

class SupplierDataTable extends DataTable
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('actions', function ($data) {
                            return '
                            ' . Form::open(['route' => ['suppliers.destroy', $data->id], 'method' => 'delete']) . '
                            <div class=\'btn-group\'>
                                <a href="' . route('suppliers.show', [$data->id]) . '" class=\'btn btn-default btn-xs\'><i class="glyphicon glyphicon-eye-open"></i></a>
                                <a href="' . route('suppliers.edit', [$data->id]) . '" class=\'btn btn-default btn-xs\'><i class="glyphicon glyphicon-edit"></i></a>                           
                            </div>
                            ' . Form::close() . '
                            ';
                        })
            ->addColumn('expand', function () {
                return  Form::button('<i class="glyphicon glyphicon-plus"></i>', [
                    'type' => 'submit',
                    'class' => 'btn btn-success btn-xs child-expand',
                ]) ;
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $suppliers = Supplier::query()->with('address');

        return $this->applyScopes($suppliers);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns(array_merge(
                [
                    'expand' => [
                        'orderable' => false,
                        'searchable' => false,
                        'printable' => false,
                        'exportable' => false
                    ]
                ],
                $this->getColumns(),
                [
                    'actions' => [
                        'orderable' => false,
                        'searchable' => false,
                        'printable' => false,
                        'exportable' => false
                    ]
                ]
            ))
            ->parameters([
                'dom' => 'Bfrtip',
                'pagingType' => 'extStyle',
                'scrollX' => true,
                'buttons' => [
                    'csv',
                    'excel',
                    'pdf',
                    'print',
                    'reset',
                    'reload'
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'name' => ['name' => 'name', 'data' => 'name'],
            'code' => ['name' => 'code', 'data' => 'code'],
            'phone' => ['name' => 'phone', 'data' => 'phone'],
            'fax' => ['name' => 'fax', 'data' => 'fax'],
            'email' => ['name' => 'email', 'data' => 'email'],
            'min_order' => ['name' => 'min_order', 'data' => 'min_order']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'suppliers';
    }
}
