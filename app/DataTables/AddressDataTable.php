<?php

namespace App\DataTables;

use App\Models\Address;
use Form;
use Yajra\Datatables\Services\DataTable;

class AddressDataTable extends DataTable
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('actions', function ($data) {
                            return '
                            ' . Form::open(['route' => ['addresses.destroy', $data->id], 'method' => 'delete']) . '
                            <div class=\'btn-group\'>
                                <a href="' . route('addresses.edit', [$data->id]) . '" class=\'btn btn-default btn-xs\'><i data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class=\'action-tooltip\'>Edit Address</span>" class="glyphicon glyphicon-edit"></i></a>
                            </div>
                            ' . Form::close() . '
                            ';
                        })
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $addresses = Address::query();

        return $this->applyScopes($addresses);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns(array_merge(
                $this->getColumns(),
                [
                    'actions' => [
                        'orderable' => false,
                        'searchable' => false,
                        'printable' => false,
                        'exportable' => false
                    ]
                ]
            ))
            ->parameters([
                'dom' => 'Bfrtip',
                'scrollX' => true,
                'pagingType' => 'extStyle',
                'buttons' => [
                    'csv',
                    'excel',
                    'pdf',
                    'print',
                    'reset',
                    'reload'
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'line_1' => ['name' => 'line_1', 'data' => 'line_1'],
            'line_2' => ['name' => 'line_2', 'data' => 'line_2'],
            'town' => ['name' => 'town', 'data' => 'town'],
            'county' => ['name' => 'county', 'data' => 'county'],
            'postcode' => ['name' => 'postcode', 'data' => 'postcode'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'addresses';
    }
}
