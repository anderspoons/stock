<?php

namespace App\DataTables;

use App\Models\JobCrane;
use Form;
use Yajra\Datatables\Services\DataTable;

class JobCraneDataTable extends DataTable
{

    protected $job_id;

    public function forJob($id) {
        $this->job_id = $id;
        return $this;
    }
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('issue_date', function($data){
                return date('d/m/Y H:i', strtotime($data->created_at));
            })
            ->addColumn('the_cost', function($data){
                return '£'.$data->cost;
            })
            ->addColumn('the_issuer', function($data){
                return $data->pin['name'];
            })
            ->addColumn('action', function ($data) {
                return '<div class="btn-group">
                    <a href="#" class="cost-btn btn btn-default btn-xs" data-toggle="modal" line-id="'.$data->id.'" line-cost="'.$data->cost.'" data-target=".bs-set-cost-modal-sm"><i data-toggle="tooltip" data-placement="right" data-original-title="Set sale cost" class="fa fa-gbp"></i></a>
                    <a href="#" class="btn btn-danger btn-xs delete-item" data-toggle="modal" line-id="'.$data->id.'" data-target=".bs-delete-modal-sm"><i data-toggle="tooltip" data-placement="right" data-original-title="Delete misc item" class="glyphicon glyphicon-trash"></i></a>
                </div>';
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $jobCranes = JobCrane::where('job_id', $this->job_id);

        return $this->applyScopes($jobCranes);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->addAction(['width' => '10%'])
            ->ajax('')
            ->parameters([
                'dom' => 'Bfrtip',
                'scrollX' => false,
                'buttons' => [
                    'create',
                    'print',
                    'reset',
                    'reload',
                    [
                         'extend'  => 'collection',
                         'text'    => '<i class="fa fa-download"></i> Export',
                         'buttons' => [
                             'csv',
                             'excel',
                             'pdf',
                         ],
                    ]
                ]
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'Issued' => ['name' => 'created_at', 'data' => 'issue_date'],
            'Name' => ['name' => 'name', 'data' => 'name', 'orderable' => false],
            'Sale Cost' => ['name' => 'the_cost', 'data' => 'the_cost', 'orderable' => false],
            'Added By' => ['name' => 'the_issuer', 'data' => 'the_issuer', 'orderable' => false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'jobCranes';
    }
}
