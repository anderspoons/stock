<?php

namespace App\DataTables;

use App\Models\CounterLabour;
use App\User;
use Yajra\Datatables\Services\DataTable;

class CounterLabourDataTable extends DataTable
{
	protected $counter_id;

	public function forCounter($id) {
		$this->counter_id = $id;
		return $this;
	}

	/**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
	    $urlquery = proper_parse_str(urldecode($_SERVER['QUERY_STRING']));
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('issue_date', function($data){
	            return date('d/m/Y H:i', strtotime($data->created_at));
            })
	        ->addColumn('the_cost', function($data){
		        return '£'.$data->cost;
	        })
            ->addColumn('the_issuer', function($data){
	            return $data->pin['name'];
            })
            ->addColumn('action', function ($data) {
	            return '<div class="btn-group">
                    <a href="#" class="cost-btn btn btn-default btn-xs" data-toggle="modal" line-id="'.$data->id.'" line-cost="'.$data->cost.'" data-target=".bs-set-cost-modal-sm"><i data-toggle="tooltip" data-placement="right" data-original-title="Set sale cost" class="fa fa-gbp"></i></a>
                    <a href="#" class="btn btn-danger btn-xs delete-item" data-toggle="modal" line-id="'.$data->id.'" data-target=".bs-delete-modal-sm"><i data-toggle="tooltip" data-placement="right" data-original-title="Delete misc item" class="glyphicon glyphicon-trash"></i></a>
                </div>';
            })
	        ->filter(function($query) use ($urlquery){
		        if(!empty($urlquery['search[value]'])){

		        }
	        })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
    	$items = CounterLabour::where('counter_id', $this->counter_id);
        return $this->applyScopes($items);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
	        ->columns($this->getColumns())
            ->ajax('')
            ->addAction(['width' => '80px'])
            ->parameters([
                'dom' => 'Bfrtip',
                'scrollX' => true,
                'buttons' => [
                    'csv',
                    'excel',
                    'pdf',
                    'print',
                    'reset',
                    'reload',
                ]
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
		    'Issued' => ['name' => 'created_at', 'data' => 'issue_date'],
		    'Name' => ['name' => 'name', 'data' => 'name', 'orderable' => false],
		    'Sale Cost' => ['name' => 'the_cost', 'data' => 'the_cost', 'orderable' => false],
		    'Added By' => ['name' => 'the_issuer', 'data' => 'the_issuer', 'orderable' => false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'counter_issues' . time();
    }
}
