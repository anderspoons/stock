<?php

namespace App\DataTables;

use App\Models\Pincodes;
use Form;
use Yajra\Datatables\Services\DataTable;

class PincodesDataTable extends DataTable
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('active_tick', function ($data) {
	            if($data->active == '1'){
		            return '<i class="fa fa-check green"></i>';
	            } else {
		            return '<i class="fa fa-times red"></i>';
	            }
            })
            ->addColumn('actions', function ($data) {
                            return Form::open(['route' => ['pincodes.destroy', $data->id], 'method' => 'delete']).'
                            <div class=\'btn-group\'>
                                <a href="' . route('pincodes.show', [$data->id]) . '" class=\'btn btn-default btn-xs\'><i data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class=\'action-tooltip\'>View Pin Code</span>"  class="glyphicon glyphicon-eye-open"></i></a>
                                <a href="' . route('pincodes.edit', [$data->id]) . '" class=\'btn btn-default btn-xs\'><i data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class=\'action-tooltip\'>Edit Pin Code</span>" class="glyphicon glyphicon-edit"></i></a>                                
                                <a href="' . url('pincodes/send', [$data->id]) . '" class=\'btn btn-default btn-xs\'><i data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class=\'action-tooltip\'>Re-send Pin Code</span>" class="glyphicon glyphicon-envelope"></i></a>                                
                            </div>
                            '.Form::close();
                        })
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $pincodes = Pincodes::query();
        return $this->applyScopes($pincodes);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns(array_merge(
                $this->getColumns(),
                [
                    'actions' => [
                        'orderable' => false,
                        'searchable' => false,
                        'printable' => false,
                        'exportable' => false
                    ]
                ]
            ))
            ->parameters([
                'dom' => 'Bfrtip',
                'scrollX' => true,
                'pagingType' => 'extStyle',
                'buttons' => [
                    'csv',
                    'excel',
                    'pdf',
                    'print',
                    'reset',
                    'reload'
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'name' => ['name' => 'name', 'data' => 'name'],
            'active' => ['name' => 'active', 'data' => 'active_tick'],
            'email' => ['name' => 'email', 'data' => 'email'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'pincodes';
    }
}
