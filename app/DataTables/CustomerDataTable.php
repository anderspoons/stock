<?php

namespace App\DataTables;

use App\Models\Customer;
use Form;
use Yajra\Datatables\Services\DataTable;

class CustomerDataTable extends DataTable
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', 'customers.datatables_actions')
            ->addColumn('expand', function () {
                return Form::button( '<i class="glyphicon glyphicon-plus"></i>', [
                    'type'           => 'submit',
                    'data-toggle'    => 'tooltip',
                    'data-placement' => 'right',
                    'title'          => 'Show / Hide Details',
                    'class'          => 'btn btn-success btn-xs child-expand',
                ] );
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $customers = Customer::query()->with('address');

        return $this->applyScopes($customers);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns(array_merge(
                [
                    'expand' => [
                        'orderable' => false,
                        'searchable' => false,
                        'printable' => false,
                        'exportable' => false
                    ]
                ],
                $this->getColumns())
            )
            ->addAction(['width' => '10%'])
            ->ajax('')
            ->parameters([
                'dom' => 'Bfrtip',
                'pagingType' => 'extStyle',
                'scrollX' => false,
                'buttons' => [
                    'create',
                    'print',
                    'reset',
                    'reload',
                    [
                         'extend'  => 'collection',
                         'text'    => '<i class="fa fa-download"></i> Export',
                         'buttons' => [
                             'csv',
                             'excel',
                             'pdf',
                         ],
                    ]
                ]
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'expand' => ['name' => 'expand', 'data' => 'expand','orderable' => false],
            'name' => ['name' => 'name', 'data' => 'name'],
            'sage_ref' => ['name' => 'sage_ref', 'data' => 'sage_ref'],
            'phone' => ['name' => 'phone', 'data' => 'phone'],
            'fax' => ['name' => 'fax', 'data' => 'fax'],
            'email' => ['name' => 'email', 'data' => 'email'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'customers';
    }
}
