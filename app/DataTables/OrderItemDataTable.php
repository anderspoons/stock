<?php

namespace App\DataTables;

use App\User;
use Yajra\Datatables\Services\DataTable;
use App\Models\Order;
use App\Models\OrderItems;
use DB;

class OrderItemDataTable extends DataTable
{
    protected $order_id;

    public function forOrder($id)
    {
        $this->order_id = $id;
        return $this;
    }


    /**
     * Build excel file and prepare for export.
     *
     * @return \Maatwebsite\Excel\Writers\LaravelExcelWriter
     */
    protected function buildExcelFile()
    {
        /** @var \Maatwebsite\Excel\Excel $excel */
        $excel = app('excel');

        return $excel->create($this->getFilename(), function ($excel) {
            $excel->sheet('exported-data', function ($sheet) {
                $sheet->setColumnFormat(array(
                    'D:H' => '@',
//                    'E:H' => '"£"#,##0.00-_'
                ));
                $sheet->rows($this->getDataForExport());
            });
        });
    }

    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
	    $request = app('datatables')->getRequest();
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('actions', 'orders.issues_actions')
            ->addColumn('issue_date', function($data){
                return date('d/m/Y H:i', strtotime($data->created_at));
            })
            ->addColumn('the_name', function($data){
                if($data->item) {
                    return $data->item->description;
                } elseif($data->misc) {
                    return $data->misc->name;
                }
            })
            ->addColumn('the_barcode', function($data){
                if($data->item) {
                    return $data->item->barcode;
                }
                return '';
            })
            ->addColumn('the_location', function($data){
                if($data->location_id != 0) {
                	return $data->location->store->code.' '.$data->location->room.$data->location->rack.$data->location->shelf.$data->location->bay;
                }
                return 'TBD';
            })
            ->addColumn('order_purpose', function($data) {
                if($data->job) {
                    $name = '';
                    if($data->job->boat){
                        $name .= $data->job->boat->number .' ';
                    } elseif($data->job->customer){
                        $name .= $data->job->customer->name .' ';
                    }
                    return 'Job: '.$name.'<a href="'.url('/jobs/'.$data->job->id.'/edit').'" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-eye-open"></i></a>';
                }
                if($data->counter) {
	                $name = '';
	                if($data->counter->boat){
		                $name .= $data->counter->boat->number .' ';
	                } elseif($data->counter->customer){
		                $name .= $data->counter->customer->name .' ';
	                }
	                return 'Counter: '.$name.'<a href="'.url('/counters/'.$data->counter->id.'/edit').'" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-eye-open"></i></a>';
                }
                return 'Stock';
            })
            ->addColumn('new_sup', function($data) {
                if($data->new_supplier == '1'){
                    return '<i style="color:#CC0000" class="fa fa-times"></i>';
                } else {
                    return '<i style="color:#00a157" class="fa fa-check"></i>';
                }
            })
            ->addColumn('the_desc', function($data){
                $str = '';
                if(isset($data->item->sizes)) {
                    foreach($data->item->sizes as $size){
                        $str .= ' '.$size->name.' x';
                    }
                    $str = rtrim($str, "x");
                }
                if(isset($data->item)) {
                    if(strlen($str) > 0 && strlen($data->item->partnumber) > 0){
                        $str .= ' : ';
                    }
                    $str .= $data->item->partnumber;
                }
                if(isset($data->misc)) {
                    $str = $data->misc->description;
                }
                return $str;
            })
            ->addColumn('total_cost', function($data) {
                return '£'.number_format((float)$data->line_cost, 2, '.', '');
            })
            ->addColumn('unit_cost', function($data){
                if($data->line_unit_cost != null){
                    return '£'.number_format((float)$data->line_unit_cost, 2, '.', '');
                } else {
                    return 'N/A';
                }
            })
            ->addColumn('expand', function ($data) {
                if ($data->misc) {
                    //Nothing
                    return '';
                } else {
                    return '<button style="padding:1px 5px 5px;" type="submit" data-toggle="tooltip" data-placement="right" data-original-title="Show / Hide Details" title="" class="btn btn-success btn-xs child-expand"><i class="glyphicon glyphicon-plus"></i></button>';
                }
            })
            ->filter(function ($query) use ($request) {

	            if(strlen($request->search['value']) > 0){
		            $query->leftjoin('items as item', 'item.id', '=', 'order_items.item_id');
		            $query->where('item.partnumber', 'like', '%'.$request->search['value'].'%');
		            $query->orWhere('item.barcode', 'like', '%'.$request->search['value'].'%');
		            $query->orWhere('item.description', 'like', '%'.$request->search['value'].'%');

		            $query->leftjoin('locations as location', 'order_items.location_id', '=', 'location.id');
		            $query->orWhere(DB::raw('CONCAT_WS("", location.room, location.rack, location.shelf, location.bay)'), 'like', '%'.$request->search['value'].'%');
	            }
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $items = OrderItems::selectRaw('DISTINCT order_items.id, 
        order_items.order_id,
        order_items.job_id,
        order_items.counter_id, 
        order_items.item_id,
        order_items.location_id, 
        order_items.qty,
        order_items.line_unit_cost, 
        order_items.line_cost,
        order_items.new_supplier,
        order_items.pin_id,
        order_items.created_at,
        order_items.updated_at, 
        order_items.deleted_at');
        $items->where('order_items.order_id', $this->order_id);
        $items->with('new_supplier');
        $items->with('size');
        $items->with('size.unit');
        $items->with('misc');
        $items->with('job');
        $items->with('counter');
        $items->with('item', 'item.sizes', 'item.sexes', 'item.types', 'item.materials', 'item.grades');
        $items->with('pin');
        $items->with('location');
        $items->orderByRaw('order_items.created_at DESC');
        return $this->applyScopes($items);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns(array_merge(
                [
                    'expand' => [
                        'orderable'  => false,
                        'searchable' => false,
                        'printable'  => false,
                        'exportable' => false
                    ]
                ],
                $this->getColumns()
            ))
            ->ajax('')
            ->parameters([
                'dom'     => 'Bfrtip',
                'scrollX' => true,
                'buttons' => [
                    'csv',
                    'excel',
                    'pdf',
                    'print',
                    'reset',
                    'reload',
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'Issued' => ['name' => 'created_at', 'data' => 'issue_date'],
            'Name' => ['name' => 'the_name', 'data' => 'the_name', 'orderable' => false],
            'Location' => ['name' => 'the_location', 'data' => 'the_location', 'orderable' => false],
            'Barcode' => ['name' => 'the_barcode', 'data' => 'the_barcode', 'orderable' => false],
            'Description / Partnumber' => ['name' => 'the_desc', 'data' => 'the_desc', 'orderable' => false],
            'Ordered For' => ['name' => 'order_purpose', 'data' => 'order_purpose', 'orderable' => false],
            'Quantity' => ['name' => 'qty', 'data' => 'qty', 'orderable' => false],
            'Unit Price' => ['name' => 'unit_cost', 'data' => 'unit_cost', 'orderable' => false],
            'Total Price' => ['name' => 'total_cost', 'data' => 'total_cost', 'orderable' => false],
            'Existing Item Supplier' => ['name' => 'new_sup', 'data' => 'new_sup', 'orderable' => false],
            'Actions' => ['name' => 'actions', 'data' => 'actions', 'orderable' => false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'order_items' . time();
    }
}
