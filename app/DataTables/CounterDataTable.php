<?php

namespace App\DataTables;

use App\Models\Counter;
use Form;
use Yajra\Datatables\Services\DataTable;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class CounterDataTable extends DataTable
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('checkbox', function ($data) {
                $is_closed = false;
                if($data->statuses()->get()){
                    foreach($data->statuses as $status){
                        if($status->id == '13'||$status->id == '9'){
                            $is_closed = true;
                        }
                    }
                }
                $elem = "<input type=\"checkbox\" name=\"closeMulti\" class=\"closeMulti\" value=\"{$data->id}\"";
                if ($is_closed) $elem .= " disabled";
                $elem .= ">";
                return $elem;
            })
            ->addColumn('created_date', function ($data) {
                $dt = Carbon::parse($data->created_at);

                return $dt->format('d/m/y');
            })
            ->addColumn('acct_ref', function ($data) {

                if (isset($data->boat->sage_ref)) {
                    if (strlen($data->boat->sage_ref) > 0) {
                        return $data->boat->sage_ref;
                    }
                }

                if (isset($data->customer->sage_ref)) {
                    if (strlen($data->customer->sage_ref) > 0) {
                        return $data->customer->sage_ref;
                    }
                }

                return '';

            })
            ->addColumn('customer_name', function ($data) {
                if (isset($data->customer->name)) {
                    return $data->customer->name;
                } else {
                    return '';
                }
            })
            ->addColumn('boat_name', function ($data) {
                if (isset($data->boat->name)) {
                    return $data->boat->name . ' : ' . $data->boat->number;
                }
                return '';

            })
            ->addColumn('counter_status', function ($data) {
                $str = '';
                foreach ($data->statuses as $status) {
                    $str .= '<span class="btn-xs ' . $status->style . '">' . $status->name . '</span><br/>';
                }

                return $str;
            })
            ->addColumn('action', function ($data) {
                $is_closed = false;
                $is_billed = false;

                if($data->statuses()->get()){
                    foreach($data->statuses as $status){
                        if($status->id == '13'){
                            $is_closed = true;
                        }
                        if($status->id == '9'){
                            $is_billed = true;
                        }
                    }
                }

                $closed = '';

                if (!$is_closed && !$is_billed) {
                    $closed .= '<span data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class=\'action-tooltip\'>Transfer to Job</span>"><a data-id="' . $data->id . '" data-toggle="modal" data-target=".bs-transfer-as-job-modal-sm" class="btn btn-default btn-xs transfer-as-job" >
                        <i class="fa fa-exchange"></i></a></span>
                        <a href="'.url('/issue/counter') . '/'. $data->id . '" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class=\'action-tooltip\'>Issue Items to Counter Sale</span>">
                            <i class="fa fa-cart-plus"></i>
				        </a>';
                }

             //    if (!$is_billed) {
             //        if (isset($data->customer->email)) {
             //            if (valid_email($data->customer->email) && $data->batch_reported == null) {
             //                $closed .= '<span data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class=\'action-tooltip\'>Email Invoice</span>"><a data-id="' . $data->id . '" data-email=' . $data->customer->email . ' data-toggle="modal" data-target=".bs-email-invoice-modal-sm" class="btn btn-default btn-xs btn-email">
					        // <i class="fa fa-envelope-o"></i>
			          //   </a></span>';
             //            }
             //        }
             //    }

                if ($is_closed && !$is_billed) {
                    $closed .= '<a href="'.url('/counters/pdf') .'/'. $data->id . '" target="_blank" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class=\'action-tooltip\'>Generate Invoice</span>" >
                        <i class="fa fa-file-pdf-o"></i>
                    </a>';
                    if (isset($data->customer->email)) {
                        if (valid_email($data->customer->email) && $data->batch_reported == null) {
                            $closed .= '<button data-id="' . $data->id . '" data-email=' . $data->customer->email . ' data-toggle="modal" data-target=".bs-email-invoice-modal-sm" class="btn btn-default btn-xs btn-email" data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class=\'action-tooltip\'>Email Invoice</span>" >
                            <i class="fa fa-envelope-o"></i>
                        </button>';
                        }
                    }
                }

                return '<div class="btn-group action-buttons">
				    <a href="' .url('/counters'). '/'. $data->id . '/edit" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class=\'action-tooltip\'>Edit Counter Sale</span>">
				        <i class="glyphicon glyphicon-edit" ></i>
				    </a>
				    <a href="' .url('/counters/issues') .'/'.  $data->id . '" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class=\'action-tooltip\'>Items List</span>">
				        <i class="glyphicon glyphicon-th-list" ></i>
				    </a>'
				    // <a href="' .url('/counters/labour') .'/'.  $data->id . '" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class=\'action-tooltip\'>Labour List</span>">
				    //     <i class="glyphicon glyphicon-user" ></i>
                    // </a>
                    // <a href=" '.url('/counters/crane') .'/'.  $data->id . '" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class=\'action-tooltip\'>Crane Work List</span>">
                    //     <i class="fa fa-level-up" ></i>
                    // </a>
                    // <a href=" '.url('/counters/carriage') .'/'.  $data->id . '" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class=\'action-tooltip\'>Carriage List</span>">
                    //     <i class="fa fa-truck" ></i>
                    // </a>
                     . $closed . '
                </div>';
            })
            ->filter(function ($query) {
                if (!empty(request()->get('search')['value'])) {
                    $term = '%'.$this->request()->input('search')['value'].'%';
                    $query->where('bt.number', 'like', $term)
                          ->orWhere('bt.name', 'like', $term)
                          ->orWhere('bt.sage_ref', 'like', $term)
                          ->orWhere('counter.ref', 'like', $term)
                          ->orWhere('counter.po', 'like', $term)
                          ->orWhere('cus.sage_ref', 'like', $term)
                          ->orWhere('cus.name', 'like', $term);
                }

                //This is a bit of a shite solution to the problem but basically its assume when all 3 of these dates are Null the counter sale will be closed, invoiced, batched
                //therefore meaning its not open anymore for viewing.

                $closed = DB::select('select counter_id from counter_statuses where status_id = ?', [13]);
                $arry   = '';
                foreach ($closed as $id) {
                    $arry .= $id->counter_id . ',';
                }
                $closed = explode(',', rtrim($arry, ','));

                $billed = DB::select('select counter_id from counter_statuses where status_id = ?', [9]);
                $arry   = '';
                foreach ($billed as $id) {
                    $arry .= $id->counter_id . ',';
                }
                $billed = explode(',', rtrim($arry, ','));

                if (!empty(request()->get('customer_filter'))) {
                    $customers = request()->get('customer_filter');
                    $query->whereIn('counter.customer_id', $customers);
                    $query->with('customer');
                } else {
                    $query->with('customer');
                }

                if (!empty(request()->get('boat_filter'))) {
                    $boats = request()->get('boat_filter');
                    $query->whereIn('counter.boat_id', $boats);
                    $query->with('boat');
                } else {
                    $query->with('boat');
                }

                if (!empty(request()->get('status_filter'))) {
                    $statuses = request()->get('status_filter');
                    $query->leftJoin('counter_statuses', 'counter.id', '=', 'counter_statuses.counter_id')
                          ->whereIn('counter_statuses.status_id', $statuses)
                          ->leftJoin('status', 'counter_statuses.status_id', '=', 'status.id')
                          ->with('statuses');
                } else {
                    $query->with('statuses');
                }

                if (!empty(request()->get('show_closed'))) {
                    if (request()->get('show_closed') == "true") {
                        $query->whereIn('counter.id', $closed);
                    } else {
                        $query->whereNotIn('counter.id', $closed);
                    }
                } else {
                    $query->whereNotIn('counter.id', $closed);
                }

                if (!empty(request()->get('show_billed'))) {
                    if (request()->get('show_billed') == "true") {
                        $query->whereIn('counter.id', $billed);
                    } else {
                        $query->whereNotIn('counter.id', $billed);
                    }
                } else {
                    $query->whereNotIn('counter.id', $billed);
                }

            })
            ->order(function($query) {
                $query->orders = null;
                $dir = request()->get('order')[0]['dir'];
                switch (request()->get('order')[0]['column']) {
                    case '1':
                        $col = 'counter.created_at';
                        break;
                    
                    case '2':
                        $col = 'bt.name';
                        break;
                    
                    case '4':
                        $col = 'cus.name';
                        break;
                    
                    case '5':
                        $col = 'counter.po';
                        break;
                    
                    case '6':
                        $col = 'bt.sage_ref';
                        break;
                    
                    case '7':
                        $col = 'counter.ref';
                        break;
                    
                    default:
                        $col = 'counter.created_at';
                        break;
                }
                $query->orderBy($col, $dir);
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $counters = Counter::query()->select('counter.*')->distinct();
        $counters->leftJoin('boats as bt', 'counter.boat_id', '=', 'bt.id');
        $counters->leftJoin('customer as cus', 'counter.customer_id', '=', 'cus.id');
        $counters->orderBy('bt.name', 'ASC');
        $counters->orderBy('cus.name', 'ASC');
        $counters->orderBy('counter.created_at', 'ASC');
        $counters->get();

        return $this->applyScopes($counters);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->addAction(['width' => '10%'])
                    ->ajax('')
                    ->parameters([
                        'dom'     => 'Bfrtip',
                        'scrollX' => false,
                        "order" => [[ 1, "DESC" ]],
                        'buttons' => [
                            'print',
                            'reset',
                            'reload',
                            [
                                'extend'  => 'collection',
                                'text'    => '<i class="fa fa-download"></i> Export',
                                'buttons' => [
                                    'csv',
                                    'excel',
                                    'pdf',
                                ],
                            ]
                        ]
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'checkbox'        => ['name' => 'checkbox', 'data' => 'checkbox','orderable' => false],
            'date'         => [
                'name'           => 'created_date',
                'data'           => 'created_date',
                'defaultContent' => ' ',
                'orderable'      => true,
                'searchable'     => false,
            ],
            'boat'         => [
                'name'           => 'boat_name',
                'data'           => 'boat_name',
                'defaultContent' => ' ',
                'orderable'      => true,
                'searchable'     => true,
            ],
            'note'         => [
                'name'           => 'note', 
                'data'           => 'note',
                'searchable'     => false
            ],
            'customer'     => [
                'name'           => 'customer_name',
                'data'           => 'customer_name',
                'defaultContent' => ' ',
                'orderable'      => true,
                'searchable'     => true
            ],
            'po'           => ['name' => 'po', 'data' => 'po'],
            'sage_account' => [
                'name'      => 'acct_ref',
                'data'      => 'acct_ref',
                'orderable' => true
            ],
            'ref'          => ['name' => 'ref', 'data' => 'ref'],
            'status'       => [
                'name'       => 'counter_status',
                'data'       => 'counter_status',
                'searchable' => false,
                'orderable'  => false
            ],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'counters';
    }
}
