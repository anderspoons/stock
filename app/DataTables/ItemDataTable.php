<?php

namespace App\DataTables;

use App\Models\Item;
use App\Models\Price;
use Form;
use League\Fractal\Pagination\PagerfantaPaginatorAdapter;
use Yajra\Datatables\Services\DataTable;
use DB;

class ItemDataTable extends DataTable
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        $urlquery = proper_parse_str(urldecode($_SERVER['QUERY_STRING']));

        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('actions', 'items.datatables_actions')
            ->addColumn('price', function ($data) {
                $var = 'Not Set';
                $prices = Price::where('item_id', $data->id)
                    ->orderBy('id', 'DESC')
                    ->get();
                $var = '£0';
                foreach($prices as $price){
                    if(isset($price->cost) && (!isset($price->supplier_id) || $price->cost > 0 )){
                        // $var = '£'.money_format("%.2n", $price->cost);
                        $var = '£'.sprintf("%1\$.2f", $price->cost);
                        break;
                    }
                }
                return $var ;
            })
            ->addColumn('expand', function () {
                return  Form::button('<i class="glyphicon glyphicon-plus"></i>', [
                    'type' => 'submit',
                    'data-toggle' => 'tooltip',
                    'data-placement' => 'right',
                    'title' => 'Show / Hide Details',
                    'class' => 'btn btn-success btn-xs child-expand',
            ]);
            })->filter(function($query) use ($urlquery){
                if (isset( $urlquery['type_filter[]'])) {
                    $types = (array)$urlquery['type_filter[]'];
                    if(isset($urlquery['search_type']) && $urlquery['search_type'] == 'specific'){
                        $query->selectRaw('(SELECT COUNT(*) FROM type_items WHERE type_items.item_id = items.id AND type_items.type_id IN ('.implode(',', $types).')) AS "actual_types"')
                            ->selectRaw('(SELECT COUNT(*) FROM type_items WHERE type_items.item_id = items.id) AS "total_types"')
                            ->leftJoin( 'type_items', 'items.id', '=', 'type_items.item_id' )
                            ->whereIn( 'type_items.type_id', $types )
                            ->leftJoin( 'type', 'type_items.type_id', '=', 'type.id' )
                            ->groupBy('type_items.item_id')
                            ->havingRaw('actual_types = '.count($types).' AND total_types = '.count($types));
                    } else {
                        $query->leftJoin( 'type_items', 'items.id', '=', 'type_items.item_id' )
                              ->whereIn( 'type_items.type_id', $types )
                              ->leftJoin( 'type', 'type_items.type_id', '=', 'type.id' );
                    }
                    $query->with('types');
                } else {
                    $query->with( 'types' );
                }

                if ( ! empty( $urlquery['material_filter[]'] ) ) {
                    $materials = (array)$urlquery['material_filter[]'];
                    if(isset($urlquery['search_type']) && $urlquery['search_type'] == 'specific') {
                        $query->selectRaw('(SELECT COUNT(*) FROM material_items WHERE material_items.item_id = items.id AND material_items.material_id IN ('.implode(',', $materials).')) AS "actual_materials"')
                              ->selectRaw('(SELECT COUNT(*) FROM material_items WHERE material_items.item_id = items.id) AS "total_materials"')
                              ->leftJoin( 'material_items', 'items.id', '=', 'material_items.item_id' )
                              ->whereIn( 'material_items.material_id', $materials )
                              ->leftJoin( 'material', 'material_items.material_id', '=', 'material.id' )
                              ->groupBy('material_items.item_id')
                              ->havingRaw('actual_materials = '.count($materials).' AND total_materials = '.count($materials));
                    } else {
                        $query->leftJoin( 'material_items', 'items.id', '=', 'material_items.item_id' )
                              ->whereIn( 'material_items.material_id', $materials)
                              ->leftJoin( 'material', 'material_items.material_id', '=', 'material.id' );
                    }
                    $query->with( 'materials' );
                } else {
                    $query->with( 'materials' );
                }

                if ( ! empty( $urlquery['grade_filter[]'] ) ) {
                    $grades = (array)$urlquery['grade_filter[]'];
                    if(isset($urlquery['search_type']) && $urlquery['search_type'] == 'specific') {
                        $query->selectRaw('(SELECT COUNT(*) FROM grade_items WHERE grade_items.item_id = items.id AND grade_items.grade_id IN ('.implode(',', $grades).')) AS "actual_grades"')
                              ->selectRaw('(SELECT COUNT(*) FROM grade_items WHERE grade_items.item_id = items.id) AS "total_grades"')
                              ->leftJoin( 'grade_items', 'items.id', '=', 'grade_items.item_id' )
                              ->whereIn( 'grade_items.grade_id', $grades )
                              ->leftJoin( 'grade', 'grade_items.grade_id', '=', 'grade.id' )
                              ->groupBy('grade_items.item_id')
                              ->havingRaw('actual_grades = '.count($grades).' AND total_grades = '.count($grades));
                    } else {
                        $query->leftJoin( 'grade_items', 'items.id', '=', 'grade_items.item_id' )
                              ->whereIn( 'grade_items.grade_id', $grades)
                              ->leftJoin( 'grade', 'grade_items.grade_id', '=', 'grade.id' );
                    }
                } else {
                    $query->with( 'grades' );
                }

                if ( ! empty( $urlquery['size_filter[]'] ) ) {
                    $sizes = (array)$urlquery['size_filter[]'];
                    if(isset($urlquery['search_type']) && $urlquery['search_type'] == 'specific') {
                        $query->selectRaw('(SELECT COUNT(DISTINCT(size_items.size_id)) FROM size_items WHERE size_items.item_id = items.id AND size_items.size_id IN ('.implode(',', $sizes).')) AS "actual_sizes"')
                              ->selectRaw('(SELECT COUNT(size_items.size_id) FROM size_items WHERE size_items.item_id = items.id AND size_items.size_id IN ('.implode(',', $sizes).')) AS "duplicate_sizes"')
                              ->selectRaw('(SELECT COUNT(size_items.size_id) FROM size_items WHERE size_items.item_id = items.id) AS "total_sizes"')
                              ->leftJoin( 'size_items as SI', 'items.id', '=', 'SI.item_id' )
                              ->whereIn( 'SI.size_id', $sizes )
                              ->leftJoin( 'size', 'SI.size_id', '=', 'size.id' )
                              ->groupBy('SI.item_id')
                              ->havingRaw('(actual_sizes = '.count($sizes).' AND total_sizes = '.count($sizes).' OR (duplicate_sizes >= total_sizes AND actual_sizes = '.count($sizes).'))');
                    } else {
                        $query->leftJoin( 'size_items as SI', 'items.id', '=', 'SI.item_id' )
                              ->whereIn( 'SI.size_id', $sizes)
                              ->leftJoin( 'size', 'SI.size_id', '=', 'size.id' );
                    }
                    $query->with( 'sizes' );
                } else {
                    $query->with( 'sizes' );
                }
                if ( ! empty( $urlquery['sex_filter[]'] ) ) {
                    $sexes = (array)$urlquery['sex_filter[]'];
                    if(isset($urlquery['search_type']) && $urlquery['search_type'] == 'specific') {
                        $query->selectRaw('(SELECT COUNT(DISTINCT(size_items.sex_id)) FROM size_items WHERE size_items.item_id = items.id AND size_items.sex_id IN ('.implode(',', $sexes).')) AS "actual_sexes"')
                              ->selectRaw('(SELECT COUNT(size_items.sex_id) FROM size_items WHERE size_items.item_id = items.id AND size_items.sex_id IN ('.implode(',', $sexes).')) AS "duplicate_sexes"')
                              ->selectRaw('(SELECT COUNT(size_items.sex_id) FROM size_items WHERE size_items.item_id = items.id) AS "total_sexes"')
                              ->leftJoin( 'size_items', 'items.id', '=', 'size_items.item_id' )
                              ->whereIn( 'size_items.sex_id', $sexes )
                              ->leftJoin( 'sex', 'size_items.sex_id', '=', 'sex.id' )
                              ->groupBy('size_items.item_id')
                              ->havingRaw('(actual_sexes = '.count($sexes).' AND total_sexes = '.count($sexes).' OR (duplicate_sexes >= total_sexes AND actual_sexes = '.count($sexes).'))');
                    } else {
                        $query->leftJoin( 'size_items as SI2', 'items.id', '=', 'SI2.item_id' )
                              ->whereIn( 'SI2.sex_id', $sexes)
                              ->leftJoin( 'sex', 'SI2.sex_id', '=', 'sex.id' );
                    }
                    $query->with( 'sexes' );
                } else {
                    $query->with( 'sexes' );
                }
                if ( ! empty( $urlquery['supplier_filter[]'] ) ) {
                    $suppliers = (array)$urlquery['supplier_filter[]'];
                    if(isset($urlquery['search_type']) && $urlquery['search_type'] == 'specific') {
                        $query->selectRaw('(SELECT COUNT(*) FROM supplier_items WHERE supplier_items.item_id = items.id AND supplier_items.supplier_id IN ('.implode(',', $suppliers).')) AS "actual_suppliers"')
                              ->selectRaw('(SELECT COUNT(*) FROM supplier_items WHERE supplier_items.item_id = items.id) AS "total_suppliers"')
                              ->leftJoin( 'supplier_items', 'items.id', '=', 'supplier_items.item_id' )
                              ->whereIn( 'supplier_items.supplier_id', $suppliers )
                              ->leftJoin( 'suppliers', 'supplier_items.supplier_id', '=', 'suppliers.id' )
                              ->groupBy('supplier_items.item_id')
                              ->havingRaw('actual_suppliers = '.count($suppliers).' AND total_suppliers = '.count($suppliers));
                    } else {
                        $query->leftJoin( 'supplier_items', 'items.id', '=', 'supplier_items.item_id' )
                              ->whereIn( 'supplier_items.supplier_id', $suppliers)
                              ->leftJoin( 'suppliers', 'supplier_items.supplier_id', '=', 'suppliers.id' );
                    }
                    $query->with( 'suppliers' );
                } else {
                    $query->with( 'suppliers' );
                }

    		        if ( ! empty( $urlquery['status_filter[]'] ) ) {
    			        $statuses = (array)$urlquery['status_filter[]'];
    			        if(isset($urlquery['search_type']) && $urlquery['search_type'] == 'specific') {
    				        $query->selectRaw('(SELECT COUNT(*) FROM items_statuses WHERE items_statuses.item_id = items.id AND items_statuses.status_id IN ('.implode(',', $statuses).')) AS "actual_statuses"')
    				              ->selectRaw('(SELECT COUNT(*) FROM items_statuses WHERE items_statuses.item_id = items.id) AS "total_statuses"')
    				              ->leftJoin( 'items_statuses', 'items.id', '=', 'items_statuses.item_id' )
    				              ->whereIn( 'items_statuses.status_id', $statuses )
    				              ->leftJoin( 'status', 'items_statuses.status_id', '=', 'status.id' )
    				              ->groupBy('items_statuses.item_id')
    				              ->havingRaw('actual_statuses = '.count($statuses).' AND total_statuses = '.count($statuses));
    			        } else {
    				        $query->leftJoin( 'items_statuses', 'items.id', '=', 'items_statuses.item_id' )
    				              ->whereIn( 'items_statuses.status_id', $statuses)
    				              ->leftJoin( 'status', 'items_statuses.status_id', '=', 'status.id' );
    			        }
    			        $query->with( 'suppliers' );
    		        } else {
    			        $query->with( 'suppliers' );
    		        }

              if ( ! empty( $urlquery['store_filter[]'] ) ) {
                $stores = (array)$urlquery['store_filter[]'];
                if(isset($urlquery['search_type']) && $urlquery['search_type'] == 'specific') {
                  $query->selectRaw('(SELECT COUNT(*) FROM locations WHERE locations.item_id = items.id AND locations.store_id IN ('.implode(',', $stores).')) AS "actual_stores"')
                        ->selectRaw('(SELECT COUNT(*) FROM locations WHERE locations.item_id = items.id) AS "total_stores"')
                        ->leftJoin( 'locations', 'items.id', '=', 'locations.item_id' )
                        ->whereIn( 'locations.store_id', $stores )
                        ->leftJoin( 'stores', 'locations.store_id', '=', 'stores.id' )
                        ->groupBy('locations.item_id')
                        ->havingRaw('actual_stores = '.count($stores).' AND total_stores = '.count($stores));
                } else {
                  $query->leftJoin( 'locations', 'items.id', '=', 'locations.item_id' )
                        ->whereIn( 'locations.store_id', $stores)
                        ->leftJoin( 'stores', 'locations.store_id', '=', 'stores.id' );
                }
                $query->with('stores');
              } else {
                $query->with('stores');
              }

              if (!empty(request()->get('search')['value'])) {
                  $term = '%'.$this->request()->input('search')['value'].'%';
                  $query->where('items.description', 'like', $term)
                        ->orWhere('items.partnumber', 'like', $term)
                        ->orWhere('items.barcode', 'like', $term);
              }
            }) //end filter
            ->make(true);
    }

//  Last price
//
//https://github.com/yajra/laravel-datatables/issues/144

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $items = Item::query()->select('items.*')->distinct();
        $items->with('stores');
        $items->with('prices');
        $items->with('unit_category');
        $items->with('supplier_units');
        $items->with('supplier_sizing');

        return $this->applyScopes($items);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns(array_merge(
                [
                    'expand' => [
                        'orderable' => false,
                        'searchable' => false,
                        'printable' => false,
                        'exportable' => false
                    ]
                ],
                $this->getColumns(),
                [
                    'price' => [
                        'orderable' => true,
                        'searchable' => true,
                        'printable' => true,
                        'exportable' => true
                    ]
                ],
                [
                    'actions' => [
                        'orderable' => false,
                        'searchable' => false,
                        'printable' => false,
                        'exportable' => false
                    ]
                ]
            ))
            ->parameters([
                'dom' => 'Bfrtip',
                'scrollX' => true,
                'buttons' => [
                    'csv',
                    'excel',
                    'pdf',
                    'print',
                    'reset',
                    'reload',
                ],
                'drawCallback' => 'function() { window.expandRows(); }',
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'description' => ['name' => 'description', 'data' => 'description'],
            'partnumber' => ['name' => 'partnumber', 'data' => 'partnumber'],
            'markup_percent' => ['name' => 'markup_percent', 'data' => 'markup_percent'],
            'markup_fixed' => ['name' => 'markup_fixed', 'data' => 'markup_fixed'],
            'barcode' => ['name' => 'barcode', 'data' => 'barcode'],
            'due_in' => ['name' => 'on_order', 'data' => 'on_order'],
            'due_out' => ['name' => 'on_reserve', 'data' => 'on_reserve']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'items';
    }

}
