<?php

namespace App\DataTables;

use App\Models\Status;
use Form;
use Yajra\Datatables\Services\DataTable;

class StatusDataTable extends DataTable
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('job_tick', function ($data) {
	            if($data->job == '1'){
		            return '<i class="fa fa-check green"></i>';
	            } else {
		            return '<i class="fa fa-times red"></i>';
	            }
            })
            ->addColumn('item_tick', function ($data) {
	            if($data->item == '1'){
		            return '<i class="fa fa-check green"></i>';
	            } else {
		            return '<i class="fa fa-times red"></i>';
	            }
            })
            ->addColumn('order_tick', function ($data) {
	            if($data->order == '1'){
		            return '<i class="fa fa-check green"></i>';
	            } else {
		            return '<i class="fa fa-times red"></i>';
	            }
            })
            ->addColumn('auto_tick', function ($data) {
	            if($data->auto_status == '1'){
		            return '<i class="fa fa-check green"></i>';
	            } else {
		            return '<i class="fa fa-times red"></i>';
	            }
            })
            ->addColumn('action', 'statuses.datatables_actions')
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $statuses = Status::query();

        return $this->applyScopes($statuses);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->addAction(['width' => '10%'])
            ->ajax('')
            ->parameters([
                'dom' => 'Bfrtip',
                'scrollX' => false,
                'pagingType' => 'extStyle',
                'buttons' => [
                    'create',
                    'print',
                    'reset',
                    'reload',
                    [
                         'extend'  => 'collection',
                         'text'    => '<i class="fa fa-download"></i> Export',
                         'buttons' => [
                             'csv',
                             'excel',
                             'pdf',
                         ],
                    ]
                ]
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
	        'name' => ['name' => 'name', 'data' => 'name'],
            'Job_Status' => ['name' => 'job', 'data' => 'job_tick'],
            'Item_Status' => ['name' => 'item', 'data' => 'item_tick'],
            'Order_Status' => ['name' => 'order', 'data' => 'order_tick'],
            'auto_status' => ['name' => 'auto_status', 'data' => 'auto_tick']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'statuses';
    }
}
