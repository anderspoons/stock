<?php

namespace App\DataTables;

use App\Models\Order;
use Form;
use Yajra\Datatables\Services\DataTable;

class OrderDataTable extends DataTable
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {

	    $request = app('datatables')->getRequest();
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', function($data){
                $cant_issue = false;
                $on_order = false;
				//This would be better as a bool field but I don't want to complicate this further.
                if (isset($data->statuses)) {
                    foreach ($data->statuses as $status) {
                        if(in_array($status->id, [2,6,14,15])){
                            $on_order = true;
                        }
                        if (in_array($status->id, [13,1,2,6,7,14,15])) {
                            $cant_issue = true;
                        }
                    }
                }

                $actions = '';
                if(isset($data->items)){
                    $actions .= '<a href="'.url('orders/items/'.$data->id).'" class="btn btn-default btn-xs">
				        <i data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class=\'action-tooltip\'>List Items on Order</span>" class="fa fa-list"></i>
				    </a>';
                }

                $actions .= '<a href="'.route('orders.edit', $data->id).'" class=\'btn btn-default btn-xs\'>
                    <i data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class=\'action-tooltip\'>Edit Order</span>" class="glyphicon glyphicon-edit"></i>
                </a>';


                if(!$cant_issue){
                	$not_recieved = false;
	                foreach($data->items as $item){
		                if($item->recieved_at === null){
			                $not_recieved = true;
		                }
	                }
                    if(count($data->items) > 0){
	                    if($not_recieved){
		                    $actions .= '<a href="#" order-id=\''.$data->id.'\' class="btn btn-success btn-xs confirm-order-action" data-toggle="modal" data-target=".bs-ordered-modal-sm">
                            <i data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class=\'action-tooltip\'>Mark as ordered</span>" class="glyphicon glyphicon-check"></i>
	                        </a>
	                        <a href="'.url('order/send/fax').'/'.$data->id.'" class="btn btn-default btn-xs fax-order">
	                            <i data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class=\'action-tooltip\'>Produce a fax report to print</span>" class="fa fa-fax"></i>
	                        </a>
	                         <a href="#" data-toggle="modal" order-email="'.$data->supplier->email.'" order-id=\''.$data->id.'\' data-target=".bs-email-order-modal-sm" class="btn btn-default btn-xs email-order">
	                            <i data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class=\'action-tooltip\'>Email to </span>" class="fa fa-envelope-o"></i>
	                        </a>';
	                    } else {
		                    $actions .= '<a href="#" class="btn btn-warning btn-xs confirm-order-action" data-toggle="modal" data-target=".bs-ordered-modal-sm">
                            <i data-toggle="tooltip" order-id=\''.$data->id.'\' data-placement="bottom" data-html="true" title="<span class=\'action-tooltip\'>Close this order.</span>" class="close-order glyphicon glyphicon-thumbs-up"></i>
	                        </a>';
	                    }
                    }
	                if($not_recieved || count($data->items) == 0) {
		                $actions .= '<a href="' . url( 'orders/issue/' . $data->id ) . '" class=\'btn btn-info btn-xs\'>
				        <i data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class=\'action-tooltip\'>Add Items</span>" class="glyphicon glyphicon-shopping-cart"></i>
				    </a>';
	                }
                }

                if($on_order){
                    $actions .= '<a href="'.url('orders/receive/'.$data->id).'" class=\'btn btn-success btn-xs\'>
				        <i data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class=\'action-tooltip\'>Receive Items</span>" class="glyphicon glyphicon-arrow-down"></i>
				    </a>';
                }

                if($this->complete($data->statuses) && $on_order){
	                $actions .= '<a href="'.url('orders/complete/'.$data->id).'" class=\'btn btn-success btn-xs\'>
				        <i data-toggle="tooltip" data-placement="bottom" data-html="true" title="<span class=\'action-tooltip\'>Complete & Close Order</span>" class="glyphicon glyphicon-ok"></i>
				    </a>';
                }

            	return '<div class=\'btn-group\'>
				    '.$actions.'
				</div>';
            })
            ->addColumn('statuses', function($data) {
                $str = '';
                foreach($data->statuses as $status){
                    $str .= '<span class="btn-xs '.$status->style.'">'.$status->name.'</span>&nbsp;';
                }
                return $str;
            })
            ->addColumn('expected', function($data) {
                if($data->expected_at == null){
	                return '';
                } else {
	                return \Carbon\Carbon::parse($data->expected_at)->format('d/m/y');
                }
            })
            ->addColumn('item_count', function($data) {
                return count($data->items);
            })->filter(function($query) use ($request) {
	            $query->whereNUll('closed_at');
	            if(strlen($request->search['value']) > 0){
                    $term = '%'.$request->search['value'].'%';
		            $query->where('po', 'like', $term);
		            $query->orWhere('custom_po', 'like', $term);
		            $query->orWhere('description', 'like', $term);
		            $query->leftjoin('suppliers as supplier', 'supplier.id', '=', 'order.supplier_id');
		            $query->orWhere('supplier.name', 'like', $term);
	            }
            })
            ->make(true);
    }

    private function complete($statuses){
	    foreach($statuses as $status){
    		if($status->id == 14 || $status->id == 15){
    			return false;
		    }
	    }	    
	    return true;
    }
    
    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
	    $orders = Order::query()->select(
                'order.id',
                'order.supplier_id',
                'order.email_sent',
                'order.store_id',
                'order.description',
                'order.po',
                'order.custom_po',
                'order.created_at',
                'order.yard_id',
                'order.deleted_at',
                'order.expected_at',
                'order.updated_at'
                );
	    $orders->with('supplier', 'items', 'store', 'statuses');
	    $orders->orderby('order.created_at', 'DESC');
	    return $this->applyScopes($orders);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->addAction(['width' => '10%'])
            ->ajax('')
            ->parameters([
                'dom' => 'Bfrtip',
                'scrollX' => false,
                'pagingType' => 'extStyle',
                'buttons' => [
                    'create',
                    'print',
                    'reset',
                    'reload',
                    [
                         'extend'  => 'collection',
                         'text'    => '<i class="fa fa-download"></i> Export',
                         'buttons' => [
                             'csv',
                             'excel',
                             'pdf',
                         ],
                    ]
                ]
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'supplier' => ['name' => 'supplier.name', 'data' => 'supplier.name', 'orderable' => false],
            'store_id' => ['name' => 'store_id', 'data' => 'store_id', 'visible' => false],
            'store' => ['name' => 'store.code', 'data' => 'store.code', 'searchable' => false, 'orderable' => false],
            'minimum_order' => ['name' => 'supplier.min_order', 'data' => 'supplier.min_order', 'searchable' => false, 'orderable' => false],
            'item_count' => ['name' => 'item_count', 'data' => 'item_count', 'searchable' => false, 'orderable' => false],
            'status' => ['name' => 'statuses', 'data' => 'statuses', 'orderable' => false],
            'description' => ['name' => 'description', 'data' => 'description'],
            'po' => ['name' => 'po', 'data' => 'po', 'searchable' => true, 'orderable' => true],
            'expected_arrival' => ['name' => 'expected', 'data' => 'expected'],
            'custom_po' => ['name' => 'custom_po', 'data' => 'custom_po'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'orders';
    }
}
