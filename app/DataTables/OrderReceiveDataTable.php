<?php

namespace App\DataTables;

use App\User;
use League\Fractal\Pagination\PagerfantaPaginatorAdapter;
use Yajra\Datatables\Services\DataTable;
use App\Models\Order;
use App\Models\OrderItems;
use App\Models\Location;
use DB;

class OrderReceiveDataTable extends DataTable
{
    protected $order_id;

    public function forOrder($id) {
	    $this->order_id = $id;

	    return $this;
    }

    /**
     * Build excel file and prepare for export.
     *
     * @return \Maatwebsite\Excel\Writers\LaravelExcelWriter
     */
    protected function buildExcelFile()
    {
        /** @var \Maatwebsite\Excel\Excel $excel */
        $excel = app('excel');

        return $excel->create($this->getFilename(), function ($excel) {
            $excel->sheet('exported-data', function ($sheet) {
                $sheet->setColumnFormat(array(
                    'D:H' => '@',
//                    'E:H' => '"£"#,##0.00-_'
                ));
                $sheet->rows($this->getDataForExport());
            });
        });
    }

    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
	    $request = app('datatables')->getRequest();
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('actions', 'orders.recieve_actions')
            ->addColumn('issue_date', function($data){
                return date('d/m/Y H:i', strtotime($data->created_at));
            })
            ->addColumn('the_name', function($data){
                if($data->item) {
                    return $data->item->description;
                } elseif($data->misc) {
                    return $data->misc->name;
                }
            })
            ->addColumn('qty_outstanding', function($data){
            	return $data->qty - $data->qty_in;
            })
            ->addColumn('the_barcode', function($data){
	            if($data->item) {
		            return $data->item->barcode;
	            }
	            return '';
            })
            ->addColumn('order_purpose', function($data) {
            	if($data->job) {
		            $name = '';
		            if($data->job->boat){
			            $name .= $data->job->boat->number .' ';
		            } elseif($data->job->customer){
			            $name .= $data->job->customer->name .' ';
		            }
		            return 'Job: '.$name.'<a href="'.url('/jobs/'.$data->job->id.'/edit').'" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-eye-open"></i></a>';
                }
	            if($data->counter) {
		            $name = '';
		            if($data->counter->boat){
			            $name .= $data->counter->boat->number .' ';
		            } elseif($data->counter->customer){
			            $name .= $data->counter->customer->name .' ';
		            }
		            return 'Counter: '.$name.'<a href="'.url('/counters/'.$data->counter->id.'/edit').'" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-eye-open"></i></a>';
	            }
	            return 'Stock';
            })
            ->addColumn('new_sup', function($data) {
                if($data->new_supplier == '1'){
                    return '<i style="color:#CC0000" class="fa fa-times"></i>';
                } else {
                    return '<i style="color:#00a157" class="fa fa-check"></i>';
                }
            })
            ->addColumn('recieved', function($data) {
                if(isset($data->recieved_at)){
                    return '<i style="color:#00a157" class="fa fa-check"></i>';
                } else {
                    return '<i style="color:#CC0000" class="fa fa-times"></i>';
                }
            })
            ->addColumn('the_desc', function($data){
                $str = '';
                if(isset($data->item->sizes)) {
                    foreach($data->item->sizes as $size){
                        $str .= ' '.$size->name.' x';
                    }
                    $str = rtrim($str, "x");
                }

                if(isset($data->item)) {
                    if(strlen($str) > 0 && strlen($data->item->partnumber) > 0){
                        $str .= ' : ';
                    }
                    $str .= $data->item->partnumber;
                }

                if(isset($data->misc)) {
                    $str = $data->misc->description;
                }
                return $str;
            })
            ->addColumn('total_cost', function($data) {
                return '£'.number_format((float)$data->line_cost, 2, '.', '');
            })
            ->addColumn('unit_cost', function($data){
                if($data->line_unit_cost != null){
                    return '£'.number_format((float)$data->line_unit_cost, 2, '.', '');
                } else {
                    return 'N/A';
                }
            })
            ->addColumn('select', function ($data) {
                if($data->recieved_at !== null){
                    //Nothing
                    return '';
                } else {
                	$for = 'stock';
                	if($data->job_id != null){
                		$for = 'job';
	                }
                	if($data->counter_id != null){
                		$for = 'counter';
	                }
                    return '<input class="icheck" type="checkbox" for="'.$for.'" line_id="'.$data->id.'" name="id['.$data->id.']" value="" />';
                }
            })
            ->addColumn('complete', function ($data) {
                if($data->recieved_at !== null){
                    //Nothing
                    return '';
                } else {
                    return '<input class="icheck complete" type="checkbox" value="" />';
                }
            })
            ->addColumn('qty-box', function ($data) {
                if($data->recieved_at !== null){
                    //Nothing
	                return '<i style="color:#00a157" class="fa fa-check"></i>';
                } else {
                    if($data->qty_in > 0){
                        return '<input class="form-control receive-qty" disabled style="width:60px" type="number" min="0" max="'.$data->qty-$data->qty_in.'" name="id['.$data->id.']" value="'.$data->recieved_qty.'" />';
                    } else {
                        return '<input class="form-control receive-qty" disabled style="width:60px" type="number" min="0" max="'.$data->qty.'" name="id['.$data->id.']" value="0" />';
                    }
                }
            })
            ->addColumn('the_location', function($data){
	            if($data->location_id != 0) {
		            return $data->location->store->code.' '.$data->location->room.$data->location->rack.$data->location->shelf.$data->location->bay;
	            }
	            return 'TBD';
            })
            ->addColumn('location-box', function ($data) {
                if($data->job_id !== null && $data->recieved_at === null){
                    return 'For Job';
                }
                if($data->counter_id !== null && $data->recieved_at === null){
                    return 'For Counter';
                }
                if($data->recieved_at !== null){
	                if($data->job_id !== null || $data->counter_id !== null){
		                return 'Issued';
	                }
                    return '<input type="text" value="'.$data->location_id.'" />';
                } else {
                    if($data->location_id !== 0){
                        $location = Location::where('id', '=', $data->location_id)->first();

                        if($location){
	                        if($location->store_id == $data->store_id){

		                        $name = '';
		                        if(isset($location->store)){
			                        $name .= $location->store->code.' : ';
		                        }
		                        $name .= $location->room;
		                        $name .= $location->rack;
		                        $name .= $location->shelf;
		                        $name .= $location->bay;
		                        $name .= ' Qty: '.$location->qty;

		                        return '<select disabled line-id="'.$data->id.'" class="location-select">
	                            <option selected value="'.$location->id.'">'.$name.'</option>
	                        </select>';
	                        }
                        }
                    }
                    return '<select disabled line-id="'.$data->id.'" class="location-select">
                            <option selected value="0">Location TBD</option>
                    </select>';
                }
            })
	        ->filter(function ($query) use ($request) {
		        if(strlen($request->search['value']) > 0){
			        $query->leftjoin('items as item', 'item.id', '=', 'order_items.item_id');
			        $query->where('item.partnumber', 'like', '%'.$request->search['value'].'%');
			        $query->orWhere('item.barcode', 'like', '%'.$request->search['value'].'%');
			        $query->orWhere('item.description', 'like', '%'.$request->search['value'].'%');

			        $query->leftjoin('locations as location', 'order_items.location_id', '=', 'location.id');
			        $query->orWhere(DB::raw('CONCAT_WS("", location.room, location.rack, location.shelf, location.bay)'), 'like', '%'.$request->search['value'].'%');
		        }
	        })
            ->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
	    $items = OrderItems::selectRaw('DISTINCT order_items.id, 
        order_items.order_id,
        order_items.job_id,
        order_items.counter_id, 
        order_items.item_id,
        order_items.qty_in,
        order_items.location_id, 
        order_items.qty,
        order_items.line_unit_cost, 
        order_items.line_cost,
        order_items.new_supplier,
        order_items.pin_id,
        order_items.recieved_at,
        order_items.created_at,
        order_items.updated_at, 
        order_items.deleted_at');
	    $items->where('order_items.order_id', $this->order_id);
	    $items->with('new_supplier');
	    $items->with('size');
	    $items->with('size.unit');
	    $items->with('misc');
	    $items->with('job');
	    $items->with('counter');
	    $items->with('item', 'item.sizes', 'item.sexes', 'item.types', 'item.materials', 'item.grades');
	    $items->with('pin');
	    $items->with('location');
	    $items->orderByRaw('order_items.created_at DESC');
	    return $this->applyScopes($items);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns(array_merge(
                $this->getColumns()
            ))
            ->ajax('')
            ->parameters([
                'dom'     => 'Bfrtip',
                'scrollX' => true,
                'buttons' => [
                    'csv',
                    'excel',
                    'pdf',
                    'print',
                    'reset',
                    'reload',
                ],
                'drawCallback' => 'function() { 
                    doDraw();                    
                }',
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'Select' => ['name' => 'select', 'data' => 'select', 'orderable' => false],
            'Complete' => ['name' => 'complete', 'data' => 'complete', 'orderable' => false],
            'Part Receipt' => ['name' => 'qty-box', 'data' => 'qty-box', 'orderable' => false],
            'Qty Ordered' => ['name' => 'qty', 'data' => 'qty', 'orderable' => false],
            'Qty Outstanding' => ['name' => 'qty_outstanding', 'data' => 'qty_outstanding', 'orderable' => false],
            'Location' => ['name' => 'location-box', 'data' => 'location-box', 'orderable' => false],
            'Issued' => ['name' => 'issue_date', 'data' => 'issue_date', 'orderable' => false],
            'Name' => ['name' => 'the_name', 'data' => 'the_name', 'orderable' => false],
            'Barcode' => ['name' => 'the_barcode', 'data' => 'the_barcode', 'orderable' => false],
            'Description / Partnumber' => ['name' => 'the_desc', 'data' => 'the_desc', 'orderable' => false],
            'Ordered For' => ['name' => 'order_purpose', 'data' => 'order_purpose', 'orderable' => false],
            'Unit Price' => ['name' => 'unit_cost', 'data' => 'unit_cost', 'orderable' => false],
            'Total Price' => ['name' => 'total_cost', 'data' => 'total_cost', 'orderable' => false],
            'Existing Supplier' => ['name' => 'new_sup', 'data' => 'new_sup', 'orderable' => false],
            'Actions' => ['name' => 'actions', 'data' => 'actions', 'orderable' => false],
            'Loc' => ['name' => 'the_location', 'data' => 'the_location', 'visible' => false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'order_items' . time();
    }

}



