<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model {

	protected $table = 'suppliers';
	public $timestamps = true;

	public function items()
	{
		return $this->hasMany('Supplier_items', 'supplier_id');
	}

}