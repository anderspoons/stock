<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partnum extends Model {

	protected $table = 'partnums';
	public $timestamps = true;
}