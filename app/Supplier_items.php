<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier_items extends Model {

	protected $table = 'supplier_items';
	public $timestamps = true;

}