<?php
use App\Models\Pincodes;

if (!function_exists('CheckPin')) {

    /**
     * description
     *
     * @param $name = Id of the name the pin entered was down as.
     * @param $pin = code entered to check against name.
     *
     * @return boolean
     */
    function CheckPin($id, $pin)
    {
    	$lookup = Pincodes::where('id', '=', $id)->where('code', $pin)->where('active', '1')->get();
	    if ($lookup->count() == 0) {
		    return false;
	    } else {
		    return true;
	    }

    }
}
