<?php
use App\Models\Counter;
use App\Models\Job;
use App\Models\Order;
use Carbon\Carbon;

if ( ! function_exists('check_order')) {
	function check_order($id)
	{
		$accounts  = false;
		$stores  = false;
		$overdue  = false;
		$accounts_attached = false;
		$stores_attached = false;
		$order = Order::where('id', '=', $id)->with('statuses', 'items', 'carriage')->first();
		if ($order != null) {

			if($order->expected_at !== null && $order->closed_at === null){
				$date = Carbon::now();
				$expected = Carbon::parse($order->expected_at);
				if($date > $expected){
					$overdue = true;
				}
			}

			if (isset($order->statuses)) {
				foreach ($order->statuses as $status) {
					if ($status->id == 14) {
						$accounts_attached = true;
					}
					if ($status->id == 15) {
						$stores_attached = true;
					}
				}
			}
			if (isset($order->items)) {
				foreach ($order->items as $item) {
					if ( ! isset($item->line_cost) || $item->line_cost == '0') {
						$accounts = true;
					}
					if ($item->qty > $item->qty_in) {
						$stores = true;
					}
				}
			}
			if (isset($order->carriage)) {
				foreach ($order->carriage as $carriage) {
					if ( ! isset($carriage->line_cost) || $carriage->line_cost == '0') {
						$accounts = true;
					}
				}
			}
			if ($accounts && $accounts_attached == false) {
				$order->statuses()->attach('14');
			}
			if ($accounts == false && $accounts_attached == true) {
				$order->statuses()->detach('14');
			}
			if ($stores && $stores_attached == false) {
				$order->statuses()->attach('15');
			}
			if ($stores == false && $stores_attached == true) {
				$order->statuses()->detach('15');
			}
			if(($accounts || $stores) && $overdue){
				$order->statuses()->attach('17');
			}
			if(!$overdue){
				$order->statuses()->detach('17');
			}
			return true;
		}
		return false;
	}
}
