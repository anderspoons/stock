<?php
use App\Models\Counter;
use App\Models\Job;
use App\Models\Order;

if ( ! function_exists('check_prices')) {

    function check_prices($type, $id)
    {
        $pricing  = false;
        $attached = false;
        switch ($type) {
            case 'job':
                $job = Job::where('id', '=', $id)->with('statuses', 'job_items', 'carriage', 'labour', 'crane')->first();

                if ($job != null) {
                    if (isset($job->statuses)) {
                        foreach ($job->statuses as $status) {
                            if ($status->id == 3) {
                                $attached = true;
                            }
                        }
                    }

                    if (isset($job->job_items)) {
                        foreach ($job->job_items as $item) {
                            if ( ! isset($item->line_cost) || $item->line_cost == '0') {
                                $pricing = true;
                            }
                        }
                    }

                    if (isset($job->carriage)) {
                        foreach ($job->carriage as $carriage) {
                            if ( ! isset($carriage->cost) || $carriage->cost == '0') {
                                $pricing = true;
                            }
                        }
                    }

                    if (isset($job->labour)) {
                        foreach ($job->labour as $labour) {
                            if ( ! isset($labour->cost) || $labour->cost == '0') {
                                $pricing = true;
                            }
                        }
                    }

                    if (isset($job->crane)) {
                        foreach ($job->crane as $crane) {
                            if ( ! isset($crane->cost) || $crane->cost == '0') {
                                $pricing = true;
                            }
                        }
                    }


                    if ($pricing && $attached == false) {
                        $job->statuses()->attach('3');

                        return true;
                    }

                    if ($pricing == false && $attached == true) {
                        $job->statuses()->detach('3');

                        return true;
                    }
                }
                break;

            case 'counter':
                $counter = Counter::where('id', '=', $id)->with('statuses', 'items', 'carriage', 'labour', 'crane')->first();

                if ($counter != null) {

                    if (isset($counter->statuses)) {
                        foreach ($counter->statuses as $status) {
                            if ($status->id == 3) {
                                $attached = true;
                            }
                        }
                    }

                    if (isset($counter->items)) {
                        foreach ($counter->items as $item) {
                            if ( ! isset($item->line_cost) || $item->line_cost == '0') {
                                $pricing = true;
                            }
                        }
                    }
                    if (isset($counter->carriage)) {
                        foreach ($counter->carriage as $carriage) {
                            if ( ! isset($carriage->cost) || $carriage->cost == '0') {
                                $pricing = true;
                            }
                        }
                    }
                    if (isset($counter->labour)) {
                        foreach ($counter->labour as $labour) {
                            if ( ! isset($labour->cost) || $labour->cost == '0') {
                                $pricing = true;
                            }
                        }
                    }
                    if (isset($counter->crane)) {
                        foreach ($counter->crane as $crane) {
                            if ( ! isset($crane->cost) || $crane->cost == '0') {
                                $pricing = true;
                            }
                        }
                    }

                    if ($pricing && $attached == false) {
                        $counter->statuses()->attach('3');
                        return true;
                    }

                    if ($pricing == false && $attached == true) {
                        $counter->statuses()->detach('3');
                        return true;
                    }

                }
                break;


            case 'order':
                $order = Order::where('id', '=', $id)->with('statuses', 'items', 'carriage')->first();

                if ($order != null) {
                    if (isset($order->statuses)) {
                        foreach ($order->statuses as $status) {
                            if ($status->id == 3) {
                                $attached = true;
                            }
                        }
                    }
                    if (isset($order->items)) {
                        foreach ($order->items as $item) {
                            if ( ! isset($item->line_cost) || $item->line_cost == '0') {
                                $pricing = true;
                            }
                        }
                    }
                    if (isset($order->carriage)) {
                        foreach ($order->carriage as $carriage) {
                            if ( ! isset($carriage->line_cost) || $carriage->line_cost == '0') {
                                $pricing = true;
                            }
                        }
                    }
                    if ($pricing && $attached == false) {
                        $order->statuses()->attach('3');
                        return true;
                    }
                    if ($pricing == false && $attached == true) {
                        $order->statuses()->detach('3');
                        return true;
                    }
                }
                break;
        }
        return false;
    }
}
