<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model {

	protected $table = 'items';
	public $timestamps = true;
	public function sizes()
	{
		return $this->hasMany('App\Size', 'item_id');
	}

	public function sexes()
	{
		return $this->belongsToMany('App\Sex', 'sex_items', 'item_id', 'sex_id');
	}

	public function types()
	{
		return $this->belongsToMany('App\Type', 'type_items', 'item_id', 'type_id');
	}

	public function prices()
	{
		return $this->hasMany('App\Price', 'item_id');
	}

	public function partnums()
	{
		return $this->hasMany('App\Partnum', 'item_id');
	}

	public function locations()
	{
		return $this->hasMany('App\Location', 'item_id');
	}

	public function suppliers()
	{
		return $this->hasMany('App\Supplier_items', 'item_id');
	}

	public function material()
	{
		return $this->belongsToMany('App\Material', 'material_items', 'item_id', 'mat_id')->withPivot('name');
	}

	public function grade()
	{
		return $this->hasMany('App\Grade', 'item_id')->withPivot('name');
	}

}