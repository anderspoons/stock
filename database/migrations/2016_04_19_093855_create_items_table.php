<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItemsTable extends Migration {

	public function up()
	{
		Schema::create('items', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('name', 30);
			$table->string('description', 500);
			$table->integer('markup_percent');
			$table->integer('markup_fixed');
			$table->string('barcode', 30);
		});
	}

	public function down()
	{
		Schema::drop('items');
	}
}