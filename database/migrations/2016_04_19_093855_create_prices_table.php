<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePricesTable extends Migration {

	public function up()
	{
		Schema::create('prices', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->float('cost');
			$table->integer('item_id');
		});
	}

	public function down()
	{
		Schema::drop('prices');
	}
}