<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	public function up()
	{
		Schema::create('users', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('title', 10);
			$table->string('fname', 30);
			$table->string('sname', 30);
			$table->string('password', 500);
			$table->string('email', 50);
			$table->string('active');
		});
	}

	public function down()
	{
		Schema::drop('users');
	}
}