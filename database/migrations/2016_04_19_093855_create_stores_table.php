<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStoresTable extends Migration {

	public function up()
	{
		Schema::create('stores', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('name', 30);
			$table->string('code', 5);
			$table->string('address_1', 50);
			$table->string('address_2', 50);
			$table->string('town', 50);
			$table->string('county', 50);
			$table->string('postcode', 10);
			$table->string('phone', 15);
			$table->string('fax', 15);
			$table->string('email', 30);
		});
	}

	public function down()
	{
		Schema::drop('stores');
	}
}