<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLocationsTable extends Migration {

	public function up()
	{
		Schema::create('locations', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('store_id');
			$table->integer('item_id');
			$table->string('room', 4);
			$table->string('rack', 4);
			$table->string('shelf', 2);
			$table->string('bay', 4);
		});
	}

	public function down()
	{
		Schema::drop('locations');
	}
}