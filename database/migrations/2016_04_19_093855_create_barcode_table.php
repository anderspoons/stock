<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBarcodeTable extends Migration {

	public function up()
	{
		Schema::create('barcode', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('barcode', 15);
			$table->integer('item_id');
		});
	}

	public function down()
	{
		Schema::drop('barcode');
	}
}