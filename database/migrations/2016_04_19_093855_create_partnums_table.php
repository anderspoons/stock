<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePartnumsTable extends Migration {

	public function up()
	{
		Schema::create('partnums', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('item_id');
			$table->integer('supplier_id');
			$table->string('number', 50);
		});
	}

	public function down()
	{
		Schema::drop('partnums');
	}
}