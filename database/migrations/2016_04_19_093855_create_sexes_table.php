<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSexesTable extends Migration {

	public function up()
	{
		Schema::create('sexes', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('item_id');
			$table->string('sex', 8);
		});
	}

	public function down()
	{
		Schema::drop('sexes');
	}
}