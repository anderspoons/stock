<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSupplierItemsTable extends Migration {

	public function up()
	{
		Schema::create('supplier_items', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('supplier_id');
			$table->integer('item_id');
		});
	}

	public function down()
	{
		Schema::drop('supplier_items');
	}
}